/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.voms.xmlobject;

import org.opensaml.core.xml.XMLObjectProviderBaseTestCase;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Unit tests for {@link GridProxyRequest}.
 */
public class GridProxyRequestTest extends XMLObjectProviderBaseTestCase {
    
    private int expectedLifetime;
    private int expectedProxyType;
    private int expectedDelegationType;
    private int expectedPolicyType;
    private String expectedOrdering;
    private String expectedTargets;
    private String expectedVerificationType;
    private String expectedFQAN;


    /** Constructor. */
    public GridProxyRequestTest() {
        singleElementFile = "/org/glite/sts/voms/xmlobject/GliteSTSProxyGridProxyRequest.xml";
        //singleElementOptionalAttributesFile = "/org/glite/sts/voms/xmlobject/GliteSTSProxyGridProxyRequestFull.xml";
        childElementsFile = "/org/glite/sts/voms/xmlobject/GliteSTSProxyGridProxyRequestFull.xml";
    }

    /** {@inheritDoc} */
    @BeforeMethod
    protected void setUp() throws Exception {
        expectedLifetime = 86400;
        expectedProxyType = 1;
        expectedDelegationType = 1;
        expectedPolicyType = 1;
    	expectedOrdering = "testOrdering";
    	expectedTargets = "testTargets";
    	expectedVerificationType = "testVerificationType";
    	expectedFQAN = "testVO:/testRole/";

    }

    /** {@inheritDoc} */
    @Test
    public void testSingleElementMarshall() {
    	GridProxyRequest proxyRequest = (GridProxyRequest) buildXMLObject(GridProxyRequest.DEFAULT_ELEMENT_NAME);
    	proxyRequest.setLifetime(expectedLifetime);
    	proxyRequest.setProxyType(expectedProxyType);
    	proxyRequest.setDelegationType(expectedDelegationType);
    	proxyRequest.setPolicyType(expectedPolicyType);
        assertXMLEquals(expectedDOM, proxyRequest);
    }
    
    /** {@inheritDoc} */
    @Test
    public void testChildElementsMarshall() {
    	FQAN fqan = (FQAN) buildXMLObject(FQAN.DEFAULT_ELEMENT_NAME);
    	fqan.setValue(expectedFQAN);
    	VomsAttributeCertificates vomsACs = (VomsAttributeCertificates) buildXMLObject(VomsAttributeCertificates.DEFAULT_ELEMENT_NAME);
    	vomsACs.setOrdering(expectedOrdering);
    	vomsACs.setTargets(expectedTargets);
    	vomsACs.setVerificationType(expectedVerificationType);
    	vomsACs.getFQANs().add(fqan);
    	GridProxyRequest proxyRequest = (GridProxyRequest) buildXMLObject(GridProxyRequest.DEFAULT_ELEMENT_NAME);
    	proxyRequest.setLifetime(expectedLifetime);
    	proxyRequest.setProxyType(expectedProxyType);
    	proxyRequest.setDelegationType(expectedDelegationType);
    	proxyRequest.setPolicyType(expectedPolicyType);
    	proxyRequest.setVomsAttributeCertificates(vomsACs);
        assertXMLEquals(expectedChildElementsDOM, proxyRequest);    	
    }

    /** {@inheritDoc} */
    @Test
    public void testSingleElementUnmarshall() {
    	GridProxyRequest proxyRequest = (GridProxyRequest) unmarshallElement(singleElementFile);
    	Assert.assertEquals(proxyRequest.getLifetime(), expectedLifetime);
    	Assert.assertEquals(proxyRequest.getProxyType(), expectedProxyType);
    	Assert.assertEquals(proxyRequest.getDelegationType(), expectedDelegationType);
    	Assert.assertEquals(proxyRequest.getPolicyType(), expectedPolicyType);
    }
    
    /** {@inheritDoc} */
    @Test
    public void testChildElementsUnmarshall() {
    	GridProxyRequest proxyRequest = (GridProxyRequest) unmarshallElement(childElementsFile);
    	VomsAttributeCertificates vomsACs = proxyRequest.getVomsAttributeCertificates();
    	Assert.assertEquals(proxyRequest.getLifetime(), expectedLifetime);
    	Assert.assertEquals(proxyRequest.getProxyType(), expectedProxyType);
    	Assert.assertEquals(proxyRequest.getDelegationType(), expectedDelegationType);
    	Assert.assertEquals(proxyRequest.getPolicyType(), expectedPolicyType);
    	Assert.assertEquals(vomsACs.getOrdering(), expectedOrdering);
    	Assert.assertEquals(vomsACs.getTargets(), expectedTargets);
    	Assert.assertEquals(vomsACs.getVerificationType(), expectedVerificationType);
    	Assert.assertEquals(vomsACs.getFQANs().get(0).getValue(), expectedFQAN);
    }
}
