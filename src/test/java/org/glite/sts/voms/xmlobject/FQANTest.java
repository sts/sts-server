/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.voms.xmlobject;

import org.opensaml.core.xml.XMLObjectProviderBaseTestCase;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Unit tests for {@link FQAN}.
 */
public class FQANTest extends XMLObjectProviderBaseTestCase {
    
    private String expectedContent;

    /** Constructor. */
    public FQANTest() {
        singleElementFile = "/org/glite/sts/voms/xmlobject/GliteSTSProxyFQAN.xml";
    }

    /** {@inheritDoc} */
    @BeforeMethod
    protected void setUp() throws Exception {
        expectedContent = "testValue";
    }

    /** {@inheritDoc} */
    @Test
    public void testSingleElementMarshall() {
    	FQAN fqan = (FQAN) buildXMLObject(FQAN.DEFAULT_ELEMENT_NAME);
    	fqan.setValue(expectedContent);
        assertXMLEquals(expectedDOM, fqan);
    }

    /** {@inheritDoc} */
    @Test
    public void testSingleElementUnmarshall() {
    	FQAN fqan = (FQAN) unmarshallElement(singleElementFile);
    	Assert.assertEquals(fqan.getValue(), expectedContent);
    }
}
