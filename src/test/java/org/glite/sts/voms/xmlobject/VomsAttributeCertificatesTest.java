/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.voms.xmlobject;

import org.opensaml.core.xml.XMLObjectProviderBaseTestCase;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Unit tests for {@link VomsAttributeCertificates}.
 */
public class VomsAttributeCertificatesTest extends XMLObjectProviderBaseTestCase {
    
    private String expectedOrdering;
    private String expectedTargets;
    private String expectedVerificationType;
    private String expectedFQAN;

    /** Constructor. */
    public VomsAttributeCertificatesTest() {
        singleElementFile = "/org/glite/sts/voms/xmlobject/GliteSTSProxyVomsAttributeCertificates.xml";
    }

    /** {@inheritDoc} */
    @BeforeMethod
    protected void setUp() throws Exception {
    	expectedOrdering = "testOrdering";
    	expectedTargets = "testTargets";
    	expectedVerificationType = "testVerificationType";
    	expectedFQAN = "testVO:/testRole/";
    }

    /** {@inheritDoc} */
    @Test
    public void testSingleElementMarshall() {
    	VomsAttributeCertificates vomsACs = (VomsAttributeCertificates) buildXMLObject(VomsAttributeCertificates.DEFAULT_ELEMENT_NAME);
    	FQAN fqan = (FQAN) buildXMLObject(FQAN.DEFAULT_ELEMENT_NAME);
    	vomsACs.setOrdering(expectedOrdering);
    	vomsACs.setTargets(expectedTargets);
    	vomsACs.setVerificationType(expectedVerificationType);
    	fqan.setValue(expectedFQAN);
    	vomsACs.getFQANs().add(fqan);
        assertXMLEquals(expectedDOM, vomsACs);
    }

    /** {@inheritDoc} */
    @Test
    public void testSingleElementUnmarshall() {
    	VomsAttributeCertificates vomsACs = (VomsAttributeCertificates) unmarshallElement(singleElementFile);
    	Assert.assertEquals(vomsACs.getOrdering(), expectedOrdering);
    	Assert.assertEquals(vomsACs.getTargets(), expectedTargets);
    	Assert.assertEquals(vomsACs.getVerificationType(), expectedVerificationType);
    	Assert.assertEquals(vomsACs.getFQANs().get(0).getValue(), expectedFQAN);
    }
}
