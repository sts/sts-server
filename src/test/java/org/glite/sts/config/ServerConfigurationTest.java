/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.config;

import org.glite.sts.STSException;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

/**
 * Unit tests for {@link ServerConfiguration}.
 */
public class ServerConfigurationTest {

	ServerConfiguration serverConfiguration;
	
	@BeforeTest
	public void initTests() throws STSException {
		serverConfiguration = new ServerConfiguration("src/test/resources/org/glite/sts/config/sts-server.ini");
	}
	
	@Test
	public void testStringValues() throws STSException {
		Assert.assertEquals(serverConfiguration.getSamlConfigurationValue("entityId"), "https://sts.example.org/saml");
		Assert.assertNull(serverConfiguration.getSamlConfigurationValue("keyPass"));
		Assert.assertEquals(serverConfiguration.getMessagesConfigurationValue("schemaDirectory"), "/etc/sts/schema");
	}
	
	@Test
	public void testBoolValues() throws STSException {
		Assert.assertFalse(serverConfiguration.getBoolConfigurationValue("LDAP", "useStartTls"));
		Assert.assertFalse(serverConfiguration.getBoolConfigurationValue("LDAP", "not_exists"));
	}
	
	@Test
	public void testIntValues() throws STSException {
		Assert.assertEquals(serverConfiguration.getIntConfigurationValue("StandaloneServer", "port"), 8080);
		Assert.assertEquals(serverConfiguration.getIntConfigurationValue("StandaloneServer", "not_exists"), -1);
	}
	
	@Test
	public void testLongValues() throws STSException {
		Assert.assertEquals(serverConfiguration.getLongConfigurationValue("StandaloneServer", "crlUpdateInterval"), 600000);
		Assert.assertEquals(serverConfiguration.getLongConfigurationValue("StandaloneServer", "not_exists"), -1);
	}
	
}
