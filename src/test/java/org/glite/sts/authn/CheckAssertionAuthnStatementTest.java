/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.authn;

import org.glite.sts.authn.CheckAssertionAuthnStatement.FutureAuthnInstantInAssertionException;
import org.glite.sts.authn.CheckAssertionAuthnStatement.InvalidRemoteAddressInAssertionException;
import org.glite.sts.authn.CheckAssertionAuthnStatement.PastAuthnInstantInAssertionException;
import org.glite.sts.profile.ActionTestingSupport;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.webflow.execution.Event;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

/**
 * Unit tests for {@link CheckAssertionAuthnStatement}.
 */
public class CheckAssertionAuthnStatementTest extends AbstractAssertionTest {
	
	private Logger log = LoggerFactory.getLogger(CheckAssertionAuthnStatementTest.class);
	
	CheckAssertionAuthnStatement check;
	long authnLifetime;
	
	@BeforeTest
	public void init() throws Exception {
		loadAssertion("/org/glite/sts/authn/samlAssertion.xml");
		super.init();
		authnLifetime = 300000;
		log.debug("Initialization was successful.");
	}
	
	@Test
	public void testValid() throws Exception {
		check = new CheckAssertionAuthnStatement(authnLifetime, true, false, null);
		assertion.getAuthnStatements().get(0).setAuthnInstant(DateTime.now());
    	Event result = ActionTestingSupport.initializeAndExecuteAction(check, springRequestContext);
        ActionTestingSupport.assertProceedEvent(result);
	}

	@Test
	public void testPast() throws Exception {
		check = new CheckAssertionAuthnStatement(authnLifetime, true, false, null);
		assertion.getAuthnStatements().get(0).setAuthnInstant(DateTime.now().minus(clockSkew + authnLifetime + 1000));
		assertException(check, PastAuthnInstantInAssertionException.class);
	}

	@Test
	public void testFuture() throws Exception {
		check = new CheckAssertionAuthnStatement(authnLifetime, true, false, null);
		assertion.getAuthnStatements().get(0).setAuthnInstant(DateTime.now().plus(clockSkew + 1000));
		assertException(check, FutureAuthnInstantInAssertionException.class);
	}

	@Test
	public void testInvalidLocality() throws Exception {
		check = new CheckAssertionAuthnStatement(authnLifetime, true, true, null);
		assertion.getAuthnStatements().get(0).setAuthnInstant(DateTime.now());
		assertion.getAuthnStatements().get(0).getSubjectLocality().setAddress("128.128.128.128");
		assertException(check, InvalidRemoteAddressInAssertionException.class);
	}

	@Test
	public void testValidLocality() throws Exception {
		check = new CheckAssertionAuthnStatement(authnLifetime, true, false, null);
		assertion.getAuthnStatements().get(0).setAuthnInstant(DateTime.now());
		assertion.getAuthnStatements().get(0).getSubjectLocality().setAddress("127.0.0.1");
    	Event result = ActionTestingSupport.initializeAndExecuteAction(check, springRequestContext);
        ActionTestingSupport.assertProceedEvent(result);
	}

}
