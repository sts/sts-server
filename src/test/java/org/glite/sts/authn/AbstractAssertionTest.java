/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.authn;

import net.shibboleth.idp.authn.AuthenticationRequestContext;
import net.shibboleth.idp.profile.AbstractProfileAction;
import net.shibboleth.idp.profile.ProfileRequestContext;
import net.shibboleth.idp.profile.RequestContextBuilder;
import net.shibboleth.idp.relyingparty.RelyingPartyConfiguration;
import net.shibboleth.idp.relyingparty.RelyingPartyContext;
import net.shibboleth.utilities.java.support.xml.ParserPool;

import org.glite.sts.profile.ActionTestingSupport;
import org.glite.sts.profile.wstrust.WsTrustActionTestingSupport;
import org.opensaml.core.OpenSAMLInitBaseTestCase;
import org.opensaml.core.xml.XMLObjectProviderBaseTestCase;
import org.opensaml.core.xml.config.XMLObjectProviderRegistrySupport;
import org.opensaml.core.xml.io.Unmarshaller;
import org.opensaml.messaging.context.navigate.ChildContextLookup;
import org.opensaml.saml.saml2.core.Assertion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.webflow.execution.RequestContext;
import org.testng.Assert;
import org.w3c.dom.Document;

/**
 * Abstract class for the unit tests related to verifying {@link Assertion}s.
 */
public abstract class AbstractAssertionTest extends OpenSAMLInitBaseTestCase {
	
	private Logger log = LoggerFactory.getLogger(AbstractAssertionTest.class);

	protected RequestContext springRequestContext;
	@SuppressWarnings("rawtypes")
	protected ProfileRequestContext profileRequestContext;
	protected AuthenticationRequestContext authnRequestContext;
	protected final AssertionContext assertionContext = new AssertionContext();
	
	protected Assertion assertion;
	protected long clockSkew;

	protected void init() throws Exception {
		RequestContextBuilder builder = new RequestContextBuilder();
		builder.setRelyingPartyProfileConfigurations(WsTrustActionTestingSupport.buildProfileConfigurations());
    	springRequestContext = builder.buildRequestContext();
		profileRequestContext = ActionTestingSupport.getProfileRequestContext(springRequestContext);
    	authnRequestContext = new AuthenticationRequestContext(null, null);	
    	assertionContext.setAssertion(assertion);
    	authnRequestContext.addSubcontext(assertionContext);
    	profileRequestContext.addSubcontext(authnRequestContext);
    	RelyingPartyContext relyingPartyCtx = new ChildContextLookup<ProfileRequestContext<?, ?>, RelyingPartyContext>(RelyingPartyContext.class, false).apply(profileRequestContext);
		clockSkew = relyingPartyCtx.getProfileConfig().getSecurityConfiguration().getClockSkew();

	}
	
	protected void loadAssertion(String assertionFile) throws Exception {
		ParserPool parserPool = XMLObjectProviderRegistrySupport.getParserPool();
		log.debug("Loading an assertion from {}", assertionFile);
		Document document = parserPool.parse(XMLObjectProviderBaseTestCase.class.getResourceAsStream(assertionFile));
		Unmarshaller unmarshaller = XMLObjectProviderRegistrySupport.getUnmarshallerFactory().getUnmarshaller(Assertion.DEFAULT_ELEMENT_NAME);
		assertion = (Assertion) unmarshaller.unmarshall(document.getDocumentElement());
		log.debug("Assertion loaded successfully.");
	}
	
	protected void setEntityId(String entityId) {
    	final RelyingPartyContext relyingPartyCtx = new ChildContextLookup<ProfileRequestContext<?, ?>, RelyingPartyContext>(RelyingPartyContext.class, false).apply(profileRequestContext);
		relyingPartyCtx.setRelyingPartyConfiguration(new RelyingPartyConfiguration("mock", entityId, WsTrustActionTestingSupport.buildProfileConfigurations()));
	}
	
	protected void assertException(AbstractProfileAction<?, ?> action, Class<? extends Exception> expected) throws Exception {
    	try {
    		ActionTestingSupport.initializeAndExecuteAction(action, springRequestContext);
    	} catch (Exception e) {
    		if (expected.isInstance(e)) {
    			return;
    		}
    		Assert.fail("Exception was " + e.getClass().toString() + ", expected " + expected.toString());
    	}
    	Assert.fail("No exception was found, expected " + expected.toString());
	}
}
