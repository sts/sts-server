/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.authn;

import org.glite.sts.authn.CheckAssertionConditions.ExpiredAssertionException;
import org.glite.sts.authn.CheckAssertionConditions.NotInAssertionAudienceException;
import org.glite.sts.authn.CheckAssertionConditions.NotYetValidAssertionException;
import org.glite.sts.profile.ActionTestingSupport;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.webflow.execution.Event;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

/**
 * Unit tests for {@link CheckAssertionConditions}.
 */
public class CheckAssertionConditionsTest extends AbstractAssertionTest {
	
	private Logger log = LoggerFactory.getLogger(CheckAssertionConditions.class);
	
	CheckAssertionConditions check;
	String entityId;
	
	@BeforeTest
	public void init() throws Exception {
		loadAssertion("/org/glite/sts/authn/samlAssertion.xml");
		super.init();
		entityId = "https://sts.example.org/saml";
		check = new CheckAssertionConditions(true, true);
		log.debug("Initialization was successful.");
	}
	
	@Test
	public void testValid() throws Exception {
		setEntityId(entityId);
    	assertion.getConditions().setNotBefore(DateTime.now());
    	assertion.getConditions().setNotOnOrAfter(DateTime.now().plusMinutes(5));    	
    	Event result = ActionTestingSupport.initializeAndExecuteAction(check, springRequestContext);
        ActionTestingSupport.assertProceedEvent(result);
	}

	@Test
	public void testInvalidEntity() throws Exception {
		setEntityId("InvalidEntityId");
    	assertion.getConditions().setNotBefore(DateTime.now());
    	assertion.getConditions().setNotOnOrAfter(DateTime.now().plusMinutes(5));    	
    	assertException(check, NotInAssertionAudienceException.class);
	}
	
	@Test
	public void testExpiredAssertion() throws Exception {
		setEntityId(entityId);
    	assertion.getConditions().setNotBefore(DateTime.now());
    	assertion.getConditions().setNotOnOrAfter(DateTime.now().minus(clockSkew + 10));    	
    	assertException(check, ExpiredAssertionException.class);
	}
	
	@Test
	public void testNotYetValidAssertion() throws Exception {
		setEntityId(entityId);
    	assertion.getConditions().setNotBefore(DateTime.now().plus(clockSkew + 10));
    	assertion.getConditions().setNotOnOrAfter(DateTime.now().plusMinutes(5));    	
    	assertException(check, NotYetValidAssertionException.class);
	}
}
