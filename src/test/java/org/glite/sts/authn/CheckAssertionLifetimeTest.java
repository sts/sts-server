/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.authn;

import java.util.Timer;

import net.shibboleth.utilities.java.support.xml.ParserPool;

import org.glite.sts.authn.CheckAssertionLifetime.FutureAssertionException;
import org.glite.sts.authn.CheckAssertionLifetime.PastAssertionException;
import org.glite.sts.profile.ActionTestingSupport;
import org.glite.sts.saml.TrustedIdpMetadataService;
import org.joda.time.DateTime;
import org.opensaml.core.xml.config.XMLObjectProviderRegistrySupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.webflow.execution.Event;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

/**
 * Unit tests for {@link CheckAssertionLifetime}.
 */
public class CheckAssertionLifetimeTest extends AbstractAssertionTest {

	private Logger log = LoggerFactory.getLogger(CheckAssertionLifetimeTest.class);
	
	TrustedIdpMetadataService idpMetadata;
	CheckAssertionLifetime check;
	int lifetime;
	
	@BeforeTest
	public void init() throws Exception {
		lifetime = 5;
		loadAssertion("/org/glite/sts/authn/samlAssertion.xml");
		super.init();
		ParserPool parserPool = XMLObjectProviderRegistrySupport.getParserPool();
		idpMetadata = new TrustedIdpMetadataService("src/test/resources/idpmetadata", parserPool, new Timer(), 3000);
		check = new CheckAssertionLifetime(lifetime);
		log.debug("Initialization was successful.");
	}
	
	@Test
	public void testValid() throws Exception {
		assertion.setIssueInstant(DateTime.now());
    	Event result = ActionTestingSupport.initializeAndExecuteAction(check, springRequestContext);
        ActionTestingSupport.assertProceedEvent(result);
	}

	@Test
	public void testNotYet() throws Exception {
		assertion.setIssueInstant(DateTime.now().plus(clockSkew + 10 + lifetime * 60000));
		assertException(check, FutureAssertionException.class);
	}

	@Test
	public void testExpired() throws Exception {
		assertion.setIssueInstant(DateTime.now().minus(clockSkew + 10));
		assertException(check, PastAssertionException.class);
	}

}
