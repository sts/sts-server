/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.authn;

import java.util.Timer;

import org.glite.sts.authn.CheckAssertionSubjectConfirmation.InvalidSubjectConfirmationException;
import org.glite.sts.profile.ActionTestingSupport;
import org.glite.sts.saml.idstore.AuthnRequestIdManager;
import org.glite.sts.saml.idstore.StorageService;
import org.glite.sts.saml.idstore.impl.InMemoryStorageService;
import org.joda.time.DateTime;
import org.opensaml.saml.saml2.core.SubjectConfirmation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.webflow.execution.Event;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

/**
 * Unit tests for {@link CheckAssertionSubjectConfirmation}.
 */
public class CheckAssertionSubjectConfirmationTest extends AbstractAssertionTest {
	
	private Logger log = LoggerFactory.getLogger(CheckAssertionSubjectConfirmationTest.class);
	
	String consumerUrl;
	String authnRequestId;
	CheckAssertionSubjectConfirmation check;
	AuthnRequestIdManager idManager;
	
	@BeforeTest
	public void init() throws Exception {
		loadAssertion("/org/glite/sts/authn/samlAssertion.xml");
		super.init();
		consumerUrl = "/soap";
		authnRequestId = "_5400983a9e3b121a90aa0bfebccfcf3d";
		StorageService storage = new InMemoryStorageService(new Timer(), 300000);
		idManager = new AuthnRequestIdManager(storage, 300000);
		check = new CheckAssertionSubjectConfirmation("/soap", true, idManager);
		log.debug("Initialization was successful.");
	}
	
	@Test
	public void testValid() throws Exception {
    	assertion.getSubject().getSubjectConfirmations().get(0).setMethod(SubjectConfirmation.METHOD_BEARER);
		assertion.getSubject().getSubjectConfirmations().get(0).getSubjectConfirmationData().setNotOnOrAfter(DateTime.now().plusMinutes(5));
    	idManager.storeAuthnRequestId(authnRequestId);
    	Event result = ActionTestingSupport.initializeAndExecuteAction(check, springRequestContext);
        ActionTestingSupport.assertProceedEvent(result);
        Assert.assertFalse(idManager.isAuthnRequestIdValid(authnRequestId));
	}

	@Test
	public void testExpired() throws Exception {
		assertion.getSubject().getSubjectConfirmations().get(0).getSubjectConfirmationData().setNotOnOrAfter(DateTime.now().minus(clockSkew + 1));
    	idManager.storeAuthnRequestId(authnRequestId);
    	assertException(check, InvalidSubjectConfirmationException.class);
    	Assert.assertTrue(idManager.isAuthnRequestIdValid(authnRequestId));
	}

	@Test
	public void testInvalidMethod() throws Exception {
		assertion.getSubject().getSubjectConfirmations().get(0).setMethod("invalidMethod");
    	idManager.storeAuthnRequestId(authnRequestId);
    	assertException(check, InvalidSubjectConfirmationException.class);
    	Assert.assertTrue(idManager.isAuthnRequestIdValid(authnRequestId));
	}
	
	@Test
	public void testNoRequestIdFound() throws Exception {
    	assertion.getSubject().getSubjectConfirmations().get(0).setMethod(SubjectConfirmation.METHOD_BEARER);
		assertion.getSubject().getSubjectConfirmations().get(0).getSubjectConfirmationData().setNotOnOrAfter(DateTime.now().plusMinutes(5));
    	assertException(check, InvalidSubjectConfirmationException.class);
    	Assert.assertFalse(idManager.isAuthnRequestIdValid(authnRequestId));
	}

}
