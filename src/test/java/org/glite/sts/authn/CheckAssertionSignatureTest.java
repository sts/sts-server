/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.authn;

import java.util.Timer;

import net.shibboleth.utilities.java.support.xml.ParserPool;

import org.glite.sts.profile.ActionTestingSupport;
import org.glite.sts.saml.TrustedIdpMetadataService;
import org.opensaml.core.xml.config.XMLObjectProviderRegistrySupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.webflow.execution.Event;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

/**
 * Unit tests for {@link CheckAssertionSignature}.
 */
public class CheckAssertionSignatureTest extends AbstractAssertionTest {
	
	private Logger log = LoggerFactory.getLogger(CheckAssertionSignatureTest.class);
	
	TrustedIdpMetadataService idpMetadata;
	CheckAssertionSignature checkSignature;
	
	@BeforeTest
	public void init() throws Exception {
		loadAssertion("/org/glite/sts/authn/samlAssertion.xml");
		super.init();
		ParserPool parserPool = XMLObjectProviderRegistrySupport.getParserPool();
		idpMetadata = new TrustedIdpMetadataService("src/test/resources/idpmetadata", parserPool, new Timer(), 3000);
		checkSignature = new CheckAssertionSignature(idpMetadata);
		log.debug("Initialization was successful.");
	}
	
	@Test
	public void testValid() throws Exception {
    	Event result = ActionTestingSupport.initializeAndExecuteAction(checkSignature, springRequestContext);
        ActionTestingSupport.assertProceedEvent(result);
	}
}