/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.saml.idstore;

import java.util.Timer;

import org.glite.sts.saml.idstore.impl.InMemoryStorageService;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

/**
 * Unit tests for {AuthnRequestIdManager}.
 */
public class AuthnRequestIdManagerTest {
	
	AuthnRequestIdManager idManager;
	StorageService storageService;
	String testId;
	
	@BeforeTest
	public void init() {
		storageService = new InMemoryStorageService(new Timer(), 200);
		idManager = new AuthnRequestIdManager(storageService, 100);
		testId = "_testAuthnRequestIdentifier";
	}
	
	@Test
	public void testStoreAndValidSearch() {
		idManager.storeAuthnRequestId(testId);
		Assert.assertTrue(idManager.isAuthnRequestIdValid(testId));
	}
	
	@Test
	public void testInvalidSearch() {
		Assert.assertFalse(idManager.isAuthnRequestIdValid("invalid"));
	}

	@Test
	public void testStoreAndInvalidSearch() {
		idManager.storeAuthnRequestId(testId);
		Assert.assertFalse(idManager.isAuthnRequestIdValid("shouldnotbefound"));
		Assert.assertTrue(idManager.isAuthnRequestIdValid(testId));
		Assert.assertFalse(idManager.isAuthnRequestIdValid(testId));
	}
	
	@Test
	public void testSweeper() {
		idManager.storeAuthnRequestId(testId);
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		Assert.assertFalse(idManager.isAuthnRequestIdValid(testId));		
	}
}
