package org.glite.sts.profile;

import net.shibboleth.idp.profile.ActionTestingSupport;
import net.shibboleth.idp.profile.ProfileRequestContext;
import net.shibboleth.idp.profile.RequestContextBuilder;

import org.glite.sts.profile.wstrust.WsTrustActionTestingSupport;
import org.opensaml.core.OpenSAMLInitBaseTestCase;
import org.opensaml.messaging.context.MessageContext;
import org.opensaml.soap.soap11.Body;
import org.opensaml.soap.soap11.Envelope;
import org.opensaml.soap.soap11.Header;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Unit testing for {@link AddEnvelopeShell} action.
 */
public class AddEnvelopeShellTest extends OpenSAMLInitBaseTestCase {

    @Test
    public void testAddEnvelope() throws Exception {
    	RequestContext springRequestContext =
                new RequestContextBuilder().setRelyingPartyProfileConfigurations(
                        WsTrustActionTestingSupport.buildProfileConfigurations()).buildRequestContext();
        AddEnvelopeShell action = new AddEnvelopeShell();
        action.setId("test");
        action.initialize();
        Event result = action.execute(springRequestContext);
        ActionTestingSupport.assertProceedEvent(result);
        ProfileRequestContext<Object, Envelope> profileRequestContext =
            (ProfileRequestContext<Object, Envelope>) springRequestContext.getConversationScope().get(
                    ProfileRequestContext.BINDING_KEY);
        MessageContext<Envelope> outMsgCtx = profileRequestContext.getOutboundMessageContext();
        Envelope envelope = outMsgCtx.getMessage();
        Assert.assertNotNull(envelope);
        Header header = envelope.getHeader();
        Assert.assertNotNull(header);
        Body body = envelope.getBody();
        Assert.assertNotNull(body);
    }
}
