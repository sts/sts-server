package org.glite.sts.profile;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import net.shibboleth.idp.profile.AbstractProfileAction;
import net.shibboleth.idp.profile.ProfileException;
import net.shibboleth.idp.profile.ProfileRequestContext;
import net.shibboleth.idp.profile.navigate.WebflowRequestContextProfileRequestContextLookup;
import net.shibboleth.utilities.java.support.component.ComponentInitializationException;

import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;
import org.testng.Assert;

public class ActionTestingSupport extends net.shibboleth.idp.profile.ActionTestingSupport {

    /**
     * Checks that the given event has a given identifier. That is, that the event is not null, that its source is not null.
     * 
     * @param event the event to check
     * @param expectedEventID the expected event id
     */
    public static void assertExpectedEvent(Event event, String expectedEventId) {
        Assert.assertNotNull(event);
        Assert.assertNotNull(event.getSource());
        Assert.assertEquals(event.getId(), expectedEventId);
    }
    
    /**
     * Sets a test id, initializes and executes the action.
     * @param action the action to be executed
     * @param springRequestContext spring request context
     * @return the result event
     * @throws ComponentInitializationException
     * @throws ProfileException
     */
    public static Event initializeAndExecuteAction(AbstractProfileAction<?, ?> action, final RequestContext springRequestContext) throws ComponentInitializationException, ProfileException {
        action.setId("test");
        action.initialize();
        return action.execute(springRequestContext);
    }
    
	public static byte[] getByteArrayFromStream(InputStream in) throws IOException {
		ByteArrayOutputStream buffer = new ByteArrayOutputStream();
		int read;
		byte[] data = new byte[8192];
		while ((read = in.read(data, 0, data.length)) != -1) {
			buffer.write(data, 0, read);
		}
		buffer.flush();
		return buffer.toByteArray();
	}
	
    /**
     * Returns the profile request context from the given Spring request context.
     * @param springRequestContext
     * @return
     */
    public static final ProfileRequestContext<?, ?> getProfileRequestContext(RequestContext springRequestContext) {
    	return new WebflowRequestContextProfileRequestContextLookup().apply(springRequestContext);
    }

}
