/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.profile.soap.xmlobject;

import org.opensaml.core.xml.XMLObjectProviderBaseTestCase;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Unit tests for {@link Status}.
 */
public class StatusTest extends XMLObjectProviderBaseTestCase {

    private String expectedCodeContent;
    private String expectedCommentContent;

    /** Constructor. */
    public StatusTest() {
        singleElementFile = "/org/glite/sts/profile/soap/xmlobject/LibertyIDWSFStatus.xml";
    }

    /** {@inheritDoc} */
    @BeforeMethod
    protected void setUp() throws Exception {
        expectedCodeContent = "Testing code";
        expectedCommentContent = "Testing";
    }

    /** {@inheritDoc} */
    @Test
    public void testSingleElementMarshall() {
        Status status = (Status) buildXMLObject(Status.DEFAULT_ELEMENT_NAME);
        status.setCode(expectedCodeContent);
        status.setComment(expectedCommentContent);
        assertXMLEquals(expectedDOM, status);
    }

    @Test
    public void testSingleElementUnmarshall() {
        Status status = (Status) unmarshallElement(singleElementFile);
        Assert.assertNotNull(status, "Unmarshalled object was null");
        Assert.assertEquals(status.getCode(), expectedCodeContent, "Code value");
        Assert.assertEquals(status.getComment(), expectedCommentContent, "Comment value");
    }
}
