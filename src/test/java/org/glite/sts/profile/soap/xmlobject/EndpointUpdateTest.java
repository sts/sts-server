/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.profile.soap.xmlobject;

import org.opensaml.core.xml.XMLObjectProviderBaseTestCase;
import org.opensaml.soap.wsaddressing.Address;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Unit tests for {@link EndpointUpdate}.
 */
public class EndpointUpdateTest extends XMLObjectProviderBaseTestCase {
    
    private String expectedContent;
    private String expectedAddress;

    /** Constructor. */
    public EndpointUpdateTest() {
        singleElementFile = "/org/glite/sts/profile/soap/xmlobject/LibertyIDWSFEndpointUpdate.xml";
    }

    /** {@inheritDoc} */
    @BeforeMethod
    protected void setUp() throws Exception {
        expectedContent = "urn:liberty:sb:2006-08:Partial";
        expectedAddress = "urn:liberty:sb:2006-08:EndpointUpdate:NoChange";
    }

    /** {@inheritDoc} */
    @Test
    public void testSingleElementMarshall() {
        EndpointUpdate endpointUpdate = (EndpointUpdate) buildXMLObject(EndpointUpdate.DEFAULT_ELEMENT_NAME);
        Address address = (Address) buildXMLObject(Address.ELEMENT_NAME);
        address.setValue(expectedAddress);
        endpointUpdate.setUpdateType(expectedContent);
        endpointUpdate.setAddress(address);
        assertXMLEquals(expectedDOM, endpointUpdate);
    }

    /** {@inheritDoc} */
    public void testSingleElementUnmarshall() {
        EndpointUpdate endpointUpdate = (EndpointUpdate) unmarshallElement(singleElementFile);
        Assert.assertNotNull(endpointUpdate, "Unmarshalled object was null");
        Assert.assertEquals(endpointUpdate.getUpdateType(), expectedContent, "updateType value");
        Assert.assertNotNull(endpointUpdate.getAddress(), "wsa:Address element");
        Assert.assertEquals(endpointUpdate.getAddress().getValue(), expectedAddress, "wsa:Address value");
    }
}
