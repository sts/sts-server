/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.profile.soap.xmlobject;

import org.opensaml.core.xml.XMLObjectProviderBaseTestCase;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Unit tests for {@link Request}.
 */
public class RequestTest extends XMLObjectProviderBaseTestCase {
	
	String expectedMessageID;
	String expectedResponseConsumerURL;
	String expectedService;
	String expectedSoapActor;
	Boolean expectedSoapMustUnderstand;
	
    /** Constructor. */
    public RequestTest() {
    	singleElementFile = "/org/glite/sts/profile/soap/xmlobject/LibertyPAOSRequest.xml";
    }

    /** {@inheritDoc} */
    @BeforeMethod
    protected void setUp() throws Exception {    	
    	expectedMessageID = "1234567890";
        expectedResponseConsumerURL = "/soap";
        expectedService = "urn:liberty:id-sis-pp:2003-08";
        expectedSoapMustUnderstand = Boolean.TRUE;
        expectedSoapActor = Request.SOAP11_ACTOR_NEXT;
    }

    /** {@inheritDoc} */
    @Test
	public void testSingleElementMarshall() {
        Request request = (Request) buildXMLObject(Request.DEFAULT_ELEMENT_NAME);
        request.setMessageID(expectedMessageID);
        request.setResponseConsumerURL(expectedResponseConsumerURL);
        request.setService(expectedService);
        request.setSOAP11Actor(expectedSoapActor);
        request.setSOAP11MustUnderstand(expectedSoapMustUnderstand);
        assertXMLEquals(expectedDOM, request);
    }

    /** {@inheritDoc} */
    @Test
   	public void testSingleElementUnmarshall() {
        Request request = (Request) unmarshallElement(singleElementFile);
        Assert.assertNotNull(request, "Unmarshalled object was null");
        Assert.assertEquals(request.getMessageID(), expectedMessageID, "Message ID");
        Assert.assertEquals(request.getResponseConsumerURL(), expectedResponseConsumerURL, "Response Consumer URL");
        Assert.assertEquals(request.getService(), expectedService, "Service");
        Assert.assertEquals(request.getSOAP11Actor(), expectedSoapActor, "SOAP Actor");
        Assert.assertEquals(request.isSOAP11MustUnderstand(), expectedSoapMustUnderstand, "SOAP Must Understand");
    }
}
