/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.profile.soap.xmlobject;

import org.opensaml.core.xml.XMLObjectProviderBaseTestCase;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Unit tests for {@link Framework}.
 */
public class FrameworkTest extends XMLObjectProviderBaseTestCase {
    
    private String expectedContent;

    /** Constructor. */
    public FrameworkTest() {
        singleElementFile = "/org/glite/sts/profile/soap/xmlobject/LibertyIDWSFFramework.xml";
    }

    /** {@inheritDoc} */
    @BeforeMethod
    protected void setUp() throws Exception {
        expectedContent = "2.0";
    }

    /** {@inheritDoc} */
    @Test
    public void testSingleElementMarshall() {
        Framework framework = (Framework) buildXMLObject(Framework.DEFAULT_ELEMENT_NAME);
        framework.setVersion(expectedContent);
        assertXMLEquals(expectedDOM, framework);
    }

    @Test
    public void testSingleElementUnmarshall() {
        Framework framework = (Framework) unmarshallElement(singleElementFile);
        Assert.assertNotNull(framework, "Unmarshalled object was null");
        Assert.assertEquals(framework.getVersion(), expectedContent, "Version value");
    }

}
