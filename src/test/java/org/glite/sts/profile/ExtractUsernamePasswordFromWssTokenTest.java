package org.glite.sts.profile;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import net.shibboleth.idp.profile.RequestContextBuilder;
import net.shibboleth.idp.profile.impl.DecodeMessage;
import net.shibboleth.utilities.java.support.xml.ParserPool;

import org.glite.sts.authn.ExtractUsernamePasswordFromWssToken;
import org.glite.sts.profile.wstrust.WsTrustActionTestingSupport;
import org.opensaml.core.OpenSAMLInitBaseTestCase;
import org.opensaml.core.xml.XMLObjectProviderBaseTestCase;
import org.opensaml.core.xml.config.XMLObjectProviderRegistrySupport;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class ExtractUsernamePasswordFromWssTokenTest extends OpenSAMLInitBaseTestCase {
	
	ParserPool parserPool;
	
	byte[] content;
	
	@BeforeTest
	public void initialize() throws Exception {
		parserPool = XMLObjectProviderRegistrySupport.getParserPool();
		content = ActionTestingSupport.getByteArrayFromStream(XMLObjectProviderBaseTestCase.class.getResourceAsStream("/org/glite/sts/profile/ExampleEnvelope.xml"));		
	}
	
	@Test
	public void testDecodingEnvelope() throws Exception {
		
		RequestContextBuilder builder = new RequestContextBuilder();
		MockHttpServletRequest servletRequest = new MockHttpServletRequest();
		servletRequest.setContent(content);
		builder.setHttpRequest(servletRequest);
		builder.setRelyingPartyProfileConfigurations(WsTrustActionTestingSupport.buildProfileConfigurations());
    	RequestContext springRequestContext = builder.buildRequestContext();
    	EnvelopeDecoderFactory factory = new EnvelopeDecoderFactory(parserPool);

    	Event result = ActionTestingSupport.initializeAndExecuteAction(new DecodeMessage(factory), springRequestContext);
        ActionTestingSupport.assertProceedEvent(result);
        
        result = ActionTestingSupport.initializeAndExecuteAction(new CheckFrameworkVersion(true), springRequestContext);
        ActionTestingSupport.assertProceedEvent(result);
        
        result = ActionTestingSupport.initializeAndExecuteAction(new InitializeMessageMetadataContext(), springRequestContext);
        ActionTestingSupport.assertProceedEvent(result);
        
        result = ActionTestingSupport.initializeAndExecuteAction(new InitializeTokenRequestMessageContext(), springRequestContext);
        ActionTestingSupport.assertProceedEvent(result);
        
        result = ActionTestingSupport.initializeAndExecuteAction(new CheckClaims(true), springRequestContext);
        ActionTestingSupport.assertExpectedEvent(result, "UsernamePassword");
        
        result = ActionTestingSupport.initializeAndExecuteAction(new ExtractUsernamePasswordFromWssToken(), springRequestContext);
        ActionTestingSupport.assertProceedEvent(result);
	}
	
	public static byte[] getByteArrayFromStream(InputStream in) throws IOException {
		ByteArrayOutputStream buffer = new ByteArrayOutputStream();
		int read;
		byte[] data = new byte[8192];
		while ((read = in.read(data, 0, data.length)) != -1) {
			buffer.write(data, 0, read);
		}
		buffer.flush();
		return buffer.toByteArray();
	}
}
