package org.glite.sts.profile.wstrust;

import java.util.ArrayList;
import java.util.Collection;

import org.glite.sts.profile.config.WsTrustInteroperabilityProfileConfiguration;
import org.opensaml.core.xml.config.XMLObjectProviderRegistrySupport;
import org.opensaml.soap.common.SOAPObjectBuilder;
import org.opensaml.soap.soap11.Body;
import org.opensaml.soap.soap11.Envelope;
import org.opensaml.soap.soap11.Header;

import net.shibboleth.idp.profile.config.ProfileConfiguration;
import net.shibboleth.idp.profile.config.SecurityConfiguration;

public class WsTrustActionTestingSupport {
	
    /**
     * Builds a {@link ProfileConfiguration} collection containing a {@link WsTrustInteroperabilityProfileConfiguration}.
     * 
     * @return the constructed {@link ProfileConfiguration}
     */
    public static Collection<ProfileConfiguration> buildProfileConfigurations() {
        ArrayList<ProfileConfiguration> profileConfigs = new ArrayList<ProfileConfiguration>();
        profileConfigs.add(buildProfileConfiguration());
        return profileConfigs;
    }
    
    /**
     * Build a {@link WsTrustInteroperabilityProfileConfiguration}.
     */
    public static WsTrustInteroperabilityProfileConfiguration buildProfileConfiguration() {
        SecurityConfiguration securityConfig = new SecurityConfiguration();
        WsTrustInteroperabilityProfileConfiguration wsTrustConfig = new WsTrustInteroperabilityProfileConfiguration();
        wsTrustConfig.setSecurityConfiguration(securityConfig);
        return wsTrustConfig;
    }
    
    /**
     * Builds an empty SOAP envelope.
     * 
     * @return the constructed envelope
     */
    public static Envelope buildEnvelope() {
        final SOAPObjectBuilder<Envelope> envelopeBuilder =
                (SOAPObjectBuilder<Envelope>) XMLObjectProviderRegistrySupport.getBuilderFactory().getBuilder(
                        Envelope.DEFAULT_ELEMENT_NAME);
        final Envelope envelope = envelopeBuilder.buildObject();
        return envelope;
    }
    
    /**
     * Builds an empty body (for a SOAP envelope).
     * 
     * @return the constructed body
     */
    public static Body buildBody() {
    	final SOAPObjectBuilder<Body> bodyBuilder =
            (SOAPObjectBuilder<Body>) XMLObjectProviderRegistrySupport.getBuilderFactory().getBuilder(
                    Body.DEFAULT_ELEMENT_NAME);
    	final Body body = bodyBuilder.buildObject();
    	return body;
    }

    /**
     * Builds an empty header (for a SOAP envelope).
     * 
     * @return the constructed header
     */
    public static Header buildHeader() {
    	final SOAPObjectBuilder<Header> headerBuilder =
            (SOAPObjectBuilder<Header>) XMLObjectProviderRegistrySupport.getBuilderFactory().getBuilder(
                    Header.DEFAULT_ELEMENT_NAME);
    	final Header header= headerBuilder.buildObject();
    	return header;
    }

}
