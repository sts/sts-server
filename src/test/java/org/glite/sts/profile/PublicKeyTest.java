package org.glite.sts.profile;

import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPublicKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.annotation.Nonnull;

import net.shibboleth.idp.profile.AbstractProfileAction;
import net.shibboleth.idp.profile.ProfileRequestContext;
import net.shibboleth.idp.profile.RequestContextBuilder;
import net.shibboleth.idp.profile.impl.DecodeMessage;
import net.shibboleth.idp.profile.navigate.WebflowRequestContextProfileRequestContextLookup;
import net.shibboleth.utilities.java.support.xml.ParserPool;

import org.apache.xml.security.exceptions.Base64DecodingException;
import org.apache.xml.security.utils.Base64;
import org.glite.sts.profile.soap.SoapActionSupport;
import org.glite.sts.profile.wstrust.WsTrustActionTestingSupport;
import org.opensaml.core.OpenSAMLInitBaseTestCase;
import org.opensaml.core.xml.XMLObject;
import org.opensaml.core.xml.XMLObjectProviderBaseTestCase;
import org.opensaml.core.xml.config.XMLObjectProviderRegistrySupport;
import org.opensaml.soap.soap11.Body;
import org.opensaml.soap.soap11.Envelope;
import org.opensaml.soap.wssecurity.BinarySecurityToken;
import org.opensaml.soap.wstrust.RequestSecurityToken;
import org.opensaml.soap.wstrust.UseKey;
import org.opensaml.xmlsec.signature.KeyInfo;
import org.opensaml.xmlsec.signature.RSAKeyValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.google.common.base.Function;

public class PublicKeyTest extends OpenSAMLInitBaseTestCase {
	
	/** Class logger. */
    private final static Logger log = LoggerFactory.getLogger(PublicKeyTest.class);
    
    private Function<RequestContext, ProfileRequestContext> profileContextLookupStrategy;
    
	ParserPool parserPool;
	
	byte[] content;
	
	@BeforeTest
	public void initialize() throws Exception {
		parserPool = XMLObjectProviderRegistrySupport.getParserPool();
		content = ActionTestingSupport.getByteArrayFromStream(XMLObjectProviderBaseTestCase.class.getResourceAsStream("/soap-example.xml"));
		profileContextLookupStrategy = new WebflowRequestContextProfileRequestContextLookup();
	}
	
	public void testDecodingEnvelope() throws Exception {
		
		RequestContextBuilder builder = new RequestContextBuilder();
		MockHttpServletRequest servletRequest = new MockHttpServletRequest();
		servletRequest.setContent(content);
		builder.setHttpRequest(servletRequest);
		builder.setRelyingPartyProfileConfigurations(WsTrustActionTestingSupport.buildProfileConfigurations());
    	RequestContext springRequestContext = builder.buildRequestContext();
    	EnvelopeDecoderFactory factory = new EnvelopeDecoderFactory(parserPool);

    	Event result = ActionTestingSupport.initializeAndExecuteAction(new DecodeMessage(factory), springRequestContext);
        ActionTestingSupport.assertProceedEvent(result);
        
        result = ActionTestingSupport.initializeAndExecuteAction(new CheckFrameworkVersion(true), springRequestContext);
        ActionTestingSupport.assertProceedEvent(result);
        
        ProfileRequestContext profileContext = profileContextLookupStrategy.apply(springRequestContext);
        Envelope envelope = (Envelope)profileContext.getInboundMessageContext().getMessage();
        
        RSAPublicKey publicKey = SoapActionSupport.getPublicKeyFromEnvelope(new CheckFrameworkVersion(true), envelope);
        Assert.assertEquals(publicKey.toString(), "testi");
   	}
	
	
	
	   /**
     * Get the {@link RSAPublicKey} from the SOAP envelope. It is expected to be found from
     * the {@link RequestSecurityToken}'s {@link UseKey} -element, inside the {@link Body}.
     *  
     * @param envelope the envelope containing the RSA public key.
     * @return the RSA public key.
     */
    public static RSAPublicKey getPublicKeyFromEnvelope(@Nonnull final AbstractProfileAction<?, ?> action, Envelope envelope) throws Exception {
    	RSAKeyValue rsaValue = getRSAKeyValueFromEnvelope(action, envelope);
    	if (rsaValue == null) {
    		log.debug("Action {}: No single RSAKeyValue found!", action.getId());
    		
    		
        	RequestSecurityToken rst = SoapActionSupport.getRequestSecurityTokenFromBody(envelope.getBody());
        	if (rst == null) {
        		log.debug("Action {}: No RST found from the envelope body", action.getId());
        		return null;
        	}
        	UseKey useKey = SoapActionSupport.getUseKeyFromRequestSecurityToken(rst);
        	if (useKey == null) {
        		log.debug("Action {}: No single UseKey found from the RST", action.getId());
        		return null;
        	}
        	BinarySecurityToken xmlObject = (BinarySecurityToken)useKey.getUnknownXMLObject();
    		byte[] publicKeyBytes = Base64.decode(xmlObject.getValue());
        	X509EncodedKeySpec pubKeySpec = new X509EncodedKeySpec(publicKeyBytes);
        	KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        	PublicKey pubKey = keyFactory.generatePublic(pubKeySpec);
        	Assert.assertEquals(((RSAPublicKey)pubKey).toString(), "testi");
    		
    		return null;
    	}
    	log.debug("Action {}: RSAKeyValue was found, reading the modulus and the exponent", action.getId());
    	if (rsaValue.getModulus() != null && rsaValue.getExponent() != null) {
    		String modulus = rsaValue.getModulus().getValue();
    		String exponent = rsaValue.getExponent().getValue();
			try {
				RSAPublicKeySpec keySpec = new RSAPublicKeySpec(new BigInteger(Base64.decode(modulus)), new BigInteger(Base64.decode(exponent)));
				return (RSAPublicKey) KeyFactory.getInstance("RSA").generatePublic(keySpec);
			} catch (Base64DecodingException e) {
				log.error("Action {}: Could not decode modulus and/or exponent", action.getId(), e);
			} catch (InvalidKeySpecException e) {
				log.error("Action {}: Internal error with key spec definitions", action.getId(), e);
			} catch (NoSuchAlgorithmException e) {
				log.error("Action {}: Internal error: RSA algorithm set as invalid", action.getId(), e);
			}
    	} else {
    		log.debug("Action {}: No modulus and/or exponent found for the RSAKeyValue!", action.getId());
    	}
    	return null;	
    }
    
    /**
     * Get the {@link RSAKeyValue} from the SOAP envelope. It is expected to be found from
     * the {@link RequestSecurityToken}'s {@link UseKey} -element, inside the {@link Body}.
     *  
     * @param envelope the envelope containing the RSA key value.
     * @return the RSA key value.
     */
    public static RSAKeyValue getRSAKeyValueFromEnvelope(@Nonnull final AbstractProfileAction<?, ?> action, Envelope envelope) {
    	RequestSecurityToken rst = SoapActionSupport.getRequestSecurityTokenFromBody(envelope.getBody());
    	if (rst == null) {
    		log.debug("Action {}: No RST found from the envelope body", action.getId());
    		return null;
    	}
    	UseKey useKey = SoapActionSupport.getUseKeyFromRequestSecurityToken(rst);
    	if (useKey == null) {
    		log.debug("Action {}: No single UseKey found from the RST", action.getId());
    		return null;
    	}
    	KeyInfo keyInfo = SoapActionSupport.getKeyInfoFromUseKey(useKey);
    	if (keyInfo == null) {
    		log.debug("Action {}: No single KeyInfo found from the UseKey", action.getId());
    		return null;
    	}
    	return SoapActionSupport.getRSAKeyValueFromKeyInfo(keyInfo);    	
    }


}
