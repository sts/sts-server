/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.profile;

import org.apache.http.HttpHeaders;
import org.glite.sts.profile.config.EcpProfileConfiguration;
import org.opensaml.core.config.InitializationService;
import org.opensaml.saml.common.xml.SAMLConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockServletContext;
import org.springframework.webflow.config.FlowDefinitionResource;
import org.springframework.webflow.config.FlowDefinitionResourceFactory;
import org.springframework.webflow.test.MockExternalContext;
import org.springframework.webflow.test.execution.AbstractXmlFlowExecutionTests;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * 
 */
public class ECPFlowTest extends AbstractXmlFlowExecutionTests {
	
	/** Class logger. */
    private final Logger log = LoggerFactory.getLogger(ECPFlowTest.class);
	
	MockExternalContext context = new MockExternalContext();
	
	@Override
	protected FlowDefinitionResource getResource(FlowDefinitionResourceFactory resourceFactory) {
		FlowDefinitionResource resource = resourceFactory.createResource("ecp-flow.xml");
		Assert.assertNotNull(resource);
		log.debug("Resource is loaded");
		return resource;
	}
	
	@Override
	protected void registerMockFlowBeans(ConfigurableBeanFactory flowBeanFactory) {
		
	}

	public void init() throws Exception {
		InitializationService.initialize();
		final MockHttpServletRequest servletRequest = new MockHttpServletRequest();
		servletRequest.addHeader(HttpHeaders.ACCEPT, EcpProfileConfiguration.PAOS_CONTENT_TYPE);
		servletRequest.addHeader(EcpProfileConfiguration.PAOS_HEADER, "\"" + SAMLConstants.SAML20ECP_NS +"\"; ver=\"" + SAMLConstants.PAOS_NS + "\"");
		log.debug("header: {}", servletRequest.getHeader(EcpProfileConfiguration.PAOS_HEADER));
		context.setNativeRequest(servletRequest);
		context.setNativeResponse(new MockHttpServletResponse());
		context.setNativeContext(new MockServletContext());
	}
	
	@Test
	public void testFlow() throws Exception {
		this.init();
		this.startFlow(context);
		this.assertFlowExecutionEnded();
		MockHttpServletResponse servletResponse = (MockHttpServletResponse) context.getNativeResponse();
		log.debug("Response: " + servletResponse.getContentAsString());
	}
}
