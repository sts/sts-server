package org.glite.sts.ta.voms.attrstore.impl;

import java.security.cert.X509Certificate;
import java.util.Timer;

import org.glite.sts.config.ServerConfiguration;
import org.testng.Assert;
import org.testng.annotations.Test;

import eu.emi.security.authn.x509.CrlCheckingMode;
import eu.emi.security.authn.x509.NamespaceCheckingMode;
import eu.emi.security.authn.x509.OCSPCheckingMode;
import eu.emi.security.authn.x509.ValidationResult;
import eu.emi.security.authn.x509.X509Credential;
import eu.emi.security.authn.x509.helpers.pkipath.AbstractValidator;
import eu.emi.security.authn.x509.impl.PEMCredential;
import eu.emi.security.authn.x509.impl.ValidatorParamsExt;

public class InMemoryStorageServiceTest {
	
//	@Test
	public void test() throws Exception {
		X509Credential credential = new PEMCredential("/users/henri/voms-admin-api-master/src/creds/cernkey.pem", "/users/henri/voms-admin-api-master/src/creds/cerncert.pem", null);
		ServerConfiguration configuration = new ServerConfiguration("src/test/resources/org/glite/sts/config/sts-server.ini");
		InMemoryStorageService service = new InMemoryStorageService(credential, configuration, new YesToAllValidator(), new Timer(), 6000000, NamespaceCheckingMode.IGNORE, CrlCheckingMode.IGNORE, OCSPCheckingMode.IGNORE, "C:/Temp/stscache.txt");
		Assert.assertNotNull(service);
		Thread.sleep(60000);
	}

	class YesToAllValidator extends AbstractValidator {
		
		public YesToAllValidator() {
			super(new ValidatorParamsExt().getInitialListeners());
			System.out.println("Validator initialized");
		}

		public synchronized ValidationResult validate(X509Certificate[] certChain) {
//			System.out.println("Validating certs");
			for (int i = 0; i < certChain.length; i++) {
//				System.out.println(certChain[i].getSubjectDN());
			}
//			System.out.println("Returning true");
			return new ValidationResult(true);
		}
		
		@Override
		public synchronized X509Certificate[] getTrustedIssuers()
		{
			return new X509Certificate[0];
		}
	}
}
