/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.x509;

import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.Security;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.RSAPublicKeySpec;

import org.apache.xml.security.utils.Base64;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

/**
 * Simple unit tests for {@link RSACertificateKeys}.
 */
public class RSACertificateKeysTest {
	
	/** Class logger. */
    private final Logger log = LoggerFactory.getLogger(RSACertificateKeysTest.class);
	
    @BeforeTest
    public void initTests() throws Exception {
    	log.debug("Initializing test");
    	Security.addProvider(new BouncyCastleProvider());
    }
    
	@Test
	public void testKeys() throws Exception {
		CertificateKeys keys = new RSACertificateKeys(2048);
		RSAPublicKey publicKey = (RSAPublicKey) keys.getPublicKey();
		String modulus = Base64.encode(publicKey.getModulus().toByteArray());
		String exponent = Base64.encode(publicKey.getPublicExponent().toByteArray());
		log.debug("Modulus  {}", modulus);
		log.debug("Exponent {}", exponent);
		Assert.assertNotNull(publicKey);
		Assert.assertNotNull(keys.getPrivateKey());
		String base64pk = Base64.encode(publicKey.getEncoded());
		RSAPublicKeySpec keySpec = new RSAPublicKeySpec(new BigInteger(Base64.decode(modulus)), new BigInteger(Base64.decode(exponent)));
		publicKey = (RSAPublicKey) KeyFactory.getInstance("RSA").generatePublic(keySpec);
		keys = new RSACertificateKeys(publicKey);
		Assert.assertNotNull(keys.getPublicKey());
		Assert.assertNull(keys.getPrivateKey());
		Assert.assertEquals(Base64.encode(keys.getPublicKey().getEncoded()), base64pk);
	}

}
