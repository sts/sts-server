package org.glite.sts.x509.caclient.xmlobject;

import org.opensaml.core.xml.XMLObjectProviderBaseTestCase;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class NewStsCertificateTest extends XMLObjectProviderBaseTestCase {
    
    private String expectedBase64Request;


    /** Constructor. */
    public NewStsCertificateTest() {
        childElementsFile = "/org/glite/sts/x509/caclient/xmlobject/NewStsCertificate.xml";
    }

    /** {@inheritDoc} */
    @BeforeMethod
    protected void setUp() throws Exception {
    	expectedBase64Request = "stringBase64";

    }

    /** {@inheritDoc} */
    public void testSingleElementMarshall() {
    	// no need
    }
    
    /** {@inheritDoc} */
    @Test
    public void testChildElementsMarshall() {
    	Base64CertificateRequest base64Request = (Base64CertificateRequest) buildXMLObject(Base64CertificateRequest.DEFAULT_ELEMENT_NAME);
    	base64Request.setValue(expectedBase64Request);
    	NewStsCertificate stsCertificate = (NewStsCertificate) buildXMLObject(NewStsCertificate.DEFAULT_ELEMENT_NAME);
    	stsCertificate.setBase64CertificateRequest(base64Request);
    	assertXMLEquals(expectedChildElementsDOM, stsCertificate);    	
    }

    /** {@inheritDoc} */
    @Test
    public void testSingleElementUnmarshall() {
    	// no need
    }
    
    /** {@inheritDoc} */
    @Test
    public void testChildElementsUnmarshall() {
    	NewStsCertificate stsCertificate = (NewStsCertificate) unmarshallElement(childElementsFile);
    	Base64CertificateRequest base64Request = stsCertificate.getBase64CertificateRequest();
    	Assert.assertEquals(base64Request.getValue(), expectedBase64Request);
    }
}