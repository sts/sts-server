package org.glite.sts.x509.caclient.xmlobject;

import org.opensaml.core.xml.XMLObjectProviderBaseTestCase;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class NewStsCertificateResponseTest extends XMLObjectProviderBaseTestCase {
    
	private String expectedActivityID;
	private String expectedCertificate;
	private String expectedMessage;
	private String expectedRequestID;
	private String expectedSuccess;
	
    /** Constructor. */
    public NewStsCertificateResponseTest() {
        childElementsFile = "/org/glite/sts/x509/caclient/xmlobject/NewStsCertificateResponse.xml";
    }

    /** {@inheritDoc} */
    @BeforeMethod
    protected void setUp() throws Exception {
    	expectedActivityID = "myActivityId";
    	expectedCertificate = "myCertificate";
    	expectedMessage = "myMessage";
    	expectedRequestID = "myRequestId";
    	expectedSuccess = "true";
    }

    /** {@inheritDoc} */
    public void testSingleElementMarshall() {
    	// no need
    }
    
    /** {@inheritDoc} */
    @Test
    public void testChildElementsMarshall() {
    	ActivityID activityID = (ActivityID) buildXMLObject(ActivityID.DEFAULT_ELEMENT_NAME);
    	activityID.setValue(expectedActivityID);
    	Certificate certificate = (Certificate) buildXMLObject(Certificate.DEFAULT_ELEMENT_NAME);
    	certificate.setValue(expectedCertificate);
    	Message message = (Message) buildXMLObject(Message.DEFAULT_ELEMENT_NAME);
    	message.setValue(expectedMessage);
    	RequestID requestID = (RequestID) buildXMLObject(RequestID.DEFAULT_ELEMENT_NAME);
    	requestID.setValue(expectedRequestID);
    	Success success = (Success) buildXMLObject(Success.DEFAULT_ELEMENT_NAME);
    	success.setValue(expectedSuccess);
    	NewStsCertificateResponse stsCertificateResponse = (NewStsCertificateResponse) buildXMLObject(NewStsCertificateResponse.DEFAULT_ELEMENT_NAME);
    	NewStsCertificateResult stsCertificateResult = (NewStsCertificateResult) buildXMLObject(NewStsCertificateResult.DEFAULT_ELEMENT_NAME);
    	stsCertificateResult.setActivityID(activityID);
    	stsCertificateResult.setCertificate(certificate);
    	stsCertificateResult.setMessage(message);
    	stsCertificateResult.setRequestID(requestID);
    	stsCertificateResult.setSuccess(success);
    	stsCertificateResponse.setNewStsCertificateResult(stsCertificateResult);
    	assertXMLEquals(expectedChildElementsDOM, stsCertificateResponse);    	
    }

    /** {@inheritDoc} */
    @Test
    public void testSingleElementUnmarshall() {
    	// no need
    }
    
    /** {@inheritDoc} */
    @Test
    public void testChildElementsUnmarshall() {
    	NewStsCertificateResponse stsCertificateResponse = (NewStsCertificateResponse) unmarshallElement(childElementsFile);
    	NewStsCertificateResult stsCertificateResult = stsCertificateResponse.getNewStsCertificateResult();
    	ActivityID activityID = stsCertificateResult.getActivityID();
    	Certificate certificate = stsCertificateResult.getCertificate();
    	Message message = stsCertificateResult.getMessage();
    	RequestID requestID = stsCertificateResult.getRequestID();
    	Success success = stsCertificateResult.getSuccess();
    	Assert.assertEquals(activityID.getValue(), expectedActivityID);
    	Assert.assertEquals(certificate.getValue(), expectedCertificate);
    	Assert.assertEquals(message.getValue(), expectedMessage);
    	Assert.assertEquals(requestID.getValue(), expectedRequestID);
    	Assert.assertEquals(success.getValue(), expectedSuccess);
    }
}