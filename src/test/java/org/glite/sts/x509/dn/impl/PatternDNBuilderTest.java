/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.x509.dn.impl;

import java.util.List;
import java.util.Vector;

import org.glite.sts.STSException;
import org.glite.sts.x509.dn.DNBuilder;
import org.glite.sts.x509.dn.DNBuildingException;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import net.shibboleth.idp.attribute.Attribute;
import net.shibboleth.idp.attribute.AttributeContext;
import net.shibboleth.idp.attribute.AttributeValue;
import net.shibboleth.idp.attribute.StringAttributeValue;

/**
 * Unit tests for {@link PatternDNBuilder}.
 */
public class PatternDNBuilderTest {
	
	DNBuilder dnBuilder;
	String cnAttributeName;
	String ouAttributeName;
	String cnAttributeValue;
	String ouAttributeValue;
	
	@BeforeTest
	public void setUp() throws STSException {
		cnAttributeName = "DN";
		ouAttributeName = "OU";
		cnAttributeValue = "TestCN";
		ouAttributeValue = "TestOU";
		String pattern = "/C=FI/O=Test/OU=${" + ouAttributeName + "}/CN=${" + cnAttributeName + "}";
		dnBuilder = new PatternDNBuilder(pattern);
	}
	
	@Test
	public void testNullContext() {
		String dn = null;
		try {
			dn = dnBuilder.createDN(null);
		} catch (DNBuildingException e) {
			// ignore, this is expected
		}
		Assert.assertNull(dn);
	}

	@Test
	public void testSuccess() throws DNBuildingException {
		AttributeContext attributeContext = generateAttributeContext(ouAttributeValue, cnAttributeValue);
		Assert.assertEquals(dnBuilder.createDN(attributeContext), "/C=FI/O=Test/OU=" + ouAttributeValue + "/CN=" + cnAttributeValue);
	}
	
	@Test
	public void testPartial() {
		AttributeContext attributeContext = generateAttributeContext(ouAttributeValue, null);
		String dn = null;
		try {
			dn = dnBuilder.createDN(attributeContext);
		} catch (DNBuildingException e) {
			// ignore, this is expected
		}
		Assert.assertNull(dn);
	}
	
	protected AttributeContext generateAttributeContext(String ou, String cn) {
		List<Attribute> attributes = new Vector<Attribute>();
		if (ou != null) {
			Attribute ouAttribute = generateAttribute(ouAttributeName, ou);
			attributes.add(ouAttribute);
		}
		if (cn != null) {
			Attribute cnAttribute = generateAttribute(cnAttributeName, cn);
			attributes.add(cnAttribute);
		}
		AttributeContext attributeContext = new AttributeContext();
		attributeContext.setAttributes(attributes);
		return attributeContext;
	}
	
	protected Attribute generateAttribute(String name, String value) {
		Attribute attribute = new Attribute(name);
		StringAttributeValue attrValue = new StringAttributeValue(value);
		@SuppressWarnings("rawtypes")
		Vector<AttributeValue> list = new Vector<AttributeValue>();
		list.add(attrValue);
		attribute.setValues(list);
		return attribute;    	
	}
}
