/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.util;

import java.util.Timer;

import org.glite.sts.config.ServerConfiguration;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

/**
 * Unit tests for {@link AuthnRequestIdManager}.
 */
public class EMIRPublisherTest {
	
	ServerConfiguration serverConfiguration;
	EMIRPublisher emirPublisher;
	
	@BeforeTest
	public void init() throws Exception {
		serverConfiguration = new ServerConfiguration("src/test/resources/org/glite/sts/config/sts-server.ini");
		emirPublisher = new EMIRPublisher(new Timer(), serverConfiguration);
	}

	@Test
	public void testExtractString() {
		String[] expected = new String[] { "test1", "test2", "test3" };
		Assert.assertEquals(EMIRPublisher.extractStringIntoArray("test1,test2,test3"), expected);
	}
	
	@Test
	public void testToJsonArray() {
		String[] actual = new String[] { "test1", "test2", "test3" };
		Assert.assertEquals(EMIRPublisher.getJsonArray(actual), "\"test1\",\"test2\",\"test3\"");
	}
}
