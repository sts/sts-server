/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.x509;

import java.security.PrivateKey;
import java.security.PublicKey;

/**
 * This interface is meant for the classes holding a public private key-pair.
 */
public interface CertificateKeys {
	
    /**
     * Sets the private part of the key-pair.
     * @param privateKey the private part of the key-pair.
     */
	public void setPrivateKey(PrivateKey privateKey);
    
    /**
     * Gets the private part of the key-pair.
     * @return the private part of the key-pair.
     */
	public PrivateKey getPrivateKey();
    
    /**
     * Sets the public part of the key-pair.
     * @param publicKey the public part of the key-pair.
     */
	public void setPublicKey(PublicKey publicKey);
    
    /**
     * Gets the public part of the key-pair.
     * @return the public part of the key-pair.
     */
	public PublicKey getPublicKey();

}
