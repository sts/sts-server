/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.x509;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;

import org.glite.sts.STSException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Implementation for the {@link CertificateKeys} using RSA algorithm.
 */
public class RSACertificateKeys implements CertificateKeys {

	/** Logging. */
	private static Logger log = LoggerFactory
			.getLogger(RSACertificateKeys.class);

	/** The private part of the RSA key-pair. */
	private PrivateKey privateKey;
	
	/** The public part of the RSA key-pair. */
	private PublicKey publicKey;

	/**
	 * Constructor. Using BC provider.
	 * @param keysize the desired key strength
	 * @throws STSException thrown if the key-pair cannot be generated.
	 */
	public RSACertificateKeys(int keysize) throws STSException {
		this(keysize, "BC");
	}

	/**
	 * Constructor.
	 * @param keysize the desired key strength
	 * @param provider security provider identifier
	 * @throws STSException thrown if the key-pair cannot be generated
	 */
	public RSACertificateKeys(int keysize, String provider) throws STSException {
		KeyPairGenerator generator = null;
		log.debug("Initializing a keypair with keysize {}", keysize);
		try {
			generator = KeyPairGenerator.getInstance("RSA", provider);
			generator.initialize(keysize);
		} catch (NoSuchAlgorithmException e) {
			throw new STSException("Invalid key algorithm", e);
		} catch (NoSuchProviderException e2) {
			throw new STSException("Provider " + provider
					+ " not found, is it installed?", e2);
		}

		KeyPair keyPair = generator.generateKeyPair();
		this.privateKey = keyPair.getPrivate();
		this.publicKey = keyPair.getPublic();

	}

	/**
	 * Constructor.
	 * @param publicKey the public part of the RSA key-pair
	 * @param privateKey the private part of the RSA key-pair, can be null
	 * @throws STSException thrown if the public key is null or either of the keys is not RSA
	 */
	public RSACertificateKeys(PublicKey publicKey, PrivateKey privateKey)
			throws STSException {
		if (publicKey == null || !(publicKey.getAlgorithm().equals("RSA"))
				|| (privateKey != null && !privateKey.getAlgorithm().equals(
						"RSA"))) {
			throw new STSException("Invalid key algorithm!");
		}
		this.privateKey = privateKey;
		this.publicKey = publicKey;
	}

	/**
	 * Constructor, leaving the private key null.
	 * @param publicKey the public part of the RSA key-pair
	 * @throws STSException thrown if the public key is null or not RSA key
	 */
	public RSACertificateKeys(PublicKey publicKey) throws STSException {
		this(publicKey, null);
	}

    /** {@inheritDoc} */
	public void setPrivateKey(PrivateKey privateKey) {
		this.privateKey = privateKey;

	}

    /** {@inheritDoc} */
	public PrivateKey getPrivateKey() {
		return this.privateKey;
	}

    /** {@inheritDoc} */
	public void setPublicKey(PublicKey publicKey) {
		this.publicKey = publicKey;
	}

    /** {@inheritDoc} */
	public PublicKey getPublicKey() {
		return this.publicKey;
	}

}
