package org.glite.sts.x509.caclient.xmlobject.impl;

import net.shibboleth.utilities.java.support.xml.ElementSupport;

import org.glite.sts.x509.caclient.xmlobject.RequestID;
import org.opensaml.core.xml.XMLObject;
import org.opensaml.core.xml.io.AbstractXMLObjectMarshaller;
import org.opensaml.core.xml.io.MarshallingException;
import org.w3c.dom.Element;

public class RequestIDMarshaller extends AbstractXMLObjectMarshaller {

    /** {@inheritDoc} */
    protected void marshallAttributes(XMLObject xmlObject, Element domElement) throws MarshallingException {
        // no attributes
    }

    /** {@inheritDoc} */
    protected void marshallElementContent(XMLObject xmlObject, Element domElement) throws MarshallingException {
        RequestID requestID = (RequestID) xmlObject;
        ElementSupport.appendTextContent(domElement, requestID.getValue());
    }
}