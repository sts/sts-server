package org.glite.sts.x509.caclient.xmlobject.impl;

import org.glite.sts.x509.caclient.xmlobject.RequestID;
import org.opensaml.core.xml.schema.impl.XSStringImpl;

public class RequestIDImpl extends XSStringImpl implements RequestID {

	protected RequestIDImpl(String namespaceURI,
			String elementLocalName, String namespacePrefix) {
		super(namespaceURI, elementLocalName, namespacePrefix);
	}
}
