package org.glite.sts.x509.caclient.xmlobject.impl;

import org.glite.sts.x509.caclient.xmlobject.Base64CertificateRequest;
import org.opensaml.core.xml.AbstractXMLObjectBuilder;
import org.opensaml.core.xml.schema.XSString;

public class Base64CertificateRequestBuilder extends AbstractXMLObjectBuilder<XSString> {

	@Override
	public Base64CertificateRequest buildObject(String namespaceURI, String localName, String namespacePrefix) {
		return new Base64CertificateRequestImpl(namespaceURI, localName, namespacePrefix);
	}	
}
