package org.glite.sts.x509.caclient.xmlobject.impl;

import org.glite.sts.x509.caclient.xmlobject.Message;
import org.opensaml.core.xml.schema.impl.XSStringImpl;

public class MessageImpl extends XSStringImpl implements Message {

	protected MessageImpl(String namespaceURI,
			String elementLocalName, String namespacePrefix) {
		super(namespaceURI, elementLocalName, namespacePrefix);
	}
}
