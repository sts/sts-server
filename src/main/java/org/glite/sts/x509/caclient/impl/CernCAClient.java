package org.glite.sts.x509.caclient.impl;

import java.io.IOException;
import java.security.GeneralSecurityException;

import net.shibboleth.utilities.java.support.logic.Constraint;
import net.shibboleth.utilities.java.support.xml.ParserPool;

import org.apache.http.client.HttpClient;
import org.glite.sts.STSException;
import org.glite.sts.config.ServerConfiguration;
import org.glite.sts.util.HttpClientBuilder;
import org.glite.sts.x509.caclient.CAClient;
import org.glite.sts.x509.caclient.CAClientException;
import org.glite.sts.x509.caclient.CAConnection;
import org.ini4j.Ini;
import org.opensaml.core.xml.io.UnmarshallerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.emi.security.authn.x509.X509CertChainValidator;
import eu.emi.security.authn.x509.X509Credential;

/**
 * An implementation of {@link CAClient} for CERN IOTA CA.
 */
public class CernCAClient implements CAClient {
	
    /** Logging. */
    private static Logger log = LoggerFactory.getLogger(CernCAClient.class);
    
    /** The HTTP client used for the connection. */
    private HttpClient httpClient;
    
    /** The server endpoint URL. */
    private String serverUrl;
    
    /** The opensaml parser pool. */
    private final ParserPool parserPool;
    
    /** The opensaml unmarshaller factory. */
    private final UnmarshallerFactory unmarshallerFactory;
    
    /**
     * Constructor.
     * @param credential The client credential for the server communication.
     * @param chainValidator The server certificate chain validator.
     * @param serverConfiguration The STS configuration.
     * @param pool The opensaml parser pool.
     * @param factory The opensaml unmarshaller factory.
     * @throws STSException For configuration errors.
     * @throws CAClientException For communication errors.
     */
    public CernCAClient(X509Credential credential, X509CertChainValidator chainValidator, ServerConfiguration serverConfiguration, final ParserPool pool, final UnmarshallerFactory factory) throws STSException, CAClientException {
    	Ini.Section iniSection = serverConfiguration.getConfigurationSection(ServerConfiguration.CFG_SECTION_CACLIENT);
    	this.serverUrl = Constraint.isNotNull(iniSection.get("serverUrl"), "The serverUrl cannot be null!");
    	this.httpClient = createHttpClient(credential, chainValidator);
    	this.parserPool = Constraint.isNotNull(pool, "Parser pool cannot be null!");
    	this.unmarshallerFactory = Constraint.isNotNull(factory, "Unmarshaller Factory cannot be null!");
    }
    
    /**
     * Get the opensaml parser pool.
     * @return parser pool.
     */
    public final ParserPool getParserPool() {
    	return this.parserPool;
    }
    
    /**
     * Get the opensaml unmarshaller factory.
     * @return unmarshaller factory.
     */
    public final UnmarshallerFactory getUnmarshallerFactory() {
    	return this.unmarshallerFactory;
    }

	@Override
	/** {@inheritDoc} */
	public CAConnection getConnection() throws CAClientException {
		return new CernCAConnection(this);
	}

	/**
	 * Creates a HTTP client corresponding to the given settings.
     * @param credential The client credential for the server communication.
     * @param chainValidator The server certificate chain validator.
	 * @return the HTTP client.
	 * @throws CAClientException If HTTP client cannot be built.
	 */
	private HttpClient createHttpClient(X509Credential credential, X509CertChainValidator chainValidator) throws CAClientException {
    	HttpClientBuilder httpBuilder = new HttpClientBuilder(this.serverUrl, credential, chainValidator);
    	HttpClient httpClient = null;
		try {
			httpClient = httpBuilder.buildClient();
		} catch (IOException e) {
			log.error("Could not build an HTTP client", e);
			throw new CAClientException(e);
		} catch (GeneralSecurityException e) {
			log.error("Security error while building an HTTP client", e);
			throw new CAClientException(e);
		}
    	return httpClient;    	
    }
    
	/**
	 * Get the server endpoint URL.
	 * @return serverUrl
	 */
    public String getServerUrl() {
    	return this.serverUrl;
    }
    
    /**
     * Get the configured HTTP client.
     * @return the HTTP client.
     */
    public HttpClient getHttpClient() {
    	return this.httpClient;
    }
    
    /** {@inheritDoc} */
    public void shutdown() {
        this.httpClient.getConnectionManager().shutdown();
        this.httpClient = null;
    }
}
