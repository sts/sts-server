package org.glite.sts.x509.caclient.xmlobject;

public class StsCertificateConstants {
	
    public static final String CERN_CA_STS_NS = "https://ca.cern.ch/iotaca-services";
    
    public static final String CERN_CA_STS_PREFIX = "sts";

}
