package org.glite.sts.x509.caclient.xmlobject.impl;

import org.glite.sts.x509.caclient.xmlobject.ActivityID;
import org.glite.sts.x509.caclient.xmlobject.Certificate;
import org.glite.sts.x509.caclient.xmlobject.Message;
import org.glite.sts.x509.caclient.xmlobject.NewStsCertificateResult;
import org.glite.sts.x509.caclient.xmlobject.RequestID;
import org.glite.sts.x509.caclient.xmlobject.Success;
import org.opensaml.core.xml.XMLObject;
import org.opensaml.core.xml.io.AbstractXMLObjectUnmarshaller;
import org.opensaml.core.xml.io.UnmarshallingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NewStsCertificateResultUnmarshaller extends AbstractXMLObjectUnmarshaller {

    /** Logger. */
    private final Logger log = LoggerFactory.getLogger(NewStsCertificateResultUnmarshaller.class);
    
    /** {@inheritDoc} */
    protected void processChildElement(XMLObject parentXMLObject, XMLObject childXMLObject)
            throws UnmarshallingException {
    	log.debug("Processing child element for NewStsCertificate");
    	NewStsCertificateResult stsCertificateResult = (NewStsCertificateResult) parentXMLObject;
    	if (childXMLObject instanceof ActivityID) {
    		log.debug("Setting ActivityID");
    		stsCertificateResult.setActivityID((ActivityID) childXMLObject);
    	} else if (childXMLObject instanceof Certificate) {
    		log.debug("Setting Certificate");
    		stsCertificateResult.setCertificate((Certificate) childXMLObject);
    	} else if (childXMLObject instanceof Message) {
    		log.debug("Setting Message");
    		stsCertificateResult.setMessage((Message) childXMLObject);
    	} else if (childXMLObject instanceof RequestID) {
    		log.debug("Setting RequestID");
    		stsCertificateResult.setRequestID((RequestID) childXMLObject);
    	} else if (childXMLObject instanceof Success) {
    		log.debug("Setting Success");
    		stsCertificateResult.setSuccess((Success) childXMLObject);
    	} else {
    		super.processChildElement(parentXMLObject, childXMLObject);
    	}
    }
}
