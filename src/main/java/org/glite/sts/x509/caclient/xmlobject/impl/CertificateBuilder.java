package org.glite.sts.x509.caclient.xmlobject.impl;

import org.glite.sts.x509.caclient.xmlobject.Certificate;
import org.opensaml.core.xml.AbstractXMLObjectBuilder;
import org.opensaml.core.xml.schema.XSString;

public class CertificateBuilder extends AbstractXMLObjectBuilder<XSString> {

	@Override
	public Certificate buildObject(String namespaceURI, String localName, String namespacePrefix) {
		return new CertificateImpl(namespaceURI, localName, namespacePrefix);
	}
}
