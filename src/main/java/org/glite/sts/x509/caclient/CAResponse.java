/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.x509.caclient;

import java.security.cert.X509Certificate;

public interface CAResponse {
	
	public X509Certificate getCertificate();

}
