/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.x509.caclient.impl;

import java.io.IOException;
import java.io.InputStream;
import java.security.cert.X509Certificate;

import org.apache.http.HttpResponse;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.x509.X509CertificateStructure;
import org.bouncycastle.jce.provider.X509CertificateObject;
import org.glite.sts.x509.caclient.CAClientException;
import org.glite.sts.x509.caclient.CAResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.novosec.pkix.asn1.cmp.CertRepMessage;
import com.novosec.pkix.asn1.cmp.CertResponse;
import com.novosec.pkix.asn1.cmp.PKIMessage;

/**
 * CMP-implementation for {@link CAResponse}.
 */
public class CMPResponse implements CAResponse {

	/** Logging. */
	private static Logger log = LoggerFactory.getLogger(CMPResponse.class);
	
	/** The PKI message constructed from the response from the online CA. */
	private PKIMessage pkiMessage;
	
	/**
	 * Constructor.
	 * @param caResponse the http response obtained from the online CA
	 * @throws CAClientException in case of errors
	 */
	public CMPResponse(HttpResponse caResponse) throws CAClientException {
		if (caResponse == null) {
			throw new CAClientException("The response was empty!");
		}
		try {
			InputStream input = caResponse.getEntity().getContent();
			ASN1InputStream asn1input = new ASN1InputStream(input);
			this.pkiMessage = PKIMessage.getInstance(asn1input.readObject());
			asn1input.close();
		} catch (IOException e) {
           log.error("Error while creating PKIMessage object!", e);
		}
	}
	
	/** {@inheritDoc} */
	public X509Certificate getCertificate() {
        CertRepMessage certRepMessage = this.pkiMessage.getBody().getCp();
        if (certRepMessage == null) {
            log.error("No certificates found from the response!");
            return null;        	
        }
        // Get the first message in the chain
        CertResponse certResp = certRepMessage.getResponse(0);
        if (certResp == null || 
        	certResp.getCertifiedKeyPair() == null ||
        	certResp.getCertifiedKeyPair().getCertOrEncCert() == null ||
        	certResp.getCertifiedKeyPair().getCertOrEncCert().getCertificate() == null) {
            log.error("No certificates found from the response!");
            return null;
        }
        X509CertificateStructure certSt = certResp.getCertifiedKeyPair().getCertOrEncCert().getCertificate();
        X509CertificateObject certObject = null;
        try {
            // generate the certificate object
            certObject = new X509CertificateObject(certSt);
        } catch (Exception e) {
            log.error("Error while creating certObject: " + e);
            return null;
        }
        X509Certificate cert = (X509Certificate)certObject;
        log.debug("Returning a certificate: {}", cert.toString());
        return cert;
	}
}
