package org.glite.sts.x509.caclient.xmlobject.impl;

import org.glite.sts.x509.caclient.xmlobject.Success;
import org.opensaml.core.xml.schema.impl.XSStringImpl;

public class SuccessImpl extends XSStringImpl implements Success {

	protected SuccessImpl(String namespaceURI,
			String elementLocalName, String namespacePrefix) {
		super(namespaceURI, elementLocalName, namespacePrefix);
	}
}
