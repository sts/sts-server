package org.glite.sts.x509.caclient.xmlobject.impl;

import org.glite.sts.x509.caclient.xmlobject.Base64CertificateRequest;
import org.glite.sts.x509.caclient.xmlobject.NewStsCertificate;
import org.opensaml.core.xml.XMLObject;
import org.opensaml.core.xml.io.AbstractXMLObjectUnmarshaller;
import org.opensaml.core.xml.io.UnmarshallingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NewStsCertificateUnmarshaller extends AbstractXMLObjectUnmarshaller {

    /** Logger. */
    private final Logger log = LoggerFactory.getLogger(NewStsCertificateUnmarshaller.class);
    
    /** {@inheritDoc} */
    protected void processChildElement(XMLObject parentXMLObject, XMLObject childXMLObject)
            throws UnmarshallingException {
    	log.debug("Processing child element for NewStsCertificate");
    	NewStsCertificate stsCertificate = (NewStsCertificate) parentXMLObject;
    	if (childXMLObject instanceof Base64CertificateRequest) {
    		log.debug("Setting Base64CertificateRequest");
    		stsCertificate.setBase64CertificateRequest((Base64CertificateRequest) childXMLObject);
    	} else {
    		super.processChildElement(parentXMLObject, childXMLObject);
    	}
    }
}
