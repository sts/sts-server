package org.glite.sts.x509.caclient.xmlobject;

import javax.xml.namespace.QName;

import org.opensaml.core.xml.AttributeExtensibleXMLObject;
import org.opensaml.core.xml.ElementExtensibleXMLObject;

public interface NewStsCertificateResult extends ElementExtensibleXMLObject, AttributeExtensibleXMLObject {
	
    /** Default element local name. */
    public static final String DEFAULT_ELEMENT_LOCAL_NAME = "NewStsCertificateResult";
    
    /** Default element name. */
    public static final QName DEFAULT_ELEMENT_NAME = new QName(StsCertificateConstants.CERN_CA_STS_NS, DEFAULT_ELEMENT_LOCAL_NAME, StsCertificateConstants.CERN_CA_STS_PREFIX);
    
    public Success getSuccess();
    
    public void setSuccess(Success newSuccess);
    
    public Message getMessage();
    
    public void setMessage(Message newMessage);
    
    public Certificate getCertificate();
    
    public void setCertificate(Certificate newCertificate);
    
    public ActivityID getActivityID();
    
    public void setActivityID(ActivityID newActivityID);
    
    public RequestID getRequestID();
    
    public void setRequestID(RequestID newRequestID);

}