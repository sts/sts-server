package org.glite.sts.x509.caclient.xmlobject.impl;

import net.shibboleth.utilities.java.support.xml.ElementSupport;

import org.glite.sts.x509.caclient.xmlobject.Message;
import org.opensaml.core.xml.XMLObject;
import org.opensaml.core.xml.io.AbstractXMLObjectMarshaller;
import org.opensaml.core.xml.io.MarshallingException;
import org.w3c.dom.Element;

public class MessageMarshaller extends AbstractXMLObjectMarshaller {

    /** {@inheritDoc} */
    protected void marshallAttributes(XMLObject xmlObject, Element domElement) throws MarshallingException {
        // no attributes
    }

    /** {@inheritDoc} */
    protected void marshallElementContent(XMLObject xmlObject, Element domElement) throws MarshallingException {
        Message message = (Message) xmlObject;
        ElementSupport.appendTextContent(domElement, message.getValue());
    }
}