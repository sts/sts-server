/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.x509.caclient.impl;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.util.EntityUtils;
import org.glite.sts.ta.x509.X509TokenGenerationContext;
import org.glite.sts.x509.caclient.CAClientException;
import org.glite.sts.x509.caclient.CAConnection;
import org.glite.sts.x509.caclient.CARequest;
import org.glite.sts.x509.caclient.CAResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The CMP-implementation for {@link CAConnection}.
 */
public class CMPConnection implements CAConnection {
	
    /** Logging. */
    private static Logger log = LoggerFactory.getLogger(CMPConnection.class);
    
    /** the CMPClient associated with this connection. */
    private CMPClient cmpClient;
    
    /**
     * Constructor.
     * @param client the CMP client to this connection
     */
    protected CMPConnection(CMPClient client) {
        this.cmpClient= client;
    }

    /** {@inheritDoc} */
    public void createRequest(X509TokenGenerationContext context)
			throws CAClientException {
		CMPRequest cmpRequest = new CMPRequest(context, cmpClient.getCmpProperties());
		context.setCaRequest(cmpRequest);		
	}

    /** {@inheritDoc} */
	public synchronized void submitRequest(X509TokenGenerationContext context)
			throws CAClientException {
		CARequest caRequest = context.getCaRequest();
		if (caRequest == null) {
			throw new CAClientException("No CARequest object found from the context!");
		}
		byte[] requestBytes = caRequest.getDEREncoded();

		ByteArrayEntity entity = new ByteArrayEntity(requestBytes);
        HttpPost cmpServerPost = new HttpPost(this.cmpClient.getServerUrl());
        cmpServerPost.setEntity(entity);
        HttpClient httpClient = this.cmpClient.getHttpClient();
        HttpResponse postResponse = null;
        try {
            log.debug("Sending the request to the online CA...");
            postResponse = httpClient.execute(cmpServerPost);
            log.debug("The response from the online CA: {}", postResponse);
        } catch (IOException e) {
            log.error("IO error while sending the request to the online CA.", e);
            throw new CAClientException(e);
        }
        CAResponse caResponse = new CMPResponse(postResponse);
        try {
        	EntityUtils.consume(postResponse.getEntity());
        } catch (IOException e) {
        	log.error("Could not consume the response from the online CA", e);
        	throw new CAClientException(e);
        }
        context.setCaResponse(caResponse);
	}
}
