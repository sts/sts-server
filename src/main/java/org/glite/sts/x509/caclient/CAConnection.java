/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.x509.caclient;

import org.glite.sts.ta.x509.X509TokenGenerationContext;

public interface CAConnection {
	
    public void createRequest(X509TokenGenerationContext context) throws CAClientException;

    public void submitRequest(X509TokenGenerationContext context) throws CAClientException;

}
