package org.glite.sts.x509.caclient.xmlobject;

import javax.xml.namespace.QName;

import org.opensaml.core.xml.schema.XSString;

public interface Base64CertificateRequest extends XSString {
	
    /** Default element local name. */
    public static final String DEFAULT_ELEMENT_LOCAL_NAME = "base64CertificateRequest";
    
    /** Default element name. */
    public static final QName DEFAULT_ELEMENT_NAME = new QName(StsCertificateConstants.CERN_CA_STS_NS, DEFAULT_ELEMENT_LOCAL_NAME, StsCertificateConstants.CERN_CA_STS_PREFIX);
}
