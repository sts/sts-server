package org.glite.sts.x509.caclient.xmlobject.impl;

import org.glite.sts.x509.caclient.xmlobject.NewStsCertificateResult;
import org.opensaml.core.xml.AbstractXMLObjectBuilder;

public class NewStsCertificateResultBuilder extends AbstractXMLObjectBuilder<NewStsCertificateResult> {

	@Override
	public NewStsCertificateResult buildObject(String namespaceURI, String localName, String namespacePrefix) {
		return new NewStsCertificateResultImpl(namespaceURI, localName, namespacePrefix);
	}
}
