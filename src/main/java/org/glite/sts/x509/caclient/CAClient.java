/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.x509.caclient;

/**
 * The main interface to be implemented by the online CA clients.
 */
public interface CAClient {

    /**
     * Returns the @link CAConnection instance.
     *
     * @return the connection object
     * @throws CAClientException if the client cannot be initialized
     */
    public CAConnection getConnection() throws CAClientException;

}
