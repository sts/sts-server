package org.glite.sts.x509.caclient.impl;

import java.io.IOException;
import java.io.InputStream;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.List;

import net.shibboleth.utilities.java.support.xml.ParserPool;
import net.shibboleth.utilities.java.support.xml.XMLParserException;

import org.apache.http.HttpResponse;
import org.glite.sts.x509.caclient.CAClientException;
import org.glite.sts.x509.caclient.CAResponse;
import org.glite.sts.x509.caclient.xmlobject.NewStsCertificateResponse;
import org.opensaml.core.xml.XMLObject;
import org.opensaml.core.xml.io.Unmarshaller;
import org.opensaml.core.xml.io.UnmarshallerFactory;
import org.opensaml.core.xml.io.UnmarshallingException;
import org.opensaml.security.x509.X509Support;
import org.opensaml.soap.soap11.Envelope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * CERN IOTA CA implementation for {@link CAResponse}.
 */
public class CernCAResponse implements CAResponse {
	
	/** Logging. */
	private static Logger log = LoggerFactory.getLogger(CernCAResponse.class);

	/** The response object. */
	private NewStsCertificateResponse stsResponse;
	
	/**
	 * Constructor.
	 * @param caResponse The response from IOTA CA.
	 * @param parserPool The opensaml parser pool.
	 * @param unmarshallerFactory The opensaml unmarshaller factory.
	 * @throws CAClientException If response parsing fails.
	 */
	public CernCAResponse(HttpResponse caResponse, final ParserPool parserPool, final UnmarshallerFactory unmarshallerFactory) throws CAClientException {
		if (caResponse == null) {
			throw new CAClientException("The response was empty!");
		}
		try {
			InputStream input = caResponse.getEntity().getContent();
			Document document = parserPool.parse(input);
			Element element = document.getDocumentElement();
			Unmarshaller unmarshaller = unmarshallerFactory.getUnmarshaller(element);
			log.debug("Using unmarshaller {}", unmarshaller);
			if (unmarshaller == null) {
				log.debug("Document is {}", document.toString());
				log.debug("Element is {} {}", element.toString(), element.getLocalName());
				throw new UnmarshallingException("Could not find unmarshaller");
			}
			Envelope envelope = (Envelope) unmarshaller.unmarshall(element);
			List<XMLObject> children = envelope.getBody().getUnknownXMLObjects(NewStsCertificateResponse.DEFAULT_ELEMENT_NAME);
			if (children == null || children.size() != 1) {
				throw new CAClientException("No certificate available in the response");
			}
			log.debug("Single NewStsCertificateResponse element found from the response");
			this.stsResponse = (NewStsCertificateResponse)children.get(0);
		} catch (IOException e) {
           log.error("Error while creating NewStsCertificateResponse object!", e);
		} catch (XMLParserException e) {
			log.error("Could not parse XML object from the response!", e);
		} catch (UnmarshallingException e) {
			log.error("Could not unmarshall NewStsCertificateResponse object!", e);
		}
	}
	
	@Override
	/** {@inheritDoc} */
	public X509Certificate getCertificate() {
		String base64Cert = this.stsResponse.getNewStsCertificateResult().getCertificate().getValue();
		base64Cert = base64Cert.replaceAll("-----BEGIN CERTIFICATE-----", "");
		base64Cert = base64Cert.replaceAll("-----END CERTIFICATE-----", "");
		X509Certificate certificate = null;
		try {
			certificate = X509Support.decodeCertificate(base64Cert);
		} catch (CertificateException e) {
			log.error("Could not parse certificate from the response", e);
		}
		if (certificate == null) {
			log.error("Could not parse certificate from the response, still null");
		} else {
			log.debug("Certificate successfully parsed from the response");
		}
		return certificate;
	}
}
