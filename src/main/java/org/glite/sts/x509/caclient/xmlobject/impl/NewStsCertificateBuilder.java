package org.glite.sts.x509.caclient.xmlobject.impl;

import org.glite.sts.x509.caclient.xmlobject.NewStsCertificate;
import org.opensaml.core.xml.AbstractXMLObjectBuilder;

public class NewStsCertificateBuilder extends AbstractXMLObjectBuilder<NewStsCertificate> {

    /** {@inheritDoc} */
    public NewStsCertificate buildObject(String namespaceURI, String localName, String namespacePrefix) {
        return new NewStsCertificateImpl(namespaceURI, localName, namespacePrefix);
    }

}
