package org.glite.sts.x509.caclient.xmlobject.impl;

import org.glite.sts.x509.caclient.xmlobject.Certificate;
import org.opensaml.core.xml.schema.impl.XSStringImpl;

public class CertificateImpl extends XSStringImpl implements Certificate {

	protected CertificateImpl(String namespaceURI,
			String elementLocalName, String namespacePrefix) {
		super(namespaceURI, elementLocalName, namespacePrefix);
	}
}
