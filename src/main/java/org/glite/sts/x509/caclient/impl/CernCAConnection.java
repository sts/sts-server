package org.glite.sts.x509.caclient.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;
import org.apache.xml.security.utils.Base64;
import org.bouncycastle.operator.OperatorCreationException;
import org.glite.sts.ta.x509.X509TokenGenerationContext;
import org.glite.sts.x509.caclient.CAClientException;
import org.glite.sts.x509.caclient.CAConnection;
import org.glite.sts.x509.caclient.CAResponse;
import org.glite.sts.x509.caclient.xmlobject.Base64CertificateRequest;
import org.glite.sts.x509.caclient.xmlobject.NewStsCertificate;
import org.opensaml.core.xml.XMLObjectBuilder;
import org.opensaml.core.xml.config.XMLObjectProviderRegistrySupport;
import org.opensaml.core.xml.io.Marshaller;
import org.opensaml.core.xml.io.MarshallingException;
import org.opensaml.soap.common.SOAPObjectBuilder;
import org.opensaml.soap.soap11.Body;
import org.opensaml.soap.soap11.Envelope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSOutput;
import org.w3c.dom.ls.LSSerializer;

/**
 * The CERN IOTA CA -implementation for {@link CAConnection}.
 */
public class CernCAConnection implements CAConnection {
	
    /** Logging. */
    private static Logger log = LoggerFactory.getLogger(CernCAConnection.class);
    
    /** the CAClient associated with this connection. */
    private final CernCAClient caClient;
    
    /**
     * Constructor.
     * @param caClient The CA client.
     */
    public CernCAConnection(final CernCAClient caClient) {
    	this.caClient = caClient;
    }

	@Override
	/** {@inheritDoc} */
	public void createRequest(X509TokenGenerationContext context)
			throws CAClientException {
		try {
			CernCARequest caRequest = new CernCARequest(context);
			context.setCaRequest(caRequest);
		} catch (OperatorCreationException e) {
			log.error("Could not generate a request for CERN CA", e);
			throw new CAClientException(e);
		}
	}

	@Override
	/** {@inheritDoc} */
	public void submitRequest(X509TokenGenerationContext context)
			throws CAClientException {
		final Envelope envelope = buildEnvelope(context);
		HttpPost post = new HttpPost(caClient.getServerUrl());
		
		HttpEntity entity = new ByteArrayEntity(envelopeToStream(envelope).toByteArray());
		post.setEntity(entity);
		post.setHeader(new BasicHeader("Content-Type", "text/xml; charset=utf-8"));
		// Send request
		HttpResponse response;
		try {
			response = caClient.getHttpClient().execute(post);
		} catch (ClientProtocolException e) {
			log.error("ClientProtocolException", e);
			throw new CAClientException(e);
		} catch (IOException e) {
			log.error("IOException", e);
			throw new CAClientException(e);
		}
	
		/*byte[] responseBytes = null;
		HttpEntity responseentity = null;

		if (response != null) {
			responseentity = response.getEntity();
		}*/

        CAResponse caResponse = new CernCAResponse(response, caClient.getParserPool(), caClient.getUnmarshallerFactory());
        try {
        	EntityUtils.consume(response.getEntity());
        } catch (IOException e) {
        	log.error("Could not consume the response from the online CA", e);
        	throw new CAClientException(e);
        }
        context.setCaResponse(caResponse);
	}

	/**
	 * Builds a SOAP envelope from the given context.
	 * @param context The X509 token generation context.
	 * @return SOAP envelope.
	 */
	@SuppressWarnings("unchecked")
	private Envelope buildEnvelope(X509TokenGenerationContext context) {
		final SOAPObjectBuilder<Envelope> envelopeBuilder =
	            (SOAPObjectBuilder<Envelope>) XMLObjectProviderRegistrySupport.getBuilderFactory().getBuilder(Envelope.DEFAULT_ELEMENT_NAME);
		final Envelope envelope = envelopeBuilder.buildObject();
        final SOAPObjectBuilder<Body> builder =
            (SOAPObjectBuilder<Body>) XMLObjectProviderRegistrySupport.getBuilderFactory().
                getBuilder(Body.DEFAULT_ELEMENT_NAME);
        final Body body = builder.buildObject();
        envelope.setBody(body);
        final XMLObjectBuilder<NewStsCertificate> stsCertBuilder = 
        		(XMLObjectBuilder<NewStsCertificate>) XMLObjectProviderRegistrySupport.getBuilderFactory().getBuilder(NewStsCertificate.DEFAULT_ELEMENT_NAME);
        final NewStsCertificate stsCert = stsCertBuilder.buildObject(NewStsCertificate.DEFAULT_ELEMENT_NAME);
        final XMLObjectBuilder<Base64CertificateRequest> base64Builder =
        		(XMLObjectBuilder<Base64CertificateRequest>) XMLObjectProviderRegistrySupport.getBuilderFactory().getBuilder(Base64CertificateRequest.DEFAULT_ELEMENT_NAME);
        final Base64CertificateRequest base64re = base64Builder.buildObject(Base64CertificateRequest.DEFAULT_ELEMENT_NAME);
        base64re.setValue(Base64.encode(context.getCaRequest().getDEREncoded()));
        log.debug("Base64 request value {}", base64re.getValue());
        stsCert.setBase64CertificateRequest(base64re);
        body.getUnknownXMLObjects().add(stsCert);
        return envelope;
	}
	
	/**
	 * Converts a SOAP envelope into a stream.
	 * @param envelope SOAP envelope.
	 * @return Output stream.
	 */
	public static ByteArrayOutputStream envelopeToStream(Envelope envelope) {

		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		
		Marshaller marshaller = XMLObjectProviderRegistrySupport
				.getMarshallerFactory().getMarshallers()
				.get(Envelope.DEFAULT_ELEMENT_NAME);
		Element element = null;
		try {
			element = marshaller.marshall(envelope);
		} catch (MarshallingException e) {
			log.error("Could not marshall the envelope!", e);
		}		
		
		Document document = element.getOwnerDocument();
        DOMImplementationLS domImplLS = (DOMImplementationLS) document.getImplementation();
        LSSerializer serializer = domImplLS.createLSSerializer();
        LSOutput lsOutput = domImplLS.createLSOutput();
        
        lsOutput.setByteStream(stream);
        serializer.write(element, lsOutput);
		
		return stream;
	}
	
	/**
	 * Converts an input stream into a byte array.
	 * @param stream The input stream.
	 * @return The byte array.
	 */
	public static byte[] turnStreamToByteArray(InputStream stream) {
		int line;
		ByteArrayOutputStream byteArrayStream = new ByteArrayOutputStream();
		try {
			while ((line = stream.read()) != -1) {
				byteArrayStream.write(line);
			}
			byteArrayStream.flush();
		} catch (IOException e) {
			log.error("IOException", e);
		}

		byte[] array = byteArrayStream.toByteArray();

		// Close stream
		if (byteArrayStream != null) {
			try {
				byteArrayStream.close();
			} catch (IOException e) {
				log.error("IOException", e);
			}
		}
		return array;
	}
}
