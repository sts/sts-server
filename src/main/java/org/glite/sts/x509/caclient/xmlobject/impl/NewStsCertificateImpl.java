package org.glite.sts.x509.caclient.xmlobject.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.glite.sts.x509.caclient.xmlobject.Base64CertificateRequest;
import org.glite.sts.x509.caclient.xmlobject.NewStsCertificate;
import org.opensaml.core.xml.AbstractExtensibleXMLObject;
import org.opensaml.core.xml.XMLObject;

public class NewStsCertificateImpl extends AbstractExtensibleXMLObject implements NewStsCertificate {

	private Base64CertificateRequest base64CertificateRequest;
	
	public NewStsCertificateImpl(String namespaceURI, String elementLocalName,
			String namespacePrefix) {
		super(namespaceURI, elementLocalName, namespacePrefix);
	}

	@Override
	public Base64CertificateRequest getBase64CertificateRequest() {
		return base64CertificateRequest;
	}

	@Override
	public void setBase64CertificateRequest(
			Base64CertificateRequest newBase64CertificateRequest) {
		base64CertificateRequest = prepareForAssignment(base64CertificateRequest, newBase64CertificateRequest);
	}
	
    /** {@inheritDoc} */
    public List<XMLObject> getOrderedChildren() {
        ArrayList<XMLObject> children = new ArrayList<XMLObject>();
        if (base64CertificateRequest != null) {
        	children.add(base64CertificateRequest);
        }
        return Collections.unmodifiableList(children);
    }
}