package org.glite.sts.x509.caclient.xmlobject.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.glite.sts.x509.caclient.xmlobject.ActivityID;
import org.glite.sts.x509.caclient.xmlobject.Certificate;
import org.glite.sts.x509.caclient.xmlobject.Message;
import org.glite.sts.x509.caclient.xmlobject.NewStsCertificateResult;
import org.glite.sts.x509.caclient.xmlobject.RequestID;
import org.glite.sts.x509.caclient.xmlobject.Success;
import org.opensaml.core.xml.AbstractExtensibleXMLObject;
import org.opensaml.core.xml.XMLObject;

public class NewStsCertificateResultImpl extends AbstractExtensibleXMLObject implements NewStsCertificateResult {

	private Success success;
	private Message message;
	private Certificate certificate;
	private ActivityID activityID;
	private RequestID requestID;
	
	public NewStsCertificateResultImpl(String namespaceURI, String elementLocalName,
			String namespacePrefix) {
		super(namespaceURI, elementLocalName, namespacePrefix);
	}

    /** {@inheritDoc} */
    public List<XMLObject> getOrderedChildren() {
        ArrayList<XMLObject> children = new ArrayList<XMLObject>();
        if (success != null) {
        	children.add(success);
        }
        if (message != null) {
        	children.add(message);
        }
        if (certificate != null) {
        	children.add(certificate);
        }
        if (activityID != null) {
        	children.add(activityID);
        }
        if (requestID != null) {
        	children.add(requestID);
        }
        return Collections.unmodifiableList(children);
    }

	@Override
	public Success getSuccess() {
		return success;
	}

	@Override
	public void setSuccess(Success newSuccess) {
		success = prepareForAssignment(success, newSuccess);
	}

	@Override
	public Message getMessage() {
		return message;
	}

	@Override
	public void setMessage(Message newMessage) {
		message = prepareForAssignment(message, newMessage);
	}

	@Override
	public Certificate getCertificate() {
		return certificate;
	}

	@Override
	public void setCertificate(Certificate newCertificate) {
		certificate = prepareForAssignment(certificate, newCertificate);
	}

	@Override
	public ActivityID getActivityID() {
		return activityID;
	}

	@Override
	public void setActivityID(ActivityID newActivityID) {
		activityID = prepareForAssignment(activityID, newActivityID);
	}

	@Override
	public RequestID getRequestID() {
		return requestID;
	}

	@Override
	public void setRequestID(RequestID newRequestID) {
		requestID = prepareForAssignment(requestID, newRequestID);
	}
}
