package org.glite.sts.x509.caclient.xmlobject.impl;

import org.glite.sts.x509.caclient.xmlobject.NewStsCertificateResponse;
import org.opensaml.core.xml.AbstractXMLObjectBuilder;

public class NewStsCertificateResponseBuilder extends AbstractXMLObjectBuilder<NewStsCertificateResponse> {

	@Override
	public NewStsCertificateResponse buildObject(String namespaceURI, String localName, String namespacePrefix) {
		return new NewStsCertificateResponseImpl(namespaceURI, localName, namespacePrefix);
	}
}
