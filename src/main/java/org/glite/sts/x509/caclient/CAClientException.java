/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.x509.caclient;

public class CAClientException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8762069936703216220L;

	/**
	 * 
	 */
	public CAClientException() {
		super();
	}

	/**
	 * @param arg0
	 */
	public CAClientException(String arg0) {
		super(arg0);
	}

	/**
	 * @param arg0
	 * @param arg1
	 */
	public CAClientException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	/**
	 * @param arg0
	 */
	public CAClientException(Throwable arg0) {
		super(arg0);
	}

}
