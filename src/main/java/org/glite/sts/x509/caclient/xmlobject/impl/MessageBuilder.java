package org.glite.sts.x509.caclient.xmlobject.impl;

import org.glite.sts.x509.caclient.xmlobject.Message;
import org.opensaml.core.xml.AbstractXMLObjectBuilder;
import org.opensaml.core.xml.schema.XSString;

public class MessageBuilder extends AbstractXMLObjectBuilder<XSString> {

	@Override
	public Message buildObject(String namespaceURI, String localName, String namespacePrefix) {
		return new MessageImpl(namespaceURI, localName, namespacePrefix);
	}
}
