package org.glite.sts.x509.caclient.xmlobject.impl;

import org.glite.sts.x509.caclient.xmlobject.Success;
import org.opensaml.core.xml.AbstractXMLObjectBuilder;
import org.opensaml.core.xml.schema.XSString;

public class SuccessBuilder extends AbstractXMLObjectBuilder<XSString> {

	@Override
	public Success buildObject(String namespaceURI, String localName, String namespacePrefix) {
		return new SuccessImpl(namespaceURI, localName, namespacePrefix);
	}
}
