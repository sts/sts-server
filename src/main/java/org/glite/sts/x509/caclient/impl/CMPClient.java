/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.x509.caclient.impl;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Properties;

import net.shibboleth.utilities.java.support.logic.Constraint;

import org.apache.http.client.HttpClient;
import org.glite.sts.STSException;
import org.glite.sts.config.ServerConfiguration;
import org.glite.sts.util.HttpClientBuilder;
import org.glite.sts.x509.caclient.CAClient;
import org.glite.sts.x509.caclient.CAClientException;
import org.glite.sts.x509.caclient.CAConnection;
import org.ini4j.Ini;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.novosec.pkix.asn1.cmp.CMPObjectIdentifiers;

import eu.emi.security.authn.x509.X509CertChainValidator;
import eu.emi.security.authn.x509.X509Credential;

/**
 * The CMP-implementation for {@link CAClient}.
 */
public class CMPClient implements CAClient {

	public static final String SERVER_URL_IDENTIFIER = "serverUrl";
	public static final String CA_DN_IDENTIFIER = "caDn";
    public static final String SENDER_DN_IDENTIFIER = "senderDn";
    public static final String RECIPIENT_DN_IDENTIFIER = "recipientDn";
    public static final String SENDER_KID_IDENTIFIER = "senderKid";
    public static final String SHARED_SECRET_IDENTIFIER = "sharedSecret";
    public static final String OWF_ALG_ID_IDENTIFIER = "owfAlgId";
    public static final String ITERATION_COUNT_IDENTIFIER = "iterCount";
    public static final String MAC_ALG_ID_IDENTIFIER = "macAlgId";
    public static final String SALT_STRING_IDENTIFIER = "saltString";
    public static final String PROTECTION_ALG_ID_IDENTIFIER = "protectionAlgId";
    
    public static final String DEFAULT_OWF_ALGID = "1.3.14.3.2.26";
    public static final String DEFAULT_ITERATION_COUNT = "1";
    public static final String DEFAULT_MAC_ALGID = "1.3.6.1.5.5.8.1.2";
    public static final String DEFAULT_PROTECTION_ALGID = CMPObjectIdentifiers.passwordBasedMac.toString();

    /** Logging. */
    private static Logger log = LoggerFactory.getLogger(CMPClient.class);
    
    /** The HTTP client used for the connection. */
    private HttpClient httpClient;
    
    /** The CMP configuration properties. */
    private Properties cmpProperties;
    
    /** The endpoint corresponding to the online CA's CMP service. */
    private String serverUrl;
   
    /**
     * Constructor.
     * @param caURL The endpoint corresponding to the online CA's CMP service
     * @param cmpProperties The configuration properties for the CMP protocol
     * @throws CAClientException If the client could not be constructed
     */
    public CMPClient(ServerConfiguration serverConfiguration) throws STSException, CAClientException {
    	this(null, null, serverConfiguration);
    }
    
    /**
     * Constructor.
     * @param caURL The endpoint corresponding to the online CA's CMP service
     * @param chainValidator The validator used for authenticating the service
     * @param cmpProperties The configuration properties for the CMP protocol
     * @throws CAClientException If the client could not be constructed
     */
    public CMPClient(X509CertChainValidator chainValidator, ServerConfiguration serverConfiguration) throws STSException, CAClientException {
    	this(null, chainValidator, serverConfiguration);
    }

    /**
     * Constructor.
     * @param caURL The endpoint corresponding to the online CA's CMP service
     * @param credential The credentials used for the client-authentication
     * @param chainValidator The validator used for authenticating the service
     * @param cmpProperties The configuration properties for the CMP protocol
     * @throws CAClientException If the client could not be constructed
     */
    public CMPClient(X509Credential credential, X509CertChainValidator chainValidator, ServerConfiguration serverConfiguration) throws STSException, CAClientException {
    	Ini.Section iniSection = serverConfiguration.getConfigurationSection(ServerConfiguration.CFG_SECTION_CACLIENT);
    	this.serverUrl = Constraint.isNotNull(iniSection.get(SERVER_URL_IDENTIFIER), "The serverUrl cannot be null!");
    	this.setCmpProperties(iniSection);
    	this.httpClient = createHttpClient(credential, chainValidator);
    }
    
    /**
     * Sets the CMP configuration properties.
     * @param cmpProps the CMP configuration properties
     * @throws CAClientException if the properties could not be set
     */
    public void setCmpProperties(Ini.Section iniSection) throws CAClientException {
        this.cmpProperties = new Properties();
        this.generateCmpProperties(iniSection);
    }

    /** {@inheritDoc} */
    public CAConnection getConnection() throws CAClientException {
        return new CMPConnection(this);
    }

    private void generateCmpProperties(Ini.Section iniSection) throws CAClientException {
        addCmpConfigurationVariable(iniSection, CA_DN_IDENTIFIER, null);
        addCmpConfigurationVariable(iniSection, SENDER_DN_IDENTIFIER, null);
        addCmpConfigurationVariable(iniSection, RECIPIENT_DN_IDENTIFIER, null);
        addCmpConfigurationVariable(iniSection, SENDER_KID_IDENTIFIER, null);
        addCmpConfigurationVariable(iniSection, SHARED_SECRET_IDENTIFIER, null);
        addCmpConfigurationVariable(iniSection, OWF_ALG_ID_IDENTIFIER, DEFAULT_OWF_ALGID);
        addCmpConfigurationVariable(iniSection, ITERATION_COUNT_IDENTIFIER, DEFAULT_ITERATION_COUNT);
        addCmpConfigurationVariable(iniSection, MAC_ALG_ID_IDENTIFIER, DEFAULT_MAC_ALGID);
        addCmpConfigurationVariable(iniSection, SALT_STRING_IDENTIFIER, "");
        addCmpConfigurationVariable(iniSection, PROTECTION_ALG_ID_IDENTIFIER, DEFAULT_PROTECTION_ALGID);
    }
        
    private void addCmpConfigurationVariable(Ini.Section iniSection, String variable, String defaultValue) throws CAClientException {
        String str = iniSection.get(variable);
        if (str == null || str.equals("")) {
            if (defaultValue == null) {
                throw new CAClientException(variable + " is a required variable!");
            } else {
                log.info("CAClient." + variable + "='" + defaultValue + "' (was null, using default)");
                this.cmpProperties.setProperty(variable, defaultValue);
            }
        } else {
            this.cmpProperties.setProperty(variable, str);
            log.info(variable + "=" + str);
        }
    }

    private HttpClient createHttpClient(X509Credential credential, X509CertChainValidator chainValidator) throws CAClientException {
    	HttpClientBuilder httpBuilder = new HttpClientBuilder(this.serverUrl, credential, chainValidator);
    	HttpClient httpClient = null;
		try {
			httpClient = httpBuilder.buildClient();
		} catch (IOException e) {
			log.error("Could not build an HTTP client", e);
			throw new CAClientException(e);
		} catch (GeneralSecurityException e) {
			log.error("Security error while building an HTTP client", e);
			throw new CAClientException(e);
		}
    	return httpClient;    	
    }
    
    /** {@inheritDoc} */
    public void shutdown() {
        this.httpClient.getConnectionManager().shutdown();
        this.httpClient = null;
    }

    /**
     * Gets the HTTP client.
     * @return the underlying HttpClient
     */
    protected HttpClient getHttpClient() {
        return this.httpClient;
    }
    
    /**
     * Gets the CMP configuration properties.
     * @return the CMP configuration variables
     */
    protected Properties getCmpProperties() {
        return this.cmpProperties;
    }
    
    /**
     * Gets the online CA's CMP service URL.
     * @return the online CA's CMP service url
     */
    protected String getServerUrl() {
        return this.serverUrl;
    }

}
