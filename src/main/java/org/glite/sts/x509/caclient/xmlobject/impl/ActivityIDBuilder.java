package org.glite.sts.x509.caclient.xmlobject.impl;

import org.glite.sts.x509.caclient.xmlobject.ActivityID;
import org.opensaml.core.xml.AbstractXMLObjectBuilder;
import org.opensaml.core.xml.schema.XSString;

public class ActivityIDBuilder extends AbstractXMLObjectBuilder<XSString> {

	@Override
	public ActivityID buildObject(String namespaceURI, String localName, String namespacePrefix) {
		return new ActivityIDImpl(namespaceURI, localName, namespacePrefix);
	}
}
