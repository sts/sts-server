package org.glite.sts.x509.caclient.xmlobject.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.glite.sts.x509.caclient.xmlobject.NewStsCertificateResponse;
import org.glite.sts.x509.caclient.xmlobject.NewStsCertificateResult;
import org.opensaml.core.xml.AbstractExtensibleXMLObject;
import org.opensaml.core.xml.XMLObject;

public class NewStsCertificateResponseImpl extends AbstractExtensibleXMLObject implements NewStsCertificateResponse {

	private NewStsCertificateResult newStsCertificateResult;
	
	public NewStsCertificateResponseImpl(String namespaceURI, String elementLocalName,
			String namespacePrefix) {
		super(namespaceURI, elementLocalName, namespacePrefix);
	}

    /** {@inheritDoc} */
    public List<XMLObject> getOrderedChildren() {
        ArrayList<XMLObject> children = new ArrayList<XMLObject>();
        if (newStsCertificateResult != null) {
        	children.add(newStsCertificateResult);
        }
        return Collections.unmodifiableList(children);
    }

	@Override
	public NewStsCertificateResult getNewStsCertificateResult() {
		return newStsCertificateResult;
	}

	@Override
	public void setNewStsCertificateResult(
			NewStsCertificateResult newNewStsCertificateResult) {
		newStsCertificateResult = prepareForAssignment(newStsCertificateResult, newNewStsCertificateResult);
	}
}
