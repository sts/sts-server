package org.glite.sts.x509.caclient.xmlobject.impl;

import org.glite.sts.x509.caclient.xmlobject.RequestID;
import org.opensaml.core.xml.AbstractXMLObjectBuilder;
import org.opensaml.core.xml.schema.XSString;

public class RequestIDBuilder extends AbstractXMLObjectBuilder<XSString> {

	@Override
	public RequestID buildObject(String namespaceURI, String localName, String namespacePrefix) {
		return new RequestIDImpl(namespaceURI, localName, namespacePrefix);
	}
}
