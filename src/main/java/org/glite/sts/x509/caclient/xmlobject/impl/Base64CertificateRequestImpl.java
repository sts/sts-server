package org.glite.sts.x509.caclient.xmlobject.impl;

import org.glite.sts.x509.caclient.xmlobject.Base64CertificateRequest;
import org.opensaml.core.xml.schema.impl.XSStringImpl;

public class Base64CertificateRequestImpl extends XSStringImpl implements Base64CertificateRequest {

	protected Base64CertificateRequestImpl(String namespaceURI,
			String elementLocalName, String namespacePrefix) {
		super(namespaceURI, elementLocalName, namespacePrefix);
	}

}
