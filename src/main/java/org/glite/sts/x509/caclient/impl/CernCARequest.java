package org.glite.sts.x509.caclient.impl;

import java.util.Iterator;

import javax.security.auth.x500.X500Principal;

import org.bouncycastle.asn1.pkcs.CertificationRequest;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.bouncycastle.pkcs.PKCS10CertificationRequestBuilder;
import org.bouncycastle.pkcs.jcajce.JcaPKCS10CertificationRequestBuilder;
import org.glite.sts.ta.x509.X509TokenGenerationContext;
import org.glite.sts.x509.CertificateExtension;
import org.glite.sts.x509.CertificateKeys;
import org.glite.sts.x509.caclient.CARequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * CERN IOTA CA implementation for {@link CARequest}.
 */
public class CernCARequest implements CARequest {
	
	/** Logging. */
	private static Logger log = LoggerFactory.getLogger(CernCARequest.class);

	/** Certificate keys corresponding to the upcoming certificate. */
	private CertificateKeys certKeys;

	/** Subject DN of the upcoming certificate. */
	private String subjectDN;
	
	/** The certificate signing request. */
	private CertificationRequest pkcs10;

	/**
	 * Constructor.
	 * @param context The token generation context.
	 * @throws OperatorCreationException If PKCS#10 cannot be built.
	 */
	public CernCARequest(X509TokenGenerationContext context) throws OperatorCreationException {
		Iterator<CertificateExtension> extensions = context.getExtensions().iterator();
		while (extensions.hasNext()) {
			this.addCertificateExtension(extensions.next());
		}
		this.setSubjectDN(context.getSubjectDN());
		this.setCertificateKeys(context.getCertKeys());
		log.debug("Trying to generate PKCS 10 object");
		PKCS10CertificationRequestBuilder p10Builder = new JcaPKCS10CertificationRequestBuilder(
			    new X500Principal(this.subjectDN), this.certKeys.getPublicKey());
		JcaContentSignerBuilder csBuilder = new JcaContentSignerBuilder("SHA512withRSA");
		ContentSigner signer = csBuilder.build(this.certKeys.getPrivateKey());
		pkcs10 = p10Builder.build(signer).toASN1Structure();
		log.debug("PKCS 10 successfully done");
	}
	
	@Override
	/** {@inheritDoc} */
	public byte[] getDEREncoded() {
		return pkcs10.getDEREncoded();
	}
	
	/**
	 * Set the subject DN of the PKCS#10.
	 * @param subjectDn the subject DN.
	 */
	private void setSubjectDN(String subjectDn) {
		this.subjectDN = subjectDn;
	}
	
	/**
	 * Set the certificate keys of the PKCS#10.
	 * @param certificateKeys The keys.
	 */
	private void setCertificateKeys(CertificateKeys certificateKeys) {
		this.certKeys = certificateKeys;
	}
	
	/** {@inheritDoc} */
	public void addCertificateExtension(CertificateExtension extension) {
		log.warn("No certificate extensions supported at the moment, skipping one..");
	}
}