package org.glite.sts.x509.caclient.xmlobject;

import javax.xml.namespace.QName;

import org.opensaml.core.xml.AttributeExtensibleXMLObject;
import org.opensaml.core.xml.ElementExtensibleXMLObject;

public interface NewStsCertificateResponse extends ElementExtensibleXMLObject, AttributeExtensibleXMLObject {
	
    /** Default element local name. */
    public static final String DEFAULT_ELEMENT_LOCAL_NAME = "NewStsCertificateResponse";
    
    /** Default element name. */
    public static final QName DEFAULT_ELEMENT_NAME = new QName(StsCertificateConstants.CERN_CA_STS_NS, DEFAULT_ELEMENT_LOCAL_NAME, StsCertificateConstants.CERN_CA_STS_PREFIX);
    
    public NewStsCertificateResult getNewStsCertificateResult();
    
    public void setNewStsCertificateResult(NewStsCertificateResult newNewStsCertificateResult);
    
}