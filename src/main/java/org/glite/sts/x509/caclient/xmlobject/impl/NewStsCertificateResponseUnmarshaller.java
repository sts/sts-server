package org.glite.sts.x509.caclient.xmlobject.impl;

import org.glite.sts.x509.caclient.xmlobject.NewStsCertificateResponse;
import org.glite.sts.x509.caclient.xmlobject.NewStsCertificateResult;
import org.opensaml.core.xml.XMLObject;
import org.opensaml.core.xml.io.AbstractXMLObjectUnmarshaller;
import org.opensaml.core.xml.io.UnmarshallingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NewStsCertificateResponseUnmarshaller extends AbstractXMLObjectUnmarshaller {

    /** Logger. */
    private final Logger log = LoggerFactory.getLogger(NewStsCertificateResponseUnmarshaller.class);
    
    /** {@inheritDoc} */
    protected void processChildElement(XMLObject parentXMLObject, XMLObject childXMLObject)
            throws UnmarshallingException {
    	log.debug("Processing child element for NewStsCertificate");
    	NewStsCertificateResponse stsCertificateResponse = (NewStsCertificateResponse) parentXMLObject;
    	if (childXMLObject instanceof NewStsCertificateResult) {
    		log.debug("Setting NewStsCertificateResult");
    		stsCertificateResponse.setNewStsCertificateResult((NewStsCertificateResult) childXMLObject);
    	} else {
    		super.processChildElement(parentXMLObject, childXMLObject);
    	}
    }
}
