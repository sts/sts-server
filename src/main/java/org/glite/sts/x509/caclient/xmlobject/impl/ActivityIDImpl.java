package org.glite.sts.x509.caclient.xmlobject.impl;

import org.glite.sts.x509.caclient.xmlobject.ActivityID;
import org.opensaml.core.xml.schema.impl.XSStringImpl;

public class ActivityIDImpl extends XSStringImpl implements ActivityID {

	protected ActivityIDImpl(String namespaceURI,
			String elementLocalName, String namespacePrefix) {
		super(namespaceURI, elementLocalName, namespacePrefix);
	}
}
