/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.x509.dn;

import org.glite.sts.STSException;

/**
 * Exception to be used for the errors during the subject DN building.
 */
public class DNBuildingException extends STSException {

	/** Serial version uid. */
	private static final long serialVersionUID = -4555595162805922909L;
	
	/**
	 * Constructor.
	 */
	public DNBuildingException() {
		super();
	}
	
	/**
	 * Constructor.
	 * @param message the reason
	 */
	public DNBuildingException(final String message) {
		super(message);
	}

}
