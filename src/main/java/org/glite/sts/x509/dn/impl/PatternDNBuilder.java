/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.x509.dn.impl;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import net.shibboleth.idp.attribute.Attribute;
import net.shibboleth.idp.attribute.AttributeContext;
import net.shibboleth.idp.attribute.AttributeValue;

import org.glite.sts.STSException;
import org.glite.sts.x509.dn.DNBuilder;
import org.glite.sts.x509.dn.DNBuildingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * PatternBuilder builds a DN based on a string pattern, containing
 * <code>${attribute-name}</code> variables. These variables will be replaced
 * by the corresponding attribute value from the {@link AttributeContext}.
 * 
 * This class is based on <code>org.glite.slcs.dn.impl.SimplePatternBuilder</code>.
 */
public class PatternDNBuilder implements DNBuilder {
	
    /** Logging. */
    static private Logger log = LoggerFactory.getLogger(PatternDNBuilder.class);
	
    /** The DN pattern with ${...} variable place holders. */
    private String pattern = null;

	/**
	 * Constructor.
	 * @param pattern the pattern to be used for building the DNs
	 */
	public PatternDNBuilder(String pattern) throws STSException {
		if (pattern == null) {
			log.error("Pattern cannot be null!");
			throw new STSException("Pattern cannot be null");
		}
		log.info("Initializing the PatternDNBuilder with the pattern {}", pattern);
		this.pattern = pattern;
	}
	
	/** {@inheritDoc} */
	public String createDN(AttributeContext attributeContext)
			throws DNBuildingException {
		if (attributeContext == null) {
			log.error("AttributeContext cannot be null!");
			throw new DNBuildingException("AttributeContext cannot be null!");
		}
		String dn = pattern;
		Map<String, Attribute> attributes = attributeContext.getAttributes();
		Set<String> attributeNames = attributes.keySet();
        Iterator<String> names = attributeNames.iterator();
        while (names.hasNext()) {
            String name = (String) names.next();
            String placeholder = "${" + name + "}";
            if (pattern.indexOf(placeholder) != -1) {
                @SuppressWarnings("rawtypes")
				Set<AttributeValue> value = attributes.get(name).getValues();
                if (value.size() != 1) {
                    log.error("Attribute {} is multi-valued, size: {}", name, value.size());
                    throw new DNBuildingException("Ambiguous muli-valued attribute: " + name + ", " + value.size() + " values.");
                }
            	String valueStr = (String) value.iterator().next().getValue();
                String replace = "\\$\\{" + name + "\\}";
                log.debug("replace regex {} by {}", replace, valueStr);
                dn = dn.replaceAll(replace, valueStr);
            }
        }

        if (dn.indexOf("${") != -1) {
            log.error("DN still contains not substitued placeholders: {}", dn);
            throw new DNBuildingException(
                    "Missing or wrong Shibboleth attributes: DN still contains placeholders: "
                            + dn);
        }
        log.debug("Constructed the following DN: {}", dn);
        return dn;
	}
}
