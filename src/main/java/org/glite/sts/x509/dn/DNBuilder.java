/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.x509.dn;

import net.shibboleth.idp.attribute.AttributeContext;

/**
 * DNBuilder is a distinguished name creator. It uses the {@link AttributeContext}
 * to construct a unique personal certificate DN.
 */
public interface DNBuilder {

	public String createDN(AttributeContext attributeContext) throws DNBuildingException;
	
}
