/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.standalone;

import java.io.IOException;
import java.security.KeyStoreException;
import java.security.Security;
import java.security.cert.CertificateException;
import java.util.Collections;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.glite.sts.STSException;
import org.mortbay.jetty.Connector;
import org.mortbay.jetty.Handler;
import org.mortbay.jetty.Server;
import org.mortbay.jetty.handler.DefaultHandler;
import org.mortbay.jetty.handler.HandlerCollection;
import org.mortbay.jetty.nio.SelectChannelConnector;
import org.mortbay.jetty.security.SslSelectChannelConnector;
import org.mortbay.jetty.servlet.ServletHolder;
import org.mortbay.jetty.webapp.WebAppContext;
import org.mortbay.thread.concurrent.ThreadPool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.emi.security.authn.x509.CrlCheckingMode;
import eu.emi.security.authn.x509.NamespaceCheckingMode;
import eu.emi.security.authn.x509.OCSPCheckingMode;
import eu.emi.security.authn.x509.OCSPParametes;
import eu.emi.security.authn.x509.ProxySupport;
import eu.emi.security.authn.x509.RevocationParameters;
import eu.emi.security.authn.x509.X509Credential;
import eu.emi.security.authn.x509.helpers.ssl.CredentialX509KeyManager;
import eu.emi.security.authn.x509.helpers.ssl.SSLTrustManager;
import eu.emi.security.authn.x509.impl.OpensslCertChainValidator;
import eu.emi.security.authn.x509.impl.PEMCredential;
import eu.emi.security.authn.x509.impl.ValidatorParams;

/**
 * This class can be used for running the STS server as a standalone, i.e. without Tomcat.
 * It exploits an embedded Jetty server.
 * 
 * Partially imported from org.glite.authz.pap.server.standalone.PapStandaloneService.
 */
public class StandaloneService {

    /**
     * Useful defaults for the standalone service
     */
    public final class StandaloneServiceDefaults {

    	/** The service scheme */
    	public static final String SCHEME = "https";
    	
        /** The service hostname */
    	public static final String HOSTNAME = "localhost";

        /** The service port on which the STS server will listen to requests */
    	public static final int PORT = 443;

        /** The shutdown service port */
    	public static final int SHUTDOWN_PORT = 8005;

        /** Max request queue size. -1 means unbounded queue is used. */
    	public static final int MAX_REQUEST_QUEUE_SIZE = -1;

        /** Max concurrent connections. */
    	public static final int MAX_CONNECTIONS = 64;

        /** Default certificate path for the service */
    	public static final String CERTIFICATE_PATH = "/etc/grid-security/hostcert.pem";

        /** Default private key path for the service */
    	public static final String PRIVATE_KEY_PATH = "/etc/grid-security/hostkey.pem";

        /**
         * How frequently the STS server should update CRLs, CAs and namespaces from the filesystem.
         * In milliseconds, the default is one 10 minutes (600 seconds)
         */
    	public static final long CRL_UPDATE_INTERVAL = 600000;

        /**
         * The directory containing all the CA certificates, CRLs and namespace definitions.
         */
    	public static final String TRUST_STORE_DIR = "/etc/grid-security/certificates";
    	
    	/** Require client SSL/TLS authentication. */
    	public static final boolean REQUIRE_CLIENT_AUTH = false;
    	
    	/** Want client SSL/TLS authentication. */
    	public static final boolean WANT_CLIENT_AUTH = false;
        
    }

    /** Logging. */
    private static final Logger log = LoggerFactory.getLogger(StandaloneService.class);

    /**
     * The option name used by callers to specify where the STS server should
     * look for the configuration
     */
    private static final String CONF_DIR_OPTION_NAME = "conf-dir";

    /** The configuration directory */
    protected String configurationDir;

    /** The jetty http server */
    protected Server jettyServer;

    /** The jetty webapp context in which the wep application is configured */
    private WebAppContext webappContext;

    /**
     * Constructor. Parses the configuration and starts the server.
     * @param args the command line arguments as passed by the {@link #main(String[])} method
     */
    public StandaloneService(String[] args) {
        try {
            parseOptions(args);
            Security.addProvider(new BouncyCastleProvider());
            StandaloneConfiguration.initialize(configurationDir);
            configureServer();
            ServletHolder statusServlet = new ServletHolder(new MonitoringServlet());
            webappContext.addServlet(statusServlet, "/status");
            jettyServer.start();
            if (webappContext.getUnavailableException() != null)
                throw webappContext.getUnavailableException();
            System.out.println("The STS server has been successfully started.");
            jettyServer.join();
        } catch (Throwable e) {
            log.error("The service encountered an error that could not be dealt with, shutting down!");
            log.error(e.getMessage());

            // Also print error message to standard error
            System.err.println("The service encountered an error that could not be dealt with, shutting down!");
            System.err.println("Error: " + e.getMessage());
            e.printStackTrace(System.err);

            if (log.isDebugEnabled())
                log.error(e.getMessage(), e);
            try {
                jettyServer.stop();
            } catch (Exception e1) {
                // Ignoring this exception, exiting in any case
            }
            System.exit(1);
        }
    }

    /**
     * Configures a {@link Connector} using the given host and port. If the scheme is https: privateKey, privateKeyPassword,
     * certificate, trustStoreDir, crlUpdateInterval, wantClientAuth, requireClientAuth, oscpCheckingMode, crlCheckingMode,
     * namespaceCheckingMode and proxySupport are exploited.
     * @param host the hostname for the connector
     * @param port the port number for the connectgor
     * @return the connector
     * @throws STSException if the connector cannot be constructed
     */
    private Connector configureConnector(String host, int port) throws STSException {
    	String scheme = getStringFromStandaloneConfiguration("scheme", StandaloneServiceDefaults.SCHEME);
    	Connector connector;
    	if (scheme.equalsIgnoreCase("https")) {
    		log.info("The configured scheme is HTTPS, certificate must be used.");
			String privateKey = getStringFromStandaloneConfiguration("privateKey", StandaloneServiceDefaults.PRIVATE_KEY_PATH);
			String privateKeyPwd = getStringFromStandaloneConfiguration("privateKeyPassword", null);
			String certificate = getStringFromStandaloneConfiguration("certificate", StandaloneServiceDefaults.CERTIFICATE_PATH);
			X509Credential credential;
			try {	
				if (privateKeyPwd == null) {
					log.debug("Initializing the PEM credential without a password");
					credential = new PEMCredential(privateKey, certificate, null);
				} else {
					log.debug("Initializing the PEM credential with a password");
					credential = new PEMCredential(privateKey, certificate, privateKeyPwd.toCharArray());
				}
			} catch (KeyStoreException e) {
				throw new STSException("Could not initialize the private key!", e);
			} catch (CertificateException e) {
				throw new STSException("Could not initialize the certificate!", e);
			} catch (IOException e) {
				throw new STSException("Could not read the private key and/or certificate from the file", e);
			}
			String trustStoreDir = getStringFromStandaloneConfiguration("trustStoreDir", String.valueOf(StandaloneServiceDefaults.TRUST_STORE_DIR));		
    		try {
    			OCSPParametes ocspParams = new OCSPParametes(OCSPCheckingMode.valueOf(getStringFromStandaloneConfiguration("ocspCheckingMode", null)));
    			RevocationParameters revocationParams = new RevocationParameters(CrlCheckingMode.valueOf(getStringFromStandaloneConfiguration("crlCheckingMode", null)), ocspParams);
    			ValidatorParams validatorParams = new ValidatorParams(revocationParams, ProxySupport.valueOf(getStringFromStandaloneConfiguration("proxySupport", null)));
    			OpensslCertChainValidator validator = new OpensslCertChainValidator(trustStoreDir, 
    					NamespaceCheckingMode.valueOf(getStringFromStandaloneConfiguration("namespaceCheckingMode", null)),
    					getLongFromStandaloneConfiguration("crlUpdateInterval", StandaloneServiceDefaults.CRL_UPDATE_INTERVAL),
    					validatorParams);
    			connector = new JettySslSelectChannelConnector(new CredentialX509KeyManager(credential), new SSLTrustManager(validator));
    			((SslSelectChannelConnector) connector).setWantClientAuth(getBooleanFromStandaloneConfiguration("wantClientAuth", String.valueOf(StandaloneServiceDefaults.WANT_CLIENT_AUTH)));
    			((SslSelectChannelConnector) connector).setNeedClientAuth(getBooleanFromStandaloneConfiguration("requireClientAuth", String.valueOf(StandaloneServiceDefaults.REQUIRE_CLIENT_AUTH)));
    		} catch (Exception e) {
    			log.error("Error initializing connector: "+e.getMessage());
    			if (log.isDebugEnabled())
    				log.error("Error initializing connector: "+e.getMessage(),e);
    			throw new STSException(e);
    		}
    	} else {
    		log.info("The configured scheme is HTTP, no certificate used.");
    		connector = new SelectChannelConnector();
    	}
		log.info("STS will listen on {}://{}:{}", new Object[] { scheme, host, port });
		connector.setPort(port);
		connector.setHost(host);
		return connector;
    }

    /**
     * Performs the jetty server configuration
     */
    private void configureServer() throws STSException {

        log.info("Configuring jetty server...");
        jettyServer = new Server();
        int maxRequestQueueSize = getIntFromStandaloneConfiguration(
                "maxRequestQueueSize",
                StandaloneServiceDefaults.MAX_REQUEST_QUEUE_SIZE);
        log.debug("maxRequestQueueSize = {}", maxRequestQueueSize);

        int maxConnections = getIntFromStandaloneConfiguration(
                "maxConnections", StandaloneServiceDefaults.MAX_CONNECTIONS);
        if (maxConnections <= 0) {
            log.error("Please specify a positive value for the 'maxConnections' configuration parameter!");
            log.error("Will use the hardcoded default '{}' instead...",
                    StandaloneServiceDefaults.MAX_CONNECTIONS);
            maxConnections = StandaloneServiceDefaults.MAX_CONNECTIONS;
        }

        log.info("maxConnections = {}", maxConnections);

        jettyServer.setSendServerVersion(false);
        jettyServer.setSendDateHeader(false);

        BlockingQueue<Runnable> requestQueue;

        if (maxRequestQueueSize < 1) {
            requestQueue = new LinkedBlockingQueue<Runnable>();
        } else {
            requestQueue = new ArrayBlockingQueue<Runnable>(maxRequestQueueSize);
        }

        ThreadPool threadPool = new ThreadPool(5, maxConnections, 60,
                TimeUnit.SECONDS, requestQueue);

        jettyServer.setThreadPool(threadPool);

        // Connectors configuration
        int port = getIntFromStandaloneConfiguration("port",
                StandaloneServiceDefaults.PORT);

        String host = getStringFromStandaloneConfiguration("hostname",
                StandaloneServiceDefaults.HOSTNAME);

        jettyServer.setConnectors(new Connector[] { configureConnector(host,port) });    

        JettyShutdownCommand shutdownCommand = new JettyShutdownCommand(jettyServer);

        ShutdownAndStatusService.startShutdownAndStatusService(getIntFromStandaloneConfiguration("shutdownPort", StandaloneServiceDefaults.SHUTDOWN_PORT), Collections
                .singletonList((Runnable) shutdownCommand));

        webappContext = new WebAppContext();

        webappContext.setContextPath("/" + getStringFromStandaloneConfiguration("webappContext", "sts"));
        webappContext.setWar(getStringFromStandaloneConfiguration("warfile", "sts.war"));
        webappContext.setParentLoaderPriority(true);

        HandlerCollection handlers = new HandlerCollection();
        handlers.setHandlers(new Handler[] { webappContext, new DefaultHandler() });

        jettyServer.setHandler(handlers);

    }

    /**
     * Utility method to fetch an int configuration parameter out of the
     * standalone-service configuration
     * @param key the configuration parameter key
     * @param defaultValue a default value in case the parameter is not defined
     * @return the configuration parameter value
     */
    private int getIntFromStandaloneConfiguration(String key, int defaultValue) {
        StandaloneConfiguration conf = null;
        try {
            conf = StandaloneConfiguration.instance();
        } catch (STSException e) {
            log.error("Configuration is not initialized!", e);
        }
        log.debug("Returning {}", conf.getInt(key, defaultValue));
        return conf.getInt(key, defaultValue);
    }

    /**
     * Utility method to fetch a long configuration parameter out of the
     * standalone-service configuration
     * @param key the configuration parameter key
     * @param defaultValue a default value in case the parameter is not defined
     * @return the configuration parameter value
     */
    private long getLongFromStandaloneConfiguration(String key, long defaultValue) {
        StandaloneConfiguration conf = null;
        try {
            conf = StandaloneConfiguration.instance();
        } catch (STSException e) {
            log.error("Configuration is not initialized!", e);
        }
        log.debug("Returning {}", conf.getLong(key, defaultValue));
        return conf.getLong(key, defaultValue);
    }

    /**
     * Utility method to fetch a string configuration parameter out of the
     * security configuration
     * @param key the configuration parameter key
     * @param defaultValue a default value in case the parameter is not defined
     * @return the configuration parameter value
     */
    private String getStringFromStandaloneConfiguration(String key,
            String defaultValue) {
        StandaloneConfiguration conf = null;
        try {
            conf = StandaloneConfiguration.instance();
        } catch (STSException e) {
            log.error("Configuration is not initialized!", e);
        }
        log.debug("Returning {}", conf.getString(key, defaultValue));
        return conf.getString(key, defaultValue);
    }
    
    /**
     * Utility method to fetch a boolean configuration parameter out of the
     * security configuration
     * @param key the configuration parameter key
     * @param defaultValue a default value in case the parameter is not defined
     * @return the configuration parameter value as true or false
     */
    private boolean getBooleanFromStandaloneConfiguration(String key,
            String defaultValue) {
        StandaloneConfiguration conf = null;
        try {
            conf = StandaloneConfiguration.instance();
        } catch (STSException e) {
            log.error("Configuration is not initialized!", e);
        }
        log.debug("Returning {}", conf.getString(key, defaultValue).equalsIgnoreCase("true"));
        return conf.getString(key, defaultValue).equalsIgnoreCase("true");
    }

    /**
     * Parses command line options
     * @param args the command line options
     */
    protected void parseOptions(String[] args) {
    	if (args.length == 0) {
    		usage();
    	}
        if (args.length > 0) {
            int currentArg = 0;
            while (currentArg < args.length) {
                if (!args[currentArg].startsWith("--")) {
                    usage();
                } else if (args[currentArg].equalsIgnoreCase("--"
                        + CONF_DIR_OPTION_NAME)) {
                    configurationDir = args[currentArg + 1];
                    log.info("Starting STS server with configuration dir: {}",
                            configurationDir);
                    currentArg = currentArg + 2;
                } else
                    usage();
            }
        }
    }

    /**
     * Prints a usage message and exits with status 1
     */
    private void usage() {
        String usage = "StandaloneService [--" + CONF_DIR_OPTION_NAME + " <confDir>]";
        System.out.println(usage);
        System.exit(1);
    }

    /**
     * Runs the service
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        new StandaloneService(args);
    }

}