/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.standalone;

import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509KeyManager;
import javax.net.ssl.X509TrustManager;

import org.mortbay.jetty.security.SslSelectChannelConnector;

/**
 * Any extension to the basic Jetty SSL connection handler that allows a pre-instantiated key and trust manager to be
 * used when create new SSL connections.
 * 
 * Imported from org.glite.authz.pap.server.standalone.JettySslSelectChannelConnector
 */
public class JettySslSelectChannelConnector extends SslSelectChannelConnector {

    /** {@link KeyManager} used by this TLS connector. */
    private X509KeyManager keyManager;

    /** {@link TrustManager} used by this TLS connector. */
    private X509TrustManager trustManager;

    /**
     * Constructor.
     * 
     * @param key the key manager used for the TLS connections
     * @param trust the trust manager used for the TLS connections
     */
    public JettySslSelectChannelConnector(X509KeyManager key, X509TrustManager trust) {
        if(key == null){
            throw new IllegalArgumentException("X.509 key manager may not be null");
        }
        keyManager = key;
        
        if(trust == null){
            throw new IllegalArgumentException("X.509 trust manager may not be null");
        }
        trustManager = trust;
    }

    /** {@inheritDoc} */
    protected SSLContext createSSLContext() throws Exception {
        SSLContext sslConext = SSLContext.getInstance("TLS");
        sslConext.init(new KeyManager[] { keyManager }, new TrustManager[] { trustManager }, null);
        return sslConext;
    }
}