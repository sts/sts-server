/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.standalone;

import java.io.File;

import org.glite.sts.STSException;
import org.ini4j.Ini;

/**
 * The configuration class for the standalone server.
 */
public class StandaloneConfiguration {
	
    /** The singleton StandaloneConfiguration instance **/
    private static StandaloneConfiguration instance;
    
    private Ini.Section iniSection;
    
    private StandaloneConfiguration(String configurationDir) throws STSException {
    	String iniFile;
    	if (!configurationDir.substring(configurationDir.length() - 1).equals("/")) {
        	iniFile = configurationDir + "/sts-server.ini";
        } else {
        	iniFile = configurationDir + "sts-server.ini";
        }
        try {
            Ini ini = new Ini(new File(iniFile));
            iniSection = ini.get("StandaloneServer");
        } catch (Exception e) {
            throw new STSException("Could not initialize the configuration", e);
        }
    }
    
    /**
     * Initializes the configuration.
     * @param configurationDir, the directory in which the configuration files will be searched for 
     * @return a StandaloneConfiguration object
     */
    public static StandaloneConfiguration initialize(String configurationDir) throws STSException {
        if (instance == null) {
            instance = new StandaloneConfiguration(configurationDir);
        }
        return instance;
    }
    
    /**
     * Returns a StandaloneConfiguration object, if properly initialized.
     * 
     * @return the active instance of the StandaloneConfiguration class
     * @throws STSException, in case the StandaloneConfiguration hasn't been properly initialized with the initialize method. 
     */
    public static StandaloneConfiguration instance() throws STSException {
        if (instance == null) {
            throw new STSException("Please initialize configuration before calling the instance method!");
        }
        return instance;
    }
    
    public String getString(String key) {
        return iniSection.get(key);
    }
    
    public String getString(String key, String defaultValue) {
        String value = getString(key);
        if (value == null || value.equals("")) {
            return defaultValue;
        } else {
            return value;
        }
    }
    
    public int getInt(String key, int defaultValue) {
        return new Integer(getString(key, "" + defaultValue)).intValue();
    }

    public long getLong(String key, long defaultValue) {
        return new Long(getString(key, "" + defaultValue)).longValue();
    }

}
