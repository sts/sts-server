/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.standalone;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.glite.sts.STSException;
import org.glite.sts.util.HttpClientBuilder;

/**
 * This class implement the client that request the shutdown of the local STS
 * standalone service.
 * 
 * Imported from org.glite.authz.pap.server.standalone.ShutdownClient
 */
public class ShutdownClient {

    /**
     * Prints the message of the exception passed as argument and exits
     * 
     * @param e, the exception to be printed
     */
    static void printErrorMessageAndExit(Exception e) {
        System.err.println("STS shutdown error: " + e.getMessage());
        e.printStackTrace();
        System.exit(1);
    }

    /**
     * Triggers the shutdown of the local STS standalone service.
     */
    static void doShutdown() {
        StandaloneConfiguration conf = null;
        try {
			conf = StandaloneConfiguration.instance();
		} catch (STSException e) {
			printErrorMessageAndExit(e);
		}
        String shutdownEndpoint = String.format("http://localhost:%s/shutdown", conf.getString("shutdownPort"));
        String shutdownCommand = conf.getString("shutdownCommand");
        HttpClientBuilder httpBuilder = new HttpClientBuilder(shutdownEndpoint, null, null);
        HttpClient httpClient = null;
        try {
			httpClient = httpBuilder.buildClient();
		} catch (Exception e) {
			printErrorMessageAndExit(e);
		}
        HttpGet get = new HttpGet(shutdownEndpoint);
        if (shutdownCommand != null) {
            get.setHeader(ShutdownServlet.SHUTDOWN_COMMAND_HEADER_NAME, shutdownCommand);
        }
        try {
            httpClient.execute(get);
        } catch (Exception e) {
            printErrorMessageAndExit(e);
        } finally {
        	httpClient.getConnectionManager().shutdown();
        }
    }

    public static void main(String[] args) {
        if (System.getProperty("STS_SERVER_HOME") == null) {

            System.err
                    .println("Please set the STS_SERVER_HOME environment variable before running this command!");
            System.exit(1);
        }
        try {
            String stsConfigurationHome = System.getProperty("STS_SERVER_HOME")
                    + "/conf";
            StandaloneConfiguration.initialize(stsConfigurationHome);
        } catch (Throwable t) {
            System.err.println("Error reading STS standalone configuration. Is the STS_SERVER_HOME system property/environment variable set correctly?");
            System.err.println(t.getMessage());
            t.printStackTrace(System.err);
            System.exit(1);
        }
        doShutdown();
    }
}