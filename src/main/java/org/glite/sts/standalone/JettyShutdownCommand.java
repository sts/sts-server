/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.standalone;

import org.mortbay.jetty.Server;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** 
 * A command that shuts down a Jetty {@link Server} if it's currently running. 
 * 
 * Imported from org.glite.authz.pap.server.standalone.JettyShutdownCommand
 */ 
public class JettyShutdownCommand implements Runnable {

    /** Class logger. */
    private Logger log = LoggerFactory.getLogger(JettyShutdownCommand.class);

    /** Server to be shutdown. */
    private Server httpServer;

    /**
     * Constructor.
     * 
     * @param targetServer server to be shutdown
     */
    public JettyShutdownCommand(Server targetServer) {
    	log.debug("Constructing a new shutdown command with {}", targetServer.toString());
        httpServer = targetServer;
    }

    /** {@inheritDoc} */
    public void run() {
        if (httpServer.isRunning()) {
        	log.debug("The server {} is running, now trying to stop it", httpServer.toString());
            try {
                httpServer.stop();
            } catch (Exception e) {
                log.error("Unable to shutdown HTTP server", e);
                System.exit(1);
            }
        } else {
        	log.debug("The server is not running, nothing left to do.");
        }
    }

}