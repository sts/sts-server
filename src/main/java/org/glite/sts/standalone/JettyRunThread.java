/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.standalone;

import org.mortbay.jetty.Server;

/** 
 * A thread that spawns a Jetty {@link Server} instance. 
 * 
 * Imported from org.glite.authz.pap.server.standalone.JettyRunThread.
 */
public class JettyRunThread extends Thread {
    
    /** Jetty server to start. */
    private Server httpServer;

    /**
     * Constructs a new <code>JettyRunThread</code>.
     * 
     * @param server Jetty server to start
     */
    public JettyRunThread(Server server) {
        httpServer = server;
    }

    /** {@inheritDoc} */
    public void run() {
        try {
            httpServer.start();
            httpServer.join();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}