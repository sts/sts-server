/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.standalone;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.glite.sts.STSException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This servlet implements the service shutdown logic.
 * 
 * Imported from org.glite.authz.pap.server.standalone.ShutdownServlet.
 */
public class ShutdownServlet extends HttpServlet {

    /** Serial number */
    private static final long serialVersionUID = 1L;

    /** The logging facility **/ 
    public static final Logger log = LoggerFactory.getLogger(ShutdownServlet.class);

    /** 
     * The name of the HTTP header coming in the shutdown request that contains the
     * shutdown command.
     */
    public static final String SHUTDOWN_COMMAND_HEADER_NAME = "STS_SHUTDOWN_COMMAND";

    /**
     * The thread that will trigger the shutdown.
     */
    private Thread shutdownCommandThread;

    /**
     * Constructs a new <code>ShutdownServlet</code>.
     * @param shutdownThread, the thread that will shutdown the service upon request
     */
    public ShutdownServlet(Thread shutdownThread) {
        this.shutdownCommandThread = shutdownThread;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String stsShutdownCommand = null;
        try {
            stsShutdownCommand = StandaloneConfiguration.instance().getString("shutdownCommand");
        } catch (STSException e) {
            log.error("Configuration is not initialized!", e);
        }
        log.info("Shutdown request received from {}.", req.getRemoteAddr());
        if (stsShutdownCommand == null) {
        	log.debug("shutdown command is null in the configuration, starting the shutdown thread");
            shutdownCommandThread.start();
        } else {
            String shutdownCommand = req.getHeader(SHUTDOWN_COMMAND_HEADER_NAME);
            log.debug("shutdown command {} read from the headers", shutdownCommand);
            if (shutdownCommand != null && shutdownCommand.equals(stsShutdownCommand)) {
                shutdownCommandThread.start();
            } else {
                log.warn("Shutdown attempted with invalid command string!"); 
            }
        }
    }
}