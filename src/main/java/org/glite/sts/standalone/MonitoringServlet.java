/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.standalone;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class MonitoringServlet extends HttpServlet {

	/** Serial version uid. */
	private static final long serialVersionUID = 8622294181310752426L;
	
	/** {@inheridDoc} */
	protected void doGet(HttpServletRequest servletRequest, HttpServletResponse servletResponse) throws ServletException, IOException {
	    Runtime r = Runtime.getRuntime();
	    servletResponse.getWriter().println("Status: OK");
	    servletResponse.getWriter().println("UsedMemory: " + r.totalMemory() + " bytes");
	    servletResponse.getWriter().println("MaxMemory: " + r.maxMemory() + " bytes");
	    servletResponse.setStatus(HttpServletResponse.SC_OK);
	}
}