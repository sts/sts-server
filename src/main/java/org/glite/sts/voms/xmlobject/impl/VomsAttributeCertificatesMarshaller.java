/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.voms.xmlobject.impl;

import java.util.Map.Entry;

import javax.xml.namespace.QName;

import net.shibboleth.utilities.java.support.xml.AttributeSupport;

import org.glite.sts.voms.xmlobject.VomsAttributeCertificates;
import org.opensaml.core.xml.XMLObject;
import org.opensaml.core.xml.config.XMLObjectProviderRegistrySupport;
import org.opensaml.core.xml.io.AbstractXMLObjectMarshaller;
import org.opensaml.core.xml.io.MarshallingException;
import org.w3c.dom.Attr;
import org.w3c.dom.Element;

/**
 * A thread-safe marshaller for {@link VomsAttributeCertificates} objects.
 */
public class VomsAttributeCertificatesMarshaller extends AbstractXMLObjectMarshaller {

    /** {@inheritDoc} */
    protected void marshallAttributes(XMLObject xmlObject, Element domElement) throws MarshallingException {
    	VomsAttributeCertificates vomsACs = (VomsAttributeCertificates) xmlObject;
        if (vomsACs.getOrdering() != null) {
            domElement.setAttributeNS(null, VomsAttributeCertificates.ORDERING_ATTRIB_NAME, vomsACs.getOrdering());
        }
        if (vomsACs.getTargets() != null) {
            domElement.setAttributeNS(null, VomsAttributeCertificates.TARGETS_ATTRIB_NAME, vomsACs.getTargets());
        }
        if (vomsACs.getVerificationType() != null) {
            domElement.setAttributeNS(null, VomsAttributeCertificates.VERIFICATION_TYPE_ATTRIB_NAME, vomsACs.getVerificationType());
        }

        Attr attr;
        for (Entry<QName, String> entry : vomsACs.getUnknownAttributes().entrySet()) {
            attr = AttributeSupport.constructAttribute(domElement.getOwnerDocument(), entry.getKey());
            attr.setValue(entry.getValue());
            domElement.setAttributeNodeNS(attr);
            if (XMLObjectProviderRegistrySupport.isIDAttribute(entry.getKey())
                    || vomsACs.getUnknownAttributes().isIDAttribute(entry.getKey())) {
                attr.getOwnerElement().setIdAttributeNode(attr, true);
            }
        }

    }

}
