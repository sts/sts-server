/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.voms.xmlobject;

/** Namespace declarations and their prefixes for STS GridProxyRequest. */
public class GridProxyConstants {

    public static final String GLITE_STS_PROXY_NS = "urn:glite.org:sts:proxy";
    
    public static final String GLITE_STS_PROXY_PREFIX = "gridProxy";

}
