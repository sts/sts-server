/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.voms.xmlobject.impl;

import javax.xml.namespace.QName;

import net.shibboleth.utilities.java.support.xml.QNameSupport;

import org.glite.sts.voms.xmlobject.FQAN;
import org.glite.sts.voms.xmlobject.VomsAttributeCertificates;
import org.opensaml.core.xml.XMLObject;
import org.opensaml.core.xml.io.AbstractXMLObjectUnmarshaller;
import org.opensaml.core.xml.io.UnmarshallingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Attr;

/**
 * A thread-safe unmarshaller for {@link VomsAttributeCertificates} objects. 
 */
public class VomsAttributeCertificatesUnmarshaller extends AbstractXMLObjectUnmarshaller {
	
    /** Logger. */
    private final Logger log = LoggerFactory.getLogger(VomsAttributeCertificatesUnmarshaller.class);
    
    /** {@inheritDoc} */
    protected void processAttribute(XMLObject xmlObject, Attr attribute) throws UnmarshallingException {
        log.debug("Processing attributes for VomsAttributeCertificates");
        VomsAttributeCertificates vomsACs = (VomsAttributeCertificates) xmlObject;

        if (attribute.getLocalName().equals(VomsAttributeCertificates.ORDERING_ATTRIB_NAME)) {
            log.debug("Ordering {} found, setting it for the value.", attribute.getValue());
            vomsACs.setOrdering(attribute.getValue());
        } else if (attribute.getLocalName().equals(VomsAttributeCertificates.TARGETS_ATTRIB_NAME)) {
            log.debug("Targets {} found, setting it for the value.", attribute.getValue());
            vomsACs.setTargets(attribute.getValue());
        } else if (attribute.getLocalName().equals(VomsAttributeCertificates.VERIFICATION_TYPE_ATTRIB_NAME)) {
            log.debug("Verification type {} found, setting it for the value.", attribute.getValue());
            vomsACs.setVerificationType(attribute.getValue());
        } else {
            QName attribQName = QNameSupport.getNodeQName(attribute);
            if (attribute.isId()) {
                vomsACs.getUnknownAttributes().registerID(attribQName);
            }
            vomsACs.getUnknownAttributes().put(attribQName, attribute.getValue());
        }
    }
    
    /** {@inheritDoc} */
    protected void processChildElement(XMLObject parentXMLObject, XMLObject childXMLObject)
            throws UnmarshallingException {
    	log.debug("Processing child element for GridProxyRequest");
    	VomsAttributeCertificates vomsACs = (VomsAttributeCertificates) parentXMLObject;
    	if (childXMLObject instanceof FQAN) {
    		log.debug("Setting a FQAN");
    		vomsACs.getFQANs().add((FQAN) childXMLObject);
    	} else {
    		super.processChildElement(parentXMLObject, childXMLObject);
    	}
    }
}
