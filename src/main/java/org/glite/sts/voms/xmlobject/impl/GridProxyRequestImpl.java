/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.voms.xmlobject.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.glite.sts.voms.xmlobject.GridProxyRequest;
import org.glite.sts.voms.xmlobject.VomsAttributeCertificates;
import org.opensaml.core.xml.AbstractExtensibleXMLObject;
import org.opensaml.core.xml.XMLObject;

/**
 * A concrete implementation for {@link GridProxyRequest}.
 */
public class GridProxyRequestImpl extends AbstractExtensibleXMLObject implements GridProxyRequest {
	
    /** lifetime attribute value. */
    private int lifetime;
    
    /** proxy type attribute value. */
    private int proxyType;
    
    /** delegation type attribute value. */
    private int delegationType;
    
    /** policy type attribute value. */
    private int policyType;
    
    /** VOMS attribute certificates related to this request. */
    private VomsAttributeCertificates vomsAttributeCertificates;
    
    /**
     * Constructor.
     * 
     * @param namespaceURI namespace of the element
     * @param elementLocalName name of the element
     * @param namespacePrefix namespace prefix of the element
     */
    protected GridProxyRequestImpl(String namespaceURI, String elementLocalName, String namespacePrefix) {
        super(namespaceURI, elementLocalName, namespacePrefix);
    }

	/** {@inheritDoc} */
	public int getLifetime() {
		return lifetime;
	}

	/** {@inheritDoc} */
	public void setLifetime(int newLifetime) {
		lifetime = newLifetime;
	}

	/** {@inheritDoc} */
	public int getProxyType() {
		return proxyType;
	}

	/** {@inheritDoc} */
	public void setProxyType(int newProxyType) {
		proxyType = newProxyType;
	}

	/** {@inheritDoc} */
	public int getDelegationType() {
		return delegationType;
	}

	/** {@inheritDoc} */
	public void setDelegationType(int newDelegationType) {
		delegationType = newDelegationType;
	}

	/** {@inheritDoc} */
	public int getPolicyType() {
		return policyType;
	}

	/** {@inheritDoc} */
	public void setPolicyType(int newPolicyType) {
		policyType = newPolicyType;
	}

	/** {@inheritDoc} */
	public VomsAttributeCertificates getVomsAttributeCertificates() {
		return vomsAttributeCertificates;
	}

	/** {@inheritDoc} */
	public void setVomsAttributeCertificates(
			VomsAttributeCertificates newVomsAttributeCertificates) {
		vomsAttributeCertificates = prepareForAssignment(vomsAttributeCertificates, newVomsAttributeCertificates);
	}
	
    /** {@inheritDoc} */
    public List<XMLObject> getOrderedChildren() {
        ArrayList<XMLObject> children = new ArrayList<XMLObject>();
        if (vomsAttributeCertificates != null) {
        	children.add(vomsAttributeCertificates);
        }
        return Collections.unmodifiableList(children);
    }
}
