/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.voms.xmlobject.impl;

import org.glite.sts.voms.xmlobject.VomsAttributeCertificates;
import org.opensaml.core.xml.AbstractXMLObjectBuilder;

/**
 * A builder for {@link VomsAttributeCertificates} objects. 
 */
public class VomsAttributeCertificatesBuilder extends AbstractXMLObjectBuilder<VomsAttributeCertificates> {

    /** {@inheritDoc} */
    public VomsAttributeCertificates buildObject(String namespaceURI, String localName, String namespacePrefix) {
        return new VomsAttributeCertificatesImpl(namespaceURI, localName, namespacePrefix);
    }

}
