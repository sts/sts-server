/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.voms.xmlobject;

import java.util.List;

import javax.xml.namespace.QName;

import org.opensaml.core.xml.AttributeExtensibleXMLObject;
import org.opensaml.core.xml.ElementExtensibleXMLObject;

/** XMLObject for the VomsAttributeCertificates element in the GridProxyRequest. */
public interface VomsAttributeCertificates extends ElementExtensibleXMLObject, AttributeExtensibleXMLObject {

    /** Default element local name. */
    public static final String DEFAULT_ELEMENT_LOCAL_NAME = "VomsAttributeCertificates";
    
    /** Default element name. */
    public static final QName DEFAULT_ELEMENT_NAME = new QName(GridProxyConstants.GLITE_STS_PROXY_NS, DEFAULT_ELEMENT_LOCAL_NAME, GridProxyConstants.GLITE_STS_PROXY_PREFIX);
    
    /** ordering attribute name. */
    public static final String ORDERING_ATTRIB_NAME = "ordering";
    
    /** targets attribute name. */
    public static final String TARGETS_ATTRIB_NAME = "targets";

    /** verificationType attribute name. */
    public static final String VERIFICATION_TYPE_ATTRIB_NAME = "verificationType";
    
    /**
     * Get the ordering attribute value.
     * 
     * @return the ordering attribute value
     */
    public String getOrdering();

    /**
     * Set the ordering attribute value.
     * 
     * @param newOrdering the new ordering attribute value
     */
    public void setOrdering(String newOrdering);

    /**
     * Get the targets attribute value.
     * 
     * @return the targets attribute value
     */
    public String getTargets();

    /**
     * Set the targets attribute value.
     * 
     * @param newTargets the new targets attribute value
     */
    public void setTargets(String newTargets);

    /**
     * Get the verification type attribute value.
     * 
     * @return the veritication type attribute value
     */
    public String getVerificationType();

    /**
     * Set the verification type attribute value.
     * 
     * @param newVerificationType the new verification type attribute value
     */
    public void setVerificationType(String newVerificationType);

    /**
     * Get the list of FQAN elements.
     * 
     * @return the list of FQAN elements.
     */
    public List<FQAN> getFQANs();

}
