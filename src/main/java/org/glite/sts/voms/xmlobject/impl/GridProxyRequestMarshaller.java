/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.voms.xmlobject.impl;

import java.util.Map.Entry;

import javax.xml.namespace.QName;

import net.shibboleth.utilities.java.support.xml.AttributeSupport;

import org.glite.sts.voms.xmlobject.GridProxyRequest;
import org.opensaml.core.xml.XMLObject;
import org.opensaml.core.xml.config.XMLObjectProviderRegistrySupport;
import org.opensaml.core.xml.io.AbstractXMLObjectMarshaller;
import org.opensaml.core.xml.io.MarshallingException;
import org.w3c.dom.Attr;
import org.w3c.dom.Element;

/**
 * A thread-safe marshaller for {@link GridProxyRequest} objects.
 */
public class GridProxyRequestMarshaller extends AbstractXMLObjectMarshaller {

    /** {@inheritDoc} */
    protected void marshallAttributes(XMLObject xmlObject, Element domElement) throws MarshallingException {
    	GridProxyRequest proxyRequest = (GridProxyRequest) xmlObject;
        if (proxyRequest.getLifetime() >= 0) {
            domElement.setAttributeNS(null, GridProxyRequest.LIFETIME_ATTRIB_NAME, Integer.toString(proxyRequest.getLifetime()));
        }
        if (proxyRequest.getProxyType() >= 0) {
            domElement.setAttributeNS(null, GridProxyRequest.PROXY_TYPE_ATTRIB_NAME, Integer.toString(proxyRequest.getProxyType()));
        }
        if (proxyRequest.getDelegationType() >= 0) {
            domElement.setAttributeNS(null, GridProxyRequest.DELEGATION_TYPE_ATTRIB_NAME, Integer.toString(proxyRequest.getDelegationType()));
        }
        if (proxyRequest.getPolicyType() >= 0) {
            domElement.setAttributeNS(null, GridProxyRequest.POLICY_TYPE_ATTRIB_NAME, Integer.toString(proxyRequest.getPolicyType()));
        }

        Attr attr;
        for (Entry<QName, String> entry : proxyRequest.getUnknownAttributes().entrySet()) {
            attr = AttributeSupport.constructAttribute(domElement.getOwnerDocument(), entry.getKey());
            attr.setValue(entry.getValue());
            domElement.setAttributeNodeNS(attr);
            if (XMLObjectProviderRegistrySupport.isIDAttribute(entry.getKey())
                    || proxyRequest.getUnknownAttributes().isIDAttribute(entry.getKey())) {
                attr.getOwnerElement().setIdAttributeNode(attr, true);
            }
        }

    }

}
