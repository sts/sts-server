/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.voms.xmlobject.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.xml.namespace.QName;

import org.glite.sts.voms.xmlobject.FQAN;
import org.glite.sts.voms.xmlobject.GridProxyConstants;
import org.glite.sts.voms.xmlobject.VomsAttributeCertificates;
import org.opensaml.core.xml.AbstractExtensibleXMLObject;
import org.opensaml.core.xml.XMLObject;
import org.opensaml.core.xml.util.IndexedXMLObjectChildrenList;

/**
 * A concrete implementation for {@link VomsAttributeCertificates}.
 */
public class VomsAttributeCertificatesImpl extends AbstractExtensibleXMLObject implements VomsAttributeCertificates {

	/** Ordering attribute value. */
	private String ordering;
	
	/** Targets attribute value. */
	private String targets;
	
	/** Verification type attribute value. */
	private String verificationType;
	
	/** List of FQANs. */
	private final IndexedXMLObjectChildrenList<FQAN> fqans;
	
    /**
     * Constructor.
     * 
     * @param namespaceURI namespace of the element
     * @param elementLocalName name of the element
     * @param namespacePrefix namespace prefix of the element
     */
    protected VomsAttributeCertificatesImpl(String namespaceURI, String elementLocalName, String namespacePrefix) {
        super(namespaceURI, elementLocalName, namespacePrefix);
        fqans = new IndexedXMLObjectChildrenList<FQAN>(this);
    }

	/** {@inheritDoc} */
	public String getOrdering() {
		return ordering;
	}

	/** {@inheritDoc} */
	public void setOrdering(String newOrdering) {
		ordering = prepareForAssignment(ordering, newOrdering);
	}

	/** {@inheritDoc} */
	public String getTargets() {
		return targets;
	}

	/** {@inheritDoc} */
	public void setTargets(String newTargets) {
		targets = prepareForAssignment(targets, newTargets);
	}

	/** {@inheritDoc} */
	public String getVerificationType() {
		return verificationType;
	}

	/** {@inheritDoc} */
	public void setVerificationType(String newVerificationType) {
		verificationType = prepareForAssignment(verificationType, newVerificationType);
	}

	/** {@inheritDoc} */
	@SuppressWarnings("unchecked")
	public List<FQAN> getFQANs() {
        QName statementQName = new QName(GridProxyConstants.GLITE_STS_PROXY_NS, FQAN.DEFAULT_ELEMENT_LOCAL_NAME,
                GridProxyConstants.GLITE_STS_PROXY_PREFIX);
        return (List<FQAN>) fqans.subList(statementQName);
	}
	
    /** {@inheritDoc} */
    public List<XMLObject> getOrderedChildren() {
        ArrayList<XMLObject> children = new ArrayList<XMLObject>();
        children.addAll(fqans);
        return Collections.unmodifiableList(children);
    }
}
