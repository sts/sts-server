/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.voms.xmlobject;

import javax.xml.namespace.QName;

import org.opensaml.core.xml.schema.XSString;

/** XMLObject for the FQAN element in the GridProxyRequest's VomsAttributeCertificates-element. */
public interface FQAN extends XSString {
	
    /** Local Name of FQAN. */
    public static final String DEFAULT_ELEMENT_LOCAL_NAME = "FQAN";

    /** Default element name. */
    public static final QName DEFAULT_ELEMENT_NAME = new QName(GridProxyConstants.GLITE_STS_PROXY_NS, DEFAULT_ELEMENT_LOCAL_NAME,
            GridProxyConstants.GLITE_STS_PROXY_PREFIX);

}
