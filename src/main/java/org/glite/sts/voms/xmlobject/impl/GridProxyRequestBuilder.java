/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.voms.xmlobject.impl;

import org.glite.sts.voms.xmlobject.GridProxyRequest;
import org.opensaml.core.xml.AbstractXMLObjectBuilder;

/**
 * A builder for {@link GridProxyRequest} objects. 
 */
public class GridProxyRequestBuilder extends AbstractXMLObjectBuilder<GridProxyRequest> {

    /** {@inheritDoc} */
    public GridProxyRequest buildObject(String namespaceURI, String localName, String namespacePrefix) {
        return new GridProxyRequestImpl(namespaceURI, localName, namespacePrefix);
    }

}
