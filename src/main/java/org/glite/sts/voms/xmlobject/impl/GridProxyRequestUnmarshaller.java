/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.voms.xmlobject.impl;

import javax.xml.namespace.QName;

import net.shibboleth.utilities.java.support.xml.QNameSupport;

import org.glite.sts.voms.xmlobject.GridProxyRequest;
import org.glite.sts.voms.xmlobject.VomsAttributeCertificates;
import org.opensaml.core.xml.XMLObject;
import org.opensaml.core.xml.io.AbstractXMLObjectUnmarshaller;
import org.opensaml.core.xml.io.UnmarshallingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Attr;

/**
 * A thread-safe unmarshaller for {@link GridProxyRequest} objects. 
 */
public class GridProxyRequestUnmarshaller extends AbstractXMLObjectUnmarshaller {
	
    /** Logger. */
    private final Logger log = LoggerFactory.getLogger(GridProxyRequestUnmarshaller.class);
    
    /** {@inheritDoc} */
    protected void processAttribute(XMLObject xmlObject, Attr attribute) throws UnmarshallingException {
        log.debug("Processing attributes for GridProxyRequest");
        GridProxyRequest proxyRequest = (GridProxyRequest) xmlObject;

        if (attribute.getLocalName().equals(GridProxyRequest.LIFETIME_ATTRIB_NAME)) {
            log.debug("Lifetime {} found, setting it for the value.", attribute.getValue());
            proxyRequest.setLifetime(new Integer(attribute.getValue()).intValue());
        } else if (attribute.getLocalName().equals(GridProxyRequest.PROXY_TYPE_ATTRIB_NAME)) {
            log.debug("Proxy type {} found, setting it for the value.", attribute.getValue());
            proxyRequest.setProxyType(new Integer(attribute.getValue()).intValue());
        } else if (attribute.getLocalName().equals(GridProxyRequest.DELEGATION_TYPE_ATTRIB_NAME)) {
            log.debug("Delegation type {} found, setting it for the value.", attribute.getValue());
            proxyRequest.setDelegationType(new Integer(attribute.getValue()).intValue());
        } else if (attribute.getLocalName().equals(GridProxyRequest.POLICY_TYPE_ATTRIB_NAME)) {
            log.debug("Policy type {} found, setting it for the value.", attribute.getValue());
            proxyRequest.setPolicyType(new Integer(attribute.getValue()).intValue());
        } else {
            QName attribQName = QNameSupport.getNodeQName(attribute);
            if (attribute.isId()) {
                proxyRequest.getUnknownAttributes().registerID(attribQName);
            }
            proxyRequest.getUnknownAttributes().put(attribQName, attribute.getValue());
        }
    }
    
    /** {@inheritDoc} */
    protected void processChildElement(XMLObject parentXMLObject, XMLObject childXMLObject)
            throws UnmarshallingException {
    	log.debug("Processing child element for GridProxyRequest");
    	GridProxyRequest proxyRequest = (GridProxyRequest) parentXMLObject;
    	if (childXMLObject instanceof VomsAttributeCertificates) {
    		log.debug("Setting VomsAttributeCertificates");
    		proxyRequest.setVomsAttributeCertificates((VomsAttributeCertificates) childXMLObject);
    	} else {
    		super.processChildElement(parentXMLObject, childXMLObject);
    	}
    }

}
