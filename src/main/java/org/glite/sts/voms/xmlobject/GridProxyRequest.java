/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.voms.xmlobject;

import javax.xml.namespace.QName;

import org.opensaml.core.xml.AttributeExtensibleXMLObject;
import org.opensaml.core.xml.ElementExtensibleXMLObject;

/** XMLObject for the GridProxyRequest element. */
public interface GridProxyRequest extends ElementExtensibleXMLObject, AttributeExtensibleXMLObject {
	
    /** Default element local name. */
    public static final String DEFAULT_ELEMENT_LOCAL_NAME = "GridProxyRequest";
    
    /** Default element name. */
    public static final QName DEFAULT_ELEMENT_NAME = new QName(GridProxyConstants.GLITE_STS_PROXY_NS, DEFAULT_ELEMENT_LOCAL_NAME, GridProxyConstants.GLITE_STS_PROXY_PREFIX);
    
    /** lifetime attribute name. */
    public static final String LIFETIME_ATTRIB_NAME = "lifetime";

    /** proxyType attribute name. */
    public static final String PROXY_TYPE_ATTRIB_NAME = "proxyType";

    /** delegationType attribute name. */
    public static final String DELEGATION_TYPE_ATTRIB_NAME = "delegationType";

    /** policyType attribute name. */
    public static final String POLICY_TYPE_ATTRIB_NAME = "policyType";

    /**
     * Get the lifetime attribute value.
     * 
     * @return the lifetime attribute value
     */
    public int getLifetime();

    /**
     * Set the lifetime attribute value.
     * 
     * @param newLifetime the new lifetime attribute value
     */
    public void setLifetime(int newLifetime);

    /**
     * Get the proxy type attribute value.
     * 
     * @return the proxy type attribute value
     */
    public int getProxyType();

    /**
     * Set the proxy type attribute value.
     * 
     * @param newProxyType the new proxy type attribute value
     */
    public void setProxyType(int newProxyType);

    /**
     * Get the delegation type attribute value.
     * 
     * @return the delegation type attribute value
     */
    public int getDelegationType();

    /**
     * Set the delegation type attribute value.
     * 
     * @param newDelegationType the new delegation type attribute value
     */
    public void setDelegationType(int newDelegationType);

    /**
     * Get the policy type attribute value.
     * 
     * @return the policy type attribute value
     */
    public int getPolicyType();

    /**
     * Set the policy type attribute value.
     * 
     * @param newPolicyType the new policy type attribute value
     */
    public void setPolicyType(int newPolicyType);
    
    /**
     * Get the VOMS attribute certificates.
     * 
     * @return the VOMS attribute certificates
     */
    public VomsAttributeCertificates getVomsAttributeCertificates();
    
    /**
     * Set the VOMS attribute certificates.
     * 
     * @param newVomsAttributeCertificates the VOMS attribute certificates
     */
    public void setVomsAttributeCertificates(VomsAttributeCertificates newVomsAttributeCertificates);
}
