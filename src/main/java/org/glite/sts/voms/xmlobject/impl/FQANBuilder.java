/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.voms.xmlobject.impl;

import org.glite.sts.voms.xmlobject.FQAN;
import org.opensaml.core.xml.AbstractXMLObjectBuilder;
import org.opensaml.core.xml.schema.XSString;

/**
 * Builder of {@link FQAN} objects.
 */
public class FQANBuilder extends AbstractXMLObjectBuilder<XSString> {

    /** {@inheritDoc} */
    public FQAN buildObject(String namespaceURI, String localName, String namespacePrefix) {
        return new FQANImpl(namespaceURI, localName, namespacePrefix);
    }
}
