/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.authn;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.shibboleth.ext.spring.webflow.Event;
import net.shibboleth.ext.spring.webflow.Events;
import net.shibboleth.idp.authn.AbstractAuthenticationAction;
import net.shibboleth.idp.authn.AuthenticationException;
import net.shibboleth.idp.authn.AuthenticationRequestContext;
import net.shibboleth.idp.profile.ActionSupport;
import net.shibboleth.idp.profile.EventIds;
import net.shibboleth.idp.profile.ProfileRequestContext;

import org.glite.sts.profile.TokenRequestMessageContext;
import org.opensaml.messaging.context.MessageContext;
import org.opensaml.messaging.context.navigate.ChildContextLookup;
import org.opensaml.saml.saml2.core.Assertion;
import org.opensaml.soap.soap11.Envelope;
import org.opensaml.soap.soap11.Header;
import org.opensaml.soap.wssecurity.Security;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.webflow.execution.RequestContext;
import org.opensaml.core.xml.XMLObject;

import com.google.common.base.Function;

/**
 * An authentication stage that extracts a SAML assertion from the WSS token attached to a SOAP message.
 * As should be obvious, this assumes that the inbound message is a SOAP {@link Envelope}.
 */
@Events({
    @Event(id = EventIds.PROCEED_EVENT_ID)})
public class ExtractSamlAssertionFromWssToken extends AbstractAuthenticationAction {
	
    /** Class logger. */
    private final Logger log = LoggerFactory.getLogger(ExtractSamlAssertionFromWssToken.class);
    
    /**
     * Strategy used to look up the {@link TokenRequestMessageContext} associated with the inbound message context.
     */
    private Function<MessageContext<Envelope>, TokenRequestMessageContext> tokenRequestMessageContextLookupStrategy;

    /**
     * Constructor.
     */
    public ExtractSamlAssertionFromWssToken() {
    	super();
        tokenRequestMessageContextLookupStrategy =
                new ChildContextLookup<MessageContext<Envelope>, TokenRequestMessageContext>(TokenRequestMessageContext.class,
                        false);
    }
    
    /** {@inheritDoc} */
	protected org.springframework.webflow.execution.Event doExecute(HttpServletRequest httpRequest,
			HttpServletResponse httpResponse,
			RequestContext springRequestContext,
			@SuppressWarnings("rawtypes") ProfileRequestContext profileRequestContext,
			AuthenticationRequestContext authenticationContext)
			throws AuthenticationException {
    	log.debug("Action {}: Extracting a SAML assertion from the Envelope", getId());
		@SuppressWarnings("unchecked")
		final MessageContext<Envelope> messageContext = profileRequestContext.getInboundMessageContext();
    	final TokenRequestMessageContext tokenRequestContext = tokenRequestMessageContextLookupStrategy.apply(messageContext);
    	String tokenReferenceUri = tokenRequestContext.getTokenReference();
    	if (!tokenReferenceUri.substring(0,1).equals("#")) {
    		log.debug("Action {}: Could not parse the SecurityReference: the URI should start with '#'!", getId());
    		throw new NonParsableSamlAssertionException("Could not parse the SecurityReference: the URI should start with '#'!");
    	}
        final Envelope envelope = messageContext.getMessage();
        final Assertion assertion = getAssertion(envelope, tokenReferenceUri.substring(1));

        authenticationContext.getSubcontext(AssertionContext.class, true).setAssertion(assertion);

        return ActionSupport.buildProceedEvent(this);
    }
    
    /**
     * Extracts the {@link Assertion} from the given {@link Envelope}.
     * 
     * @param message the message from which the token should be extracted
     * @param tokenReference the id for the assertion
     * @return the extracted token
     */
    private Assertion getAssertion(Envelope message, String tokenReference) throws AuthenticationException {
        final Header header = message.getHeader();

        List<XMLObject> objects = header.getUnknownXMLObjects(Security.ELEMENT_NAME);
        if (objects.size() != 1) {
        	log.debug("Action {}: More than one security elements in the header!", getId());
        	throw new AuthenticationException("More than one security elements in the header!");
        }
        objects = ((Security) objects.get(0)).getUnknownXMLObjects(Assertion.DEFAULT_ELEMENT_NAME);
        if (objects == null || objects.size() != 1) {
        	log.debug("Action {}: Invalid amount of assertions in the security header!", getId());
        	throw new NonParsableSamlAssertionException("Invalid amount of assertions in the security header!");
        }
        log.debug("Action {}: Found one SAML assertion.", getId());
        Assertion assertion = (Assertion) objects.get(0);
        if (tokenReference.equals(assertion.getID())) {
        	return assertion;
        } else {
        	throw new NonParsableSamlAssertionException("No assertion with the id=" + tokenReference + " found!");
        }
    }
    
    /**
     * An authentication exception that occurs when the SAML assertion cannot be exctracted.
     */
    public class NonParsableSamlAssertionException extends AuthenticationException {

        /** Serial version UID. */
        private static final long serialVersionUID = 6474772810183235231L;

        /**
         * Constructor.
         * 
         * @param message the message describing the exception
         */
        public NonParsableSamlAssertionException(String message) {
            super(message);
        }
    }    
}
