/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.authn;

import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.joda.time.DateTime;
import org.opensaml.messaging.context.navigate.ChildContextLookup;
import org.opensaml.saml.saml2.core.Assertion;
import org.opensaml.saml.saml2.core.Audience;
import org.opensaml.saml.saml2.core.AudienceRestriction;
import org.opensaml.saml.saml2.core.Conditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.webflow.execution.RequestContext;

import com.google.common.base.Function;

import net.shibboleth.ext.spring.webflow.Event;
import net.shibboleth.ext.spring.webflow.Events;
import net.shibboleth.idp.authn.AbstractAuthenticationAction;
import net.shibboleth.idp.authn.AuthenticationException;
import net.shibboleth.idp.authn.AuthenticationRequestContext;
import net.shibboleth.idp.profile.ActionSupport;
import net.shibboleth.idp.profile.EventIds;
import net.shibboleth.idp.profile.ProfileRequestContext;
import net.shibboleth.idp.relyingparty.RelyingPartyContext;

/**
 * An action that validates the conditions in the assertion. Lifetime and audience are checked.
 */
@Events({
    @Event(id = EventIds.PROCEED_EVENT_ID)})
public class CheckAssertionConditions extends AbstractAuthenticationAction {
		
    /** Class logger. */
    private final Logger log = LoggerFactory.getLogger(CheckAssertionConditions.class);
    
    /**
     * Strategy used to extract, and create if necessary, the {@link AssertionContext} from the
     * {@link AuthenticationRequestContext}.
     */
    private Function<AuthenticationRequestContext, AssertionContext> assertionCtxLookupStrategy;
    
    /**
     * Strategy used to look up the {@link RelyingPartyContext} associated with the given 
     * {@link ProfileRequestContext}.
     */
    private Function<ProfileRequestContext<?, ?>, RelyingPartyContext> rpContextLookupStrategy;
    
    /** Require conditions from the assertion. */
    private boolean requireConditions;
    
    /** Require audience restrictions from the assertion. */
    private boolean requireAudience;
    
    /**
     * Constructor.
     * @param conditionsRequired are conditions required from the assertion
     * @param audienceRequired is audience required from the assertion
     */
    public CheckAssertionConditions(boolean conditionsRequired, boolean audienceRequired) {
    	super();
        assertionCtxLookupStrategy = new ChildContextLookup<AuthenticationRequestContext, AssertionContext>(AssertionContext.class, false);
        rpContextLookupStrategy =
                new ChildContextLookup<ProfileRequestContext<?, ?>, RelyingPartyContext>(RelyingPartyContext.class, false);
        this.requireConditions = conditionsRequired;
        this.requireAudience = audienceRequired;
    }

    /** {@inheritDoc} */
	protected org.springframework.webflow.execution.Event doExecute(HttpServletRequest httpRequest,
			HttpServletResponse httpResponse,
			RequestContext springRequestContext,
			@SuppressWarnings("rawtypes") ProfileRequestContext profileRequestContext,
			AuthenticationRequestContext authenticationContext)
			throws AuthenticationException {
		final Assertion assertion = assertionCtxLookupStrategy.apply(authenticationContext).getAssertion();
		final RelyingPartyContext relyingPartyCtx = rpContextLookupStrategy.apply(profileRequestContext);
		Conditions conditions = assertion.getConditions();
		if (conditions == null) {
			if (this.requireConditions) {
				log.debug("Action {}: No conditions found in the assertion, but they are required.", getId());
				throw new NoConditionsInAssertionException("No conditions found in the assertion, even though they're required!");
			} else {
				log.debug("Action {}: No conditions found in the assertion, nothing left to do.", getId());
				return ActionSupport.buildProceedEvent(this);
			}
		}
		long currentTime = System.currentTimeMillis();
		long clockSkew = relyingPartyCtx.getProfileConfig().getSecurityConfiguration().getClockSkew();
		long notBefore = conditions.getNotBefore().getMillis();
		if (notBefore > currentTime + clockSkew) {
			log.debug("Action {}: This assertion cannot be consumed before {}", getId(), new DateTime(notBefore).toString());
			throw new NotYetValidAssertionException(notBefore);
		}
		long notOnOrAfter = conditions.getNotOnOrAfter().getMillis();
		if (notOnOrAfter < currentTime - clockSkew) {
			log.debug("Action {}: This assertion has been expired at {}", getId(), new DateTime(notOnOrAfter).toString());
			throw new ExpiredAssertionException(notOnOrAfter);
		}
		if (!checkAudience(conditions.getAudienceRestrictions(), relyingPartyCtx)) {
			log.debug("Action {}: This assertion is not targeted to the STS.", getId());
			throw new NotInAssertionAudienceException();
		}
		return ActionSupport.buildProceedEvent(this);
	}
	
	/**
	 * Checks whether the STS is included in the assertion's audience.
	 * @param restrictions list of audience restrictions
	 * @param relyingPartyCtx relying party context containing the provider Id
	 * @return true if included in the audience, false otherwise
	 */
	protected boolean checkAudience(List<AudienceRestriction> restrictions, final RelyingPartyContext relyingPartyCtx) {
		if (restrictions == null) {
			log.debug("Action {}: No audience restrictions in the assertion", getId());
			if (this.requireAudience) {
				return false;
			} else {
				return true;
			}
		}
		for (int i = 0; i < restrictions.size(); i++) {
			AudienceRestriction restriction = restrictions.get(i);
			List<Audience> audiences = restriction.getAudiences();
			if (audiences != null) {
				Iterator<Audience> iterator = audiences.iterator();
				while (iterator.hasNext()) {
					Audience audience = iterator.next();
					log.debug("Action {}: Found {} in the audience", getId(), audience.getAudienceURI());
					if (relyingPartyCtx.getConfiguration().getResponderEntityId().equals(audience.getAudienceURI())) {
						return true;
					}
				}
			}
		}
		return false;
	}
	
    /**
     * An authentication exception thrown when the STS is not in the audience of the assertion.
     */
    public class NotInAssertionAudienceException extends AuthenticationException {

        /** Serial version UID. */
        private static final long serialVersionUID = 6432112310213341233L;

        /**
         * Constructor.
         */
        public NotInAssertionAudienceException() {
            super("This assertion is not targeted to the STS.");
        }
    }
    
    /**
     * An authentication exception thrown when the assertion cannot be consumed yet.
     */
     public class NotYetValidAssertionException extends AuthenticationException {
    	 
         /** Serial version UID. */
         private static final long serialVersionUID = 6432112342342141233L;

         /**
          * Constructor.
          */
         public NotYetValidAssertionException(long notBefore) {
             super("This assertion cannot be consumed before " + new DateTime(notBefore).toString());
         }
     }
     
     /**
      * An authentication exception thrown when the assertion cannot be consumed anymore.
      */
      public class ExpiredAssertionException extends AuthenticationException {
     	 
          /** Serial version UID. */
    	  private static final long serialVersionUID = 6432112342342141235L;

          /**
           * Constructor.
           */
          public ExpiredAssertionException(long notOnOrAfter) {
              super("This assertion has been expired at " + new DateTime(notOnOrAfter).toString());
          }
      }     

      /**
       * An authentication exception thrown when the assertion does not have conditions even though they are required.
       */
       public class NoConditionsInAssertionException extends AuthenticationException {
      	 
           /** Serial version UID. */
     	  private static final long serialVersionUID = 6322112342342141235L;

           /**
            * Constructor.
            */
           public NoConditionsInAssertionException(String message) {
               super(message);
           }
       }     

}
