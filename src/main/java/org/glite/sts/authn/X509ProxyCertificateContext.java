/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.authn;

import java.security.cert.X509Certificate;

import javax.annotation.Nonnull;

import net.shibboleth.utilities.java.support.logic.Constraint;

import org.opensaml.messaging.context.BaseContext;

/**
 * Context, usually attached to {@link AuthenticationRequestContext}, that carries an array of {@link X509Certificate} to be
 * validated, ie. the proxy certificate chain.
 */
public class X509ProxyCertificateContext extends BaseContext {
	
    /** The certificate chain to be validated. */
    private X509Certificate[] certificateChain;

    /**
     * Gets the certificate chain to be validated.
     * 
     * @return the certificate chain to be validated
     */
    @Nonnull public X509Certificate[] getCertificateChain() {
        return certificateChain;
    }

    /**
     * Sets the certificate chain to be validated.
     * 
     * @param cert certificate chain to be validated
     * 
     * @return this context
     */
    public X509ProxyCertificateContext setCertificate(@Nonnull final X509Certificate[] certChain) {
        certificateChain = Constraint.isNotNull(certChain, "Certificate chain can not be null");
        return this;
    }
    
}
