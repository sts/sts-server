/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.authn;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.glite.sts.saml.TrustedIdpMetadataService;
import org.opensaml.messaging.context.navigate.ChildContextLookup;
import org.opensaml.saml.saml2.core.Assertion;
import org.opensaml.security.credential.Credential;
import org.opensaml.xmlsec.signature.Signature;
import org.opensaml.xmlsec.signature.support.SignatureException;
import org.opensaml.xmlsec.signature.support.SignatureValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.webflow.execution.RequestContext;

import com.google.common.base.Function;

import net.shibboleth.ext.spring.webflow.Event;
import net.shibboleth.ext.spring.webflow.Events;
import net.shibboleth.idp.authn.AbstractAuthenticationAction;
import net.shibboleth.idp.authn.AuthenticationException;
import net.shibboleth.idp.authn.AuthenticationRequestContext;
import net.shibboleth.idp.profile.ActionSupport;
import net.shibboleth.idp.profile.EventIds;
import net.shibboleth.idp.profile.ProfileRequestContext;

/**
 * An action that validates the XML signature in the assertion. 
 */
@Events({
    @Event(id = EventIds.PROCEED_EVENT_ID)})
public class CheckAssertionSignature extends AbstractAuthenticationAction {
		
    /** Class logger. */
    private final Logger log = LoggerFactory.getLogger(CheckAssertionSignature.class);
    
    /** The metadata of the trusted identity providers. */
    private TrustedIdpMetadataService idpMetadata;
    
    /**
     * Strategy used to extract, and create if necessary, the {@link AssertionContext} from the
     * {@link AuthenticationRequestContext}.
     */
    private Function<AuthenticationRequestContext, AssertionContext> assertionCtxLookupStrategy;
    
    /**
     * Constructor.
     * @param metadataService the metadata of the trusted identity providers
     */
    public CheckAssertionSignature(TrustedIdpMetadataService metadataService) {
        assertionCtxLookupStrategy = new ChildContextLookup<AuthenticationRequestContext, AssertionContext>(AssertionContext.class, false);
    	this.idpMetadata = metadataService;
    }

    /** {@inheritDoc} */
	protected org.springframework.webflow.execution.Event doExecute(HttpServletRequest httpRequest,
			HttpServletResponse httpResponse,
			RequestContext springRequestContext,
			@SuppressWarnings("rawtypes") ProfileRequestContext profileRequestContext,
			AuthenticationRequestContext authenticationContext)
			throws AuthenticationException {
		Assertion assertion = assertionCtxLookupStrategy.apply(authenticationContext).getAssertion();
		String entityId = assertion.getIssuer().getValue();
		log.debug("Action {}: Verifying the assertion provided by {}", getId(), entityId);
		Credential idpCredential = this.idpMetadata.getCredential(entityId);
		if (!signatureValidated(assertion.getSignature(), idpCredential, entityId)) {
			throw new InvalidAssertionSignatureException(assertion.getID(), entityId);
		} 
		return ActionSupport.buildProceedEvent(this);
	}
	
	/**
	 * Verify a XML digital signature.
	 * @param signature the signature from the assertion
	 * @param validatingCredential the credentials used for the validation
	 * @return true if valid, false otherwise
	 */
	protected boolean signatureValidated(Signature signature, Credential validatingCredential, String entityId) {
		SignatureValidator validator = new SignatureValidator(
				validatingCredential);
		try {
			validator.validate(signature);
		} catch (SignatureException e) {
			log.warn("The signature could not be validated for {}", entityId);
			log.debug("The stack for the failure", e);
			return false;
		}
		return true;
	}
	
    /**
     * A profile processing exception that occurs when the signature inside assertion could not be validated
     */
    public class InvalidAssertionSignatureException extends AuthenticationException {

        /** Serial version UID. */
        private static final long serialVersionUID = 6474772810183231233L;

        /**
         * Constructor.
         * 
         * @param messageId the ID of the assertion, never null
         * @param entityId the issuer of the assertion, never null
         */
        public InvalidAssertionSignatureException(String assertionId, String entityId) {
            super("Signature in assertion " + assertionId + ", issued by " + entityId
                    + " could not be validated.");
        }
    }


}
