/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.authn;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.joda.time.DateTime;
import org.opensaml.messaging.context.navigate.ChildContextLookup;
import org.opensaml.saml.saml2.core.Assertion;
import org.opensaml.saml.saml2.core.AuthnStatement;
import org.opensaml.saml.saml2.core.SubjectLocality;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.webflow.execution.RequestContext;

import com.google.common.base.Function;

import net.shibboleth.ext.spring.webflow.Event;
import net.shibboleth.ext.spring.webflow.Events;
import net.shibboleth.idp.authn.AbstractAuthenticationAction;
import net.shibboleth.idp.authn.AuthenticationException;
import net.shibboleth.idp.authn.AuthenticationRequestContext;
import net.shibboleth.idp.profile.ActionSupport;
import net.shibboleth.idp.profile.EventIds;
import net.shibboleth.idp.profile.ProfileRequestContext;
import net.shibboleth.idp.relyingparty.RelyingPartyContext;
import net.shibboleth.utilities.java.support.primitive.StringSupport;

/**
 * An action that validates the authentication statement in the assertion. Lifetime and locality are checked.
 */
@Events({
    @Event(id = EventIds.PROCEED_EVENT_ID)})
public class CheckAssertionAuthnStatement  extends AbstractAuthenticationAction {
		
    /** Class logger. */
    private final Logger log = LoggerFactory.getLogger(CheckAssertionAuthnStatement.class);
    
    /**
     * Strategy used to extract, and create if necessary, the {@link AssertionContext} from the
     * {@link AuthenticationRequestContext}.
     */
    private Function<AuthenticationRequestContext, AssertionContext> assertionCtxLookupStrategy;
    
    /**
     * Strategy used to look up the {@link RelyingPartyContext} associated with the given 
     * {@link ProfileRequestContext}.
     */
    private Function<ProfileRequestContext<?, ?>, RelyingPartyContext> rpContextLookupStrategy;
    
    /** Amount of time, in milliseconds, for which authentication is valid. */
    private long authenticationLifetime;
    
    /** Require single authentication statement from the assertion. */
    private boolean requireStatement;
    
    /** Require locality match from the authentication statement. */
    private boolean requireLocalityMatch;
    
    /** HTTP header key corresponding to the client IP address. */
    private String proxyHeaderKey;
    
    /**
     * Constructor.
     * @param authnLifetime how long authentication is valid in milliseconds, 0 means forever
     * @param statementRequired is authentication statement required from the assertion
     * @param localityRequired is locality matching required from the assertion
     * @param proxyHeader the HTTP header corresponding to the client IP address
     */
    public CheckAssertionAuthnStatement(long authnLifetime, boolean statementRequired, boolean localityRequired, String proxyHeader) {
    	super();
        assertionCtxLookupStrategy = new ChildContextLookup<AuthenticationRequestContext, AssertionContext>(AssertionContext.class, false);
        rpContextLookupStrategy =
                new ChildContextLookup<ProfileRequestContext<?, ?>, RelyingPartyContext>(RelyingPartyContext.class, false);
        this.authenticationLifetime = authnLifetime;
        this.requireStatement = statementRequired;
        this.requireLocalityMatch = localityRequired;
        this.proxyHeaderKey = proxyHeader;
    }

    /** {@inheritDoc} */
	protected org.springframework.webflow.execution.Event doExecute(HttpServletRequest httpRequest,
			HttpServletResponse httpResponse,
			RequestContext springRequestContext,
			@SuppressWarnings("rawtypes") ProfileRequestContext profileRequestContext,
			AuthenticationRequestContext authenticationContext)
			throws AuthenticationException {
		final Assertion assertion = assertionCtxLookupStrategy.apply(authenticationContext).getAssertion();
		final RelyingPartyContext relyingPartyCtx = rpContextLookupStrategy.apply(profileRequestContext);
		List<AuthnStatement> statements = assertion.getAuthnStatements();
		if (this.requireStatement && statements.size() != 1) {
			log.debug("Action {}: Invalid number of authentication statements {}", getId(), statements.size());
			throw new NoSingleAuthnStatementInAssertionException(statements.size());
		} else {
			AuthnStatement statement = statements.get(0);
			if (requireLocalityMatch) {
				verifyLocality(httpRequest, statement.getSubjectLocality());
			}
			verifyAuthnInstant(statement.getAuthnInstant().getMillis(), 
					relyingPartyCtx.getProfileConfig().getSecurityConfiguration().getClockSkew());
		}
		return ActionSupport.buildProceedEvent(this);
	}
	
	/**
	 * Verifies the remote address against the subject locality.
	 * @param httpRequest the request to obtain the remote address from
	 * @param locality the subject locality element
	 * @throws AuthenticationException if remote address cannot be accepted
	 */
	protected void verifyLocality(HttpServletRequest httpRequest, SubjectLocality locality) throws AuthenticationException {
		String localityIp = locality.getAddress();
		String remoteIp;
		if (StringSupport.trimOrNull(this.proxyHeaderKey) == null) {
			remoteIp = httpRequest.getRemoteAddr();
		} else {
			log.debug("Trying to resolve the remote IP address from the header {}", this.proxyHeaderKey);
			remoteIp = httpRequest.getHeader(this.proxyHeaderKey);
			if (remoteIp == null) {
				log.error("Could not resolve the remote address from the header {}", this.proxyHeaderKey);
				throw new InvalidRemoteAddressInAssertionException("Could not resolve the remote address");
			}
		}
		log.debug("Action {}: Comparing locality address {} vs. remote address {}", new Object[] { getId(), localityIp, remoteIp });
		if (!remoteIp.equals(localityIp)) {
			log.warn("Action {}: IP addresses did not match even though they are required, remote address {}", getId(), remoteIp);
			throw new InvalidRemoteAddressInAssertionException(localityIp, remoteIp);
		}
	}
	
	/**
	 * Verifies the authentication instant against the system time.
	 * @param issueInstant the instant when the authentication occured
	 * @param clockskew the acceptable clock skew
	 * @throws AuthenticationException if authentication instant cannot be accepted
	 */
	protected void verifyAuthnInstant(long issueInstant, long clockskew) throws AuthenticationException {
		long currentTime = System.currentTimeMillis();
        if (issueInstant + this.authenticationLifetime < currentTime - clockskew) {
        	log.debug("Action {}: Issue instant has been expired", getId());
            throw new PastAuthnInstantInAssertionException(issueInstant);
        }

        if (issueInstant > currentTime + clockskew) {
        	log.debug("Action {}: Issue instant is in the too far future", getId());
            throw new FutureAuthnInstantInAssertionException(issueInstant);
        }
	}
	
    /**
     * An authentication exception thrown when no single authentication statement is found in the assertion.
     */
    public class NoSingleAuthnStatementInAssertionException extends AuthenticationException {

        /** Serial version UID. */
        private static final long serialVersionUID = 6432111231234341233L;

        /**
         * Constructor.
         */
        public NoSingleAuthnStatementInAssertionException(int amount) {
            super("No single AuthnStatement found in the assertion, there were: " + amount + " of them.");
        }
    }
    
    /**
     * An authentication exception thrown when IP address in the authentication statement is not matching the
     * remote address.
     */
    public class InvalidRemoteAddressInAssertionException extends AuthenticationException {

        /** Serial version UID. */
        private static final long serialVersionUID = 3232111231234341233L;

        /**
         * Constructor.
         */
        public InvalidRemoteAddressInAssertionException(String message) {
        	super(message);
        }
        
        /**
         * Constructor.
         */
        public InvalidRemoteAddressInAssertionException(String localityIp, String remoteIp) {
        	super("Invalid SubjectLocality (" + localityIp + ") in AuthnStatement, remote address is " + remoteIp);
        }
    }

    /**
     * An authentication exception thrown when authentication is expired.
     */
    public class PastAuthnInstantInAssertionException extends AuthenticationException {

        /** Serial version UID. */
        private static final long serialVersionUID = 3232111231232135763L;

        /**
         * Constructor.
         */
        public PastAuthnInstantInAssertionException(long instant) {
        	super("Authentication instant is too far in the past: " + new DateTime(instant) + " and cannot be accepted.");
        }
    }

    /**
     * An authentication exception thrown when authentication is too far in the future.
     */
    public class FutureAuthnInstantInAssertionException extends AuthenticationException {

        /** Serial version UID. */
        private static final long serialVersionUID = 3232111987544135763L;

        /**
         * Constructor.
         */
        public FutureAuthnInstantInAssertionException(long instant) {
        	super("Authentication instant is in the future: " + new DateTime(instant) + " and cannot be accepted.");
        }
    }
}
