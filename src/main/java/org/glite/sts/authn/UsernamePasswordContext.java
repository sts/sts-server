/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.authn;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.opensaml.messaging.context.BaseContext;

import net.shibboleth.idp.authn.AuthenticationRequestContext;
import net.shibboleth.utilities.java.support.logic.Constraint;

/**
 * Context, usually attached to {@link AuthenticationRequestContext}, that carries a username/password pair to be
 * validated and information whether the password is stored as plaintext or hashed.
 * 
 * Based on net.shibboleth.idp.authn.UsernamePasswordContext.
 */
public class UsernamePasswordContext extends BaseContext {

    /** The username. */
    private String username;

    /** The password associated with the username. */
    private String password;
    
    /** Indicating whether the password is hashed or not. */
    private boolean digest;

    /**
     * Gets the username.
     * 
     * @return the username
     */
    @Nullable public String getUsername() {
        return this.username;
    }

    /**
     * Sets the username.
     * 
     * @param username the username
     * @return this context
     */
    public UsernamePasswordContext setUsername(@Nonnull final String username) {
        this.username = Constraint.isNotNull(username, "Username can not be null");
        return this;
    }

    /**
     * Gets the password associated with the username.
     * 
     * @return password associated with the username
     */
    @Nullable public String getPassword() {
        return this.password;
    }

    /**
     * Sets the password associated with the username.
     * 
     * @param password password associated with the username
     * @return this context
     */
    public UsernamePasswordContext setPassword(@Nonnull final String password) {
        this.password = Constraint.isNotNull(password, "Password can not be null");
        return this;
    }
    
    /**
     * Sets the password hashed indicator.
     * 
     * @param digest true if hashed, false otherwise.
     * @return this context
     */
    public UsernamePasswordContext setDigest(boolean hashed) {
    	this.digest = hashed;
    	return this;
    }
    
    /**
     * Gets the password hashed indicator.
     * 
     * @return true if password is hashed, false otherwise
     */
    public boolean isPasswordHashed() {
    	return this.digest;
    }
}