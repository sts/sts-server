/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.authn;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.xml.security.utils.Base64;
import org.ldaptive.BindOperation;
import org.ldaptive.BindRequest;
import org.ldaptive.Connection;
import org.ldaptive.Credential;
import org.ldaptive.LdapAttribute;
import org.ldaptive.LdapEntry;
import org.ldaptive.LdapException;
import org.ldaptive.SearchOperation;
import org.ldaptive.SearchRequest;
import org.ldaptive.SearchResult;
import org.ldaptive.pool.ConnectionPool;
import org.opensaml.messaging.context.navigate.ChildContextLookup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.webflow.execution.RequestContext;

import com.google.common.base.Function;

import net.shibboleth.ext.spring.webflow.Event;
import net.shibboleth.ext.spring.webflow.Events;
import net.shibboleth.idp.authn.AbstractAuthenticationAction;
import net.shibboleth.idp.authn.AuthenticationException;
import net.shibboleth.idp.authn.AuthenticationRequestContext;
import net.shibboleth.idp.profile.ActionSupport;
import net.shibboleth.idp.profile.EventIds;
import net.shibboleth.idp.profile.ProfileRequestContext;
import net.shibboleth.utilities.java.support.logic.Constraint;

/**
 * An authentication stage that validates username and password against an LDAP directory.
 */
@Events({
    @Event(id = EventIds.PROCEED_EVENT_ID),
    @Event(id = EventIds.INVALID_PROFILE_CTX)})
public class ValidateUsernamePasswordAgainstLdap extends AbstractAuthenticationAction {
		
    /** Class logger. */
    private final Logger log = LoggerFactory.getLogger(ValidateUsernamePasswordAgainstLdap.class);
    
    public static final String DEFAULT_UID_ATTRIBUTE = "uid";
    
    public static final String DEFAULT_PASSWORD_ATTRIBUTE = "userPassword";
	
    /**
     * Strategy used to extract the {@link UsernamePasswordContext} from the
     * {@link AuthenticationRequestContext}.
     */
    private Function<AuthenticationRequestContext, UsernamePasswordContext> usernameCtxLookupStrategy;
    
    /** The LDAP connection pool. */
    private ConnectionPool ldapConnectionPool;
    
    /** The base DN for the users in the LDAP directory. */
    private String ldapBaseDn;
    
    /** The DN of the user used for binding. If null, the username context is used. */
    private String ldapBindDn;
    
    /** The credential of the user used for binding, if ldapBindDn is set. */
    private Credential ldapBindCredential;
    
    /** The password attribute name in the LDAP directory. */
    private String ldapPasswordAttributeName;
    
    /** The uid attribute name in the LDAP directory. */
    private String ldapUidAttributeName;
    
    /** The hashing algorithm identifier. */
    private String ldapHashAlgorithm;
    
    /**
     * Constructor.
     * @param connectionPool the LDAP connection pool
     * @param baseDn base DN used for users in the directory
     */
    public ValidateUsernamePasswordAgainstLdap(ConnectionPool connectionPool, String baseDn) {
    	this(connectionPool, baseDn, null, null, DEFAULT_UID_ATTRIBUTE, DEFAULT_PASSWORD_ATTRIBUTE, null);
    }
    
    /**
     * Constructor.
     * @param connectionPool the LDAP connection pool
     * @param baseDn base DN used for users in the directory
     * @param bindDn the DN of the user used for binding
     * @param bindCredential the credential of the user
     */
    public ValidateUsernamePasswordAgainstLdap(ConnectionPool connectionPool, String baseDn, String bindDn, String bindCredential) {
    	this(connectionPool, baseDn, bindDn, bindCredential, DEFAULT_UID_ATTRIBUTE, DEFAULT_PASSWORD_ATTRIBUTE, null);
    }

    /**
     * Constructor.
     * @param connectionPool the LDAP connection pool
     * @param baseDn base DN used for users in the directory
     * @param bindDn the DN of the user used for binding
     * @param bindCredential the credential of the user
     * @param uid the uid attribute name in the directory
     * @param password the password attribute name in the directory
     * @param hashAlgorithm the hashing algorithm used for the passwords
     */
    public ValidateUsernamePasswordAgainstLdap(ConnectionPool connectionPool, String baseDn, String bindDn, String bindCredential, String uid, String password, String hashAlgorithm) {
    	super();
        usernameCtxLookupStrategy = new ChildContextLookup<AuthenticationRequestContext, UsernamePasswordContext>(UsernamePasswordContext.class, false);
        this.ldapConnectionPool = Constraint.isNotNull(connectionPool, "LDAP connection pool cannot be null!");
        this.ldapBaseDn = Constraint.isNotNull(baseDn, "Base DN cannot be null!");
        this.ldapBindDn = bindDn;
        if (bindCredential != null) {
        	this.ldapBindCredential = new Credential(bindCredential);
        } else {
        	this.ldapBindCredential = null;
        }
        this.ldapUidAttributeName = Constraint.isNotNull(uid, "LDAP uid attribute cannot be null!");
        this.ldapPasswordAttributeName = Constraint.isNotNull(password, "LDAP password attribute cannot be null!");
        this.ldapHashAlgorithm = hashAlgorithm;
    }

    /** {@inheritDoc} */
	protected org.springframework.webflow.execution.Event doExecute(HttpServletRequest httpRequest,
			HttpServletResponse httpResponse,
			RequestContext springRequestContext,
			@SuppressWarnings("rawtypes") ProfileRequestContext profileRequestContext,
			AuthenticationRequestContext authenticationContext)
			throws AuthenticationException {
		UsernamePasswordContext usernameContext = usernameCtxLookupStrategy.apply(authenticationContext);
		if (usernameContext == null) {
			log.debug("Action {}: No UsernamePasswordContext found, nothing left to do.", getId());
			return ActionSupport.buildEvent(this, EventIds.INVALID_PROFILE_CTX);
		}
		String username = usernameContext.getUsername();
		return logResultAndGo(ldapSearchAndCompare(usernameContext), username);
	}

	/**
	 * Writes the authentication result to the logs and proceeds to next event if authentication was
	 * successful.
	 * @param successful the authentication result
	 * @param username the uid of the user
	 * @return proceed event
	 * @throws AuthenticationException if authentication was not successful
	 */
	protected org.springframework.webflow.execution.Event logResultAndGo(boolean successful, String username) throws AuthenticationException {
		if (successful) {
			log.debug("Action {}: Successfully authenticated {}", getId(), username);
			return ActionSupport.buildProceedEvent(this);
		} else {
			log.debug("Action {}: Could not authenticate {}", getId(), username);
			throw new InvalidUsernamePasswordException(username);			
		}
	}
	
	/**
	 * Searches the LDAP for the user and matches the passwords. If the username is used
	 * for binding, then authentication succeeds without further matching.
	 * @param usernameContext the context containing the username and password
	 * @return true if authentication was successful, false otherwise 
	 */
	protected boolean ldapSearchAndCompare(UsernamePasswordContext usernameContext) {
    	String username = usernameContext.getUsername();
		Connection conn;
		try {
			conn = this.ldapConnectionPool.getConnection();
		} catch (LdapException e) {
			log.error("Action {}: Could not get a connection from the pool!", getId());
			log.debug("Caused by", e);
			return false;
		}
		String bindDn;
		Credential bindCredential;
		if (this.ldapBindDn == null) {
			bindDn = this.ldapUidAttributeName + "=" + username + "," + this.ldapBaseDn;
			if (usernameContext.isPasswordHashed()) {
				log.warn("Incoming password is hashed, ldapBindDn must be configured in order to authenticate the user!");
				return false;
			} else {
				bindCredential = new Credential(usernameContext.getPassword());
			}
		} else {
			bindDn = this.ldapBindDn;
			bindCredential = this.ldapBindCredential;
		}
		LdapEntry entry;
		try {
			conn.open();
			BindOperation bind = new BindOperation(conn);
			log.debug("Action {}: Attempting to authenticate {}", getId(), bindDn);
			bind.execute(new BindRequest(bindDn, bindCredential));
			if (this.ldapBindDn != null) {
				SearchOperation search = new SearchOperation(conn);
				SearchRequest req = new SearchRequest(this.ldapBaseDn, "(uid=" + username + ")");
				SearchResult result = search.execute(req).getResult();
				entry = result.getEntry();
			} else {
				log.debug("Action {}: BIND operation was successful, user is authenticated.", getId());
				return true;
			}
		} catch (LdapException e) {
			log.debug("Action {}: Could not access the LDAP directory: {}", getId(), e.getMessage());
			log.debug("Caused by", e);
			return false;			
		} finally {
			conn.close();
		}
		if (entry == null) {
			return false;
		}
		return validatePassword(entry.getAttribute(this.ldapPasswordAttributeName), usernameContext);
	}
	
	/**
	 * Compares the passwords given in the parameters.
	 * @param ldapPassword the password object from the LDAP directory
	 * @param usernameContext the context containing the username and password
	 * @return true if passwords match, false otherwise
	 */
	protected boolean validatePassword(LdapAttribute ldapPassword, UsernamePasswordContext usernameContext) {
		String storedPassword = ldapPassword.getStringValue();
		String givenPassword = usernameContext.getPassword();
		if (this.ldapHashAlgorithm == null) {
			return givenPassword.equals(storedPassword);
		} else if (this.ldapHashAlgorithm.equalsIgnoreCase("sha1")) {
			String hashedPassword;
			try {
				hashedPassword = calculateHash(givenPassword);
			} catch (NoSuchAlgorithmException e) {
				log.error("Could not use the SHA1 algorithm", e);
				return false;
			} catch (UnsupportedEncodingException e) {
				log.error("UTF-8 not supported in the platform!", e);
				return false;
			}
			return hashedPassword.equals(storedPassword);
		} else {
			log.error("Unsupported hashing algorithm configured: {}", this.ldapHashAlgorithm);
			return false;
		}
	}
	
	/**
	 * Calculates a SHA1 hash to the Base64-encoded string.
	 * @param text the string to be hashed
	 * @return the SHA1 hashed string, Base64-encoded
	 * @throws NoSuchAlgorithmException if SHA-1 is not supported by the platform
	 * @throws UnsupportedEncodingException if UTF-8 is not supported by the platform
	 */
	public static String calculateHash(String text) throws NoSuchAlgorithmException,
			UnsupportedEncodingException {
		MessageDigest md = MessageDigest.getInstance("SHA-1");
		byte[] sha1hash = new byte[40];
		md.update(text.getBytes("UTF-8"), 0, text.length());
		sha1hash = md.digest();
		return Base64.encode(sha1hash);
	}
	
    /**
     * A profile processing exception that occurs when the username and/or password are invalid.
     */
    public class InvalidUsernamePasswordException extends AuthenticationException {

        /** Serial version UID. */
        private static final long serialVersionUID = 6474772810183235438L;

        /**
         * Constructor.
         * 
         * @param username the username whose credentials could not be validated
         */
        public InvalidUsernamePasswordException(String username) {
            super("The password for " + username + " was invalid.");
        }
    }

}
