/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.authn;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.glite.sts.saml.idstore.AuthnRequestIdManager;
import org.joda.time.DateTime;
import org.opensaml.messaging.context.navigate.ChildContextLookup;
import org.opensaml.saml.saml2.core.Assertion;
import org.opensaml.saml.saml2.core.Subject;
import org.opensaml.saml.saml2.core.SubjectConfirmation;
import org.opensaml.saml.saml2.core.SubjectConfirmationData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.webflow.execution.RequestContext;

import com.google.common.base.Function;

import net.shibboleth.ext.spring.webflow.Event;
import net.shibboleth.ext.spring.webflow.Events;
import net.shibboleth.idp.authn.AbstractAuthenticationAction;
import net.shibboleth.idp.authn.AuthenticationException;
import net.shibboleth.idp.authn.AuthenticationRequestContext;
import net.shibboleth.idp.profile.ActionSupport;
import net.shibboleth.idp.profile.EventIds;
import net.shibboleth.idp.profile.ProfileRequestContext;
import net.shibboleth.idp.relyingparty.RelyingPartyContext;
import net.shibboleth.utilities.java.support.logic.Constraint;

/**
 * An action that validates the subject confirmation in the assertion. Bearer is currently supported.
 */
@Events({
    @Event(id = EventIds.PROCEED_EVENT_ID)})
public class CheckAssertionSubjectConfirmation extends AbstractAuthenticationAction {
		
    /** Class logger. */
    private final Logger log = LoggerFactory.getLogger(CheckAssertionSubjectConfirmation.class);
    
    /**
     * Strategy used to extract, and create if necessary, the {@link AssertionContext} from the
     * {@link AuthenticationRequestContext}.
     */
    private Function<AuthenticationRequestContext, AssertionContext> assertionCtxLookupStrategy;
    
    /**
     * Strategy used to look up the {@link RelyingPartyContext} associated with the given {@link ProfileRequestContext}.
     */
    private Function<ProfileRequestContext<?, ?>, RelyingPartyContext> rpContextLookupStrategy;
    
    /** The url expected to be in the recipient, ignored if null. */
    private String consumerServiceUrl;
    
    /** The manager object for the authentication request id storage. */
    AuthnRequestIdManager authnRequestIdManager;

    /**
     * Constructor.
     * 
     * @param consumerServiceUrl the URL to the assertion consumer service
     * @param checkInResponse should we test that the initial authentication request id is found from the storage
     * @param authnIdManager the manager class for the authentication request id storage
     */
    public CheckAssertionSubjectConfirmation(String consumerServiceUrl, boolean checkInResponse, AuthnRequestIdManager authnIdManager) {
    	super();
        assertionCtxLookupStrategy = new ChildContextLookup<AuthenticationRequestContext, AssertionContext>(AssertionContext.class, false);
        rpContextLookupStrategy =
                new ChildContextLookup<ProfileRequestContext<?, ?>, RelyingPartyContext>(RelyingPartyContext.class, false);
        this.consumerServiceUrl = consumerServiceUrl;
        if (checkInResponse) {
        	this.authnRequestIdManager = Constraint.isNotNull(authnIdManager, "Authentication request id manager cannot be null if response-to check is required!");
        } else {
        	this.authnRequestIdManager = null;
        }
    }

    /** {@inheritDoc} */
	protected org.springframework.webflow.execution.Event doExecute(HttpServletRequest httpRequest,
			HttpServletResponse httpResponse,
			RequestContext springRequestContext,
			@SuppressWarnings("rawtypes") ProfileRequestContext profileRequestContext,
			AuthenticationRequestContext authenticationContext)
			throws AuthenticationException {
		final Assertion assertion = assertionCtxLookupStrategy.apply(authenticationContext).getAssertion();
		final RelyingPartyContext relyingPartyCtx = rpContextLookupStrategy.apply(profileRequestContext);
		Subject subject = assertion.getSubject();
		if (subject == null) {
			log.debug("Action {}: Subject was not found from the assertion!", getId());
			throw new InvalidSubjectConfirmationException("Subject is not found inside the assertion!");
		}
		List<SubjectConfirmation> subjectConfirmations = assertion.getSubject().getSubjectConfirmations();
		if (subjectConfirmations != null) {
			for (int i = 0; i < subjectConfirmations.size(); i++) {
				if (verifySubjectConfirmation(subjectConfirmations.get(i), relyingPartyCtx.getProfileConfig().getSecurityConfiguration().getClockSkew())) {
					log.debug("Action {}: SubjectConfirmation supported and validated, nothing left to do.", getId());
					return ActionSupport.buildProceedEvent(this);
				} else {
					log.debug("Action {}: Ignored a non-supported SubjectConfirmation", getId());
				}
			}
		}
		throw new InvalidSubjectConfirmationException("Could not find any supported SubjectConfirmations");
	}
	
	/**
	 * Verifies that the given subject confirmation method is bearer and not expired.
	 * @param subjectConfirmation the subject confirmation to be verified
	 * @param clockSkew allowed clock skew
	 * @return true if valid, false otherwise
	 * @throws AuthenticationException
	 */
	protected boolean verifySubjectConfirmation(SubjectConfirmation subjectConfirmation, long clockSkew) throws AuthenticationException {
		if (SubjectConfirmation.METHOD_BEARER.equals(subjectConfirmation.getMethod())) {
			SubjectConfirmationData confirmationData = subjectConfirmation.getSubjectConfirmationData();
			if (confirmationData != null) {
				log.debug("Action {}: A valid bearer confirmation method was found.", getId());
				long currentTime = System.currentTimeMillis();
				long expirationTime = confirmationData.getNotOnOrAfter().getMillis(); 
				if (currentTime - clockSkew > expirationTime) {
					log.debug("Action {}: A bearer method was found, but it was expired.", getId());
					throw new InvalidSubjectConfirmationException(expirationTime);
				}
				String recipient = confirmationData.getRecipient();
				if (this.consumerServiceUrl != null && recipient != null && !this.consumerServiceUrl.equals(recipient)) {
					log.debug("Action {}: This subject confirmation is not targeted to the STS", getId());
					throw new InvalidSubjectConfirmationException("This subject confirmation is not targeted to this STS");
				}
				if (this.authnRequestIdManager != null) {
					log.debug("Action {}: Authentication request id must be found from the local storage", getId());
					if (!this.authnRequestIdManager.isAuthnRequestIdValid(confirmationData.getInResponseTo())) {
						log.debug("Action {}: Authentication request id was not found from the local storage", getId());
						throw new InvalidSubjectConfirmationException("Initial authentication request from the STS was not found or is expired");
					}
				}
				return true;
			}
		}
		return false;
	}
	
    /**
     * An authentication exception thrown when the subject confirmation element is not supported or has been expired.
     */
    public class InvalidSubjectConfirmationException extends AuthenticationException {

        /** Serial version UID. */
        private static final long serialVersionUID = 6432112310183231233L;

        /**
         * Constructor.
         * 
         * @param message the message describing the cause for the exception
         */
        public InvalidSubjectConfirmationException(String message) {
            super(message);
        }
        
        /**
         * Constructor.
         * 
         * @param expirationTime time in millis when the confirmation method was expired
         */
        public InvalidSubjectConfirmationException(long expirationTime) {
        	super("Valid confirmation method was found, but it was expired on " + new DateTime(expirationTime).toString());
        }
    }

}
