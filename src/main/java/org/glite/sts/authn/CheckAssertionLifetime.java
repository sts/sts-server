/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.authn;

import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.joda.time.DateTime;
import org.opensaml.messaging.context.navigate.ChildContextLookup;
import org.opensaml.saml.saml2.core.Assertion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.webflow.execution.RequestContext;

import com.google.common.base.Function;

import net.shibboleth.ext.spring.webflow.Event;
import net.shibboleth.ext.spring.webflow.Events;
import net.shibboleth.idp.authn.AbstractAuthenticationAction;
import net.shibboleth.idp.authn.AuthenticationException;
import net.shibboleth.idp.authn.AuthenticationRequestContext;
import net.shibboleth.idp.profile.ActionSupport;
import net.shibboleth.idp.profile.EventIds;
import net.shibboleth.idp.profile.ProfileRequestContext;
import net.shibboleth.idp.relyingparty.RelyingPartyContext;
import net.shibboleth.utilities.java.support.component.UnmodifiableComponentException;

/** An action that checks that the assertion should be considered valid based upon when it was issued. 
 * Based on {@link net.shibboleth.idp.profile.impl.CheckMessageLifetime}. */
@Events({
    @Event(id = EventIds.PROCEED_EVENT_ID)})
public class CheckAssertionLifetime extends AbstractAuthenticationAction {
		
    /** Class logger. */
    private final Logger log = LoggerFactory.getLogger(CheckAssertionLifetime.class);
    
    /** Amount of time, in milliseconds, for which a assertion is valid. Default value: 5 minutes */
    private long assertionLifetime;
	
    /**
     * Strategy used to extract, and create if necessary, the {@link AssertionContext} from the
     * {@link AuthenticationRequestContext}.
     */
    private Function<AuthenticationRequestContext, AssertionContext> assertionCtxLookupStrategy;
    
    /**
     * Strategy used to look up the {@link RelyingPartyContext} associated with the given {@link ProfileRequestContext}.
     */
    private Function<ProfileRequestContext<?, ?>, RelyingPartyContext> rpContextLookupStrategy;
    
    /**
     * Constructor.
     * @param assertionLifetimeInMinutes how long the assertions can be used after their issuance
     */
    public CheckAssertionLifetime(int assertionLifetimeInMinutes) {
    	super();
        assertionLifetime = TimeUnit.MILLISECONDS.convert(assertionLifetimeInMinutes, TimeUnit.MINUTES);
        assertionCtxLookupStrategy = new ChildContextLookup<AuthenticationRequestContext, AssertionContext>(AssertionContext.class, false);
        rpContextLookupStrategy =
                new ChildContextLookup<ProfileRequestContext<?, ?>, RelyingPartyContext>(RelyingPartyContext.class, false);

    }

    /** {@inheritDoc} */
	protected org.springframework.webflow.execution.Event doExecute(HttpServletRequest httpRequest,
			HttpServletResponse httpResponse,
			RequestContext springRequestContext,
			@SuppressWarnings("rawtypes") ProfileRequestContext profileRequestContext,
			AuthenticationRequestContext authenticationContext)
			throws AuthenticationException {

        final RelyingPartyContext relyingPartyCtx = rpContextLookupStrategy.apply(profileRequestContext);
		Assertion assertion = assertionCtxLookupStrategy.apply(authenticationContext).getAssertion();
		final long issueInstant = assertion.getIssueInstant().getMillis();
        if (issueInstant <= 0) {
        	log.debug("Action {}: No issue instant found", getId());
        	throw new NoIssueInstantException(assertion.getID());
        }

        final long clockskew = relyingPartyCtx.getProfileConfig().getSecurityConfiguration().getClockSkew();
        final long currentTime = System.currentTimeMillis();

        if (issueInstant < currentTime - clockskew) {
        	log.debug("Action {}: Issue instant has been expired", getId());
            throw new PastAssertionException(assertion.getID(), issueInstant);
        }

        if (issueInstant > currentTime + assertionLifetime + clockskew) {
        	log.debug("Action {}: Issue instant is in the too far future", getId());
            throw new FutureAssertionException(assertion.getID(), issueInstant);
        }

		return ActionSupport.buildProceedEvent(this);
	}
	
    /**
     * Gets the amount of time, in milliseconds, for which an assertion is valid.
     * 
     * @return amount of time, in milliseconds, for which an assertion is valid
     */
    public long getAssertionLifetime() {
        return assertionLifetime;
    }

    /**
     * Sets the amount of time, in milliseconds, for which an assertion is valid.
     * 
     * @param lifetime amount of time, in milliseconds, for which an assertion is valid
     */
    public synchronized void setAssertionLifetime(long lifetime) {
        if (isInitialized()) {
            throw new UnmodifiableComponentException("Action " + getId()
                    + ": Assertion liftime can not be changed after action has been initialized");
        }
        assertionLifetime = lifetime;
    }
    
    /**
     * A profile processing exception that occurs when the assertion was issued from a point in time to far in the
     * future.
     */
    public class FutureAssertionException extends AuthenticationException {

        /** Serial version UID. */
        private static final long serialVersionUID = -6474772810183213211L;

        /**
         * Constructor.
         * 
         * @param messageId the ID of the assertion, never null
         * @param instant the issue instant of the assertion in milliseconds since the epoch
         */
        public FutureAssertionException(String assertionId, long instant) {
            super("Assertion " + assertionId + " was issued on " + new DateTime(instant).toString()
                    + " and is not yet valid.");
        }
    }

    /**
     * A profile processing exception that occurs when the assertion was issued from a point in time to far in the
     * past.
     */
    public class PastAssertionException extends AuthenticationException {

        /** Serial version UID. */
        private static final long serialVersionUID = 6474772810183231233L;

        /**
         * Constructor.
         * 
         * @param messageId the ID of the assertion, never null
         * @param instant the issue instant of the assertion in milliseconds since the epoch
         */
        public PastAssertionException(String assertionId, long instant) {
            super("Assertion " + assertionId + " was issued on " + new DateTime(instant).toString()
                    + " is now considered expired.");
        }
    }
    
    /**
     * A profile processing exception that occurs when the assertion did not contain issue instant.
     */
    public class NoIssueInstantException extends AuthenticationException {

        /** Serial version UID. */
        private static final long serialVersionUID = 6474772810183231333L;

        /**
         * Constructor.
         * 
         * @param messageId the ID of the assertion, never null
         */
        public NoIssueInstantException(String assertionId) {
            super("Assertion " + assertionId + " did not contain issue instant.");
        }
    }

}
