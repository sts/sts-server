/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.authn;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import net.shibboleth.utilities.java.support.logic.Constraint;

import org.opensaml.messaging.context.BaseContext;
import org.opensaml.saml.saml2.core.Assertion;

/**
 * Context, usually attached to {@link AuthenticationRequestContext}, that carries a SAML assertion to be
 * validated.
 */
public class AssertionContext extends BaseContext {

    /** The assertion. */
    private Assertion assertion;

    /**
     * Gets the assertion.
     * 
     * @return the assertion
     */
    @Nullable public Assertion getAssertion() {
        return assertion;
    }

    /**
     * Sets the assertion.
     * 
     * @param samlAssertion the assertion
     * @return this context
     */
    public AssertionContext setAssertion(@Nonnull final Assertion samlAssertion) {
        assertion = Constraint.isNotNull(samlAssertion, "Assertion can not be null");
        return this;
    }
}
