/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.authn;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.shibboleth.idp.authn.AbstractAuthenticationAction;
import net.shibboleth.idp.authn.AuthenticationException;
import net.shibboleth.idp.authn.AuthenticationRequestContext;
import net.shibboleth.idp.profile.ActionSupport;
import net.shibboleth.idp.profile.ProfileRequestContext;
import net.shibboleth.utilities.java.support.primitive.StringSupport;

import org.glite.sts.profile.TokenRequestMessageContext;
import org.opensaml.core.xml.XMLObject;
import org.opensaml.messaging.context.MessageContext;
import org.opensaml.messaging.context.navigate.ChildContextLookup;
import org.opensaml.soap.soap11.Envelope;
import org.opensaml.soap.soap11.Header;
import org.opensaml.soap.wssecurity.Password;
import org.opensaml.soap.wssecurity.Security;
import org.opensaml.soap.wssecurity.Username;
import org.opensaml.soap.wssecurity.UsernameToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.webflow.execution.RequestContext;

import com.google.common.base.Function;

/**
 * An authentication stage that extracts a username/password from the WSS Username/Password attached to a SOAP message.
 * As should be obvious, this assumes that the inbound message is a SOAP {@link Envelope}.
 * 
 * Based on {@link net.shibboleth.idp.authn.impl.ExtractUsernamePasswordFromWssToken}.
 * 
 * @Events({
 *  @Event(id = EventIds.PROCEED_EVENT_ID)})
 */
public class ExtractUsernamePasswordFromWssToken extends AbstractAuthenticationAction {
	
	/** Full URI for password digest in the username token. */
	public static final String PASSWORD_DIGEST = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest";
	
	/** Full URI for password text in the username token. */
	public static final String PASSWORD_TEXT = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText";
	
    /** Class logger. */
    private final Logger log = LoggerFactory.getLogger(ExtractUsernamePasswordFromWssToken.class);
	
    /**
     * Strategy used to look up the {@link TokenRequestMessageContext} associated with the inbound message context.
     */
    private Function<MessageContext<Envelope>, TokenRequestMessageContext> tokenRequestMessageContextLookupStrategy;
    
    /**
     * Constructor.
     */
    public ExtractUsernamePasswordFromWssToken() {
    	super();
        tokenRequestMessageContextLookupStrategy =
                new ChildContextLookup<MessageContext<Envelope>, TokenRequestMessageContext>(TokenRequestMessageContext.class,
                        false);
    }
	
    /** {@inheritDoc} */
	protected org.springframework.webflow.execution.Event doExecute(HttpServletRequest httpRequest,
			HttpServletResponse httpResponse,
			RequestContext springRequestContext,
			@SuppressWarnings("rawtypes") ProfileRequestContext profileRequestContext,
			AuthenticationRequestContext authenticationContext)
			throws AuthenticationException {

		@SuppressWarnings("unchecked")
		final MessageContext<Envelope> messageContext = profileRequestContext.getInboundMessageContext();
    	final TokenRequestMessageContext tokenRequestContext = tokenRequestMessageContextLookupStrategy.apply(messageContext);
    	String tokenReferenceUri = tokenRequestContext.getTokenReference();
    	if (!tokenReferenceUri.substring(0,1).equals("#")) {
    		log.debug("Action {}: Could not parse the SecurityReference: the URI should start with '#'!", getId());
    		throw new NonParsableUsernamePasswordException("Could not parse the SecurityReference: the URI should start with '#'!");
    	}
        final Envelope inboundMessage = messageContext.getMessage();
        final UsernameToken usernameToken = getUsernameToken(inboundMessage, tokenReferenceUri.substring(1));
        final UsernamePasswordContext usernameContext = authenticationContext.getSubcontext(UsernamePasswordContext.class, true);
        extractUsernamePassword(usernameToken, usernameContext);
        return ActionSupport.buildProceedEvent(this);
    }

    /**
     * Extracts the {@link UsernameToken} from the given {@link Envelope}.
     * 
     * @param message the message from which the token should be extracted
     * @param tokenReference the id for the username token
     * @return the extracted token
     */
    private UsernameToken getUsernameToken(Envelope message, String tokenReference) throws AuthenticationException {
    	log.debug("Action {}: Looking for the username token with id {}", getId(), tokenReference);
        final Header header = message.getHeader();

        List<XMLObject> objects = header.getUnknownXMLObjects(Security.ELEMENT_NAME);
        if (objects.size() != 1) {
        	throw new AuthenticationException("More than one security elements in the header!");
        } else {
        	objects = ((Security) objects.get(0)).getUnknownXMLObjects(UsernameToken.ELEMENT_NAME);
        	if (objects.size() != 1) {
        		throw new NonParsableUsernamePasswordException("No single UsernameToken found in the security headers!");
        	} else {
        		UsernameToken usernameToken = (UsernameToken) objects.get(0);
        		if (tokenReference.equals(usernameToken.getWSUId())) {
        			return usernameToken;
        		} else {
        			throw new NonParsableUsernamePasswordException("No UsernameToken with id=" + tokenReference + " found in the security headers!");
        		}
        	}
        }
    }

    /**
     * Extracts a username/password from the given {@link UsernameToken}.
     * 
     * @param usernameToken the token from which the username/password should be extracted
     */
    private void extractUsernamePassword(UsernameToken usernameToken, final UsernamePasswordContext usernameContext) throws AuthenticationException {
        final Username username = usernameToken.getUsername();
        if (username == null || StringSupport.trimOrNull(username.getValue()) == null) {
        	throw new NonParsableUsernamePasswordException("Could not extract username from the token");
        }
        final List<XMLObject> passwords = usernameToken.getUnknownXMLObjects(Password.ELEMENT_NAME);
        if (passwords.size() != 1) {
        	throw new NonParsableUsernamePasswordException("Not one single password elements found from the token");
        }
        Password password = (Password) passwords.get(0);
        if (password.getType() == null || StringSupport.trimOrNull(password.getValue()) == null) {
        	throw new NonParsableUsernamePasswordException("Could not get the password value from the token");        	
        }
        usernameContext.setUsername(username.getValue()).setPassword(password.getValue());
        if (password.getType().equals(PASSWORD_DIGEST)) {
        	log.debug("Setting the password digest to true");
        	usernameContext.setDigest(true);
        } else if (password.getType().equals(PASSWORD_TEXT)) {
        	log.debug("Setting the password digest to false");
        	usernameContext.setDigest(false);
        } else {
        	log.warn("Unsupported password type {}", password.getType());
        	throw new NonParsableUsernamePasswordException("Unsupported password type in the token!");
        }
    }
    
    /**
     * An authentication exception that occurs when the username and/or password cannot be parsed.
     */
    public class NonParsableUsernamePasswordException extends AuthenticationException {

        /** Serial version UID. */
        private static final long serialVersionUID = 6474772810183235231L;

        /**
         * Constructor.
         * 
         * @param message the message describing the exception
         */
        public NonParsableUsernamePasswordException(String message) {
            super(message);
        }
    }
}