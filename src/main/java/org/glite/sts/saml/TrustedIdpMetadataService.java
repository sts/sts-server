/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.saml;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;

import org.opensaml.core.xml.config.XMLObjectProviderRegistrySupport;
import org.opensaml.core.xml.io.Unmarshaller;
import org.opensaml.core.xml.io.UnmarshallingException;
import org.opensaml.saml.common.SAMLObjectBuilder;
import org.opensaml.saml.common.xml.SAMLConstants;
import org.opensaml.saml.saml2.core.IDPEntry;
import org.opensaml.saml.saml2.core.IDPList;
import org.opensaml.saml.saml2.metadata.EntityDescriptor;
import org.opensaml.saml.saml2.metadata.IDPSSODescriptor;
import org.opensaml.saml.saml2.metadata.KeyDescriptor;
import org.opensaml.saml.saml2.metadata.SingleSignOnService;
import org.opensaml.security.credential.Credential;
import org.opensaml.security.x509.BasicX509Credential;
import org.opensaml.security.x509.X509Support;
import org.opensaml.xmlsec.signature.KeyInfo;
import org.opensaml.xmlsec.signature.X509Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import net.shibboleth.idp.service.AbstractReloadableService;
import net.shibboleth.utilities.java.support.logic.Constraint;
import net.shibboleth.utilities.java.support.xml.ParserPool;
import net.shibboleth.utilities.java.support.xml.XMLParserException;

/**
 * This class contains the list of trusted Identity Providers, used for generating ECP authentication requests and
 * validating the SAML security tokens. 
 */
public class TrustedIdpMetadataService extends AbstractReloadableService {

	/** Class logger. */
	private final Logger log = LoggerFactory.getLogger(TrustedIdpMetadataService.class);
	
	public static final String METADATA_SCHEMA_FILE = "saml-schema-metadata-2.0.xsd";
	
	/** The list of trusted Identity Providers. */
	private IDPList idpList;
	
	/** The map of trusted Identity Providers and their credentials. */
	private Map<String, Credential> credentials;
	
	private ParserPool parserPool;
	
	public TrustedIdpMetadataService(String metadataFolder, ParserPool pool, Timer timer, long frequency) {
		super();
		this.parserPool = Constraint.isNotNull(pool, "Parser pool can not be null");
		this.credentials = new HashMap<String, Credential>();
		this.idpList = buildIDPListObject();
		super.setId(getClass().getName());
		super.setReloadCheckDelay(frequency);
		super.setReloadTaskTimer(timer);
		this.parseMetadataFolder(metadataFolder);
	}
	
	/**
	 * Gets the list of trusted Identity Providers.
	 * @return the list of trusted Identity Providers.
	 */
	public IDPList getTrustedIdpList() {
		return this.idpList;
	}
	
	/**
	 * Gets the credential for a given Identity Provider.
	 * @return the credential found from the metadata
	 */
	public Credential getCredential(String entityId) {
		return this.credentials.get(entityId);
	}
	
	/**
	 * Loads a list of trusted Identity Providers and their signing credentials from a folder.
	 * @param idpMetadataFile the folder name containing metadata files
	 */
	private void parseMetadataFolder(String metadataFolder) {
		File folder = new File(metadataFolder);
		String[] metadataFiles = folder.list();
		if (metadataFiles == null) {
			log.warn("No files found from the folder {}, no trusted Identity Providers.", metadataFolder);
		} else {
			for (String filename : metadataFiles) {			
				filename = metadataFolder + "/" + filename;
				log.debug("Extracting entry from {}", filename);
				extractEntry(filename);	
			}
		}
	}
	
	/**
	 * Builds an {@link IDPList} object.
	 * @return an empty idp list
	 */
	protected IDPList buildIDPListObject() {
		@SuppressWarnings("unchecked")
		SAMLObjectBuilder<IDPList> idpListBuilder = (SAMLObjectBuilder<IDPList>) XMLObjectProviderRegistrySupport.getBuilderFactory().getBuilder(IDPList.DEFAULT_ELEMENT_NAME);
		return idpListBuilder.buildObject();
	}
	
	/**
	 * Builds an {@link IDPEntry} object.
	 * @return an empty idp entry
	 */
	protected IDPEntry buildIDPEntryObject() {
		@SuppressWarnings("unchecked")
		SAMLObjectBuilder<IDPEntry> idpEntryBuilder = (SAMLObjectBuilder<IDPEntry>) XMLObjectProviderRegistrySupport.getBuilderFactory().getBuilder(IDPEntry.DEFAULT_ELEMENT_NAME);
		return idpEntryBuilder.buildObject();
	}
	
	/**
	 * Extract an IDPentry that indicates ECP support from IdP metadata, and its
	 * signing credential, both read from a file.
	 * 
	 * @param idpMetadataFile the file containing the IdP metadata
	 */
	public void extractEntry(String idpMetadataFile) {
		EntityDescriptor ed = parseMetadata(idpMetadataFile);
		if (ed == null) {
			log.warn("File {} did not contain any metadata", idpMetadataFile);
			return;
		}
		String entityId = ed.getEntityID();
		IDPEntry entry = buildIDPEntryObject();

		// IF the ECP profile is the only SSO Profile that uses SOAP, then this
		// information can be relied on.
		IDPSSODescriptor ssoDescriptor = ed.getIDPSSODescriptor(SAMLConstants.SAML20P_NS);
		X509Certificate certificate = extractSigningCertificate(ssoDescriptor);
		if (certificate != null) {
			log.debug("Added a credential to {}", entityId);
			this.credentials.put(entityId, new BasicX509Credential(certificate));
		} else {
			log.debug("Could not find a credential for {}", entityId);
		}
		
		List<SingleSignOnService> list = ssoDescriptor.getSingleSignOnServices(); // = list of all
																// sso services
																// including ECP
		// https://wiki.shibboleth.net/confluence/display/SHIB2/IdP+ECP+Extension
		// Find ECP support.
		for (SingleSignOnService singleSignOnService : list) {

			if (singleSignOnService.getBinding().equals(SAMLConstants.SAML2_SOAP11_BINDING_URI)) {
				entry.setProviderID(entityId); // or getID(), find out
														// if either is MUST and
														// get that.
				entry.setLoc(singleSignOnService.getLocation());
				entry.setName("ECP");
				this.idpList.getIDPEntrys().add(entry);
				log.info("Added an IdP {} from file {}", entry.getProviderID(), idpMetadataFile);
			}
		}
	}

	protected X509Certificate extractSigningCertificate(IDPSSODescriptor ssoDescriptor) {
		try {
			List<KeyDescriptor> keyDescriptors = ssoDescriptor.getKeyDescriptors();
			if (keyDescriptors != null && keyDescriptors.size() > 0) {
				KeyInfo keyInfo = keyDescriptors.get(0).getKeyInfo();
				List<X509Data> x509datas = keyInfo.getX509Datas();
				if (x509datas != null && x509datas.size() > 0) {
					List<org.opensaml.xmlsec.signature.X509Certificate> certificates = x509datas.get(0).getX509Certificates();
					if  (certificates != null && certificates.size() > 0) {
						return X509Support.decodeCertificate(certificates.get(0).getValue());
					}
				}
			}
		} catch (CertificateException e) {
			log.debug("Could not extract a certificate from the metadata", e);
		}
		return null;
	}
	
	/**
	 * Parse a String of Metadata (An entitydescriptor (Metadatastring could
	 * contain many of these, cant handle that yet, I think.)) and return the
	 * resulting Entitydescriptor object.
	 * 
	 * @param metadataString
	 * @return
	 */
	public EntityDescriptor parseMetadata(String idpMetadataFile) {

		InputStream fileInput;
		try {
			fileInput = new FileInputStream(idpMetadataFile);
		} catch (FileNotFoundException e2) {
			return null;
		}

		// TODO: Validate that the file contains metadata.
		//if (ValidateXML.isValidEntityDescriptor(new ByteArrayInputStream(entityDescriptorString.getBytes())) == false)
			//return null;

		Element element;
		try {
			Document doc = parserPool.parse(fileInput);
			if (doc == null) {
				return null;
			}
			element = doc.getDocumentElement();
		} catch (XMLParserException e1) {
			log.debug("Could not parse the file {}", fileInput, e1);
			return null;
		}
		
		Unmarshaller unmarshaller= XMLObjectProviderRegistrySupport.getUnmarshallerFactory().getUnmarshaller(EntityDescriptor.DEFAULT_ELEMENT_NAME);

		try {
			return (EntityDescriptor) unmarshaller.unmarshall(element);
		} catch (UnmarshallingException e) {
			log.debug("Could not unmarshall the element from {}", idpMetadataFile);
		}
		return null;
	}

	/** {@inheritDoc} */
	protected boolean shouldReload() {
		// TODO Auto-generated method stub
		return false;
	}
}
