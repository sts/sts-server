/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.saml.idstore;

/**
 * This interface defines methods for the authentication request id storage service. 
 */
public interface StorageService {

	/**
	 * Puts the given identifier to the storage.
	 * @param authnRequestId the id to be stored
	 * @param lifetime the lifetime in millis
	 */
	public void put(String authnRequestId, long lifetime);
	
	/**
	 * Removes the given id from the storage.
	 * @param id the identifier to be removed
	 */
	public void remove(String id);
	
	/**
	 * Checks whether the given id is found from the storage.
	 * @param id the identifier to be checked
	 * @return true if the gived id was found, false otherwise
	 */
	public boolean exists(String id);
	
}
