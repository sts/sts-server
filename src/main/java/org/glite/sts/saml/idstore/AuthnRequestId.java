/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.saml.idstore;

/**
 * This interface defines the object for the authentication request identifiers.
 */
public interface AuthnRequestId {
	
	/**
	 * Checks whether this identifier should still be stored.
	 * @return true if valid, false otherwise
	 */
	public boolean isValid();
	
	/**
	 * Gets the authentication request id.
	 * @return the authentication request id
	 */
	public String getId();

}
