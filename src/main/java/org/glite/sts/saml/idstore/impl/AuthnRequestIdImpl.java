/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.saml.idstore.impl;

import org.glite.sts.saml.idstore.AuthnRequestId;

/**
 * An implementation for {@link AuthnRequestId}.
 */
public class AuthnRequestIdImpl implements AuthnRequestId {
	
	/** The id of this {@link AuthnRequestId}. */
	private String id;
	
	/** Expiration time in millis. */
	private long expirationTime;
	
	/**
	 * Constructor.
	 * @param identifier the id of this {@link AuthnRequestId}
	 * @param lifetime the lifetime in millis
	 */
	public AuthnRequestIdImpl(String identifier, long lifetime) {
		this.id = identifier;
		this.expirationTime = System.currentTimeMillis() + lifetime;
	}

	/** {@inheritDoc} */
	public boolean isValid() {
		return (this.expirationTime - System.currentTimeMillis()) > 0;
	}

	/** {@inheritDoc} */
	public String getId() {
		return this.id;
	}

}
