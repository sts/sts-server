/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.saml.idstore;

import net.shibboleth.utilities.java.support.logic.Constraint;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A manager class for the {@link StorageService} implementation. 
 */
public class AuthnRequestIdManager {
	
	/** Logging. */
    private static final Logger log = LoggerFactory.getLogger(AuthnRequestIdManager.class);
    
    /** The implementation for the {@link StorageService}. */
    private StorageService idStorageService;
    
    /** The lifetime of the {@link AuthnRequestId}s. */
    private long idLifetime;
    
    /**
     * Constructor.
     * @param storageService the {@link StorageService} implementation
     * @param lifetime the lifetime of the {@link AuthnRequestId}s
     */
    public AuthnRequestIdManager(StorageService storageService, long lifetime) {
    	this.idStorageService = Constraint.isNotNull(storageService, "The storage service cannot be null!");
    	this.idLifetime = lifetime;
    }
    
    /**
     * Stores an authentication request identifier to the storage.
     * @param id the authentication request identifier
     */
    public void storeAuthnRequestId(String id) {
    	log.debug("Storing authentication request with id {} for {} millis", id, this.idLifetime);
    	this.idStorageService.put(id, this.idLifetime);
    }
    
    /**
     * Checks whether the given authentication request identifier is found from the storage. If found, it's
     * deleted from the storage.
     * @param id the authentication request identifier
     * @return true if found, false otherwise
     */
    public boolean isAuthnRequestIdValid(String id) {
    	if (this.idStorageService.exists(id)) {
    		log.debug("Removing authentication request {} from the storage", id);
    		this.idStorageService.remove(id);
    		return true;
    	} else {
    		log.info("No authentication request {} found from the storage", id);
    		return false;
    	}
    }

}
