/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.saml.idstore.impl;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Timer;
import java.util.TimerTask;

import org.glite.sts.saml.idstore.AuthnRequestId;
import org.glite.sts.saml.idstore.StorageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * An in-memory implementation for {@link StorageService}. The invalid {@link AuthnRequestId}s are
 * removed in the given frequency.
 */
public class InMemoryStorageService implements StorageService {

	/** Logging. */
	private final Logger log = LoggerFactory.getLogger(InMemoryStorageService.class);
	
	/** The storage table. */
	private Hashtable<String, AuthnRequestId> authnRequestIds;

	/**
	 * Constructor.
	 * @param timer the scheduler
	 * @param frequency the frequency for sweeping the expired identifiers
	 */
	public InMemoryStorageService(Timer timer, long frequency) {
		log.info("Building a new Storage Service with sweeping frequency of {}", frequency);
		this.authnRequestIds = new Hashtable<String, AuthnRequestId>();
		timer.schedule(new MemoryCleaningTask(), 0, frequency);
	}
	
	/** {@inheritDoc} */
	public void put(String id, long lifetime) {
		AuthnRequestId authnRequestId = new AuthnRequestIdImpl(id, lifetime);
		log.debug("Putting an id {} to the storage", authnRequestId.getId());
		this.authnRequestIds.put(authnRequestId.getId(), authnRequestId);
	}

	/** {@inheritDoc} */
	public void remove(String id) {
		log.debug("Removing an id {} from the storage", id);
		this.authnRequestIds.remove(id);
	}

	/** {@inheritDoc} */
	public boolean exists(String id) {
		if (id == null) {
			return false;
		}
		if (this.authnRequestIds.containsKey(id)) {
			return true;
		} else {
			log.debug("Id {} not found", id);
			return false;
		}
	}

	/**
	 * The task for cleaning the expired identifiers from the storage.
	 */
	class MemoryCleaningTask extends TimerTask {

		@Override
		public void run() {
			log.debug("Running the cleaning task for the expired identifiers");
			Enumeration<AuthnRequestId> enumeration = authnRequestIds.elements();
			while (enumeration.hasMoreElements()) {
				AuthnRequestId authnRequestId = enumeration.nextElement();
				if (!authnRequestId.isValid()) {
					log.debug("Cleaning the id {} from the storage", authnRequestId.getId());
					authnRequestIds.remove(authnRequestId.getId());
				}
			}
		}
		
	}
}
