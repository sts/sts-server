/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.profile;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.opensaml.core.xml.XMLObject;
import org.opensaml.soap.soap11.Envelope;
import org.opensaml.soap.soap11.Header;
import org.opensaml.soap.wsaddressing.To;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.webflow.execution.RequestContext;

import net.shibboleth.ext.spring.webflow.Event;
import net.shibboleth.ext.spring.webflow.Events;
import net.shibboleth.idp.profile.AbstractProfileAction;
import net.shibboleth.idp.profile.ActionSupport;
import net.shibboleth.idp.profile.EventIds;
import net.shibboleth.idp.profile.ProfileException;
import net.shibboleth.idp.profile.ProfileRequestContext;

/** Checks whether the inbound SOAP envelope is targeted to this STS. */
@Events({
    @Event(id = EventIds.PROCEED_EVENT_ID)})
public class CheckRecipient extends AbstractProfileAction<Envelope, Object> {
	
    /** Class logger. */
    private final Logger log = LoggerFactory.getLogger(CheckAction.class);
    
    /** The required recipient identifier, if null this is not required. */
    private String recipientId;
    
    /**
     * Constructor.
     * @param expectedId if set, this is forced from the incoming messages
     */
    public CheckRecipient(String expectedId) {
    	this.recipientId = expectedId;
    }
    
    /** {@inheritDoc} */
    protected org.springframework.webflow.execution.Event doExecute(final HttpServletRequest httpRequest, final HttpServletResponse httpResponse,
            final RequestContext springRequestContext,
            final ProfileRequestContext<Envelope, Object> profileRequestContext) throws ProfileException {

        final Envelope envelope = profileRequestContext.getInboundMessageContext().getMessage();
        String recipient = getRecipientFromEnvelope(envelope);
        if (this.recipientId == null) {
       		log.debug("Action {}: Recipient is not required.", getId());
        } else {
        	if (!this.recipientId.equals(recipient)) {
        		log.warn("Action {}: Ignoring the request, wrong target ({})", getId(), recipient);
        		throw new InvalidRecipientException("This envelope is not targeted to this STS!");
        	}
        }
        return ActionSupport.buildProceedEvent(this);
    }
    
    protected String getRecipientFromEnvelope(final Envelope envelope) throws ProfileException {
        final Header header = envelope.getHeader();
        if (header == null) {
            throw new ProfileException("Header was not found from the envelope!");
        } else {
            List<XMLObject> objects = header.getUnknownXMLObjects(To.ELEMENT_NAME);
            if (objects.size() == 1) {
            	To recipient = (To) objects.get(0);
            	return recipient.getValue();
            } else {
            	throw new ProfileException("No single To -element found from the envelope header!");
            }
        }
    }

    /** Exception thrown when the incoming message was not targeted to this STS. */
    public static class InvalidRecipientException extends ProfileException {

        /** Serial version UID. */
        private static final long serialVersionUID = -872917213127634555L;

        /**
         * Constructor.
         * 
         * @param message exception message
         */
        public InvalidRecipientException(String message) {
            super(message);
        }
    }

}
