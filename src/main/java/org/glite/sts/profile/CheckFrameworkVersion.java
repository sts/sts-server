/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.profile;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.glite.sts.profile.soap.xmlobject.Framework;
import org.opensaml.core.xml.XMLObject;
import org.opensaml.soap.soap11.Envelope;
import org.opensaml.soap.soap11.Header;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.webflow.execution.RequestContext;

import com.google.common.base.Objects;

import net.shibboleth.ext.spring.webflow.Event;
import net.shibboleth.ext.spring.webflow.Events;
import net.shibboleth.idp.profile.AbstractProfileAction;
import net.shibboleth.idp.profile.ActionSupport;
import net.shibboleth.idp.profile.EventIds;
import net.shibboleth.idp.profile.ProfileException;
import net.shibboleth.idp.profile.ProfileRequestContext;

/** Checks whether the inbound SOAP envelope has the appropriate framework version (2.0). */
@Events({
    @Event(id = EventIds.PROCEED_EVENT_ID),
    @Event(id = EventIds.INVALID_PROFILE_CTX)})
public class CheckFrameworkVersion extends AbstractProfileAction<Envelope, Object> {
    
    /** Class logger. */
    private final Logger log = LoggerFactory.getLogger(CheckFrameworkVersion.class);
    
    /** Whether require the 2.0 version to be used or not. */
    private boolean requireVersion;
    
    /**
     * Constructor.
     * @param forceVersion whether the version 2.0 is required or not
     */
    public CheckFrameworkVersion(boolean forceVersion) {
    	this.requireVersion = forceVersion;
    }
    
    /** {@inheritDoc} */
    protected org.springframework.webflow.execution.Event doExecute(final HttpServletRequest httpRequest, final HttpServletResponse httpResponse,
            final RequestContext springRequestContext,
            final ProfileRequestContext<Envelope, Object> profileRequestContext) throws ProfileException {

        final Header header = profileRequestContext.getInboundMessageContext().getMessage().getHeader();
        if (header == null) {
        	log.error("Action {}: Header was not found from the envelope!", getId());
        	return ActionSupport.buildEvent(this, EventIds.INVALID_PROFILE_CTX);
        }
        final Framework framework = getFrameworkFromEnvelopeHeader(profileRequestContext, header);
        String version = framework.getVersion();
        
        if (Objects.equal("2.0", version)) {
            log.debug("Action {}: Framework version check was ok, proceeding to the next event", getId());
            return ActionSupport.buildProceedEvent(this);
        } else {
        	if (this.requireVersion) {
        		log.debug("Action {}: Invalid Framework version {} detected, generating fault context", getId(), version);
        		throw new InvalidFrameworkVersionException(version);
        	} else {
        		log.debug("Action {}: Invalid Framework version {} detected, but not forced, nothing left to do.", getId(), version);
                return ActionSupport.buildProceedEvent(this);
        	}
        }
    }
    
    protected Framework getFrameworkFromEnvelopeHeader(final ProfileRequestContext<Envelope, Object> profileRequestContext, final Header header) throws ProfileException {
    	List<XMLObject> objects = header.getUnknownXMLObjects(Framework.DEFAULT_ELEMENT_NAME);
    	if (objects == null || objects.size() == 0 || objects.size() > 1) {
    		if (this.requireVersion) {
    			throw new ProfileException("Invalid number of Framework -elements found from the SOAP Header!");
    		} else {
    			return null;
    		}
        } else {
        	return (Framework) objects.get(0);
        }
    }

    /** Exception thrown when the incoming message was not an expected ID-WSF Framework version. */
    public static class InvalidFrameworkVersionException extends ProfileException {

        /** Serial version UID. */
        private static final long serialVersionUID = -872917446217634555L;

        /**
         * Constructor.
         * 
         * @param message exception message
         */
        public InvalidFrameworkVersionException(String version) {
            super("Invalid version, was " + version + ", expected 2.0!");
        }
    }

}
