/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.profile;

import javax.annotation.Nonnull;
import javax.servlet.http.HttpServletRequest;

import org.opensaml.messaging.decoder.MessageDecoder;
import org.opensaml.messaging.decoder.MessageDecodingException;
import org.opensaml.soap.soap11.Envelope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.shibboleth.idp.profile.HttpServletRequestMessageDecoderFactory;
import net.shibboleth.utilities.java.support.component.ComponentInitializationException;
import net.shibboleth.utilities.java.support.logic.Constraint;
import net.shibboleth.utilities.java.support.xml.ParserPool;

/** Decoder factory that produces decoders that handle {@link Envelope} requests. */
public class EnvelopeDecoderFactory implements HttpServletRequestMessageDecoderFactory<Envelope> {
	
    /** Class logger. */
    private final Logger log = LoggerFactory.getLogger(EnvelopeDecoderFactory.class);
    		
    /** Pool of XML parsers used to parse incoming messages. */
    private final ParserPool parserPool;

    /**
     * Constructor.
     * 
     * @param pool pool of parsers used to parse incoming XML
     */
    public EnvelopeDecoderFactory(@Nonnull final ParserPool pool) {
        this.parserPool = Constraint.isNotNull(pool, "Parser pool can not be null");
    }

	/** {@inheritDoc} */
	public MessageDecoder<Envelope> newDecoder(
			HttpServletRequest httpRequest) throws MessageDecodingException {
		log.debug("Constructing a new EnvelopeDecoder..");
		EnvelopeDecoder decoder = new EnvelopeDecoder();
		decoder.setHttpServletRequest(httpRequest);
		decoder.setParserPool(parserPool);
		try {
			decoder.initialize();
		} catch (ComponentInitializationException e) {
			log.error("Could not initialize the EnvelopeDecorder!", e);
		}
		return decoder;
	}

}
