/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.profile;

import java.security.KeyStoreException;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPublicKey;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.glite.sts.STSException;
import org.glite.sts.profile.soap.SoapActionSupport;
import org.glite.sts.ta.TokenGenerationContext;
import org.glite.sts.ta.voms.VomsTokenGenerationContext;
import org.glite.sts.ta.voms.attrstore.AttributeToUserLinkManager;
import org.glite.sts.ta.x509.X509TokenGenerationContext;
import org.glite.sts.voms.xmlobject.FQAN;
import org.glite.sts.voms.xmlobject.GridProxyRequest;
import org.glite.sts.voms.xmlobject.VomsAttributeCertificates;
import org.glite.sts.x509.CertificateKeys;
import org.glite.sts.x509.RSACertificateKeys;
import org.opensaml.core.xml.XMLObject;
import org.opensaml.messaging.context.MessageContext;
import org.opensaml.messaging.context.navigate.ChildContextLookup;
import org.opensaml.soap.soap11.Envelope;
import org.opensaml.soap.wstrust.RequestSecurityToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.webflow.execution.RequestContext;

import com.google.common.base.Function;

import eu.emi.security.authn.x509.X509Credential;
import eu.emi.security.authn.x509.impl.KeyAndCertCredential;
import net.shibboleth.ext.spring.webflow.Event;
import net.shibboleth.ext.spring.webflow.Events;
import net.shibboleth.idp.attribute.Attribute;
import net.shibboleth.idp.attribute.AttributeContext;
import net.shibboleth.idp.attribute.AttributeValue;
import net.shibboleth.idp.profile.AbstractProfileAction;
import net.shibboleth.idp.profile.ActionSupport;
import net.shibboleth.idp.profile.EventIds;
import net.shibboleth.idp.profile.ProfileException;
import net.shibboleth.idp.profile.ProfileRequestContext;

/** Initializes the VOMS token generation context and attaches it to the token generation context. */
@Events({
    @Event(id = EventIds.PROCEED_EVENT_ID),
    @Event(id = EventIds.INVALID_MSG_CTX)})
public class InitializeVomsTokenGenerationContext  extends AbstractProfileAction<Envelope, Envelope> {
	
	/** Class logger. */
	private final Logger log = LoggerFactory.getLogger(InitializeVomsTokenGenerationContext.class);

    /**
     * Strategy used to extract the {@link TokenGenerationContext} from the outbound
     * {@link MessageContext}.
     */
    private Function<MessageContext<Envelope>, TokenGenerationContext> tokenGenerationCtxLookupStrategy;
    
    /**
     * Strategy used to locate the {@link X509TokenGenerationContext} associated with a given {@link TokenGenerationContext}.
     */
    private Function<TokenGenerationContext, X509TokenGenerationContext> x509TokenGenerationContextLookupStrategy;

    /**
     * Strategy used to locate the {@link AttributeContext} associated with a given {@link ProfileRequestContext}.
     */
    private Function<ProfileRequestContext<?,?>, AttributeContext> attributeContextLookupStrategy;
    
    /** The strength of the key-pair to be generated. */
    private int keysize;
    
    /** The manager for the users' attribute references to VOMS identity. */
    private AttributeToUserLinkManager attributeLinkManager;

    /** Constructor. */
    public InitializeVomsTokenGenerationContext(int strength) {
    	this(strength, null);
    }
    
    public InitializeVomsTokenGenerationContext(int strength, AttributeToUserLinkManager linkManager) {
        tokenGenerationCtxLookupStrategy = new ChildContextLookup<MessageContext<Envelope>, TokenGenerationContext>(TokenGenerationContext.class, false);
        x509TokenGenerationContextLookupStrategy =
        		new ChildContextLookup<TokenGenerationContext, X509TokenGenerationContext>(X509TokenGenerationContext.class,
        				false);
        attributeContextLookupStrategy = new ChildContextLookup<ProfileRequestContext<?, ?>, AttributeContext>(AttributeContext.class, false);
        keysize = strength;
        attributeLinkManager = linkManager;
    }
    
    /** {@inheritDoc} */
    protected org.springframework.webflow.execution.Event doExecute(final HttpServletRequest httpRequest, final HttpServletResponse httpResponse,
            final RequestContext springRequestContext,
            final ProfileRequestContext<Envelope, Envelope> profileRequestContext) throws ProfileException {
    	MessageContext<Envelope> inMessageContext = profileRequestContext.getInboundMessageContext();
    	if (inMessageContext == null) {
    		log.debug("Action {}: inbound message context is null!", getId());
    		ActionSupport.buildEvent(this, EventIds.INVALID_MSG_CTX);
    	}
    	MessageContext<Envelope> outMessageContext = profileRequestContext.getOutboundMessageContext();
    	if (outMessageContext == null) {
    		log.debug("Action {}: outbound message context is null!", getId());
    		ActionSupport.buildEvent(this, EventIds.INVALID_MSG_CTX);
    	}
    	final TokenGenerationContext tokenGenerationContext = tokenGenerationCtxLookupStrategy.apply(outMessageContext);
    	final VomsTokenGenerationContext vomsTokenGenerationContext = new VomsTokenGenerationContext();
    	CertificateKeys keys;
    	RSAPublicKey publicKey = SoapActionSupport.getPublicKeyFromEnvelope(this, inMessageContext.getMessage());
		try {
			if (publicKey != null) {
				log.debug("Action {}: using the public key from the request message", getId());
				keys = new RSACertificateKeys(publicKey);
			} else {
				log.debug("Action {}: generating a new key-pair", getId());
				keys = new RSACertificateKeys(keysize);
			}
		} catch (STSException e) {
			log.error("Action {}: Could not generate a key-pair", getId(), e);
			throw new ProfileException("Could not generate a key-pair", e);
		}
		vomsTokenGenerationContext.setProxyKeys(keys);
		X509TokenGenerationContext x509ctx = x509TokenGenerationContextLookupStrategy.apply(tokenGenerationContext);
		X509Credential issuerCredential;
		try {
			issuerCredential = new KeyAndCertCredential(x509ctx.getCertKeys().getPrivateKey(), new X509Certificate[] { x509ctx.getCaResponse().getCertificate() });
		} catch (KeyStoreException e) {
			log.error("Action {}: Could not generate an X509Credential object from the given private key and certificate", getId(), e);
			throw new ProfileException("Could not generate a key-pair", e);
		}
		vomsTokenGenerationContext.setIssuerCredentials(issuerCredential);
		RequestSecurityToken rst = SoapActionSupport.getRequestSecurityTokenFromBody(inMessageContext.getMessage().getBody());
		GridProxyRequest proxyRequest = getGridProxyRequestFromRST(rst);
		vomsTokenGenerationContext.setLifetime(proxyRequest.getLifetime());
		vomsTokenGenerationContext.setDelegationType(proxyRequest.getDelegationType());
		vomsTokenGenerationContext.setPolicyType("" + proxyRequest.getPolicyType());
		vomsTokenGenerationContext.setProxyType(proxyRequest.getProxyType());
		VomsAttributeCertificates attributeCertificates = proxyRequest.getVomsAttributeCertificates();
		if (attributeCertificates != null) {
			List<FQAN> fqans = proxyRequest.getVomsAttributeCertificates().getFQANs();
			vomsTokenGenerationContext.setFqans(parseFQANs(fqans));
		} else {
			log.debug("No VOMS attribute certificates in the request");
			vomsTokenGenerationContext.setFqans(new HashMap<String, List<String>>());
		}
		if (attributeLinkManager != null) {
			String attributeId = attributeLinkManager.getIncomingAttributeId();
	    	AttributeContext attributeContext = attributeContextLookupStrategy.apply(profileRequestContext);
	    	String attributeReference = null;
	    	if (attributeContext == null) {
	    		log.warn("Action {}: Attribute context is null, cannot resolve the linking attribute", getId());
	    	} else {
	    		Map<String, Attribute> attributes = attributeContext.getAttributes();
	    		Set<String> attributeNames = attributes.keySet();
	            Iterator<String> names = attributeNames.iterator();
	            while (names.hasNext()) {
	                String name = (String) names.next();
	                if (name.equals(attributeId)) {
	    				@SuppressWarnings("rawtypes")
						Set<AttributeValue> value = attributes.get(name).getValues();
	                    if (value.size() != 1) {
	                        log.error("Action {}: Attribute {} is multi-valued, size: {}", new Object[] { getId(), name, value.size() });
	                    } else {
	                    	String valueStr = (String) value.iterator().next().getValue();
	                    	log.debug("Action {}: Setting value {} to the VOMS token generation context", getId(), valueStr);
	                    	attributeReference = valueStr;
	                    }
	                }
	            }

	    	}
        	vomsTokenGenerationContext.setAttributeReference(attributeReference);
		}
		tokenGenerationContext.addSubcontext(vomsTokenGenerationContext);
		return ActionSupport.buildProceedEvent(this);
    }

	/**
	 * Get the {@link GridProxyRequest} object from {@link RequestSecurityToken}.
	 * @param rst the request security token object
	 * @return Grid proxy request
	 */
	protected GridProxyRequest getGridProxyRequestFromRST(RequestSecurityToken rst) {
		List<XMLObject> objects = rst.getUnknownXMLObjects(GridProxyRequest.DEFAULT_ELEMENT_NAME);
		if (objects == null || objects.size() != 1) {
			log.debug("Action {}: No single GridProxyRequest -element found from the RST", getId());
			return null;
		}
		return (GridProxyRequest) objects.get(0);
	}
	
	/**
	 * Get the FQANs from the {@link GridProxyRequest}.
	 * @param the Grid proxy request
	 * @return the list of FQANs
	 */
	protected List<FQAN> getFQANsFromGridProxyRequest(GridProxyRequest proxyRequest) {
		return proxyRequest.getVomsAttributeCertificates().getFQANs();
	}
		
	protected Map<String, List<String>> parseFQANs(List<FQAN> fqans) throws ProfileException {
		Map<String, List<String>> requestOptions = new HashMap<String, List<String>>();
        for ( int i = 0; i < fqans.size(); i++ ) {
            String[] opts = fqans.get(i).getValue().split( ":" );
            if (opts.length != 2) {
            	throw new InvalidVomsFqanStructureException(fqans.get(i));
            }
            String voName = opts[0];
            List<String> o;
            
            if (requestOptions.containsKey(voName)) {
                o = requestOptions.get(voName);
            } else {
                o = new Vector<String>();
                requestOptions.put(voName, o);
            }
            o.add(opts[1]);
        }
        return requestOptions;
	}
	
    /** Exception thrown when the requested security token type is not supported. */
    public static class InvalidVomsFqanStructureException extends ProfileException {

        /** Serial version UID. */
        private static final long serialVersionUID = -872912123123872315L;

        /**
         * Constructor.
         * 
         * @param message exception message
         */
        public InvalidVomsFqanStructureException(FQAN fqan) {
            super("Could not process the FQAN " + fqan.getValue() + " in the VOMS proxy request.");
        }
    }
}
