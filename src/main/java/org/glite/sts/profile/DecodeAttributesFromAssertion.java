/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.profile;

import java.util.List;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.shibboleth.ext.spring.webflow.Event;
import net.shibboleth.ext.spring.webflow.Events;
import net.shibboleth.idp.attribute.Attribute;
import net.shibboleth.idp.attribute.AttributeContext;
import net.shibboleth.idp.attribute.AttributeValue;
import net.shibboleth.idp.attribute.StringAttributeValue;
import net.shibboleth.idp.authn.AuthenticationRequestContext;
import net.shibboleth.idp.profile.AbstractProfileAction;
import net.shibboleth.idp.profile.ActionSupport;
import net.shibboleth.idp.profile.EventIds;
import net.shibboleth.idp.profile.ProfileException;
import net.shibboleth.idp.profile.ProfileRequestContext;

import org.glite.sts.authn.AssertionContext;
import org.opensaml.core.xml.XMLObject;
import org.opensaml.core.xml.schema.XSAny;
import org.opensaml.core.xml.schema.XSString;
import org.opensaml.messaging.context.MessageContext;
import org.opensaml.messaging.context.navigate.ChildContextLookup;
import org.opensaml.saml.saml2.core.Assertion;
import org.opensaml.saml.saml2.core.AttributeStatement;
import org.opensaml.soap.soap11.Envelope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.webflow.execution.RequestContext;

import com.google.common.base.Function;

/** Resolves the attributes from SAML assertion and attaches them to the generated {@link AttributeContext}. */
@Events({
    @Event(id = EventIds.PROCEED_EVENT_ID),
    @Event(id = EventIds.INVALID_MSG_CTX)})
public class DecodeAttributesFromAssertion extends AbstractProfileAction<Envelope, Envelope> {

	/** Class logger. */
	private final Logger log = LoggerFactory.getLogger(DecodeAttributesFromAssertion.class);
	
    /**
     * Strategy used to extract the {@link AuthenticationRequestContext} from the
     * {@link ProfileRequestContext}.
     */
    private Function<ProfileRequestContext<Envelope, Envelope>, AuthenticationRequestContext> authnCtxLookupStrategy;
    
    /**
     * Strategy used to extract the {@link AssertionContext} from the {@link AuthenticationRequestContext}.
     */
    private Function<AuthenticationRequestContext, AssertionContext> assertionCtxLookupStrategy;
    
    /** {@inheritDoc} */
    public DecodeAttributesFromAssertion() {
    	super();
        authnCtxLookupStrategy = 
        		new ChildContextLookup<ProfileRequestContext<Envelope, Envelope>, AuthenticationRequestContext>(AuthenticationRequestContext.class, 
        				false);
        assertionCtxLookupStrategy = new ChildContextLookup<AuthenticationRequestContext, AssertionContext>(AssertionContext.class, false);
    }
	
    /** {@inheritDoc} */
    protected org.springframework.webflow.execution.Event doExecute(final HttpServletRequest httpRequest, final HttpServletResponse httpResponse,
            final RequestContext springRequestContext,
            final ProfileRequestContext<Envelope, Envelope> profileRequestContext) throws ProfileException {
    	MessageContext<Envelope> inMessageContext = profileRequestContext.getInboundMessageContext();
    	if (inMessageContext == null) {
    		log.debug("Action {}: inbound message context is null!", getId());
    		ActionSupport.buildEvent(this, EventIds.INVALID_MSG_CTX);
    	}
    	final MessageContext<Envelope> outMessageContext = profileRequestContext.getOutboundMessageContext();
    	if (outMessageContext == null) {
    		log.debug("Action {}: outbound message context is null!", getId());
    		ActionSupport.buildEvent(this, EventIds.INVALID_MSG_CTX);
    	}
    	final AuthenticationRequestContext authnContext = authnCtxLookupStrategy.apply(profileRequestContext);
    	final AssertionContext assertionContext = assertionCtxLookupStrategy.apply(authnContext);

    	final AttributeContext attributeContext = new AttributeContext();
		attributeContext.setAttributes(getAttributesFromAssertion(assertionContext.getAssertion()));
		profileRequestContext.addSubcontext(attributeContext, false);

    	return ActionSupport.buildProceedEvent(this);

    }
    
    /**
     * Decodes a list of {@link Attribute} from a SAML assertion.
     * @param assertion SAML assertion
     * @return a list of attributes
     */
    protected List<Attribute> getAttributesFromAssertion(Assertion assertion) throws ProfileException {
		Vector<Attribute> attributes = new Vector<Attribute>();
    	List<AttributeStatement> statements = assertion.getAttributeStatements();
    	for (int i = 0; i < statements.size(); i++) {
    		List<org.opensaml.saml.saml2.core.Attribute> samlAttributes = statements.get(i).getAttributes();
    		for (int j = 0; j < samlAttributes.size(); j++) {
    			attributes.add(generateAttribute(samlAttributes.get(j)));
    		}
    	}
    	return attributes;
    }
    
    /**
     * Generates an {link @Attribute} from a SAML attribute.
     * @param samlAttribute SAML attribute
     * @return an attribute containing an ID and value
     */
    protected Attribute generateAttribute(org.opensaml.saml.saml2.core.Attribute samlAttribute) throws ProfileException {
    	String attributeId;
    	if ((attributeId = samlAttribute.getFriendlyName()) == null) {
    		attributeId = samlAttribute.getName();
    	}
		Attribute attribute = new Attribute(attributeId);
		log.debug("Action {}: Adding an attribute {}", getId(), attributeId);
		StringAttributeValue attrValue = getSamlAttributeValue(samlAttribute);
		@SuppressWarnings("rawtypes")
		Vector<AttributeValue> list = new Vector<AttributeValue>();
		list.add(attrValue);
		attribute.setValues(list);
		return attribute;    	
    }
    
    /**
     * Get a string value from a SAML attribute.
     * @param samlAttribute SAML attribute
     * @return a string value containing the SAML attribute value
     */
    protected StringAttributeValue getSamlAttributeValue(org.opensaml.saml.saml2.core.Attribute samlAttribute) throws ProfileException {
    	String stringValue;
    	XMLObject samlValue = (XMLObject) samlAttribute.getAttributeValues().get(0);
    	if (samlValue instanceof XSString) {
    		stringValue = ((XSString) samlValue).getValue(); 
    	} else if (samlValue instanceof XSAny) {
    		stringValue = ((XSAny) samlValue).getTextContent();
    	} else {
    		throw new UnSupportedAttributeValueTypeException(samlValue.getClass().toString());
    	}
		return new StringAttributeValue(stringValue);
    }
    
    /** Exception thrown when the attribute value cannot be decoded. */
    public static class UnSupportedAttributeValueTypeException extends ProfileException {

        /** Serial version UID. */
		private static final long serialVersionUID = -7234512349423732050L;
		
        /**
         * Constructor.
         * 
         * @param message exception message
         */
        public UnSupportedAttributeValueTypeException(String type) {
        	super("Un-supported attribute value type: " + type);
        }    	
    }
}
