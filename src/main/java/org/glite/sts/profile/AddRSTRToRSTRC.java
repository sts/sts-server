/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.profile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.glite.sts.profile.wstrust.WsTrustActionSupport;
import org.opensaml.soap.soap11.Envelope;
import org.opensaml.soap.wstrust.RequestSecurityTokenResponseCollection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.webflow.execution.RequestContext;

import net.shibboleth.ext.spring.webflow.Event;
import net.shibboleth.ext.spring.webflow.Events;
import net.shibboleth.idp.profile.AbstractProfileAction;
import net.shibboleth.idp.profile.ActionSupport;
import net.shibboleth.idp.profile.EventIds;
import net.shibboleth.idp.profile.ProfileException;
import net.shibboleth.idp.profile.ProfileRequestContext;

/**
 * Builds an {@link RequestSecurityTokenResponse} and adds it to the {@link RequestSecurityTokenResponseCollection}, inside the {@link Body} of 
 * the {@link Envelope} set as the message of the {@link ProfileRequestContext#getOutboundMessageContext()}.
 */
@Events({
        @Event(id = EventIds.PROCEED_EVENT_ID),
        @Event(id = EventIds.INVALID_MSG_CTX)})
public class AddRSTRToRSTRC extends AbstractProfileAction<Object, Envelope> {

    /** Class logger. */
    private final Logger log = LoggerFactory.getLogger(AddRSTRCToEnvelope.class);
    
    /** Constructor. */
    public AddRSTRToRSTRC() {
    	super();
    }
    
    /** {@inheritDoc} */
    protected org.springframework.webflow.execution.Event doExecute(final HttpServletRequest httpRequest, final HttpServletResponse httpResponse,
            final RequestContext springRequestContext,
            final ProfileRequestContext<Object, Envelope> profileRequestContext) throws ProfileException {
    	log.debug("Action {}: Attempting to add an RSTR object to the RSTRC inside the outgoing Envelope", getId());
    	
    	Envelope envelope = profileRequestContext.getOutboundMessageContext().getMessage();
    	if (envelope == null) {
    		log.debug("Action {}: Envelope was not found from the outbound context.", getId());
    		return ActionSupport.buildEvent(this, EventIds.INVALID_MSG_CTX);    		
    	}
    	RequestSecurityTokenResponseCollection rstrc = (RequestSecurityTokenResponseCollection) envelope.getBody().getUnknownXMLObjects(RequestSecurityTokenResponseCollection.ELEMENT_NAME).get(0);
    	WsTrustActionSupport.addRSTRtoRSTRC(this, rstrc);
    	
    	return ActionSupport.buildProceedEvent(this);
    }
    
}
