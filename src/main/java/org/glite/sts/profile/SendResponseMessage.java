/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.profile;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.shibboleth.idp.profile.AbstractProfileAction;
import net.shibboleth.idp.profile.ActionSupport;
import net.shibboleth.idp.profile.ProfileException;
import net.shibboleth.idp.profile.ProfileRequestContext;

import org.opensaml.core.xml.config.XMLObjectProviderRegistrySupport;
import org.opensaml.core.xml.io.Marshaller;
import org.opensaml.core.xml.io.MarshallingException;
import org.opensaml.soap.soap11.Envelope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.webflow.execution.RequestContext;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSOutput;
import org.w3c.dom.ls.LSSerializer;

/**
 * 
 */
public class SendResponseMessage extends AbstractProfileAction<Envelope, Envelope> {
	
	/** Class logger. */
    private final Logger log = LoggerFactory.getLogger(SendResponseMessage.class);
    
    /** {@inheritDoc} */
    protected org.springframework.webflow.execution.Event doExecute(final HttpServletRequest httpRequest, final HttpServletResponse httpResponse,
            final RequestContext springRequestContext,
            final ProfileRequestContext<Envelope, Envelope> profileRequestContext) throws ProfileException {
    	httpResponse.setContentType("text/xml");
    	Envelope envelope = profileRequestContext.getOutboundMessageContext().getMessage();
    	Marshaller marshaller =
	            XMLObjectProviderRegistrySupport.getMarshallerFactory().
	            getMarshallers().get(Envelope.DEFAULT_ELEMENT_NAME);
    	Element element;
		try {
			element = marshaller.marshall(envelope);
		} catch (MarshallingException e) {
			log.error("Could not marshall the envelope!", e);
			throw new ProfileException("Could not marshall the envelope!", e);
		}
    	Document document = element.getOwnerDocument();
    	DOMImplementationLS domImplLS = (DOMImplementationLS) document.getImplementation();
    	LSSerializer serializer = domImplLS.createLSSerializer();
    	LSOutput lsOutput = domImplLS.createLSOutput();
    	try {
			lsOutput.setByteStream(httpResponse.getOutputStream());
		} catch (IOException e) {
			log.error("Could not write the output to the servlet response!", e);
			throw new ProfileException("Could not write the output to the servlet response!", e);
		}
    	serializer.write(element, lsOutput);
    	springRequestContext.getExternalContext().recordResponseComplete();
    	return ActionSupport.buildProceedEvent(this);
    }

}