/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.profile;

import java.util.Collection;
import java.util.Iterator;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.shibboleth.ext.spring.webflow.Event;
import net.shibboleth.ext.spring.webflow.Events;
import net.shibboleth.idp.attribute.Attribute;
import net.shibboleth.idp.attribute.AttributeContext;
import net.shibboleth.idp.attribute.AttributeValue;
import net.shibboleth.idp.attribute.StringAttributeValue;
import net.shibboleth.idp.authn.AuthenticationRequestContext;
import net.shibboleth.idp.profile.AbstractProfileAction;
import net.shibboleth.idp.profile.ActionSupport;
import net.shibboleth.idp.profile.EventIds;
import net.shibboleth.idp.profile.ProfileException;
import net.shibboleth.idp.profile.ProfileRequestContext;
import net.shibboleth.utilities.java.support.logic.Constraint;

import org.glite.sts.authn.UsernamePasswordContext;
import org.ldaptive.BindOperation;
import org.ldaptive.BindRequest;
import org.ldaptive.Connection;
import org.ldaptive.Credential;
import org.ldaptive.LdapAttribute;
import org.ldaptive.LdapEntry;
import org.ldaptive.LdapException;
import org.ldaptive.SearchOperation;
import org.ldaptive.SearchRequest;
import org.ldaptive.SearchResult;
import org.ldaptive.pool.ConnectionPool;
import org.opensaml.messaging.context.MessageContext;
import org.opensaml.messaging.context.navigate.ChildContextLookup;
import org.opensaml.soap.soap11.Envelope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.webflow.execution.RequestContext;

import com.google.common.base.Function;


/** An action for resolving the attributes from the LDAP directory. */
@Events({
    @Event(id = EventIds.PROCEED_EVENT_ID),
    @Event(id = EventIds.INVALID_MSG_CTX)})
public class ResolveAttributesFromLdap extends AbstractProfileAction<Envelope, Envelope> {

	/** Class logger. */
	private final Logger log = LoggerFactory.getLogger(ResolveAttributesFromLdap.class);
	
	public static final String DEFAULT_UID_ATTRIBUTE = "uid";
	
    /** The LDAP connection pool. */
    private ConnectionPool ldapConnectionPool;

    /** The base DN for the users in the LDAP directory. */
    private String ldapBaseDn;
    
    /** The uid attribute name in the LDAP directory. */
    private String ldapUidAttributeName;
    
    /** The DN of the user used for binding. If null, the username context is used. */
    private String ldapBindDn;
    
    /** The credential of the user used for binding, if ldapBindDn is set. */
    private Credential ldapBindCredential;

    /**
     * Strategy used to extract the {@link AuthenticationRequestContext} from the
     * {@link ProfileRequestContext}.
     */
    private Function<ProfileRequestContext<Envelope, Envelope>, AuthenticationRequestContext> authnCtxLookupStrategy;
    
    /**
     * Strategy used to extract the {@link UsernamePasswordContext} from the {@link AuthenticationRequestContext}.
     */
    private Function<AuthenticationRequestContext, UsernamePasswordContext> usernamePasswordCtxLookupStrategy;

    /**
     * Constructor.
     * @param connectionPool the LDAP connection pool
     * @param baseDn base DN used for users in the directory
     */
    public ResolveAttributesFromLdap(ConnectionPool connectionPool, String baseDn) {
    	this(connectionPool, baseDn, DEFAULT_UID_ATTRIBUTE, null, null);
    }
    
    /**
     * Constructor.
     * @param connectionPool the LDAP connection pool
     * @param baseDn base DN used for users in the directory
     * @param uid the uid attribute name in the directory
     */
    public ResolveAttributesFromLdap(ConnectionPool connectionPool, String baseDn, String uid) {
    	this(connectionPool, baseDn, uid, null, null);
    }
    
    /**
     * Constructor.
     * @param connectionPool the LDAP connection pool
     * @param baseDn base DN used for users in the directory
     * @param bindDn the DN of the user used for binding
     * @param bindCredential the credential of the user
     */
    public ResolveAttributesFromLdap(ConnectionPool connectionPool, String baseDn, String bindDn, String bindCredential) {
    	this(connectionPool, baseDn, DEFAULT_UID_ATTRIBUTE, bindDn, bindCredential);
    }
    
    /**
     * Constructor.
     * @param connectionPool the LDAP connection pool
     * @param baseDn base DN used for users in the directory
     * @param uid the uid attribute name in the directory
     * @param bindDn the DN of the user used for binding
     * @param bindCredential the credential of the user
     */
    public ResolveAttributesFromLdap(ConnectionPool connectionPool, String baseDn, String uid, String bindDn, String bindCredential) {
    	super();
        authnCtxLookupStrategy = 
        		new ChildContextLookup<ProfileRequestContext<Envelope, Envelope>, AuthenticationRequestContext>(AuthenticationRequestContext.class, 
        				false);
        usernamePasswordCtxLookupStrategy = new ChildContextLookup<AuthenticationRequestContext, UsernamePasswordContext>(UsernamePasswordContext.class, false);
        this.ldapConnectionPool = connectionPool;
        this.ldapBaseDn = Constraint.isNotNull(baseDn, "Base DN must be configured to the LDAP connection");
        this.ldapUidAttributeName = Constraint.isNotNull(uid, "The uid attribute name for the LDAP cannot be null");
        this.ldapBindDn = bindDn;
        if (bindCredential != null) {
        	this.ldapBindCredential = new Credential(bindCredential);
        } else {
        	this.ldapBindCredential = null;
        }
    }


    /** {@inheritDoc} */
    protected org.springframework.webflow.execution.Event doExecute(final HttpServletRequest httpRequest, final HttpServletResponse httpResponse,
            final RequestContext springRequestContext,
            final ProfileRequestContext<Envelope, Envelope> profileRequestContext) throws ProfileException {
    	MessageContext<Envelope> inMessageContext = profileRequestContext.getInboundMessageContext();
    	if (inMessageContext == null) {
    		log.debug("Action {}: inbound message context is null!", getId());
    		ActionSupport.buildEvent(this, EventIds.INVALID_MSG_CTX);
    	}
    	final MessageContext<Envelope> outMessageContext = profileRequestContext.getOutboundMessageContext();
    	if (outMessageContext == null) {
    		log.debug("Action {}: outbound message context is null!", getId());
    		ActionSupport.buildEvent(this, EventIds.INVALID_MSG_CTX);
    	}
    	AuthenticationRequestContext authnContext = authnCtxLookupStrategy.apply(profileRequestContext);
   		UsernamePasswordContext usernameContext = usernamePasswordCtxLookupStrategy.apply(authnContext);
   		Collection<LdapAttribute> ldapAttributes = resolveAttributes(usernameContext);
   		Vector<Attribute> attributes = convertAttributes(ldapAttributes);
   		AttributeContext attributeContext = new AttributeContext();
   		attributeContext.setAttributes(attributes);
   		profileRequestContext.addSubcontext(attributeContext, true);
    	return ActionSupport.buildProceedEvent(this);
    }
    
    /**
     * Resolves the attributes from the LDAP.
     * @param usernameContext the username context used for finding the user
     * @return the collection of the user's LDAP attributes
     */
    protected Collection<LdapAttribute> resolveAttributes(UsernamePasswordContext usernameContext) {
    	String username = usernameContext.getUsername();
		Connection conn;
		try {
			conn = this.ldapConnectionPool.getConnection();
		} catch (LdapException e) {
			log.error("Action {}: Could not get a connection from the pool!", getId());
			log.debug("Caused by", e);
			return null;
		}
		String bindDn;
		Credential bindCredential;
		if (this.ldapBindDn == null) {
			bindDn = this.ldapUidAttributeName + "=" + username + "," + this.ldapBaseDn;
			if (usernameContext.isPasswordHashed()) {
				log.warn("Incoming password is hashed, ldapBindDn must be configured in order to obtain the attributes for the user!");
				return null;
			} else {
				bindCredential = new Credential(usernameContext.getPassword());
			}
		} else {
			bindDn = this.ldapBindDn;
			bindCredential = this.ldapBindCredential;
		}
		LdapEntry entry;
		try {
			conn.open();
			BindOperation bind = new BindOperation(conn);
			log.debug("Action {}: Attempting to authenticate {}", getId(), bindDn);
			bind.execute(new BindRequest(bindDn, bindCredential));
			SearchOperation search = new SearchOperation(conn);
			SearchRequest req = new SearchRequest(this.ldapBaseDn, "(uid=" + username + ")");
			SearchResult result = search.execute(req).getResult();
			entry = result.getEntry();
		} catch (LdapException e) {
			log.warn("Action {}: Could not access the LDAP directory: {}", getId(), e.getMessage());
			log.debug("Caused by", e);
			return null;			
		} finally {
			conn.close();
		}
		if (entry == null) {
			return null;
		} else {
			return entry.getAttributes();
		}
    }
    
    /**
     * Converts a collection of LDAP attributes into a vector of Shibboleth attributes. 
     * @param ldapAttributes the collection of LDAP attributes
     * @return a vector of Shibboleth attributes
     */
    protected Vector<Attribute> convertAttributes(Collection<LdapAttribute> ldapAttributes) {
    	Vector<Attribute> result = new Vector<Attribute>();
    	if (ldapAttributes != null) {
    		Iterator<LdapAttribute> attributeIterator = ldapAttributes.iterator();
    		while (attributeIterator.hasNext()) {
    			LdapAttribute ldapAttribute = attributeIterator.next();
    			log.debug("Action {}: Adding an attribute {}", getId(), ldapAttribute.getName());
    			result.add(generateAttribute(ldapAttribute.getName(), ldapAttribute.getStringValue()));
    		}
    	} else {
    		log.debug("Action {}: No attributes found to be converted", getId());
    	}
    	return result;
    }
    
    /**
     * Generates an {@link Attribute} with a given id and value.
     * @param attributeId the attribute id
     * @param attributeValue the attribute value
     * @return the Shibboleth attribute
     */
    protected Attribute generateAttribute(String attributeId, String attributeValue) {
		Attribute attribute = new Attribute(attributeId);
		log.debug("Action {}: Converting an attribute {}", getId(), attributeId);
		StringAttributeValue attrValue = new StringAttributeValue(attributeValue);
		@SuppressWarnings("rawtypes")
		Vector<AttributeValue> list = new Vector<AttributeValue>();
		list.add(attrValue);
		attribute.setValues(list);
		return attribute;    	
    }

}
