/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.profile;

import javax.xml.namespace.QName;

import org.glite.sts.profile.soap.xmlobject.LibertyIDWSFConstants;
import org.opensaml.messaging.context.BaseContext;

public class SoapFaultContext extends BaseContext {
	
	public static final QName FAULTCODE_VERSION_MISMATCH = new QName(LibertyIDWSFConstants.IDWSF_SBF_NS, "FrameworkVersionMismatch", LibertyIDWSFConstants.IDWSF_SBF_PREFIX);
	
	private QName faultcode;
	
	private String faultstring;
	
	private String statusCode;
	
	private String statusRef;
	
	private String statusComment;
	
	public SoapFaultContext(QName code) {
		this(code, null, null, null, null);
	}
	
	public SoapFaultContext(QName code, String message) {
		this(code, message, null, null, null);
	}
	
	public SoapFaultContext(QName code, String message, String statusCode) {
		this(code, message, statusCode, null, null);
	}
	
	public SoapFaultContext(QName code, String message, String statusCode, String statusRef) {
		this(code, message, statusCode, statusRef, null);
	}
	
	public SoapFaultContext(QName code, String message, String statusCode, String statusRef, String statusComment) {
		super();
		this.faultcode = code;
		this.faultstring = message;
		this.statusCode = statusCode;
		this.statusRef = statusRef;
		this.statusComment = statusComment;
	}
	
	public QName getFaultcode() {
		return this.faultcode;
	}
	
	public String getFaultstring() {
		return this.faultstring;
	}
	
	public String getStatusCode() {
		return this.statusCode;
	}
	
	public String getStatusRef() {
		return this.statusRef;
	}
	
	public String getStatusComment() {
		return this.statusComment;
	}

}
