package org.glite.sts.profile;

import java.io.IOException;
import java.security.cert.X509Certificate;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.rpc.ServiceException;

import net.shibboleth.idp.profile.AbstractProfileAction;
import net.shibboleth.idp.profile.ActionSupport;
import net.shibboleth.idp.profile.EventIds;
import net.shibboleth.idp.profile.ProfileException;
import net.shibboleth.idp.profile.ProfileRequestContext;
import net.shibboleth.utilities.java.support.logic.Constraint;

import org.apache.axis.AxisFault;
import org.apache.axis.configuration.SimpleProvider;
import org.glite.security.voms.User;
import org.glite.security.voms.service.certificates.VOMSCertificates;
import org.glite.security.voms.service.certificates.VOMSCertificatesServiceLocator;
import org.glite.sts.STSException;
import org.glite.sts.config.ServerConfiguration;
import org.glite.sts.ta.TokenGenerationContext;
import org.glite.sts.ta.voms.VomsTokenGenerationContext;
import org.glite.sts.ta.voms.attrstore.AttributeToUserLinkManager;
import org.glite.sts.ta.voms.attrstore.impl.SSLConfigSender;
import org.opensaml.messaging.context.MessageContext;
import org.opensaml.messaging.context.navigate.ChildContextLookup;
import org.opensaml.soap.soap11.Envelope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.webflow.execution.RequestContext;

import com.google.common.base.Function;

import eu.emi.security.authn.x509.CrlCheckingMode;
import eu.emi.security.authn.x509.NamespaceCheckingMode;
import eu.emi.security.authn.x509.OCSPCheckingMode;
import eu.emi.security.authn.x509.X509CertChainValidator;
import eu.emi.security.authn.x509.X509Credential;
import eu.emi.security.authn.x509.impl.OpensslNameUtils;

/**
 * This action registers the issuer certificate from {@link VomsTokenGenerationContext} to the VOMS server
 * as an alias to the existing VOMS user.
 */
public class RegisterVomsAlias extends AbstractProfileAction<Envelope, Envelope> {
	
	/** Logging. */
	private final Logger log = LoggerFactory.getLogger(RegisterVomsAlias.class);
	
    /**
     * Strategy used to extract the {@link TokenGenerationContext} from the outbound
     * {@link MessageContext}.
     */
    private Function<MessageContext<Envelope>, TokenGenerationContext> tokenGenerationCtxLookupStrategy;

    /**
     * Strategy used to locate the {@link VomsTokenGenerationContext} associated with a given {@link TokenGenerationContext}.
     */
    private Function<TokenGenerationContext, VomsTokenGenerationContext> vomsTokenGenerationContextLookupStrategy;
	
    /** The VOMS certificate service instance. */
    private VOMSCertificates vomsCertService;
	
    /** The manager for the users' attribute references to VOMS identity. */
    private AttributeToUserLinkManager attributeLinkManager;
	
    /**
     * Constructor.
     * @param credential Client credentials used for communication with the VOMS server.
     * @param serverConfiguration The server configuration.
     * @param chainValidator The server certificate chain validator.
     * @param namespaceCheckingMode The namespace checking mode for server validation.
     * @param crlCheckingMode The CRL checking mode for server validation.
     * @param ocspCheckingMode The OCSP checking mode for server validation.
     * @param linkManager The instance of the attribute to user -link manager.
     * @throws AxisFault Web Service related errors.
     * @throws STSException Configuration and other common errors.
     * @throws ServiceException Service communication related errors.
     * @throws IOException Service communication related errors.
     */
	public RegisterVomsAlias(X509Credential credential, ServerConfiguration serverConfiguration, X509CertChainValidator chainValidator, 
			NamespaceCheckingMode namespaceCheckingMode, CrlCheckingMode crlCheckingMode, OCSPCheckingMode ocspCheckingMode,
			AttributeToUserLinkManager linkManager) throws AxisFault, STSException, ServiceException, IOException {
        tokenGenerationCtxLookupStrategy = new ChildContextLookup<MessageContext<Envelope>, TokenGenerationContext>(TokenGenerationContext.class, false);
        vomsTokenGenerationContextLookupStrategy =
        		new ChildContextLookup<TokenGenerationContext, VomsTokenGenerationContext>(VomsTokenGenerationContext.class,
        				false);
        SimpleProvider provider = SSLConfigSender
                .getTransportProvider(credential, chainValidator);
        String baseUrl = serverConfiguration.getVomsConfigurationValue("baseUrl");
        log.debug("Using baseUrl={}", baseUrl);
        VOMSCertificatesServiceLocator attrLocator = new VOMSCertificatesServiceLocator(
                provider);
        attrLocator.setVOMSCertificatesEndpointAddress(baseUrl + "/services/VOMSCertificates");
        this.vomsCertService = attrLocator.getVOMSCertificates();
        this.attributeLinkManager = Constraint.isNotNull(linkManager, "linkManager cannot be null!");
        log.info("Bean successfully initialized");
	}

    /** {@inheritDoc} */
    protected org.springframework.webflow.execution.Event doExecute(final HttpServletRequest httpRequest, final HttpServletResponse httpResponse,
            final RequestContext springRequestContext,
            final ProfileRequestContext<Envelope, Envelope> profileRequestContext) throws ProfileException {
    	final MessageContext<Envelope> inMessageContext = profileRequestContext.getInboundMessageContext();
    	if (inMessageContext == null) {
    		log.debug("Action {}: inbound message context is null!", getId());
    		ActionSupport.buildEvent(this, EventIds.INVALID_MSG_CTX);
    	}
    	final MessageContext<Envelope> outMessageContext = profileRequestContext.getOutboundMessageContext();
    	if (outMessageContext == null) {
    		log.debug("Action {}: outbound message context is null!", getId());
    		ActionSupport.buildEvent(this, EventIds.INVALID_MSG_CTX);
    	}
    	final TokenGenerationContext tokenGenerationContext = tokenGenerationCtxLookupStrategy.apply(outMessageContext);
    	final VomsTokenGenerationContext vomsTokenGenerationContext = vomsTokenGenerationContextLookupStrategy.apply(tokenGenerationContext);
    	if (vomsTokenGenerationContext == null) {
    		log.debug("Action {}: VOMS token generation context is null, unable to proceed", getId());
    		return ActionSupport.buildEvent(this, EventIds.INVALID_MSG_CTX);
    	}
    	final X509Certificate aliasCertificate = vomsTokenGenerationContext.getIssuerCredentials().getCertificate();
    	if (aliasCertificate == null) {
    		log.debug("Action {}: VOMS token generation context did not contain certificate, unable to proceed", getId());
    		return ActionSupport.buildEvent(this, EventIds.INVALID_MSG_CTX);
    	}
    	final String attributeReference = vomsTokenGenerationContext.getAttributeReference();
    	if (attributeReference == null) {
    		log.debug("Action {}: VOMS token generation context does not contain attribute reference, unable to proceed", getId());
    		return ActionSupport.buildEvent(this, EventIds.INVALID_MSG_CTX);    		
    	}
    	User vomsUser = attributeLinkManager.getUser(attributeReference);
    	if (vomsUser == null) {
    		log.debug("Action {}: Could not find the corresponding user {} from VOMS", getId(), attributeReference);
    		throw new ProfileException("Could not find user " + attributeReference + " from VOMS");
    	}
    	log.debug("User {} is corresponding to {}", vomsUser.getDN(), attributeReference);
    	if (!aliasExistsForCertificate(vomsUser, aliasCertificate)) {
    		registerCertificate(vomsUser, aliasCertificate);
    	}
		return ActionSupport.buildProceedEvent(this);
    }
    
    /**
     * Registers the certificate as an alias to the given VOMS user.
     * @param vomsUser the existing VOMS user.
     * @param alias the certificate to be added.
     * @throws ProfileException If something goes wrong.
     */
    public void registerCertificate(User vomsUser, X509Certificate alias) throws ProfileException {
    	log.info("Registering {} as an alias to {}", alias.getSubjectDN(), vomsUser.getDN());
        try {
            org.glite.security.voms.service.certificates.X509Certificate newCert = new org.glite.security.voms.service.certificates.X509Certificate(
                    alias.getEncoded(), alias.getSerialNumber().longValue(),
                    alias.getIssuerDN().toString(), alias.getNotAfter()
                            .toString(), alias.getSubjectDN().toString());
            this.vomsCertService.addCertificate(vomsUser.getDN(), vomsUser.getCA(), newCert);
        } catch (Exception e) {
            throw new ProfileException(
                    "Could not add new certificate to the VOMS certificate service!",
                    e);
        }
    }
    
    /**
     * Checks whether the alias already exists for a VOMS user.
     * @param vomsUser the existing VOMS user.
     * @param alias The certificate to be added.
     * @return true if exists, false otherwise.
     * @throws ProfileException If something goes wrong.
     */
    public boolean aliasExistsForCertificate(User vomsUser, X509Certificate alias) throws ProfileException {
    	log.debug("Checking whether {} already exists for user {}", alias.getSubjectDN(), vomsUser.getDN());
    	try {
    		org.glite.security.voms.service.certificates.X509Certificate[] certificates = vomsCertService.getCertificates(vomsUser.getDN(), vomsUser.getCA());
    		for (int i = 0; i < certificates.length; i++) {
    			final String dn = OpensslNameUtils.convertFromRfc2253(alias.getSubjectX500Principal().toString(), false);
    			log.debug("Comparing {} vs DN = {}", certificates[i].getSubject(), dn);
    			if (certificates[i].getSubject().equals(dn)) {
    				log.debug("Matching certificate found, returning true");
    				return true;
    			}
    		}
    	} catch (Exception e) {
    		throw new ProfileException("Could not obtain the existing VOMS certificates for user " + vomsUser.getDN(), e);
    	}
    	log.debug("No matching certificate found, returning false");
    	return false;
    }
}
