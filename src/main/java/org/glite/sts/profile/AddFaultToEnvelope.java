/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.profile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.shibboleth.ext.spring.webflow.Event;
import net.shibboleth.ext.spring.webflow.Events;
import net.shibboleth.idp.profile.AbstractProfileAction;
import net.shibboleth.idp.profile.ActionSupport;
import net.shibboleth.idp.profile.EventIds;
import net.shibboleth.idp.profile.ProfileException;
import net.shibboleth.idp.profile.ProfileRequestContext;

import org.glite.sts.profile.soap.SoapActionSupport;
import org.glite.sts.profile.soap.xmlobject.Status;
import org.opensaml.messaging.context.navigate.ChildContextLookup;
import org.opensaml.soap.soap11.Body;
import org.opensaml.soap.soap11.Envelope;
import org.opensaml.soap.soap11.Fault;
import org.opensaml.soap.soap11.FaultCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.webflow.execution.RequestContext;

import com.google.common.base.Function;

/**
 * Builds an {@link Fault} and adds it to the {@link Envelope} set as the message of the
 * {@link ProfileRequestContext#getOutboundMessageContext()}.
 */
@Events({
    @Event(id = EventIds.PROCEED_EVENT_ID),
    @Event(id = EventIds.INVALID_MSG_CTX)})
public class AddFaultToEnvelope extends AbstractProfileAction<Object, Envelope> {
	
	/** Class logger. */
    private final Logger log = LoggerFactory.getLogger(AddFaultToEnvelope.class);
    
    /**
     * Strategy used to locate the {@link SoapFaultContext} associated with a given {@link ProfileRequestContext}.
     */
    private Function<ProfileRequestContext<?, ?>, SoapFaultContext> soapFaultContextLookupStrategy;
    
    /** Constructor. */
    public AddFaultToEnvelope() {
    	super();
        soapFaultContextLookupStrategy =
                new ChildContextLookup<ProfileRequestContext<?, ?>, SoapFaultContext>(SoapFaultContext.class,
                        false);
    }
    
    /** {@inheritDoc} */
    protected org.springframework.webflow.execution.Event doExecute(final HttpServletRequest httpRequest, final HttpServletResponse httpResponse,
            final RequestContext springRequestContext,
            final ProfileRequestContext<Object, Envelope> profileRequestContext) throws ProfileException {
    	log.debug("Action {}: Attempting to add Fault into the body of an envelope", getId());
    	Envelope envelope = profileRequestContext.getOutboundMessageContext().getMessage();
    	if (envelope == null) {
    		log.debug("Action {}: Envelope was not found from the outbound context.", getId());
    		return ActionSupport.buildEvent(this, EventIds.INVALID_MSG_CTX);    		
    	}
    	Body body = envelope.getBody();
    	final SoapFaultContext faultContext = soapFaultContextLookupStrategy.apply(profileRequestContext);
    	final Fault fault = SoapActionSupport.addFaultToBody(this, body);
    	if (faultContext == null) { // unknown cause, so return internal server error
    		fault.getCode().setValue(FaultCode.SERVER);
    		fault.getMessage().setValue("Internal server error");
    	} else { // cause is known
        	fault.getCode().setValue(faultContext.getFaultcode());
        	fault.getMessage().setValue(faultContext.getFaultstring());
        	if (faultContext.getStatusCode() != null) { // details found (lu:Status)
        		final Status status = SoapActionSupport.addStatusToDetail(this, fault.getDetail());
        		status.setCode(faultContext.getStatusCode());
        		status.setRef(faultContext.getStatusRef());
        		status.setComment(faultContext.getStatusComment());
        	}
    	}
        log.debug("Action {}: All done, proceeding to the next state", getId());
        return ActionSupport.buildProceedEvent(this);
    }
}
