/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.profile;

import javax.servlet.http.HttpServletRequest;

import net.shibboleth.utilities.java.support.xml.ParserPool;

import org.opensaml.core.xml.config.XMLObjectProviderRegistrySupport;
import org.opensaml.core.xml.io.Unmarshaller;
import org.opensaml.messaging.context.MessageContext;
import org.opensaml.messaging.decoder.MessageDecodingException;
import org.opensaml.messaging.decoder.servlet.BaseHttpServletRequestXmlMessageDecoder;
import org.opensaml.soap.soap11.Envelope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

/** Decodes an incoming SOAP envelope. */
public class EnvelopeDecoder extends BaseHttpServletRequestXmlMessageDecoder<Envelope> {
    
    /** Class logger. */
    private final Logger log = LoggerFactory.getLogger(EnvelopeDecoder.class);
	
	/** {@inheritDoc} */
	protected void doDecode() throws MessageDecodingException {
		HttpServletRequest httpRequest = getHttpServletRequest();
		log.debug("Obtaining the parser pool");
		ParserPool parserPool = getParserPool();
		log.debug("Attempting to parse the servlet request into an Envelope");
		Envelope envelope;
		try {
			Document document = parserPool.parse(httpRequest.getInputStream());
			Unmarshaller unmarshaller = XMLObjectProviderRegistrySupport.getUnmarshallerFactory().getUnmarshaller(Envelope.DEFAULT_ELEMENT_NAME);
			envelope = (Envelope) unmarshaller.unmarshall(document.getDocumentElement());
		} catch (Exception e) {
			log.debug("Could not parse the request into an Envelope", e);
		    throw new MessageDecodingException("Could not parse the request into an Envelope", e);
		}
		log.debug("Parsing was successful, now attaching it into the message context");
		MessageContext<Envelope> messageContext = new MessageContext<Envelope>();
		messageContext.setMessage(envelope);
		setMessageContext(messageContext);
	}

}
