/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.profile;

import javax.annotation.Nonnull;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.glite.sts.profile.config.WsTrustInteroperabilityProfileConfiguration;
import org.opensaml.messaging.context.MessageContext;
import org.opensaml.messaging.context.navigate.ChildContextLookup;
import org.opensaml.soap.soap11.Envelope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.webflow.execution.RequestContext;

import com.google.common.base.Function;

import net.shibboleth.ext.spring.webflow.Event;
import net.shibboleth.ext.spring.webflow.Events;
import net.shibboleth.idp.authn.AuthenticationRequestContext;
import net.shibboleth.idp.profile.AbstractProfileAction;
import net.shibboleth.idp.profile.ActionSupport;
import net.shibboleth.idp.profile.ProfileException;
import net.shibboleth.idp.profile.ProfileRequestContext;
import net.shibboleth.utilities.java.support.component.IdentifiableComponent;

/** 
 * Choose the flow to extract incoming claims. Also generates the {@link AuthenticationRequestContext} and
 * attaches it into the {@link ProfileRequestContext}.
 */
@Events({
    @Event(id = "UsernamePassword"),
    @Event(id = "SAMLAssertion")})
public class CheckClaims extends AbstractProfileAction<Envelope, Object> {
	
    /** Class logger. */
    private final Logger log = LoggerFactory.getLogger(CheckClaims.class);
    
    /**
     * Strategy used to look up the {@link TokenRequestMessageContext} associated with the inbound message context.
     */
    private Function<MessageContext<Envelope>, TokenRequestMessageContext> tokenRequestMessageContextLookupStrategy;
    
    /** Is LDAP, i.e. UsernamePassword token enabled. */
    private boolean ldapEnabled;
    
    /** 
     * Constructor. 
     * 
     * @param enableLdap is LDAP configuration enabled
     */
    public CheckClaims(boolean enableLdap) {
    	super();
        tokenRequestMessageContextLookupStrategy =
                new ChildContextLookup<MessageContext<Envelope>, TokenRequestMessageContext>(TokenRequestMessageContext.class,
                        false);
        this.ldapEnabled = enableLdap;
    }
    
    /** {@inheritDoc} */
    protected org.springframework.webflow.execution.Event doExecute(final HttpServletRequest httpRequest, final HttpServletResponse httpResponse,
            final RequestContext springRequestContext,
            final ProfileRequestContext<Envelope, Object> profileRequestContext) throws ProfileException {
    	final MessageContext<Envelope> messageContext = profileRequestContext.getInboundMessageContext();
    	final TokenRequestMessageContext tokenRequestContext = tokenRequestMessageContextLookupStrategy.apply(messageContext);
    	String claimsDialect = tokenRequestContext.getClaimsDialect();
    	if (claimsDialect.equals(WsTrustInteroperabilityProfileConfiguration.TOKEN_TYPE_USERNAME_ID)) {
    		log.debug("Action {} UsernamePassword token in the request.", getId());
    		if (this.ldapEnabled) {
    			return generateAuthenticationContextAndProceed(profileRequestContext, this, "UsernamePassword");
    		} else {
    			log.debug("Action {} Ignoring UsernamePassword token in the request, as LDAP is not enabled.", getId());
    			throw new UnSupportedIncomingTokenTypeException(claimsDialect);
    		}
    	} else if (claimsDialect.equals(WsTrustInteroperabilityProfileConfiguration.TOKEN_TYPE_SAML_ID)) {
    		log.debug("Action {} SAMLAssertion token in the request.", getId());
    		return generateAuthenticationContextAndProceed(profileRequestContext, this, "SAMLAssertion");
    	} else {
    		log.debug("Action {}: Wrong claims dialect {} in the request.", getId(), claimsDialect);
    		throw new UnSupportedIncomingTokenTypeException(claimsDialect);
    	}
    }
    
    /**
     * Generates the {@link AuthenticationRequestContext} and attaches it into the given {@link ProfileRequestContext}.
     * @param profileRequestContext the profile request context
     * @param source the source action
     * @param eventId event id to be returned
     * @return the result of this action, never null
     */
    protected org.springframework.webflow.execution.Event generateAuthenticationContextAndProceed(final ProfileRequestContext<Envelope, Object> profileRequestContext, @Nonnull final IdentifiableComponent source, @Nonnull final String eventId) {
    	profileRequestContext.addSubcontext(new AuthenticationRequestContext(null, null), true);
    	return ActionSupport.buildEvent(source, eventId);
    }
    
    /** Exception thrown when the incoming WS-Trust message had not claims. */
    public static class UnSupportedIncomingTokenTypeException extends ProfileException {

        /** Serial version UID. */
		private static final long serialVersionUID = -7078373199425421250L;
		
        /**
         * Constructor.
         * 
         * @param message exception message
         */
        public UnSupportedIncomingTokenTypeException(String wrongType) {
        	super("Token type '" + wrongType + "' is not supported as an incoming token!");
        }
    	
    }

}
