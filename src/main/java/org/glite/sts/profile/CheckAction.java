/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.profile;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.glite.sts.profile.config.WsTrustInteroperabilityProfileConfiguration;
import org.opensaml.core.xml.XMLObject;
import org.opensaml.soap.soap11.Envelope;
import org.opensaml.soap.soap11.Header;
import org.opensaml.soap.wsaddressing.Action;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.webflow.execution.RequestContext;

import net.shibboleth.ext.spring.webflow.Event;
import net.shibboleth.ext.spring.webflow.Events;
import net.shibboleth.idp.profile.AbstractProfileAction;
import net.shibboleth.idp.profile.ActionSupport;
import net.shibboleth.idp.profile.EventIds;
import net.shibboleth.idp.profile.ProfileException;
import net.shibboleth.idp.profile.ProfileRequestContext;

/** Checks whether the inbound SOAP envelope has a supported action header (ISSUE-operation). */
@Events({
    @Event(id = EventIds.PROCEED_EVENT_ID),
    @Event(id = EventIds.INVALID_PROFILE_CTX)})
public class CheckAction extends AbstractProfileAction<Envelope, Object> {
	
    /** Class logger. */
    private final Logger log = LoggerFactory.getLogger(CheckAction.class);
    
    /** {@inheritDoc} */
    protected org.springframework.webflow.execution.Event doExecute(final HttpServletRequest httpRequest, final HttpServletResponse httpResponse,
            final RequestContext springRequestContext,
            final ProfileRequestContext<Envelope, Object> profileRequestContext) throws ProfileException {

    	final Envelope envelope = profileRequestContext.getInboundMessageContext().getMessage();
    	final Header header = envelope.getHeader();
        if (header == null) {
        	log.error("Action {}: Header was not found from the envelope!", getId());
        	return ActionSupport.buildEvent(this, EventIds.INVALID_PROFILE_CTX);
        }
        String action = getActionFromEnvelopeHeader(header);
        if (!WsTrustInteroperabilityProfileConfiguration.ISSUE_OPERATION_ID.equals(action)) {
        	log.warn("Action {}: Ignoring an {} operation request.", getId(), action);
        	throw new UnSupportedActionException(action);
        }
        return ActionSupport.buildProceedEvent(this);
    }
    
    protected String getActionFromEnvelopeHeader(final Header header) throws ProfileException {
    	List<XMLObject> objects = header.getUnknownXMLObjects(Action.ELEMENT_NAME);
    	if (objects.size() == 1) {
    		Action action = (Action) objects.get(0);
    		return action.getValue();
    	} else {
    		throw new ProfileException("No single Action element found from the envelope header!");
    	}
    }
    
    /** Exception thrown when the incoming message had not supported action. */
    public static class UnSupportedActionException extends ProfileException {

        /** Serial version UID. */
        private static final long serialVersionUID = -872763232432124555L;

        /**
         * Constructor.
         * 
         * @param message exception message
         */
        public UnSupportedActionException(String action) {
        	super("Action " + action + " is not supported!");
        }
    }
}
