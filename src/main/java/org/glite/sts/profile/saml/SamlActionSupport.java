/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.profile.saml;

import javax.annotation.Nonnull;

import net.shibboleth.idp.profile.AbstractProfileAction;

import org.opensaml.core.xml.config.XMLObjectProviderRegistrySupport;
import org.opensaml.saml.common.SAMLObjectBuilder;
import org.opensaml.saml.saml2.core.AuthnRequest;
import org.opensaml.saml.saml2.core.Issuer;
import org.opensaml.saml.saml2.ecp.Request;
import org.opensaml.soap.soap11.Body;
import org.opensaml.soap.soap11.Header;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** Helper methods for SAML-related actions. */
public class SamlActionSupport {
	
	/**
	 * Constructor.
	 */
	public SamlActionSupport() {

	}
	
    /**
     * Constructs and adds {@link Request} to the given {@link Header}.
     *
     * @param action the action
     * @param header the header to which the request will be added
     * @return the request that was added to the header
     */
    @Nonnull public static Request addEcpRequestToHeader(@Nonnull final AbstractProfileAction<?, ?> action,
            @Nonnull final Header header) { 
        @SuppressWarnings("unchecked")
		final SAMLObjectBuilder<Request> builder =
            (SAMLObjectBuilder<Request>) XMLObjectProviderRegistrySupport.getBuilderFactory().
                getBuilder(Request.DEFAULT_ELEMENT_NAME);
        final Request request = builder.buildObject();
        header.getUnknownXMLObjects().add(request);
        getLogger().debug("Action {}: ECP request added to a Header", action.getId());
        return request;
    }    

    /**
     * Constructs an {@link Issuer}.
     *
     * @param action the action
     * @param value the value of the issuer
     * @return the issuer that was constructed.
     */
    @Nonnull public static Issuer buildIssuer(@Nonnull final AbstractProfileAction<?, ?> action, String value) {
        @SuppressWarnings("unchecked")
		final SAMLObjectBuilder<Issuer> builder =
                (SAMLObjectBuilder<Issuer>) XMLObjectProviderRegistrySupport.getBuilderFactory().getBuilder(
                        Issuer.DEFAULT_ELEMENT_NAME);
        final Issuer issuer = builder.buildObject();
        issuer.setValue(value);
        getLogger().debug("Action {}: Issuer constructed.", action.getId());
        return issuer;
    }    

    /**
     * Constructs and adds {@link AuthnRequest} to the given {@link Body}.
     *
     * @param action the action
     * @param body the body to which the request will be added
     * @return the authentication request that was added to the body
     */
    @Nonnull public static AuthnRequest addAuthnRequestToBody(@Nonnull final AbstractProfileAction<?, ?> action,
            @Nonnull final Body body) { 
        @SuppressWarnings("unchecked")
		final SAMLObjectBuilder<AuthnRequest> builder =
            (SAMLObjectBuilder<AuthnRequest>) XMLObjectProviderRegistrySupport.getBuilderFactory().
                getBuilder(AuthnRequest.DEFAULT_ELEMENT_NAME);
        final AuthnRequest request = builder.buildObject();
        body.getUnknownXMLObjects().add(request);
        getLogger().debug("Action {}: authentication request added to a Body", action.getId());
        return request;
    }    
    
    /**
     * Gets the logger for this class.
     * 
     * @return logger for this class, never null
     */
    private static Logger getLogger() {
        return LoggerFactory.getLogger(SamlActionSupport.class);
    }

}
