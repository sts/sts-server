/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.profile;

import java.io.IOException;

import javax.annotation.Nonnull;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.dom.DOMSource;
import javax.xml.validation.Schema;
import javax.xml.validation.Validator;

import net.shibboleth.ext.spring.webflow.Event;
import net.shibboleth.ext.spring.webflow.Events;
import net.shibboleth.idp.profile.AbstractProfileAction;
import net.shibboleth.idp.profile.ActionSupport;
import net.shibboleth.idp.profile.EventIds;
import net.shibboleth.idp.profile.ProfileException;
import net.shibboleth.idp.profile.ProfileRequestContext;
import net.shibboleth.utilities.java.support.logic.Constraint;

import org.opensaml.core.xml.XMLObject;
import org.opensaml.messaging.context.MessageContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.webflow.execution.RequestContext;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

/** An action that schema validates inbound XML messages. 
 *
 * Based on <code>net.shibboleth.idp.profile.impl.SchemaValidateXmlMessage</code>.
 */
@Events({@Event(id = EventIds.PROCEED_EVENT_ID),
        @Event(id = EventIds.INVALID_MSG_CTX, description = "No inbound message context or message")})
public class CheckXmlSchema extends AbstractProfileAction<XMLObject, Object> {

    /** Class logger. */
    private Logger log = LoggerFactory.getLogger(CheckXmlSchema.class);

    /** Schema used to validate incoming messages. */
    private final Schema validationSchema;

    /**
     * Constructor.
     * 
     * @param schema schema used to validate incoming messages
     */
    public CheckXmlSchema(@Nonnull final Schema schema) {
        validationSchema = Constraint.isNotNull(schema, "Schema can not be null");
    }

    /**
     * Gets the schema used to validate incoming messages.
     * 
     * @return schema used to validate incoming messages, not null after action is initialized
     */
    @Nonnull public Schema getValidationSchema() {
        return validationSchema;
    }

    /** {@inheritDoc} */
    protected org.springframework.webflow.execution.Event doExecute(HttpServletRequest httpRequest,
            HttpServletResponse httpResponse, RequestContext springRequestContext,
            ProfileRequestContext<XMLObject, Object> profileRequestContext) throws ProfileException {

        log.debug("Action {}: Attempting to schema validate incoming message", getId());

        final MessageContext<XMLObject> msgCtx = profileRequestContext.getInboundMessageContext();
        if (msgCtx == null) {
            log.debug("Action {}: Inbound message context is null, unable to proceed", getId());
            return ActionSupport.buildEvent(this, EventIds.INVALID_MSG_CTX);
        }

        final XMLObject request = msgCtx.getMessage();
        if (request == null) {
            log.debug("Action {}: Inbound message context did not contain a message, unable to proceed", getId());
            return ActionSupport.buildEvent(this, EventIds.INVALID_MSG_CTX);
        }

        final Element requestElem = request.getDOM();
        if (requestElem == null) {
            log.debug("Action {}: Inbound message doesn't contain a DOM, unable to proceed", getId());
            return ActionSupport.buildEvent(this, EventIds.INVALID_MSG_CTX);
        }

        try {
            final Validator schemaValidator = validationSchema.newValidator();
            schemaValidator.validate(new DOMSource(requestElem));
        } catch (SAXException e) {
            log.debug("Action {}: Incoming request {} is not schema valid",
                    new Object[] {getId(), request.getElementQName(), e});
            throw new SchemaInvalidXmlMessageException(e.getMessage());
        } catch (IOException e) {
            log.debug("Action {}: Unable to read incoming message");
            throw new SchemaInvalidXmlMessageException(e.getMessage());
        }

        log.debug("Action {}: Incoming message is valid", getId());
        return ActionSupport.buildProceedEvent(this);
    }
    
    /** Exception thrown when the incoming XML message does not pass schema validation. */
    public static class SchemaInvalidXmlMessageException extends ProfileException {

        /** Serial version UID. */
		private static final long serialVersionUID = -7012312349423732050L;
		
        /**
         * Constructor.
         * 
         * @param message exception message
         */
        public SchemaInvalidXmlMessageException(String message) {
        	super("Schema validation error: " + message);
        }
    	
    }
}