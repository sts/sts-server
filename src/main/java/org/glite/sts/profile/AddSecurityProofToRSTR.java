/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.profile;

import java.io.ByteArrayOutputStream;
import java.security.PrivateKey;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.shibboleth.ext.spring.webflow.Event;
import net.shibboleth.ext.spring.webflow.Events;
import net.shibboleth.idp.profile.AbstractProfileAction;
import net.shibboleth.idp.profile.ActionSupport;
import net.shibboleth.idp.profile.EventIds;
import net.shibboleth.idp.profile.ProfileException;
import net.shibboleth.idp.profile.ProfileRequestContext;

import org.bouncycastle.util.encoders.Base64;
import org.glite.sts.profile.wstrust.WsTrustActionSupport;
import org.glite.sts.ta.TokenGenerationContext;
import org.glite.sts.ta.voms.VomsTokenGenerationContext;
import org.glite.sts.ta.x509.X509TokenGenerationContext;
import org.glite.sts.x509.CertificateKeys;
import org.opensaml.messaging.context.MessageContext;
import org.opensaml.messaging.context.navigate.ChildContextLookup;
import org.opensaml.soap.soap11.Envelope;
import org.opensaml.soap.wstrust.BinarySecret;
import org.opensaml.soap.wstrust.RequestSecurityTokenResponse;
import org.opensaml.soap.wstrust.RequestSecurityTokenResponseCollection;
import org.opensaml.soap.wstrust.RequestedProofToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.webflow.execution.RequestContext;

import com.google.common.base.Function;

import eu.emi.security.authn.x509.impl.CertificateUtils;
import eu.emi.security.authn.x509.impl.CertificateUtils.Encoding;

/**
 * Builds an {@link RequestedProofToken} and adds it to the {@link RequestSecurityTokenResponse}, inside the {@link RequestSecurityTokenResponseCollection}
 * in the {@link Body} of the {@link Envelope} set as the message of the {@link ProfileRequestContext#getOutboundMessageContext()}.
 */
@Events({
        @Event(id = EventIds.PROCEED_EVENT_ID),
        @Event(id = EventIds.INVALID_MSG_CTX)})
public class AddSecurityProofToRSTR extends AbstractProfileAction<Object, Envelope> {
	
    /** Class logger. */
    private final Logger log = LoggerFactory.getLogger(AddSecurityProofToRSTR.class);
    
    /**
     * Strategy used to locate the {@link TokenGenerationContext} associated with a given {@link MessageContext}.
     */
    private Function<MessageContext<?>, TokenGenerationContext> tokenGenerationContextLookupStrategy;
    
    /**
     * Strategy used to locate the {@link X509TokenGenerationContext} associated with a given {@link TokenGenerationContext}.
     */
    private Function<TokenGenerationContext, X509TokenGenerationContext> x509TokenGenerationContextLookupStrategy;
    
    /**
     * Strategy used to locate the {@link VomsTokenGenerationContext} associated with a given {@link TokenGenerationContext}.
     */
    private Function<TokenGenerationContext, VomsTokenGenerationContext> vomsTokenGenerationContextLookupStrategy;
    
    /** Constructor. */
    public AddSecurityProofToRSTR() {
    	super();
        tokenGenerationContextLookupStrategy =
                new ChildContextLookup<MessageContext<?>, TokenGenerationContext>(TokenGenerationContext.class,
                        false);
        x509TokenGenerationContextLookupStrategy =
        		new ChildContextLookup<TokenGenerationContext, X509TokenGenerationContext>(X509TokenGenerationContext.class,
        				false);
        vomsTokenGenerationContextLookupStrategy =
        		new ChildContextLookup<TokenGenerationContext, VomsTokenGenerationContext>(VomsTokenGenerationContext.class,
        				false);
    }
	
    /** {@inheritDoc} */
    protected org.springframework.webflow.execution.Event doExecute(final HttpServletRequest httpRequest, final HttpServletResponse httpResponse,
            final RequestContext springRequestContext,
            final ProfileRequestContext<Object, Envelope> profileRequestContext) throws ProfileException {
    	log.debug("Action {}: Attempting to add a proof token to outgoing RSTR inside an Envelope", getId());
    	final Envelope envelope = profileRequestContext.getOutboundMessageContext().getMessage();
    	if (envelope == null) {
    		log.debug("Action {}: Envelope was not found from the outbound context.", getId());
    		return ActionSupport.buildEvent(this, EventIds.INVALID_MSG_CTX);    		
    	}
    	TokenGenerationContext tokenGenerationCtx = tokenGenerationContextLookupStrategy.apply(profileRequestContext.getOutboundMessageContext());
		if (tokenGenerationCtx == null) {
            log.debug("Action {}: No TokenGenerationContext available, nothing left to do", getId());
            return ActionSupport.buildProceedEvent(this);
		}
		VomsTokenGenerationContext vomsContext = vomsTokenGenerationContextLookupStrategy.apply(tokenGenerationCtx);
		if (vomsContext != null) {
			log.debug("Action {}: VomsTokenGenerationContext is available for TokenGenerationContext{}", getId(), tokenGenerationCtx.getId());
			CertificateKeys proxyKeys = vomsContext.getProxyKeys();
			if (proxyKeys == null || proxyKeys.getPrivateKey() == null) {
				log.debug("Action {}: No private key available in the proxy credentials, nothing left to do", getId());
				return ActionSupport.buildProceedEvent(this);
			}
			encodePrivateKeyToRSTR(envelope, vomsContext.getProxyKeys().getPrivateKey());
			return ActionSupport.buildProceedEvent(this);
		}
    	X509TokenGenerationContext x509Context = x509TokenGenerationContextLookupStrategy.apply(tokenGenerationCtx);
		if (x509Context == null) {
            log.debug("Action {}: No X509TokenGenerationContext available for TokenGenerationContext {}, nothing left to do", getId(), tokenGenerationCtx.getId());
            return ActionSupport.buildProceedEvent(this);
		}
		if (x509Context.getCaResponse() == null || x509Context.getCaResponse().getCertificate() == null) {
			log.debug("Action {}: No corresponding certificate available for proof, nothing left to do", getId());
            return ActionSupport.buildProceedEvent(this);
		}
		CertificateKeys certKeys = x509Context.getCertKeys();
		if (certKeys == null || certKeys.getPrivateKey() == null) {
			log.debug("Action {}: No private key available in the certificate keys, nothing left to do", getId());
            return ActionSupport.buildProceedEvent(this);
		}
		encodePrivateKeyToRSTR(envelope, certKeys.getPrivateKey());
		return ActionSupport.buildProceedEvent(this);
    }
    
    /**
     * Encode the given private key to inside the RSTR.
     * @param envelope the envelope containing the RSTR
     * @param privateKey the private key to be encoded
     */
    protected void encodePrivateKeyToRSTR(final Envelope envelope, PrivateKey privateKey) {
    	RequestSecurityTokenResponseCollection rstrc = (RequestSecurityTokenResponseCollection) envelope.getBody().getUnknownXMLObjects(RequestSecurityTokenResponseCollection.ELEMENT_NAME).get(0);
    	RequestSecurityTokenResponse rstr = rstrc.getRequestSecurityTokenResponses().get(0);
    	
    	RequestedProofToken proofToken = WsTrustActionSupport.addRequestedProofTokenToRSTR(this, rstr);
    	BinarySecret secret = WsTrustActionSupport.addBinarySecretToProofToken(this, proofToken);
    	ByteArrayOutputStream baos = new ByteArrayOutputStream();
    	try {
    		CertificateUtils.savePrivateKey(baos, privateKey, Encoding.DER, null, null);
        } catch (Exception e) {
            log.warn("Action {}: Could not encode Private Key, nothing left to do", getId(), e);
            return;
        }
    	String proof = new String(Base64.encode(baos.toByteArray()));
    	secret.setType(BinarySecret.TYPE_ASYMMETRIC_KEY);
    	secret.setValue(proof);    	
    }
}
