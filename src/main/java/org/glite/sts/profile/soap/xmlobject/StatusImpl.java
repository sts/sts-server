/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.profile.soap.xmlobject;

import java.util.List;

import org.opensaml.core.xml.AbstractXMLObject;
import org.opensaml.core.xml.XMLObject;

/** Implementation for {@link Status}. */
public class StatusImpl extends AbstractXMLObject implements Status {
	
    /** Code attribute value. */
    private String code;
    
    /** Ref attribute value. */
    private String ref;
    
    /** Comment attribute value. */
    private String comment;
    
    /**
     * Constructor.
     * 
     * @param namespaceURI namespace of the element
     * @param elementLocalName name of the element
     * @param namespacePrefix namespace prefix of the element
     */
    protected StatusImpl(String namespaceURI, String elementLocalName, String namespacePrefix) {
        super(namespaceURI, elementLocalName, namespacePrefix);
    }

    /** {@inheritDoc} */
	public String getCode() {
		return this.code;
	}

    /** {@inheritDoc} */
	public void setCode(String newCode) {
		this.code = newCode;
	}

    /** {@inheritDoc} */
	public String getRef() {
		return this.ref;
	}

    /** {@inheritDoc} */
	public void setRef(String newRef) {
		this.ref = newRef;
	}

    /** {@inheritDoc} */
	public String getComment() {
		return this.comment;
	}

    /** {@inheritDoc} */
	public void setComment(String newComment) {
		this.comment = newComment;
	}

    /** {@inheritDoc} */
	public List<XMLObject> getOrderedChildren() {
		return null;
	}

}
