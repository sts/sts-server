/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.profile.soap.xmlobject;

import org.opensaml.core.xml.AbstractExtensibleXMLObject;

/** Implementation for {@link Framework}. */
public class FrameworkImpl extends AbstractExtensibleXMLObject implements Framework {
    
    /** Version attribute value. */
    private String version;
    
    /**
     * Constructor.
     * 
     * @param namespaceURI namespace of the element
     * @param elementLocalName name of the element
     * @param namespacePrefix namespace prefix of the element
     */
    protected FrameworkImpl(String namespaceURI, String elementLocalName, String namespacePrefix) {
        super(namespaceURI, elementLocalName, namespacePrefix);
    }

    /** {@inheritDoc} */
   public String getVersion() {
        return version;
    }

   /** {@inheritDoc} */
   public void setVersion(String newVersion) {
        version = prepareForAssignment(version, newVersion);
    }

}
