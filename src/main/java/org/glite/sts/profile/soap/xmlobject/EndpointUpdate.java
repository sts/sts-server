/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.profile.soap.xmlobject;

import javax.xml.namespace.QName;

import org.opensaml.core.xml.AttributeExtensibleXMLObject;
import org.opensaml.core.xml.ElementExtensibleXMLObject;
import org.opensaml.soap.wsaddressing.EndpointReferenceType;

/** XMLObject for the Liberty ID-WSF SOAP Binding's EndpointUpdate. */
public interface EndpointUpdate extends EndpointReferenceType, ElementExtensibleXMLObject, AttributeExtensibleXMLObject {
    
    /** Default element local name. */
    public static final String DEFAULT_ELEMENT_LOCAL_NAME = "EndpointUpdate";
    
    /** Default element name. */
    public static final QName DEFAULT_ELEMENT_NAME = new QName(LibertyIDWSFConstants.IDWSF_SB_NS, DEFAULT_ELEMENT_LOCAL_NAME, LibertyIDWSFConstants.IDWSF_SB_PREFIX);
    
    /** updateType attribute name. */
    public static final String UPDATE_TYPE_ATTRIB_NAME = "updateType";
    
    /** Value for the complete end-point update */
    public static final String COMPLETE_VALUE = "urn:liberty:sb:2006-08:EndpointUpdate:Complete";
    
    /** Value for the partial complete end-point update */
    public static final String PARTIAL_COMPLETE_VALUE = "urn:liberty:sb:2006-08:EndpointUpdate:Partial";
    
    /**
     * Get the updateType attribute value.
     * 
     * @return the updateType attribute value
     */
    public String getUpdateType();

    /**
     * Set the updateType attribute value.
     * 
     * @param newUpdateType the new updateType attribute value
     */
    public void setUpdateType(String newUpdateType);
    
}
