/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.profile.soap.xmlobject;

import javax.annotation.concurrent.ThreadSafe;

import org.opensaml.core.xml.AbstractXMLObjectBuilder;

/** Builder of {@link EndpointUpdate} objects. */
@ThreadSafe
public class EndpointUpdateBuilder extends AbstractXMLObjectBuilder<EndpointUpdate> {

    /** {@inheritDoc} */
    public EndpointUpdate buildObject(String namespaceURI, String localName, String namespacePrefix) {
        return new EndpointUpdateImpl(namespaceURI, localName, namespacePrefix);
    }
    
    /**
     * Build a EndpointUpdate element with the default namespace prefix and element name.
     * 
     * @return a new instance of {@link EndpointUpdate}
     */
    public EndpointUpdate buildObject() {
        return buildObject(LibertyIDWSFConstants.IDWSF_SB_NS, EndpointUpdate.DEFAULT_ELEMENT_LOCAL_NAME,
                LibertyIDWSFConstants.IDWSF_SB_PREFIX);
    }

}

