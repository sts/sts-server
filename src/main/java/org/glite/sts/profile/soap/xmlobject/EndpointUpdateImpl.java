/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.profile.soap.xmlobject;

import org.opensaml.soap.wsaddressing.impl.EndpointReferenceTypeImpl;

/** Implementation for {@link Framework}. */
public class EndpointUpdateImpl extends EndpointReferenceTypeImpl implements EndpointUpdate {
    
    /** UpdateType attribute value. */
    private String updateType;
    
    /**
     * Constructor.
     * 
     * @param namespaceURI namespace of the element
     * @param elementLocalName name of the element
     * @param namespacePrefix namespace prefix of the element
     */
    protected EndpointUpdateImpl(String namespaceURI, String elementLocalName, String namespacePrefix) {
        super(namespaceURI, elementLocalName, namespacePrefix);
    }

    /** {@inheritDoc} */
   public String getUpdateType() {
        return updateType;
    }

   /** {@inheritDoc} */
   public void setUpdateType(String newUpdateType) {
        updateType = prepareForAssignment(updateType, newUpdateType);
    }

}
