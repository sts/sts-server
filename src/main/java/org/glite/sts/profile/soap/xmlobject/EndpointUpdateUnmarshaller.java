/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.profile.soap.xmlobject;

import javax.annotation.concurrent.ThreadSafe;

import org.opensaml.core.xml.XMLObject;
import org.opensaml.core.xml.io.UnmarshallingException;
import org.opensaml.core.xml.util.XMLObjectSupport;
import org.opensaml.soap.wsaddressing.impl.EndpointReferenceTypeUnmarshaller;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Attr;

/** Unmarshaller for {@link EndpointUpdate}. */
@ThreadSafe
public class EndpointUpdateUnmarshaller extends EndpointReferenceTypeUnmarshaller {

    /** Logger. */
    private final Logger log = LoggerFactory.getLogger(EndpointUpdateUnmarshaller.class);
    
    /** {@inheritDoc} */
    protected void processAttribute(XMLObject xmlObject, Attr attribute) throws UnmarshallingException {
        log.debug("Processing attributes for EndpointUpdate");
        EndpointUpdate endpointUpdate = (EndpointUpdate) xmlObject;
        XMLObjectSupport.unmarshallToAttributeMap(endpointUpdate.getUnknownAttributes(), attribute);
    }

}
