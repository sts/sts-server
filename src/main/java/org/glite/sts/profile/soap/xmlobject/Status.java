/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.profile.soap.xmlobject;

import javax.xml.namespace.QName;

import org.opensaml.core.xml.XMLObject;

/** XMLObject for the Liberty ID-WSF Utility's Status. */
public interface Status extends XMLObject {
	
    /** Default element local name. */
    public static final String DEFAULT_ELEMENT_LOCAL_NAME = "Status";
    
    /** Default element name. */
    public static final QName DEFAULT_ELEMENT_NAME = new QName(LibertyIDWSFConstants.IDWSF_UTIL_NS, DEFAULT_ELEMENT_LOCAL_NAME, LibertyIDWSFConstants.IDWSF_UTIL_PREFIX);
    
    /** code attribute name. */
    public static final String CODE_ATTRIB_NAME = "code";

    /** ref attribute name. */
    public static final String REF_ATTRIB_NAME = "ref";
    
    /** comment attribute name. */
    public static final String COMMENT_ATTRIB_NAME = "comment";
    
    /** code value if the Liberty ID-WSF message is not understood. */
    public static final String CODE_ID_STAR_MSG_NOT_UNDERSTOOD = "IDStarMsgNotUnderstood";
    
    /** code value if the timestamp cannot be accepted. */
    public static final String CODE_STALE_MSG = "StaleMsg";
    
    /** code value if RelatesTo doesn't match with the corresponding MessageID. */
    public static final String CODE_INVALID_REF_TO_MSG_ID = "InvalidRefToMsgID";
    
    /**
     * Get the code attribute value.
     * 
     * @return the code attribute value
     */
    public String getCode();

    /**
     * Set the code attribute value.
     * 
     * @param newCode the new code attribute value
     */
    public void setCode(String newCode);

    /**
     * Get the ref attribute value.
     * 
     * @return the ref attribute value
     */
    public String getRef();

    /**
     * Set the ref attribute value.
     * 
     * @param newRef the new ref attribute value
     */
    public void setRef(String newRef);

    /**
     * Get the comment attribute value.
     * 
     * @return the comment attribute value
     */
    public String getComment();

    /**
     * Set the comment attribute value.
     * 
     * @param newComment the new comment attribute value
     */
    public void setComment(String newComment);

}
