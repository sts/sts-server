/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.profile.soap.xmlobject;

import javax.annotation.concurrent.ThreadSafe;
import org.opensaml.core.xml.XMLObject;
import org.opensaml.core.xml.io.MarshallingException;
import org.opensaml.core.xml.util.XMLObjectSupport;
import org.opensaml.saml.common.AbstractSAMLObjectMarshaller;
import org.w3c.dom.Element;

/** Marshaller for {@link Request}. */
@ThreadSafe
public class RequestMarshaller extends AbstractSAMLObjectMarshaller {
	
    /** {@inheritDoc} */
    protected void marshallAttributes(XMLObject xmlObject, Element domElement) throws MarshallingException {
        Request request = (Request) xmlObject;

        if (request.getMessageID() != null) {
            domElement.setAttributeNS(null, Request.MESSAGE_ID_ATTRIB_NAME, request.getMessageID());
        }
        if (request.getResponseConsumerURL() != null) {
        	domElement.setAttributeNS(null, Request.RESPONSE_CONSUMER_URL_ATTRIB_NAME, request.getResponseConsumerURL());
        }
        if (request.getService() != null) {
        	domElement.setAttributeNS(null, Request.SERVICE_ATTRIB_NAME, request.getService());
        }
        if (request.getSOAP11Actor() != null) {
            XMLObjectSupport.marshallAttribute(Request.SOAP11_ACTOR_ATTR_NAME, request.getSOAP11Actor(), domElement, false);
        }
        if (request.isSOAP11MustUnderstandXSBoolean() != null) {
            XMLObjectSupport.marshallAttribute(Request.SOAP11_MUST_UNDERSTAND_ATTR_NAME, request.isSOAP11MustUnderstandXSBoolean().toString(), domElement, false);
        }
    }
}
