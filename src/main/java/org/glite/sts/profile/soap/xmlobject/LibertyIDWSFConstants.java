/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.profile.soap.xmlobject;

/** Namespace declarations and their prefixes for Liberty ID-WSF. */
public class LibertyIDWSFConstants {
    
    public static final String IDWSF_SBF_NS = "urn:liberty:sb";
    
    public static final String IDWSF_SBF_PREFIX = "sbf";
    
    public static final String IDWSF_SB_NS = "urn:liberty:sb:2006-08";
    
    public static final String IDWSF_SB_PREFIX = "sb";
    
    public static final String IDWSF_UTIL_NS = "urn:liberty:util:2006-08";
    
    public static final String IDWSF_UTIL_PREFIX = "lu";

}
