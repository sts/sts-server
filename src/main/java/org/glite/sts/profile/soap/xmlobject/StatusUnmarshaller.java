/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.profile.soap.xmlobject;

import javax.annotation.concurrent.ThreadSafe;

import org.opensaml.core.xml.XMLObject;
import org.opensaml.core.xml.io.AbstractXMLObjectUnmarshaller;
import org.opensaml.core.xml.io.UnmarshallingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Attr;

/** Unmarshaller for {@link Status}. */
@ThreadSafe
public class StatusUnmarshaller extends AbstractXMLObjectUnmarshaller {
	
    /** Logger. */
    private final Logger log = LoggerFactory.getLogger(StatusUnmarshaller.class);
    
    /** {@inheritDoc} */
    protected void processAttribute(XMLObject xmlObject, Attr attribute) throws UnmarshallingException {
        log.debug("Processing attributes for Status");
        Status status = (Status) xmlObject;

        if (attribute.getLocalName().equals(Status.CODE_ATTRIB_NAME)) {
            log.debug("Code {} found, setting it for the value.", attribute.getValue());
            status.setCode(attribute.getValue());
        } else if (attribute.getLocalName().equals(Status.REF_ATTRIB_NAME)) {
        	log.debug("Ref {} found, setting it for the value.", attribute.getValue());
        	status.setRef(attribute.getValue());
        } else if (attribute.getLocalName().equals(Status.COMMENT_ATTRIB_NAME)) {
        	log.debug("Comment {} found, setting it for the value.", attribute.getValue());
        	status.setComment(attribute.getValue());
        }
    }

}
