/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.profile.soap.xmlobject;

import javax.xml.namespace.QName;

import org.opensaml.saml.common.SAMLObject;
import org.opensaml.saml.common.xml.SAMLConstants;
import org.opensaml.soap.soap11.ActorBearing;
import org.opensaml.soap.soap11.MustUnderstandBearing;

/** XMLObject for the Liberty Reverse HTTP Binding for SOAP (PAOS) 1.1.'s Request */
public interface Request extends SAMLObject, MustUnderstandBearing, ActorBearing {
	    
	/** Default element local name. */
	public static final String DEFAULT_ELEMENT_LOCAL_NAME = "Request";

	/** Default element name. */
	public static final QName DEFAULT_ELEMENT_NAME = new QName(SAMLConstants.PAOS_NS, DEFAULT_ELEMENT_LOCAL_NAME, SAMLConstants.PAOS_PREFIX);
	
    /** responseConsumerURL attribute name. */
    public static final String RESPONSE_CONSUMER_URL_ATTRIB_NAME = "responseConsumerURL";

    /** service attribute name. */
    public static final String SERVICE_ATTRIB_NAME = "service";
    
    /** messageID attribute name. */
    public static final String MESSAGE_ID_ATTRIB_NAME = "messageID";
    
    /**
     * Get the responseConsumerURL attribute value.
     * 
     * @return the responseConsumerURL attribute value
     */
    public String getResponseConsumerURL();
    
    /**
     * Set the responseConsumerURL attribute value.
     * 
     * @param newResponseConsumerURL the new responseConsumerURL attribute value
     */
    public void setResponseConsumerURL(String newResponseConsumerURL);
    
    /**
     * Get the service attribute value.
     * 
     * @return the service attribute value
     */
    public String getService();
    
    /**
     * Set the service attribute value.
     * 
     * @param newService the new service attribute value
     */
    public void setService(String newService);
    
    /**
     * Get the messageID attribute value.
     * 
     * @return the messageID attribute value
     */
    public String getMessageID();
    
    /**
     * Set the messageID attribute value.
     * 
     * @param newMessageID the new messageID attribute value
     */
    public void setMessageID(String newMessageID);

}
