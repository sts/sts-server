/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.profile.soap.xmlobject;

import javax.annotation.concurrent.ThreadSafe;

import org.opensaml.core.xml.XMLObject;
import org.opensaml.core.xml.io.AbstractXMLObjectMarshaller;
import org.opensaml.core.xml.io.MarshallingException;
import org.w3c.dom.Element;

/** Marshaller for {@link Status}. */
@ThreadSafe
public class StatusMarshaller extends AbstractXMLObjectMarshaller {

    /** {@inheritDoc} */
    protected void marshallAttributes(XMLObject xmlObject, Element domElement) throws MarshallingException {
        Status status = (Status) xmlObject;

        if (status.getCode() != null) {
            domElement.setAttributeNS(null, Status.CODE_ATTRIB_NAME, status.getCode());
        }
        
        if (status.getRef() != null) {
        	domElement.setAttributeNS(null, Status.REF_ATTRIB_NAME, status.getRef());
        }
        
        if (status.getComment() != null) {
        	domElement.setAttributeNS(null, Status.COMMENT_ATTRIB_NAME, status.getComment());
        }

    }
}
