/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.profile.soap.xmlobject;

import javax.annotation.concurrent.ThreadSafe;
import javax.xml.namespace.QName;

import net.shibboleth.utilities.java.support.xml.QNameSupport;

import org.opensaml.core.xml.XMLObject;
import org.opensaml.core.xml.io.AbstractXMLObjectUnmarshaller;
import org.opensaml.core.xml.io.UnmarshallingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Attr;

/** Unmarshaller for {@link Framework}. */
@ThreadSafe
public class FrameworkUnmarshaller extends AbstractXMLObjectUnmarshaller {

    /** Logger. */
    private final Logger log = LoggerFactory.getLogger(FrameworkUnmarshaller.class);
    
    /** {@inheritDoc} */
    protected void processAttribute(XMLObject xmlObject, Attr attribute) throws UnmarshallingException {
        log.debug("Processing attributes for Framework");
        Framework framework = (Framework) xmlObject;

        if (attribute.getLocalName().equals(Framework.VERSION_ATTRIB_NAME)) {
            log.debug("Version {} found, setting it for the value.", attribute.getValue());
            framework.setVersion(attribute.getValue());
        } else {
            QName attribQName = QNameSupport.getNodeQName(attribute);
            if (attribute.isId()) {
                framework.getUnknownAttributes().registerID(attribQName);
            }
            framework.getUnknownAttributes().put(attribQName, attribute.getValue());
        }
    }

}
