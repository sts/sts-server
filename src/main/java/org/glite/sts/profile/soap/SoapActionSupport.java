/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.profile.soap;

import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPublicKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.List;

import javax.annotation.Nonnull;

import org.apache.xml.security.exceptions.Base64DecodingException;
import org.apache.xml.security.utils.Base64;
import org.glite.sts.profile.soap.xmlobject.Framework;
import org.glite.sts.profile.soap.xmlobject.FrameworkBuilder;
import org.glite.sts.profile.soap.xmlobject.Request;
import org.glite.sts.profile.soap.xmlobject.RequestBuilder;
import org.glite.sts.profile.soap.xmlobject.Status;
import org.glite.sts.profile.soap.xmlobject.StatusBuilder;
import org.opensaml.core.xml.XMLObject;
import org.opensaml.core.xml.config.XMLObjectProviderRegistrySupport;
import org.opensaml.soap.common.SOAPObjectBuilder;
import org.opensaml.soap.soap11.Body;
import org.opensaml.soap.soap11.Detail;
import org.opensaml.soap.soap11.Envelope;
import org.opensaml.soap.soap11.Fault;
import org.opensaml.soap.soap11.FaultActor;
import org.opensaml.soap.soap11.FaultCode;
import org.opensaml.soap.soap11.FaultString;
import org.opensaml.soap.soap11.Header;
import org.opensaml.soap.wsaddressing.Action;
import org.opensaml.soap.wsaddressing.MessageID;
import org.opensaml.soap.wsaddressing.RelatesTo;
import org.opensaml.soap.wsaddressing.WSAddressingObjectBuilder;
import org.opensaml.soap.wssecurity.BinarySecurityToken;
import org.opensaml.soap.wstrust.RequestSecurityToken;
import org.opensaml.soap.wstrust.UseKey;
import org.opensaml.xmlsec.signature.KeyInfo;
import org.opensaml.xmlsec.signature.KeyValue;
import org.opensaml.xmlsec.signature.RSAKeyValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.shibboleth.idp.profile.AbstractProfileAction;

/** Helper methods for SOAP-related actions. */
public final class SoapActionSupport {
    
    /** Constructor. */
    private SoapActionSupport() {
    	// no op
    }
    
    /** 
     * Constructs and adds {@link Body} to the given {@link Envelope}.
     *
     * @param action the action
     * @param envelope the envelope to which the body will be added
     * 
     * @return the body that was added to the envelope
     */
    @Nonnull public static Body addBodytoEnvelope(@Nonnull final AbstractProfileAction<?, ?> action,
            @Nonnull final Envelope envelope) {
        Body body = envelope.getBody();
        if (body == null) {
            @SuppressWarnings("unchecked")
            final SOAPObjectBuilder<Body> builder =
                (SOAPObjectBuilder<Body>) XMLObjectProviderRegistrySupport.getBuilderFactory().
                    getBuilder(Body.DEFAULT_ELEMENT_NAME);
            body = builder.buildObject();
            envelope.setBody(body);
            getLogger().debug("Action {}: Envelope did not already contain a Body, one was added", action.getId());
        } else {
            getLogger().debug("Action {}: Envelope already contains a Body, nothing was done", action.getId());
        }
        return body;
    }

    /**
     * Constructs and adds {@link Header} to the given {@link Envelope}.
     *
     * @param action the action
     * @param envelope the envelope to which the header will be added
     * 
     * @return the header that was added to the envelope
     */
    @Nonnull public static Header addHeadertoEnvelope(@Nonnull final AbstractProfileAction<?, ?> action,
            @Nonnull final Envelope envelope) {
        Header header = envelope.getHeader();
        if (header == null) {
            @SuppressWarnings("unchecked")
            final SOAPObjectBuilder<Header> builder =
                (SOAPObjectBuilder<Header>) XMLObjectProviderRegistrySupport.getBuilderFactory().
                    getBuilder(Header.DEFAULT_ELEMENT_NAME);
            header = builder.buildObject();
            envelope.setHeader(header);
            getLogger().debug("Action {}: Envelope did not already contain a Header, one was added", action.getId());
        } else {
            getLogger().debug("Action {}: Envelope already contains a Header, nothing was done", action.getId());
        }
        return header;
    }

    /**
     * Constructs and adds {@link Action} to the given {@link Header}.
     *
     * @param action the action
     * @param header the header to which the action will be added
     * 
     * @return the action that was added to the header
     */
    @Nonnull public static Action addActionToHeader(@Nonnull final AbstractProfileAction<?, ?> action,
            @Nonnull final Header header) {
        @SuppressWarnings("unchecked")
        final WSAddressingObjectBuilder<Action> builder =
            (WSAddressingObjectBuilder<Action>) XMLObjectProviderRegistrySupport.getBuilderFactory().
                getBuilder(Action.ELEMENT_NAME);
        final Action soapAction = builder.buildObject();
        header.getUnknownXMLObjects().add(soapAction);
        getLogger().debug("Action {}: Action added to a Header", action.getId());
        return soapAction;
    }

    /**
     * Constructs and adds {@link MessageID} to the given {@link Header}.
     *
     * @param action the action
     * @param header the header to which the message id will be added
     * @param messageIDValue the value of the message id to be added
     * @return the message id that was added to the header
     */
    @Nonnull public static MessageID addMessageIDToHeader(@Nonnull final AbstractProfileAction<?, ?> action,
            @Nonnull final Header header, @Nonnull final String messageIDValue) {
        @SuppressWarnings("unchecked")
        final WSAddressingObjectBuilder<MessageID> builder =
            (WSAddressingObjectBuilder<MessageID>) XMLObjectProviderRegistrySupport.getBuilderFactory().
                getBuilder(MessageID.ELEMENT_NAME);
        final MessageID messageID = builder.buildObject();
        messageID.setValue(messageIDValue);
        header.getUnknownXMLObjects().add(messageID);
        getLogger().debug("Action {}: MessageID {} added to a Header", action.getId(), messageIDValue);
        return messageID;
    }
    
    /**
     * Constructs and adds {@link RelatesTo} to the given {@link Header}.
     *
     * @param action the action
     * @param header the header to which the message id will be added
     * @param messageIDValue the value of the message id to be added
     * @return the message id that was added to the header
     */
    @Nonnull public static RelatesTo addRelatesToToHeader(@Nonnull final AbstractProfileAction<?, ?> action,
            @Nonnull final Header header, @Nonnull final String relatesToValue) {
        @SuppressWarnings("unchecked")
        final WSAddressingObjectBuilder<RelatesTo> builder =
            (WSAddressingObjectBuilder<RelatesTo>) XMLObjectProviderRegistrySupport.getBuilderFactory().
                getBuilder(RelatesTo.ELEMENT_NAME);
        final RelatesTo relatesTo = builder.buildObject();
        relatesTo.setValue(relatesToValue);
        header.getUnknownXMLObjects().add(relatesTo);
        getLogger().debug("Action {}: RelatesTo {} added to a Header", action.getId(), relatesToValue);
        return relatesTo;
    }
    
    /**
     * Constructs and adds {@link Framework} to the given {@link Header}.
     *
     * @param action the action
     * @param header the header to which the framework will be added
     * @param version the version value of the framework to be added
     * @return the framework that was added to the header
     */
    @Nonnull public static Framework addFrameworkHeader(@Nonnull final AbstractProfileAction<?, ?> action,
            @Nonnull final Header header, @Nonnull final String version) { 
        final FrameworkBuilder builder =
            (FrameworkBuilder) XMLObjectProviderRegistrySupport.getBuilderFactory().
                getBuilder(Framework.DEFAULT_ELEMENT_NAME);
        final Framework framework = builder.buildObject();
        framework.setVersion(version);
        header.getUnknownXMLObjects().add(framework);
        getLogger().debug("Action {}: Framework version {} added to a Header", action.getId(), version);
        return framework;
    }

    /**
     * Constructs and adds {@link Fault} to the given {@link Body}.
     *
     * @param action the action
     * @param body the body to which the fault will be added
     * @return the fault that was added to the body
     */
    @Nonnull public static Fault addFaultToBody(@Nonnull final AbstractProfileAction<?, ?> action,
            @Nonnull final Body body) {
    	@SuppressWarnings("unchecked")
		final SOAPObjectBuilder<Fault> builder = (SOAPObjectBuilder<Fault>) XMLObjectProviderRegistrySupport.getBuilderFactory().
                getBuilder(Fault.DEFAULT_ELEMENT_NAME);
        final Fault fault = builder.buildObject();
        addFaultMessage(action, fault);
        addFaultCode(action, fault);
        addFaultDetail(action, fault);
        body.getUnknownXMLObjects().add(fault);
        getLogger().debug("Action {}: Fault {} added to a Body", action.getId(), fault.toString());
        return fault;
    }
    
    /**
     * Constructs and adds {@link FaultActor} to the given {@link Fault}.
     *
     * @param action the action
     * @param fault the fault to which the fault actor will be added
     * @return the fault actor that was added to the fault
     */
    @Nonnull public static FaultActor addFaultActor(@Nonnull final AbstractProfileAction<?, ?> action,
            @Nonnull final Fault fault) {
    	@SuppressWarnings("unchecked")
		final SOAPObjectBuilder<FaultActor> builder = (SOAPObjectBuilder<FaultActor>) XMLObjectProviderRegistrySupport.getBuilderFactory().
                getBuilder(FaultActor.DEFAULT_ELEMENT_NAME);
        final FaultActor faultActor = builder.buildObject();
        fault.setActor(faultActor);
        getLogger().debug("Action {}: FaultActor {} added to a Fault", action.getId(), faultActor.toString());
        return faultActor;
    }
    
    /**
     * Constructs and adds {@link FaultCode} to the given {@link Fault}.
     *
     * @param action the action
     * @param fault the fault to which the fault code will be added
     * @return the fault code that was added to the fault
     */
    @Nonnull public static FaultCode addFaultCode(@Nonnull final AbstractProfileAction<?, ?> action,
            @Nonnull final Fault fault) {
    	@SuppressWarnings("unchecked")
		final SOAPObjectBuilder<FaultCode> builder = (SOAPObjectBuilder<FaultCode>) XMLObjectProviderRegistrySupport.getBuilderFactory().
                getBuilder(FaultCode.DEFAULT_ELEMENT_NAME);
        final FaultCode faultCode = builder.buildObject();
        fault.setCode(faultCode);
        getLogger().debug("Action {}: FaultCode {} added to a Fault", action.getId(), faultCode.toString());
        return faultCode;
    }
    
    /**
     * Constructs and adds {@link Detail} to the given {@link Fault}.
     *
     * @param action the action
     * @param fault the fault to which the detail will be added
     * @return the detail that was added to the fault
     */
    @Nonnull public static Detail addFaultDetail(@Nonnull final AbstractProfileAction<?, ?> action,
            @Nonnull final Fault fault) {
    	@SuppressWarnings("unchecked")
		final SOAPObjectBuilder<Detail> builder = (SOAPObjectBuilder<Detail>) XMLObjectProviderRegistrySupport.getBuilderFactory().
                getBuilder(Detail.DEFAULT_ELEMENT_NAME);
        final Detail faultDetail = builder.buildObject();
        fault.setDetail(faultDetail);
        getLogger().debug("Action {}: Detail {} added to a Fault", action.getId(), faultDetail.toString());
        return faultDetail;
    }

    /**
     * Constructs and adds {@link FaultString} to the given {@link Fault}.
     *
     * @param action the action
     * @param fault the fault to which the message will be added
     * @return the message that was added to the fault
     */
    @Nonnull public static FaultString addFaultMessage(@Nonnull final AbstractProfileAction<?, ?> action,
            @Nonnull final Fault fault) {
    	@SuppressWarnings("unchecked")
		final SOAPObjectBuilder<FaultString> builder = (SOAPObjectBuilder<FaultString>) XMLObjectProviderRegistrySupport.getBuilderFactory().
                getBuilder(FaultString.DEFAULT_ELEMENT_NAME);
        final FaultString faultString = builder.buildObject();
        fault.setMessage(faultString);
        getLogger().debug("Action {}: Message {} added to a Fault", action.getId(), faultString.toString());
        return faultString;
    }

    /**
     * Constructs and adds {@link Status} to the given {@link Detail}.
     *
     * @param action the action
     * @param detail the detail to which the status will be added
     * @return the status that was added to the detail
     */
    @Nonnull public static Status addStatusToDetail(@Nonnull final AbstractProfileAction<?, ?> action,
            @Nonnull final Detail detail) {
    	final StatusBuilder builder = (StatusBuilder) XMLObjectProviderRegistrySupport.getBuilderFactory().
    			getBuilder(Status.DEFAULT_ELEMENT_NAME);
        final Status status = builder.buildObject();
        detail.getUnknownXMLObjects().add(status);
        getLogger().debug("Action {}: Status {} added to a Detail", action.getId(), status.toString());
        return status;
    }
    
    /**
     * Constructs and adds {@link Request} to the given {@link Header}.
     *
     * @param action the action
     * @param header the header to which the request will be added
     * @return the request that was added to the header
     */
    @Nonnull public static Request addPaosRequestToHeader(@Nonnull final AbstractProfileAction<?, ?> action,
            @Nonnull final Header header) { 
        final RequestBuilder builder =
            (RequestBuilder) XMLObjectProviderRegistrySupport.getBuilderFactory().
                getBuilder(Request.DEFAULT_ELEMENT_NAME);
        final Request request = builder.buildObject();
        header.getUnknownXMLObjects().add(request);
        getLogger().debug("Action {}: PAOS request added to a Header", action.getId());
        return request;
    }
    
    /**
     * Get the {@link RSAPublicKey} from the SOAP envelope. It is expected to be found from
     * the {@link RequestSecurityToken}'s {@link UseKey} -element, inside the {@link Body}.
     *  
     * @param envelope the envelope containing the RSA public key.
     * @return the RSA public key.
     */
    public static RSAPublicKey getPublicKeyFromEnvelope(@Nonnull final AbstractProfileAction<?, ?> action, Envelope envelope) {
    	Logger log = getLogger();
    	RSAKeyValue rsaValue = getRSAKeyValueFromEnvelope(action, envelope);
    	if (rsaValue != null) {
    		log.debug("Action {}: RSAKeyValue was found, reading the modulus and the exponent", action.getId());
    		if (rsaValue.getModulus() != null && rsaValue.getExponent() != null) {
    			String modulus = rsaValue.getModulus().getValue();
    			String exponent = rsaValue.getExponent().getValue();
    			try {
    				RSAPublicKeySpec keySpec = new RSAPublicKeySpec(new BigInteger(Base64.decode(modulus)), new BigInteger(Base64.decode(exponent)));
    				return (RSAPublicKey) KeyFactory.getInstance("RSA").generatePublic(keySpec);
    			} catch (Base64DecodingException e) {
    				log.error("Action {}: Could not decode modulus and/or exponent", action.getId(), e);
    			} catch (InvalidKeySpecException e) {
    				log.error("Action {}: Internal error with key spec definitions", action.getId(), e);
    			} catch (NoSuchAlgorithmException e) {
    				log.error("Action {}: Internal error: RSA algorithm set as invalid", action.getId(), e);
    			}
    		} else {
    			log.debug("Action {}: No modulus and/or exponent found for the RSAKeyValue!", action.getId());
    		}
    	} else {
    		log.debug("Action {}: No RSAKeyValue found, trying BinarySecurityToken", action.getId());
        	RequestSecurityToken rst = SoapActionSupport.getRequestSecurityTokenFromBody(envelope.getBody());
        	if (rst == null) {
        		log.debug("Action {}: No RST found from the envelope body", action.getId());
        		return null;
        	}
        	UseKey useKey = SoapActionSupport.getUseKeyFromRequestSecurityToken(rst);
        	if (useKey == null) {
        		log.debug("Action {}: No single UseKey found from the RST", action.getId());
        		return null;
        	}
        	XMLObject xmlObject = useKey.getUnknownXMLObject();
        	if (xmlObject != null && BinarySecurityToken.ELEMENT_LOCAL_NAME.equals(xmlObject.getElementQName().getLocalPart())) {
        		BinarySecurityToken binaryToken = (BinarySecurityToken) xmlObject;
        		RSAPublicKey publicKey;
        		try {
        			byte[] publicKeyBytes = Base64.decode(binaryToken.getValue());
        			X509EncodedKeySpec pubKeySpec = new X509EncodedKeySpec(publicKeyBytes);
        			KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        			publicKey = (RSAPublicKey) keyFactory.generatePublic(pubKeySpec);
				} catch (Base64DecodingException e) {
	        		log.debug("Action {}: Could not base64-decode BinarySecurityToken's value", action.getId(), e);
					return null;
				} catch (NoSuchAlgorithmException e) {
	        		log.debug("Action {}: Could not construct an RSA public key from BinarySecurityToken's value", action.getId(), e);
					return null;
				} catch (InvalidKeySpecException e) {
	        		log.debug("Action {}: Could not construct an RSA public key from BinarySecurityToken's value", action.getId(), e);
					return null;
				}
        		return publicKey;
        	}
    	}
    	return null;	
    }
    
    /**
     * Get the {@link RSAKeyValue} from the SOAP envelope. It is expected to be found from
     * the {@link RequestSecurityToken}'s {@link UseKey} -element, inside the {@link Body}.
     *  
     * @param envelope the envelope containing the RSA key value.
     * @return the RSA key value.
     */
    public static RSAKeyValue getRSAKeyValueFromEnvelope(@Nonnull final AbstractProfileAction<?, ?> action, Envelope envelope) {
    	Logger log = getLogger();
    	RequestSecurityToken rst = getRequestSecurityTokenFromBody(envelope.getBody());
    	if (rst == null) {
    		log.debug("Action {}: No RST found from the envelope body", action.getId());
    		return null;
    	}
    	UseKey useKey = getUseKeyFromRequestSecurityToken(rst);
    	if (useKey == null) {
    		log.debug("Action {}: No single UseKey found from the RST", action.getId());
    		return null;
    	}
    	KeyInfo keyInfo = getKeyInfoFromUseKey(useKey);
    	if (keyInfo == null) {
    		log.debug("Action {}: No single KeyInfo found from the UseKey", action.getId());
    		return null;
    	}
    	return getRSAKeyValueFromKeyInfo(keyInfo);    	
    }

	/**
	 * Get the single {@link RequestSecurityToken} element from the {@link Body}. If zero or more than
	 * one elements are found, null is returned.
	 * 
	 * @param body the {@link Envelope} body containing the request security token.
	 * @return the single request security token.
	 */
    public static RequestSecurityToken getRequestSecurityTokenFromBody(Body body) {
    	List<XMLObject> objects = body.getUnknownXMLObjects(RequestSecurityToken.ELEMENT_NAME);
    	if (objects.size() == 1) {
    		return (RequestSecurityToken) objects.get(0);
    	} else {
    		return null;
    	}
    }

    /**
     * Get the single {@link UseKey} element from the {@link RequestSecurityToken}. If zero or more than
     * one elements are found, null is returned.
     * 
     * @param rst the {@link RequestSecurityToken} containing the use key element.
     * @return the single use key element.
     */
    public static UseKey getUseKeyFromRequestSecurityToken(RequestSecurityToken rst) {
    	List<XMLObject> objects = rst.getUnknownXMLObjects(UseKey.ELEMENT_NAME);
    	if (objects.size() == 1) {
    		return (UseKey) objects.get(0);
    	} else {
    		return null;
    	}
    }
	
	/**
	 * Get the {@link KeyInfo} element from {@link UseKey} if exists, null otherwise.
	 * 
	 * @param useKey the {@link UseKey} containing the key info.
	 * @return the key info element if exists, null otherwise.
	 */
    public static KeyInfo getKeyInfoFromUseKey(UseKey useKey) {
		XMLObject keyInfo = useKey.getUnknownXMLObject();
    	if (keyInfo instanceof KeyInfo) {
    		return (KeyInfo) keyInfo;
    	} else {
    		return null;
    	}		
	}
	
	/**
	 * Get the single {@link RSAKeyValue} from the {@link KeyInfo]. If zero or more than one
	 * elements are found, null is returned.
	 * 
	 * @param keyInfo the {@link KeyInfo} containing the RSA key value.
	 * @return the RSA key value element.
	 */
    public static RSAKeyValue getRSAKeyValueFromKeyInfo(KeyInfo keyInfo) {
		List<KeyValue> keyValues = keyInfo.getKeyValues();
		if (keyValues.size() == 1) {
			return keyValues.get(0).getRSAKeyValue();
		} else {
    		return null;
    	}	
	}

    /**
     * Gets the logger for this class.
     * 
     * @return logger for this class, never null
     */
    private static Logger getLogger() {
        return LoggerFactory.getLogger(SoapActionSupport.class);
    }

}
