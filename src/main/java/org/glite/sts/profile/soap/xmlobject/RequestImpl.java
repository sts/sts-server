/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.profile.soap.xmlobject;

import java.util.List;

import org.opensaml.core.xml.XMLObject;
import org.opensaml.core.xml.schema.XSBooleanValue;
import org.opensaml.saml.common.AbstractSAMLObject;

/** Implementation for {@link Request}. */
public class RequestImpl extends AbstractSAMLObject implements Request {
	
    /** responseConsumerURL attribute value. */
    private String responseConsumerURL;

    /** service attribute value. */
    private String service;
    
    /** messageID attribute value. */
    private String messageID;
    
    /** soap:actor attribute value. */
    private String soapActor;
    
    /** soap:mustUnderstand value. */
    private XSBooleanValue soapMustUnderstand;
	
    /**
     * Constructor.
     * 
     * @param namespaceURI namespace of the element
     * @param elementLocalName name of the element
     * @param namespacePrefix namespace prefix of the element
     */
    protected RequestImpl(String namespaceURI, String elementLocalName, String namespacePrefix) {
        super(namespaceURI, elementLocalName, namespacePrefix);
    }

    /** {@inheritDoc} */
    public List<XMLObject> getOrderedChildren() {
		// no op
		return null;
	}

    /** {@inheritDoc} */
	public Boolean isSOAP11MustUnderstand() {
        if (this.soapMustUnderstand != null) {
            return this.soapMustUnderstand.getValue();
        }
        return Boolean.FALSE;
	}

    /** {@inheritDoc} */
	public XSBooleanValue isSOAP11MustUnderstandXSBoolean() {
		return this.soapMustUnderstand;
	}

    /** {@inheritDoc} */
	public void setSOAP11MustUnderstand(Boolean newMustUnderstand) {
        if (newMustUnderstand != null) {
            this.soapMustUnderstand = super.prepareForAssignment(this.soapMustUnderstand, new XSBooleanValue(newMustUnderstand, true));
        } else {
            this.soapMustUnderstand = super.prepareForAssignment(this.soapMustUnderstand, null);
        }
	}

    /** {@inheritDoc} */
	public void setSOAP11MustUnderstand(XSBooleanValue newMustUnderstand) {
		this.soapMustUnderstand = newMustUnderstand;
	}

    /** {@inheritDoc} */
	public String getSOAP11Actor() {
		return this.soapActor;
	}

    /** {@inheritDoc} */
	public void setSOAP11Actor(String newActor) {
		this.soapActor = newActor;
	}

    /** {@inheritDoc} */
	public String getResponseConsumerURL() {
		return this.responseConsumerURL;
	}

    /** {@inheritDoc} */
	public void setResponseConsumerURL(String newResponseConsumerURL) {
		this.responseConsumerURL = newResponseConsumerURL;
	}

    /** {@inheritDoc} */
	public String getService() {
		return this.service;
	}

    /** {@inheritDoc} */
	public void setService(String newService) {
		this.service = newService;
	}

    /** {@inheritDoc} */
	public String getMessageID() {
		return this.messageID;
	}

    /** {@inheritDoc} */
	public void setMessageID(String newMessageID) {
		this.messageID = newMessageID;		
	}

}
