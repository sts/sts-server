/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.profile.soap.xmlobject;

import javax.annotation.concurrent.ThreadSafe;
import javax.xml.namespace.QName;

import net.shibboleth.utilities.java.support.xml.QNameSupport;

import org.opensaml.core.xml.XMLObject;
import org.opensaml.core.xml.io.UnmarshallingException;
import org.opensaml.core.xml.schema.XSBooleanValue;
import org.opensaml.saml.common.AbstractSAMLObjectUnmarshaller;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Attr;

/** Unmarshaller for {@link Request}. */
@ThreadSafe
public class RequestUnmarshaller extends AbstractSAMLObjectUnmarshaller {
	
    /** Logger. */
    private final Logger log = LoggerFactory.getLogger(RequestUnmarshaller.class);

    /** {@inheritDoc} */
    protected void processAttribute(XMLObject xmlObject, Attr attribute) throws UnmarshallingException {
        log.debug("Processing attributes for Request");
        Request request = (Request) xmlObject;
        
        QName attrName = QNameSupport.getNodeQName(attribute);
        if (attribute.getLocalName().equals(Request.MESSAGE_ID_ATTRIB_NAME)) {
            log.debug("MessageID {} found, setting it for the value.", attribute.getValue());
            request.setMessageID(attribute.getValue());
        } else if (attribute.getLocalName().equals(Request.RESPONSE_CONSUMER_URL_ATTRIB_NAME)) {
            log.debug("ResponseConsumerURL {} found, setting it for the value.", attribute.getValue());
            request.setResponseConsumerURL(attribute.getValue());
        } else if (attribute.getLocalName().equals(Request.SERVICE_ATTRIB_NAME)) {
            log.debug("Service {} found, setting it for the value.", attribute.getValue());
            request.setService(attribute.getValue());
        } else if (Request.SOAP11_MUST_UNDERSTAND_ATTR_NAME.equals(attrName)) {
        	log.debug("SOAP mustUnderstand {} found, setting it for the value.", attribute.getValue());
            request.setSOAP11MustUnderstand(XSBooleanValue.valueOf(attribute.getValue()));
        } else if (Request.SOAP11_ACTOR_ATTR_NAME.equals(attrName)) {
        	log.debug("SOAP actor {} found, setting it for the value.", attribute.getValue());
            request.setSOAP11Actor(attribute.getValue()); 
        } else {
        	log.debug("Unknown attribute {} = {}, ignored.", attrName.toString(), attribute.getValue());
        }
    }
}
