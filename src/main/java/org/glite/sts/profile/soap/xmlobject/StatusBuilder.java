/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.profile.soap.xmlobject;

import javax.annotation.concurrent.ThreadSafe;

import org.opensaml.core.xml.AbstractXMLObjectBuilder;

/** Builder of {@link Status} objects. */
@ThreadSafe
public class StatusBuilder extends AbstractXMLObjectBuilder<Status> {

    /** {@inheritDoc} */
    public Status buildObject(String namespaceURI, String localName, String namespacePrefix) {
        return new StatusImpl(namespaceURI, localName, namespacePrefix);
    }
    
    /**
     * Build a Status element with the default namespace prefix and element name.
     * 
     * @return a new instance of {@link Status}
     */
    public Status buildObject() {
        return buildObject(LibertyIDWSFConstants.IDWSF_UTIL_NS, Status.DEFAULT_ELEMENT_LOCAL_NAME,
                LibertyIDWSFConstants.IDWSF_UTIL_PREFIX);
    }

}
