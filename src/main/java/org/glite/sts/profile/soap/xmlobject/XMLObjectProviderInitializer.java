package org.glite.sts.profile.soap.xmlobject;

import org.opensaml.core.xml.config.AbstractXMLObjectProviderInitializer;

/**
 * XMLObject provider initializer for module "soap".
 */
public class XMLObjectProviderInitializer extends AbstractXMLObjectProviderInitializer {
    
    /** Config resources. */
    private static String[] configs = {
        "/idwsf-sbf-config.xml",
        "/idwsf-util-config.xml",
        "/paos-config.xml",
        "/glite-sts-proxy-config.xml",
        "/cern-ca-sts-config.xml"
        };

    /** {@inheritDoc} */
    protected String[] getConfigResources() {
        return configs;
    }

}
