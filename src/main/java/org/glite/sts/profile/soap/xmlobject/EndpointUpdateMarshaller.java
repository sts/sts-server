/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.profile.soap.xmlobject;

import javax.annotation.concurrent.ThreadSafe;
import org.opensaml.core.xml.XMLObject;
import org.opensaml.core.xml.io.AbstractXMLObjectMarshaller;
import org.opensaml.core.xml.io.MarshallingException;
import org.opensaml.core.xml.util.XMLObjectSupport;
import org.w3c.dom.Element;

/** Marshaller for {@link EndpointUpdate}. */
@ThreadSafe
public class EndpointUpdateMarshaller extends AbstractXMLObjectMarshaller {
    
    /** {@inheritDoc} */
    protected void marshallAttributes(XMLObject xmlObject, Element domElement) throws MarshallingException {
        EndpointUpdate endpointUpdate = (EndpointUpdate) xmlObject;
        XMLObjectSupport.marshallAttributeMap(endpointUpdate.getUnknownAttributes(), domElement);
        if (endpointUpdate.getUpdateType() != null) {
            domElement.setAttributeNS(null, EndpointUpdate.UPDATE_TYPE_ATTRIB_NAME, endpointUpdate.getUpdateType());
        }
    }
}
