/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.profile.soap.xmlobject;

import javax.annotation.concurrent.ThreadSafe;

import org.opensaml.core.xml.AbstractXMLObjectBuilder;

/** Builder of {@link Framework} objects. */
@ThreadSafe
public class FrameworkBuilder extends AbstractXMLObjectBuilder<Framework> {

    /** {@inheritDoc} */
    public Framework buildObject(String namespaceURI, String localName, String namespacePrefix) {
        return new FrameworkImpl(namespaceURI, localName, namespacePrefix);
    }
    
    /**
     * Build a Framework element with the default namespace prefix and element name.
     * 
     * @return a new instance of {@link Framework}
     */
    public Framework buildObject() {
        return buildObject(LibertyIDWSFConstants.IDWSF_SBF_NS, Framework.DEFAULT_ELEMENT_LOCAL_NAME,
                LibertyIDWSFConstants.IDWSF_SBF_PREFIX);
    }

}
