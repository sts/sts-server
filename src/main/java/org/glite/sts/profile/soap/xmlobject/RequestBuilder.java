/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.profile.soap.xmlobject;

import javax.annotation.concurrent.ThreadSafe;

import org.opensaml.saml.common.AbstractSAMLObjectBuilder;
import org.opensaml.saml.common.xml.SAMLConstants;

/** Builder of {@link Request} objects. */
@ThreadSafe
public class RequestBuilder extends AbstractSAMLObjectBuilder<Request> {
	
    /** {@inheritDoc} */
    public Request buildObject(String namespaceURI, String localName, String namespacePrefix) {
        return new RequestImpl(namespaceURI, localName, namespacePrefix);
    }
    
    /**
     * Build a Request element with the default namespace prefix and element name.
     * 
     * @return a new instance of {@link Request}
     */
    public Request buildObject() {
        return buildObject(SAMLConstants.PAOS_NS, Request.DEFAULT_ELEMENT_LOCAL_NAME, SAMLConstants.PAOS_PREFIX);
    }

}
