/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.profile.soap.xmlobject;

import java.util.Map.Entry;

import javax.annotation.concurrent.ThreadSafe;
import javax.xml.namespace.QName;

import net.shibboleth.utilities.java.support.xml.AttributeSupport;

import org.opensaml.core.xml.XMLObject;
import org.opensaml.core.xml.config.XMLObjectProviderRegistrySupport;
import org.opensaml.core.xml.io.AbstractXMLObjectMarshaller;
import org.opensaml.core.xml.io.MarshallingException;
import org.w3c.dom.Attr;
import org.w3c.dom.Element;

/** Marshaller for {@link Framework}. */
@ThreadSafe
public class FrameworkMarshaller extends AbstractXMLObjectMarshaller {
    
    /** {@inheritDoc} */
    protected void marshallAttributes(XMLObject xmlObject, Element domElement) throws MarshallingException {
        Framework framework = (Framework) xmlObject;

        if (framework.getVersion() != null) {
            domElement.setAttributeNS(null, Framework.VERSION_ATTRIB_NAME, framework.getVersion());
        }

        Attr attr;
        for (Entry<QName, String> entry : framework.getUnknownAttributes().entrySet()) {
            attr = AttributeSupport.constructAttribute(domElement.getOwnerDocument(), entry.getKey());
            attr.setValue(entry.getValue());
            domElement.setAttributeNodeNS(attr);
            if (XMLObjectProviderRegistrySupport.isIDAttribute(entry.getKey())
                    || framework.getUnknownAttributes().isIDAttribute(entry.getKey())) {
                attr.getOwnerElement().setIdAttributeNode(attr, true);
            }
        }
    }

}
