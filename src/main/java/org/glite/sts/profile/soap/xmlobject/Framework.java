/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.profile.soap.xmlobject;

import javax.xml.namespace.QName;

import org.opensaml.core.xml.AttributeExtensibleXMLObject;
import org.opensaml.core.xml.ElementExtensibleXMLObject;

/** XMLObject for the Liberty ID-WSF SOAP Binding's Framework. */
public interface Framework extends ElementExtensibleXMLObject, AttributeExtensibleXMLObject {
    
    /** Default element local name. */
    public static final String DEFAULT_ELEMENT_LOCAL_NAME = "Framework";
    
    /** Default element name. */
    public static final QName DEFAULT_ELEMENT_NAME = new QName(LibertyIDWSFConstants.IDWSF_SBF_NS, DEFAULT_ELEMENT_LOCAL_NAME, LibertyIDWSFConstants.IDWSF_SBF_PREFIX);
    
    /** version attribute name. */
    public static final String VERSION_ATTRIB_NAME = "version";
    
    /**
     * Get the version attribute value.
     * 
     * @return the version attribute value
     */
    public String getVersion();

    /**
     * Set the version attribute value.
     * 
     * @param newVersion the new version attribute value
     */
    public void setVersion(String newVersion);

}
