/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.profile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.shibboleth.ext.spring.webflow.Event;
import net.shibboleth.ext.spring.webflow.Events;
import net.shibboleth.idp.profile.AbstractProfileAction;
import net.shibboleth.idp.profile.ActionSupport;
import net.shibboleth.idp.profile.EventIds;
import net.shibboleth.idp.profile.ProfileException;
import net.shibboleth.idp.profile.ProfileRequestContext;

import org.glite.sts.ta.TokenGenerationContext;
import org.glite.sts.ta.TokenGenerationException;
import org.glite.sts.ta.TokenGenerator;
import org.glite.sts.ta.voms.VomsTokenGenerationContext;
import org.glite.sts.ta.voms.VomsTokenGenerator;
import org.opensaml.messaging.context.MessageContext;
import org.opensaml.messaging.context.navigate.ChildContextLookup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.webflow.execution.RequestContext;

import com.google.common.base.Function;

/** Issues VOMS proxy certificate using the configured {@link TokenGenerator} implementation. The data used for the new
 * certificate is obtained from {@link TokenGenerationContext}, attached into the {@link ProfileRequestContext}. */
@Events({
    @Event(id = EventIds.PROCEED_EVENT_ID),
    @Event(id = EventIds.INVALID_MSG_CTX)})
public class IssueVomsProxyCertificate extends AbstractProfileAction<Object, Object> {
   
    /** Class logger. */
    private final Logger log = LoggerFactory.getLogger(IssueCertificate.class);
    
    /**
     * Strategy used to locate the {@link TokenGenerationContext} associated with a given {@link ProfileRequestContext}.
     */
    private Function<MessageContext<?>, TokenGenerationContext> tokenGenerationContextLookupStrategy;
    
    /**
     * Strategy used to locate the {@link VomsTokenGenerationContext} associated with a given {@link TokenGenerationContext}.
     */
    private Function<TokenGenerationContext, VomsTokenGenerationContext> vomsTokenGenerationContextLookupStrategy;
    
    /**
     * The TokenGenerator implementation used for issuing the VOMS proxy certificates.
     */
    private TokenGenerator tokenGenerator;
    
    /** Constructor. */
    public IssueVomsProxyCertificate() {
    	super();
        tokenGenerationContextLookupStrategy =
                new ChildContextLookup<MessageContext<?>, TokenGenerationContext>(TokenGenerationContext.class,
                        false);
        vomsTokenGenerationContextLookupStrategy =
        		new ChildContextLookup<TokenGenerationContext, VomsTokenGenerationContext>(VomsTokenGenerationContext.class,
        				false);
    }

    /** Constructor. */
    public IssueVomsProxyCertificate(VomsTokenGenerator vomsTokenGenerator) {
    	this();
    	this.tokenGenerator = vomsTokenGenerator;
    }

    /** {@inheritDoc} */
    protected org.springframework.webflow.execution.Event doExecute(final HttpServletRequest httpRequest, final HttpServletResponse httpResponse,
            final RequestContext springRequestContext,
            final ProfileRequestContext<Object, Object> profileRequestContext) throws ProfileException {

    	final TokenGenerationContext tokenGenerationContext = tokenGenerationContextLookupStrategy.apply(profileRequestContext.getOutboundMessageContext());
        if (tokenGenerationContext == null) {
            log.debug("Action {}: Token generation context is null, unable to proceed", getId());
            return ActionSupport.buildEvent(this, EventIds.INVALID_MSG_CTX);
        }
    	final VomsTokenGenerationContext vomsTokenGenerationContext = vomsTokenGenerationContextLookupStrategy.apply(tokenGenerationContext);
    	if (vomsTokenGenerationContext == null) {
    		log.debug("Action {}: VOMS token generation context is null, unable to proceed", getId());
    		return ActionSupport.buildEvent(this, EventIds.INVALID_MSG_CTX);
    	}
    	try {
			tokenGenerator.issueToken(tokenGenerationContext);
		} catch (TokenGenerationException e) {
			log.error("Could not initialize the proxy certificate", e);
			throw new ProxyCertificateInitializationException(e);
		}
        return ActionSupport.buildProceedEvent(this);
    }
    
    /** Exception thrown when the proxy certificate initialization failed. */
    public static class ProxyCertificateInitializationException extends ProfileException {

        /** Serial version UID. */
        private static final long serialVersionUID = -872917422123353555L;

        /**
         * Constructor.
         * 
         * @param message exception message
         */
        public ProxyCertificateInitializationException(Throwable e) {
            super(e.getMessage());
        }
    }
}
