/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.profile.wssecurity;

import javax.annotation.Nonnull;

import net.shibboleth.idp.profile.AbstractProfileAction;

import org.joda.time.DateTime;
import org.joda.time.chrono.ISOChronology;
import org.opensaml.core.xml.config.XMLObjectProviderRegistrySupport;
import org.opensaml.soap.soap11.Header;
import org.opensaml.soap.wssecurity.Created;
import org.opensaml.soap.wssecurity.Security;
import org.opensaml.soap.wssecurity.Timestamp;
import org.opensaml.soap.wssecurity.WSSecurityObjectBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class contains helper methods for dealing with WS-Security objects.
 */
public final class WsSecurityActionSupport {
	
	/**
	 * Builds a {@link Security} and attaches it to the header
	 * @param action the action
	 * @param header the parent of the security
	 * @return the security
	 */
	@Nonnull public static Security addSecurityToHeader(@Nonnull final AbstractProfileAction<?, ?> action, @Nonnull final Header header) {
		@SuppressWarnings("unchecked")
		final WSSecurityObjectBuilder<Security> builder = (WSSecurityObjectBuilder<Security>) XMLObjectProviderRegistrySupport.getBuilderFactory().getBuilder(Security.ELEMENT_NAME);
		final Security security = builder.buildObject();
		addTimestampToSecurity(action, security);
		header.getUnknownXMLObjects().add(security);
		getLogger().debug("Action {}: Security added to a Header", action.getId());
		return security;
	}
	
	/**
	 * Builds a {@link Timestamp} and attaches it to the security
	 * @param action the action
	 * @param security the parent of the timestamp
	 * @return the timestamp
	 */
	@Nonnull public static Timestamp addTimestampToSecurity(@Nonnull final AbstractProfileAction<?, ?> action, @Nonnull final Security security) {
		@SuppressWarnings("unchecked")
		final WSSecurityObjectBuilder<Timestamp> builder = (WSSecurityObjectBuilder<Timestamp>) XMLObjectProviderRegistrySupport.getBuilderFactory().getBuilder(Timestamp.ELEMENT_NAME);
		final Timestamp timestamp = builder.buildObject();
		addCreatedToTimestamp(action, timestamp);
		security.getUnknownXMLObjects().add(timestamp);
		getLogger().debug("Action {}: Timestamp added to the Security element", action.getId());
		return timestamp;
	}
	
	/**
	 * Buils a {@link Created} and attaches it to the timestamp
	 * @param action the action
	 * @param timestamp the parent of the created
	 * @return the created
	 */
	@Nonnull public static Created addCreatedToTimestamp(@Nonnull final AbstractProfileAction<?, ?> action, @Nonnull final Timestamp timestamp) {
		@SuppressWarnings("unchecked")
		final WSSecurityObjectBuilder<Created> builder = (WSSecurityObjectBuilder<Created>) XMLObjectProviderRegistrySupport.getBuilderFactory().getBuilder(Created.ELEMENT_NAME);
		final Created created = builder.buildObject();
		created.setDateTime(new DateTime(ISOChronology.getInstanceUTC()));
		timestamp.setCreated(created);
		getLogger().debug("Action {}: Created (with the current time) added to the Timestamp element", action.getId());
		return created;
	}
	
    /**
     * Gets the logger for this class.
     * 
     * @return logger for this class, never null
     */
    private static Logger getLogger() {
        return LoggerFactory.getLogger(WsSecurityActionSupport.class);
    }

}
