/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.profile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.glite.sts.ta.TokenGenerationContext;
import org.glite.sts.ta.TokenGenerationException;
import org.glite.sts.ta.TokenGenerator;
import org.glite.sts.ta.x509.X509TokenGenerationContext;
import org.glite.sts.ta.x509.X509TokenGenerator;
import org.opensaml.messaging.context.MessageContext;
import org.opensaml.messaging.context.navigate.ChildContextLookup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.webflow.execution.RequestContext;

import com.google.common.base.Function;

import net.shibboleth.ext.spring.webflow.Event;
import net.shibboleth.ext.spring.webflow.Events;
import net.shibboleth.idp.profile.AbstractProfileAction;
import net.shibboleth.idp.profile.ActionSupport;
import net.shibboleth.idp.profile.EventIds;
import net.shibboleth.idp.profile.ProfileException;
import net.shibboleth.idp.profile.ProfileRequestContext;

/** Issues X.509 certificate using the configured {@link CAClient} implementation. The data used for the new
 * certificate is obtained from {@link TokenGenerationContext}, attached into the {@link ProfileRequestContext}. */
@Events({
    @Event(id = EventIds.PROCEED_EVENT_ID),
    @Event(id = EventIds.INVALID_PROFILE_CTX)})
public class IssueCertificate extends AbstractProfileAction<Object, Object> {
   
    /** Class logger. */
    private final Logger log = LoggerFactory.getLogger(IssueCertificate.class);
    
    /**
     * Strategy used to locate the {@link TokenGenerationContext} associated with a given {@link ProfileRequestContext}.
     */
    private Function<MessageContext<?>, TokenGenerationContext> tokenGenerationContextLookupStrategy;
    
    /**
     * Strategy used to locate the {@link X509TokenGenerationContext} associated with a given {@link TokenGenerationContext}.
     */
    private Function<TokenGenerationContext, X509TokenGenerationContext> x509TokenGenerationContextLookupStrategy;
    
    /**
     * The TokenGenerator implementation used for issuing the X.509 certificates.
     */
    private TokenGenerator tokenGenerator;
    
    /** Constructor. */
    public IssueCertificate() {
    	super();
        tokenGenerationContextLookupStrategy =
                new ChildContextLookup<MessageContext<?>, TokenGenerationContext>(TokenGenerationContext.class,
                        false);
        x509TokenGenerationContextLookupStrategy =
        		new ChildContextLookup<TokenGenerationContext, X509TokenGenerationContext>(X509TokenGenerationContext.class,
        				false);
    }
    
    /** Constructor. */
    public IssueCertificate(X509TokenGenerator x509TokenGenerator) {
    	this();
    	this.tokenGenerator = x509TokenGenerator;
    }
    
    /** {@inheritDoc} */
    protected org.springframework.webflow.execution.Event doExecute(final HttpServletRequest httpRequest, final HttpServletResponse httpResponse,
            final RequestContext springRequestContext,
            final ProfileRequestContext<Object, Object> profileRequestContext) throws ProfileException {

    	TokenGenerationContext tokenGenerationContext = tokenGenerationContextLookupStrategy.apply(profileRequestContext.getOutboundMessageContext());
        if (tokenGenerationContext == null) {
            log.debug("Action {}: Token generation context is null, unable to proceed", getId());
            return ActionSupport.buildEvent(this, EventIds.INVALID_PROFILE_CTX);
        }
    	final X509TokenGenerationContext x509TokenGenerationContext = x509TokenGenerationContextLookupStrategy.apply(tokenGenerationContext);
    	if (x509TokenGenerationContext == null) {
    		log.debug("Action {}: X.509 token generation context is null, unable to proceed", getId());
    		return ActionSupport.buildEvent(this, EventIds.INVALID_PROFILE_CTX);
    	}
    	try {
			tokenGenerator.issueToken(tokenGenerationContext);
		} catch (TokenGenerationException e) {
			log.error("Could not issue a certificate", e);
			throw new CertificateIssuanceException(e);
		}
    	if (x509TokenGenerationContext.getCaResponse() == null || x509TokenGenerationContext.getCaResponse().getCertificate() == null) {
    		log.debug("Action {}: Could not obtain a certificate from the online CA", getId());
    		throw new CertificateIssuanceException("The online CA could not issue a certificate");
    	}
        return ActionSupport.buildProceedEvent(this);
    }
    
    /** Exception thrown when the certificate issuance failed. */
    public static class CertificateIssuanceException extends ProfileException {

        /** Serial version UID. */
        private static final long serialVersionUID = -872917442222234555L;

        /**
         * Constructor.
         * 
         * @param e cause
         */
        public CertificateIssuanceException(Throwable e) {
            super(e.getMessage());
        }
        
        /**
         * Constructor.
         * 
         * @param message exception message
         */
        public CertificateIssuanceException(String message) {
            super(message);
        }

    }
}
