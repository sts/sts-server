/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.profile;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import net.shibboleth.idp.profile.ActionSupport;
import net.shibboleth.idp.profile.navigate.WebflowRequestContextHttpServletResponseLookup;
import net.shibboleth.utilities.java.support.component.AbstractIdentifiableInitializableComponent;
import net.shibboleth.utilities.java.support.component.IdentifiableComponent;
import net.shibboleth.utilities.java.support.logic.Constraint;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.webflow.execution.Action;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;

import com.google.common.base.Function;

/**
 * A simple action that sends a configured error message in the response. It also sets the response
 * in the {@link RequestContext}'s {@link ExternalContext} to be completed.
 */
public class SendErrorResponse extends AbstractIdentifiableInitializableComponent implements Action, IdentifiableComponent {
	
	/** The error message displayed by default. */
	public static final String DEFAULT_ERROR_MESSAGE = "Request not understood, this is a SOAP-based Web Service.";
	
	/** Class logger. */
    private final Logger log = LoggerFactory.getLogger(SendErrorResponse.class);
    
    /** Strategy used to lookup the {@link HttpServletResponse} from a given WebFlow {@link RequestContext}. */
    private Function<RequestContext, HttpServletResponse> httpResponseLookupStrategy;
    
    /** The error message that will be sent in the response. */
    private String errorMessage;
    
    /**
     * Constructor.
     */
    public SendErrorResponse() {
    	this(DEFAULT_ERROR_MESSAGE);
    }
    
    /**
     * Constructor.
     * @param message the error message that will be sent in the response
     */
    public SendErrorResponse(String message) {
    	super();
		setId(getClass().getName());
    	this.errorMessage = Constraint.isNotNull(message, "The error message cannot be null!");
    	httpResponseLookupStrategy = new WebflowRequestContextHttpServletResponseLookup();
    }

    /** {@inheritDoc} */
	public Event execute(RequestContext springRequestContext) throws Exception {
		return this.execute(springRequestContext, null);
	}

	/**
	 * Executes the state with a dynamic error message.
	 * @param springRequestContext the Spring external context
	 * @param message the desired error message
	 * @return proceed event
	 * @throws Exception if something goes wrong in the internal process
	 */
	public Event execute(RequestContext springRequestContext, String message) throws Exception {
    	log.info("Action {}: Ignoring a non-supported request message and closing the flow", getId());
    	final HttpServletResponse httpResponse = httpResponseLookupStrategy.apply(springRequestContext);
    	try {
    		if (message != null) {
    			httpResponse.getWriter().write(message);
    		} else {
    			httpResponse.getWriter().write(this.errorMessage);
    		}
		} catch (IOException e) {
			log.error("Could not write to the HTTP response", e);
		}
    	springRequestContext.getExternalContext().recordResponseComplete();
		return ActionSupport.buildProceedEvent(this);
	}

}
