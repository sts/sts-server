/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.profile;

import java.security.interfaces.RSAPublicKey;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.glite.sts.STSException;
import org.glite.sts.profile.config.WsTrustInteroperabilityProfileConfiguration;
import org.glite.sts.profile.soap.SoapActionSupport;
import org.glite.sts.ta.TokenGenerationContext;
import org.glite.sts.ta.x509.X509TokenGenerationContext;
import org.glite.sts.x509.CertificateKeys;
import org.glite.sts.x509.RSACertificateKeys;
import org.opensaml.messaging.context.MessageContext;
import org.opensaml.messaging.context.navigate.ChildContextLookup;
import org.opensaml.soap.soap11.Envelope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.webflow.execution.RequestContext;

import com.google.common.base.Function;

import net.shibboleth.ext.spring.webflow.Event;
import net.shibboleth.ext.spring.webflow.Events;
import net.shibboleth.idp.profile.AbstractProfileAction;
import net.shibboleth.idp.profile.ActionSupport;
import net.shibboleth.idp.profile.EventIds;
import net.shibboleth.idp.profile.ProfileException;
import net.shibboleth.idp.profile.ProfileRequestContext;

/** Initializes the X.509 token generation context and attaches it to the token generation context. */
@Events({
    @Event(id = EventIds.PROCEED_EVENT_ID),
    @Event(id = EventIds.INVALID_MSG_CTX)})
public class InitializeX509TokenGenerationContext extends AbstractProfileAction<Envelope, Envelope> {
			
	/** Class logger. */
	private final Logger log = LoggerFactory.getLogger(InitializeX509TokenGenerationContext.class);
	    
    /**
     * Strategy used to extract the {@link TokenGenerationContext} from the outbound
     * {@link MessageContext}.
     */
    private Function<MessageContext<Envelope>, TokenGenerationContext> tokenGenerationCtxLookupStrategy;
    
    /** The strength of the key-pair to be generated. */
    private int keysize;

    /** {@inheritDoc} */
    public InitializeX509TokenGenerationContext(int strength) {
    	super();
    	keysize = strength;
        tokenGenerationCtxLookupStrategy = new ChildContextLookup<MessageContext<Envelope>, TokenGenerationContext>(TokenGenerationContext.class, false);
    }
    
    /** {@inheritDoc} */
    protected org.springframework.webflow.execution.Event doExecute(final HttpServletRequest httpRequest, final HttpServletResponse httpResponse,
            final RequestContext springRequestContext,
            final ProfileRequestContext<Envelope, Envelope> profileRequestContext) throws ProfileException {
    	MessageContext<Envelope> inMessageContext = profileRequestContext.getInboundMessageContext();
    	if (inMessageContext == null) {
    		log.debug("Action {}: inbound message context is null!", getId());
    		ActionSupport.buildEvent(this, EventIds.INVALID_MSG_CTX);
    	}
    	MessageContext<Envelope> outMessageContext = profileRequestContext.getOutboundMessageContext();
    	if (outMessageContext == null) {
    		log.debug("Action {}: outbound message context is null!", getId());
    		ActionSupport.buildEvent(this, EventIds.INVALID_MSG_CTX);
    	}
    	final TokenGenerationContext tokenGenerationContext = tokenGenerationCtxLookupStrategy.apply(outMessageContext);
		final X509TokenGenerationContext x509tokenGenerationContext = new X509TokenGenerationContext();
		CertificateKeys keys;
		RSAPublicKey publicKey;
		// only use the public key from the request if the request type is matching to X.509
		// otherwise the public key is meant to be used for another token, like VOMS proxy
		if (tokenGenerationContext.getTokenType().equals(WsTrustInteroperabilityProfileConfiguration.TOKEN_TYPE_X509_ID)) {
			publicKey = SoapActionSupport.getPublicKeyFromEnvelope(this, inMessageContext.getMessage());
		} else {
			publicKey = null;
		}
		try {
			if (publicKey != null) {
				log.debug("Action {}: using the public key from the request message", getId());
				keys = new RSACertificateKeys(publicKey);
			} else {
				log.debug("Action {}: generating a new key-pair of {} bits", getId(), keysize);
				keys = new RSACertificateKeys(keysize);
			}
		} catch (STSException e) {
			log.error("Action {}: Could not generate a key-pair", getId(), e);
			throw new ProfileException("Could not generate a key-pair", e);
		}
		x509tokenGenerationContext.setCertKeys(keys);
		tokenGenerationContext.addSubcontext(x509tokenGenerationContext);
    	return ActionSupport.buildProceedEvent(this);
    }
        
}
