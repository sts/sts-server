/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.profile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.glite.sts.profile.config.WsTrustInteroperabilityProfileConfiguration;
import org.glite.sts.ta.TokenGenerationContext;
import org.opensaml.messaging.context.MessageContext;
import org.opensaml.messaging.context.navigate.ChildContextLookup;
import org.opensaml.soap.soap11.Envelope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.webflow.execution.RequestContext;

import com.google.common.base.Function;

import net.shibboleth.ext.spring.webflow.Event;
import net.shibboleth.ext.spring.webflow.Events;
import net.shibboleth.idp.profile.AbstractProfileAction;
import net.shibboleth.idp.profile.ActionSupport;
import net.shibboleth.idp.profile.EventIds;
import net.shibboleth.idp.profile.ProfileException;
import net.shibboleth.idp.profile.ProfileRequestContext;

/** Initializes the security token generation context and attaches it to the outbound message context. */
@Events({
    @Event(id = EventIds.PROCEED_EVENT_ID),
    @Event(id = EventIds.INVALID_MSG_CTX),
    @Event(id = InitializeTokenGenerationContext.EVENT_ID_X509_TOKEN_TYPE),
    @Event(id = InitializeTokenGenerationContext.EVENT_ID_X509_PROXY_TOKEN_TYPE)})
public class InitializeTokenGenerationContext extends AbstractProfileAction<Envelope, Envelope> {
	
    /** Class logger. */
    private final Logger log = LoggerFactory.getLogger(InitializeTokenGenerationContext.class);
    
    public static final String EVENT_ID_X509_TOKEN_TYPE = "X509TokenType";
    public static final String EVENT_ID_X509_PROXY_TOKEN_TYPE = "X509ProxyTokenType";
    
    /** Whether certificate issuance is enabled. */
    private boolean certEnabled;
    
    /** Whether proxy issuance is enabled. */
    private boolean proxyEnabled;
    
    /**
     * Strategy used to look up the {@link TokenRequestMessageContext} associated with the inbound message context.
     */
    private Function<MessageContext<Envelope>, TokenRequestMessageContext> tokenRequestMessageContextLookupStrategy;
    
    /** {@inheritDoc} */
    public InitializeTokenGenerationContext() {
    	this(true, true);
    }
    
    /**
     * Initializes this action with the configured settings.
     * @param enableX509 Whether certificate issuance is enabled.
     * @param enableProxy Whether proxy issuance is enabled.
     */
    public InitializeTokenGenerationContext(boolean enableX509, boolean enableProxy) {
    	super();
    	log.info("X.509 token issuance enabled = {}, Proxy issuance enabled = {}", enableX509, enableProxy);
    	certEnabled = enableX509;
    	proxyEnabled = enableProxy;
    	tokenRequestMessageContextLookupStrategy =
                new ChildContextLookup<MessageContext<Envelope>, TokenRequestMessageContext>(TokenRequestMessageContext.class,
                        false);    	
    }
    
    /** {@inheritDoc} */
    protected org.springframework.webflow.execution.Event doExecute(final HttpServletRequest httpRequest, final HttpServletResponse httpResponse,
            final RequestContext springRequestContext,
            final ProfileRequestContext<Envelope, Envelope> profileRequestContext) throws ProfileException {
    	MessageContext<Envelope> inMessageContext = profileRequestContext.getInboundMessageContext();
    	
    	
    	TokenRequestMessageContext tokenRequestContext = tokenRequestMessageContextLookupStrategy.apply(inMessageContext);
    	if (tokenRequestContext == null) {
    		log.debug("Action {}: Token request message context is null, unable to proceed", getId());
    		ActionSupport.buildEvent(this, EventIds.INVALID_MSG_CTX);
    	}
    	
    	final MessageContext<Envelope> outMessageContext = profileRequestContext.getOutboundMessageContext();
    	final TokenGenerationContext tokenGenerationContext = new TokenGenerationContext();
    	outMessageContext.addSubcontext(tokenGenerationContext);
    	String tokenType = tokenRequestContext.getTokenType();
    	tokenGenerationContext.setTokenType(tokenType);

    	if (certEnabled && tokenType.equals(WsTrustInteroperabilityProfileConfiguration.TOKEN_TYPE_X509_ID)) {
    		log.debug("Action {}: Requested token type is X.509", getId());
    		return ActionSupport.buildEvent(this, InitializeTokenGenerationContext.EVENT_ID_X509_TOKEN_TYPE);
    	} else if (proxyEnabled && tokenType.equals(WsTrustInteroperabilityProfileConfiguration.TOKEN_TYPE_X509_PROXY_ID)) {
    		log.debug("Action {}: Requested token type is X.509 proxy", getId());
    		return ActionSupport.buildEvent(this, InitializeTokenGenerationContext.EVENT_ID_X509_PROXY_TOKEN_TYPE);
    	} else {
    		log.info("Action {}: Ignoring a request with a desired token type {}", getId(), tokenType);
    		throw new UnSupportedOutgoingTokenTypeException(tokenType);
    	}
    }
    
    /** Exception thrown when the requested security token type is not supported. */
    public static class UnSupportedOutgoingTokenTypeException extends ProfileException {

        /** Serial version UID. */
        private static final long serialVersionUID = -872912132137634555L;

        /**
         * Constructor.
         * 
         * @param message exception message
         */
        public UnSupportedOutgoingTokenTypeException(String tokenType) {
            super("Un-supported token type " + tokenType);
        }
    }
}
