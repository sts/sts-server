/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.profile;

import java.security.cert.CertificateEncodingException;

import java.security.cert.X509Certificate;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.xml.security.utils.Base64;
import org.glite.sts.profile.wstrust.WsTrustActionSupport;
import org.glite.sts.ta.TokenGenerationContext;
import org.glite.sts.ta.x509.X509TokenGenerationContext;
import org.glite.sts.x509.caclient.CAResponse;
import org.opensaml.messaging.context.MessageContext;
import org.opensaml.messaging.context.navigate.ChildContextLookup;
import org.opensaml.soap.soap11.Envelope;
import org.opensaml.soap.wssecurity.BinarySecurityToken;
import org.opensaml.soap.wssecurity.Reference;
import org.opensaml.soap.wssecurity.Security;
import org.opensaml.soap.wssecurity.SecurityTokenReference;
import org.opensaml.soap.wstrust.RequestSecurityTokenResponse;
import org.opensaml.soap.wstrust.RequestSecurityTokenResponseCollection;
import org.opensaml.soap.wstrust.RequestedSecurityToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.webflow.execution.RequestContext;

import com.google.common.base.Function;

import net.shibboleth.ext.spring.webflow.Event;
import net.shibboleth.ext.spring.webflow.Events;
import net.shibboleth.idp.profile.AbstractProfileAction;
import net.shibboleth.idp.profile.ActionSupport;
import net.shibboleth.idp.profile.EventIds;
import net.shibboleth.idp.profile.ProfileException;
import net.shibboleth.idp.profile.ProfileRequestContext;

/**
 * Builds a {@link BinarySecurityToken} containing an X.509 certificate. It is added to the {@link RequestSecurityTokenResponse}, 
 * inside the {@link RequestSecurityTokenResponseCollection} in the {@link Body} of the {@link Envelope} set as the message of 
 * the {@link ProfileRequestContext#getOutboundMessageContext()}.
 */
@Events({
        @Event(id = EventIds.PROCEED_EVENT_ID),
        @Event(id = EventIds.INVALID_MSG_CTX)})
public class AddX509CertificateToRSTR extends AbstractProfileAction<Object, Envelope> {
    
    /** URI identifier used in the token reference */
    public static final String URI_ID = "#X509SecurityToken";
	
    /** Class logger. */
    private final Logger log = LoggerFactory.getLogger(AddX509CertificateToRSTR.class);
    
    /**
     * Strategy used to locate the {@link TokenGenerationContext} associated with a given {@link MessageContext}.
     */
    private Function<MessageContext<?>, TokenGenerationContext> tokenGenerationContextLookupStrategy;
    
    /**
     * Strategy used to locate the {@link X509TokenGenerationContext} associated with a given {@link TokenGenerationContext}.
     */
    private Function<TokenGenerationContext, X509TokenGenerationContext> x509TokenGenerationContextLookupStrategy;
    
    /** Constructor. */
    public AddX509CertificateToRSTR() {
    	super();
        tokenGenerationContextLookupStrategy =
                new ChildContextLookup<MessageContext<?>, TokenGenerationContext>(TokenGenerationContext.class,
                        false);
        x509TokenGenerationContextLookupStrategy =
        		new ChildContextLookup<TokenGenerationContext, X509TokenGenerationContext>(X509TokenGenerationContext.class,
        				false);
    }
    
    /** {@inheritDoc} */
    protected org.springframework.webflow.execution.Event doExecute(final HttpServletRequest httpRequest, final HttpServletResponse httpResponse,
            final RequestContext springRequestContext,
            final ProfileRequestContext<Object, Envelope> profileRequestContext) throws ProfileException {
    	log.debug("Action {}: Attempting to add a certificate to outgoing RSTR inside an Envelope", getId());
    	
    	TokenGenerationContext tokenGenerationCtx = tokenGenerationContextLookupStrategy.apply(profileRequestContext.getOutboundMessageContext());
		if (tokenGenerationCtx == null) {
            log.debug("Action {}: No TokenGenerationContext available, nothing left to do", getId());
            return ActionSupport.buildProceedEvent(this);
		}
    	X509TokenGenerationContext x509Context = x509TokenGenerationContextLookupStrategy.apply(tokenGenerationCtx);
		if (x509Context == null) {
            log.debug("Action {}: No X509TokenGenerationContext available for TokenGenerationContext {}, nothing left to do", getId(), tokenGenerationCtx.getId());
            return ActionSupport.buildProceedEvent(this);
		}
		CAResponse caResponse = x509Context.getCaResponse();
		if (caResponse == null) {
			log.debug("Action {}: No CAResponse available in the X509TokenGenerationContext {}, nothing left to do", getId(), x509Context.getId());
			return ActionSupport.buildProceedEvent(this);
		}
		X509Certificate x509Certificate = caResponse.getCertificate();
		if (x509Certificate == null) {
			log.debug("Action {}: No X509Certificate available in the CAResponse, nothing left to do", getId());
			return ActionSupport.buildProceedEvent(this);
		}
		byte[] x509bytes;
		try {
			x509bytes = x509Certificate.getEncoded();
		} catch (CertificateEncodingException e) {
			log.warn("Action {}: Could not encode the certificate into bytes, nothing left to do", getId());
			return ActionSupport.buildProceedEvent(this);
		}
		String x509 = new String(Base64.encode(x509bytes));
		log.debug("Pre-requisities fine, now generating the element");
    	
    	Envelope envelope = profileRequestContext.getOutboundMessageContext().getMessage();
    	if (envelope == null) {
    		log.debug("Action {}: Envelope was not found from the outbound context.", getId());
    		return ActionSupport.buildEvent(this, EventIds.INVALID_MSG_CTX);    		
    	}
    	
    	// hard-coded to the first, so no support for multiple responses at the moment
    	final RequestSecurityTokenResponseCollection rstrc = (RequestSecurityTokenResponseCollection) envelope.getBody().getUnknownXMLObjects(RequestSecurityTokenResponseCollection.ELEMENT_NAME).get(0);
    	final RequestSecurityTokenResponse rstr = rstrc.getRequestSecurityTokenResponses().get(0);
    	final Security securityHeader = (Security) envelope.getHeader().getUnknownXMLObjects(Security.ELEMENT_NAME).get(0);
    	
    	final RequestedSecurityToken requestedToken = WsTrustActionSupport.addRequestedTokenToRSTR(this, rstr);
    	final SecurityTokenReference tokenReference = WsTrustActionSupport.addSecurityTokenReferenceToRequestedToken(this, requestedToken);
    	final Reference reference = WsTrustActionSupport.addReferenceToSecurityTokenReference(this, tokenReference);
    	reference.setURI(URI_ID);

    	final BinarySecurityToken binaryToken = WsTrustActionSupport.addBinarySecurityTokenToSecurityHeader(this, securityHeader);
    	binaryToken.setEncodingType("http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-x509-token-profile-1.0#Base64Binary");
    	binaryToken.setWSUId(URI_ID);
    	binaryToken.setValue(x509);
    	
    	return ActionSupport.buildProceedEvent(this);
    }
    
}
