/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.profile.wstrust;

import javax.annotation.Nonnull;

import org.glite.sts.profile.soap.SoapActionSupport;
import org.opensaml.core.xml.config.XMLObjectProviderRegistrySupport;
import org.opensaml.soap.soap11.Envelope;
import org.opensaml.soap.wssecurity.BinarySecurityToken;
import org.opensaml.soap.wssecurity.Reference;
import org.opensaml.soap.wssecurity.Security;
import org.opensaml.soap.wssecurity.SecurityTokenReference;
import org.opensaml.soap.wssecurity.WSSecurityObjectBuilder;
import org.opensaml.soap.wstrust.BinarySecret;
import org.opensaml.soap.wstrust.RequestSecurityTokenResponse;
import org.opensaml.soap.wstrust.RequestSecurityTokenResponseCollection;
import org.opensaml.soap.wstrust.RequestedProofToken;
import org.opensaml.soap.wstrust.RequestedSecurityToken;
import org.opensaml.soap.wstrust.TokenType;
import org.opensaml.soap.wstrust.WSTrustObjectBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.shibboleth.idp.profile.AbstractProfileAction;

/**
 * This class contains helper methods for dealing with the WS-Trust objects.
 */
public final class WsTrustActionSupport {
	
	/**
	 * Builds a {@link BinarySecurityToken} and attaches it to requested token.
	 * @param action the action
	 * @param requestedToken the parent of the token
	 * @return the binary security token
	 */
	@Nonnull public static BinarySecurityToken addBinarySecurityTokenToRequestedToken(@Nonnull final AbstractProfileAction<?, ?> action, @Nonnull final RequestedSecurityToken requestedToken) {
		@SuppressWarnings("unchecked")
		WSSecurityObjectBuilder<BinarySecurityToken> builder = (WSSecurityObjectBuilder<BinarySecurityToken>) XMLObjectProviderRegistrySupport.getBuilderFactory().getBuilder(BinarySecurityToken.ELEMENT_NAME); 
		final BinarySecurityToken token = builder.buildObject();
        requestedToken.setUnknownXMLObject(token);
        getLogger().debug("Action {}: Added BinarySecurityToken as the RequestedToken's content", action.getId());
        return token;
	}
	
    /**
     * Builds a {@link BinarySecurityToken} and attaches it to security header.
     * @param action the action
     * @param security the parent of the token
     * @return the binary security token
     */
	@Nonnull public static BinarySecurityToken addBinarySecurityTokenToSecurityHeader(@Nonnull final AbstractProfileAction<?, ?> action, @Nonnull final Security security) {
        @SuppressWarnings("unchecked")
		WSSecurityObjectBuilder<BinarySecurityToken> builder = (WSSecurityObjectBuilder<BinarySecurityToken>) XMLObjectProviderRegistrySupport.getBuilderFactory().getBuilder(BinarySecurityToken.ELEMENT_NAME); 
        final BinarySecurityToken token = builder.buildObject();
        security.getUnknownXMLObjects().add(token);
        getLogger().debug("Action {}: Added BinarySecurityToken as the Security's content", action.getId());
        return token;
    }
	
	/**
	 * Builds a {@link SecurityTokenReference} and attaches it to requested token.
	 * @param action the action
	 * @param requestedToken the parent of the security token reference
	 * @return the security token reference
	 */
	@Nonnull public static SecurityTokenReference addSecurityTokenReferenceToRequestedToken(@Nonnull final AbstractProfileAction<?, ?> action, @Nonnull final RequestedSecurityToken requestedToken) {
        @SuppressWarnings("unchecked")
		WSSecurityObjectBuilder<SecurityTokenReference> builder = (WSSecurityObjectBuilder<SecurityTokenReference>) XMLObjectProviderRegistrySupport.getBuilderFactory().getBuilder(SecurityTokenReference.ELEMENT_NAME); 
        final SecurityTokenReference reference = builder.buildObject();
        requestedToken.setUnknownXMLObject(reference);
        getLogger().debug("Action {}: Added SecurityTokenReference as the RequestedToken's content", action.getId());
        return reference;
    }

	/**
	 * Builds a {@link Reference} and attaches it to the security token reference.
	 * @param action the action
	 * @param tokenReference the parent of the reference
	 * @return the reference
	 */
	@Nonnull public static Reference addReferenceToSecurityTokenReference(@Nonnull final AbstractProfileAction<?, ?> action, @Nonnull final SecurityTokenReference tokenReference) {
	    @SuppressWarnings("unchecked")
		WSSecurityObjectBuilder<Reference> builder = (WSSecurityObjectBuilder<Reference>) XMLObjectProviderRegistrySupport.getBuilderFactory().getBuilder(Reference.ELEMENT_NAME); 
	    final Reference reference = builder.buildObject();
	    tokenReference.getUnknownXMLObjects().add(reference);
	    getLogger().debug("Action {}: Added Reference as the SecurityTokenReference's content", action.getId());
	    return reference;
	}

	/**
	 * Builds a {@link BinarySecret} and attaches it to the requested proof token.
	 * @param action the action
	 * @param proofToken the parent of the binary secret
	 * @return the binary secret
	 */
	@Nonnull public static BinarySecret addBinarySecretToProofToken(@Nonnull final AbstractProfileAction<?, ?> action, @Nonnull final RequestedProofToken proofToken) {
		@SuppressWarnings("unchecked")
		WSTrustObjectBuilder<BinarySecret> builder = (WSTrustObjectBuilder<BinarySecret>) XMLObjectProviderRegistrySupport.getBuilderFactory().getBuilder(BinarySecret.ELEMENT_NAME); 
		final BinarySecret secret = builder.buildObject();
		proofToken.setUnknownXMLObject(secret);
		getLogger().debug("Action {}: Added BinarySecret to RequestedProofToken", action.getId());
		return secret;
	}
	
	/**
	 * Builds a {@link TokenType} and attaches it to the request security token response
	 * @param action the action
	 * @param rstr the request security token response
	 * @return the token type
	 */
	@Nonnull public static TokenType addTokenTypeToRSTR(@Nonnull final AbstractProfileAction<?, ?> action, @Nonnull final RequestSecurityTokenResponse rstr) {
        @SuppressWarnings("unchecked")
		final WSTrustObjectBuilder<TokenType> builder = (WSTrustObjectBuilder<TokenType>) XMLObjectProviderRegistrySupport.getBuilderFactory().getBuilder(TokenType.ELEMENT_NAME);
        final TokenType tokenType = builder.buildObject();
        rstr.getUnknownXMLObjects().add(tokenType);
        getLogger().debug("Action {}: Added TokenType to RSTR", action.getId());
		return tokenType;
	}
	
	/**
	 * Builds a {@link RequestedSecurityToken} and attaches it to the request security token response 
	 * @param action the action
	 * @param rstr the request security token response
	 * @return the requested security token
	 */
	@Nonnull public static RequestedSecurityToken addRequestedTokenToRSTR(@Nonnull final AbstractProfileAction<?, ?> action, @Nonnull final RequestSecurityTokenResponse rstr) {
        @SuppressWarnings("unchecked")
		final WSTrustObjectBuilder<RequestedSecurityToken> builder = (WSTrustObjectBuilder<RequestedSecurityToken>) XMLObjectProviderRegistrySupport.getBuilderFactory().getBuilder(RequestedSecurityToken.ELEMENT_NAME);
        final RequestedSecurityToken requestedToken = builder.buildObject();
        rstr.getUnknownXMLObjects().add(requestedToken);
        getLogger().debug("Action {}: Added RequestedSecurityToken to RSTR", action.getId());
		return requestedToken;
	}
	
	/**
	 * Buils a {@link RequestedProofToken} and attaches it to the request security token response
	 * @param action the action
	 * @param rstr the request security token response
	 * @return the requested proof token
	 */
	@Nonnull public static RequestedProofToken addRequestedProofTokenToRSTR(@Nonnull final AbstractProfileAction<?, ?> action, @Nonnull final RequestSecurityTokenResponse rstr) {
        @SuppressWarnings("unchecked")
		final WSTrustObjectBuilder<RequestedProofToken> builder = (WSTrustObjectBuilder<RequestedProofToken>) XMLObjectProviderRegistrySupport.getBuilderFactory().getBuilder(RequestedProofToken.ELEMENT_NAME);
        final RequestedProofToken requestedProof = builder.buildObject();
        rstr.getUnknownXMLObjects().add(requestedProof);
        getLogger().debug("Action {}: Added RequestedProofToken to RSTR", action.getId());
		return requestedProof;
	}

	/**
	 * Builds a {@link RequestSecurityTokenResponse} and attaches it to the request security token response collection
	 * @param action the action
	 * @param rstrc the request security token response collection
	 * @return the request security token response
	 */
	@Nonnull public static RequestSecurityTokenResponse addRSTRtoRSTRC(@Nonnull final AbstractProfileAction<?, ?> action, @Nonnull final RequestSecurityTokenResponseCollection rstrc) {
        @SuppressWarnings("unchecked")
		final WSTrustObjectBuilder<RequestSecurityTokenResponse> builder = (WSTrustObjectBuilder<RequestSecurityTokenResponse>) XMLObjectProviderRegistrySupport.getBuilderFactory().getBuilder(RequestSecurityTokenResponse.ELEMENT_NAME);
        final RequestSecurityTokenResponse rstr = builder.buildObject();
        rstrc.getRequestSecurityTokenResponses().add(rstr);
        getLogger().debug("Action {}: Added RSTR object to RSTRC", action.getId());
        return rstr;
	}

	/**
	 * Builds a {@link RequestSecurityTokenResponseCollection} and attaches it to the envelope body
	 * @param action the action
	 * @param envelope the envelope
	 * @return the request security token response collection
	 */
	@Nonnull public static RequestSecurityTokenResponseCollection addRSTRCtoEnvelopeBody(@Nonnull final AbstractProfileAction<?, ?> action, @Nonnull final Envelope envelope) {
        @SuppressWarnings("unchecked")
		final WSTrustObjectBuilder<RequestSecurityTokenResponseCollection> builder = (WSTrustObjectBuilder<RequestSecurityTokenResponseCollection>) XMLObjectProviderRegistrySupport.getBuilderFactory().getBuilder(RequestSecurityTokenResponseCollection.ELEMENT_NAME);
        final RequestSecurityTokenResponseCollection rstrc = builder.buildObject();
        SoapActionSupport.addBodytoEnvelope(action, envelope).getUnknownXMLObjects().add(rstrc);
        getLogger().debug("Action {}: Added RSTRC object to the Envelope's Body", action.getId());
        return rstrc;
	}
	
    /**
     * Gets the logger for this class.
     * 
     * @return logger for this class, never null
     */
    private static Logger getLogger() {
        return LoggerFactory.getLogger(WsTrustActionSupport.class);
    }

}
