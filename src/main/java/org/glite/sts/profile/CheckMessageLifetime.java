/*
 * Licensed to the University Corporation for Advanced Internet Development, 
 * Inc. (UCAID) under one or more contributor license agreements.  See the 
 * NOTICE file distributed with this work for additional information regarding
 * copyright ownership. The UCAID licenses this file to You under the Apache 
 * License, Version 2.0 (the "License"); you may not use this file except in 
 * compliance with the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.glite.sts.profile;

import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.shibboleth.idp.profile.AbstractProfileAction;
import net.shibboleth.idp.profile.ActionSupport;
import net.shibboleth.idp.profile.ProfileException;
import net.shibboleth.idp.profile.ProfileRequestContext;
import net.shibboleth.idp.relyingparty.RelyingPartyContext;

import org.joda.time.DateTime;
import org.opensaml.messaging.context.BasicMessageMetadataContext;
import org.opensaml.messaging.context.MessageContext;
import org.opensaml.messaging.context.navigate.ChildContextLookup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;

import com.google.common.base.Function;

/**
 * An action that checks that the inbound message should be considered valid based upon when it was issued.
 *  
 * Based on net.shibboleth.idp.profile.impl.CheckMessageLifetime  
 */
public final class CheckMessageLifetime extends AbstractProfileAction<Object, Object> {
	
    /** Class logger. */
    private final Logger log = LoggerFactory.getLogger(CheckMessageLifetime.class);

    /** Amount of time, in milliseconds, for which a message is valid. Default value: 5 minutes */
    private long messageLifetime;
    
    /** Is issue instant required from the incoming messages */
    private boolean requireIssueInstant;

    /**
     * Strategy used to look up the {@link RelyingPartyContext} associated with the given {@link ProfileRequestContext}.
     */
    private Function<ProfileRequestContext<Object, Object>, RelyingPartyContext> rpContextLookupStrategy;

    /**
     * Strategy used to look up the {@link BasicMessageMetadataContext} associated with the inbound message context.
     */
    private Function<MessageContext<Object>, BasicMessageMetadataContext> messageMetadataContextLookupStrategy;

    /**
     * Constructor.
     * 
     * Initializes {@link #messageLifetime} to 5 minutes.
     */
    public CheckMessageLifetime(boolean issueInstantRequired) {
    	this(issueInstantRequired, TimeUnit.MILLISECONDS.convert(5, TimeUnit.MINUTES));
    }
    
    /**
     * Constructor.
     * 
     * Initializes {@link #messageLifetime} according to the parameter. 

     * @param lifetime
     */
    public CheckMessageLifetime(boolean issueInstantRequired, long lifetime) {
        super();
        this.requireIssueInstant = issueInstantRequired;
        this.messageLifetime = lifetime;
        rpContextLookupStrategy =
                new ChildContextLookup<ProfileRequestContext<Object, Object>, RelyingPartyContext>(RelyingPartyContext.class, false);
        messageMetadataContextLookupStrategy =
                new ChildContextLookup<MessageContext<Object>, BasicMessageMetadataContext>(BasicMessageMetadataContext.class,
                        false);    	
    }

    /** {@inheritDoc} */
    protected Event doExecute(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
            RequestContext springRequestContext, ProfileRequestContext<Object, Object> profileRequestContext) throws ProfileException {

    	if (!this.requireIssueInstant) {
    		log.debug("Action {}: No issue instant required, ignoring the lifetime check!", getId());
    		return ActionSupport.buildProceedEvent(this);
    	}
    	
        final RelyingPartyContext relyingPartyCtx = rpContextLookupStrategy.apply(profileRequestContext);
        final BasicMessageMetadataContext messageSubcontext =
                messageMetadataContextLookupStrategy.apply(profileRequestContext.getInboundMessageContext());
        
        if (messageSubcontext.getMessageIssueInstant() <= 0) {
        	throw new NoIssueInstantException(messageSubcontext.getMessageId());
        }

        final long clockskew = relyingPartyCtx.getProfileConfig().getSecurityConfiguration().getClockSkew();
        final long issueInstant = messageSubcontext.getMessageIssueInstant();
        final long currentTime = System.currentTimeMillis();

        if (issueInstant < currentTime - clockskew) {
            throw new PastMessageException(messageSubcontext.getMessageId(), issueInstant);
        }

        if (issueInstant > currentTime + messageLifetime + clockskew) {
            throw new FutureMessageException(messageSubcontext.getMessageId(), issueInstant);
        }

        return ActionSupport.buildProceedEvent(this);
    }

    /**
     * A profile processing exception that occurs when the inbound message was issued from a point in time to far in the
     * future.
     */
    public class FutureMessageException extends ProfileException {

        /** Serial version UID. */
        private static final long serialVersionUID = -6474772810189615621L;

        /**
         * Constructor.
         * 
         * @param messageId the ID of the message, never null
         * @param instant the issue instant of the message in milliseconds since the epoch
         */
        public FutureMessageException(String messageId, long instant) {
            super("Action " + getId() + ": Message " + messageId + " was issued on " + new DateTime(instant).toString()
                    + " and is not yet valid.");
        }
    }

    /**
     * A profile processing exception that occurs when the inbound message was issued from a point in time to far in the
     * past.
     */
    public class PastMessageException extends ProfileException {

        /** Serial version UID. */
        private static final long serialVersionUID = 18935109782906635L;

        /**
         * Constructor.
         * 
         * @param messageId the ID of the message, never null
         * @param instant the issue instant of the message in milliseconds since the epoch
         */
        public PastMessageException(String messageId, long instant) {
            super("Action " + getId() + ": Message " + messageId + " was issued on " + new DateTime(instant).toString()
                    + " is now considered expired.");
        }
    }
    
    /**
     * A profile processing exception that occurs when the no issue instant exists in the inbound message.
     * past.
     */
    public class NoIssueInstantException extends ProfileException {

        /** Serial version UID. */
        private static final long serialVersionUID = 18935321312306635L;

        /**
         * Constructor.
         * 
         * @param messageId the ID of the message, never null
         */
        public NoIssueInstantException(String messageId) {
            super("Action " + getId() + ": Message " + messageId + " had no issue instant, even though it's required!");
        }
    }

}