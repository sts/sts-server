/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.profile;

import java.util.ArrayList;
import java.util.Collection;

import net.shibboleth.idp.profile.ActionSupport;
import net.shibboleth.idp.profile.ProfileRequestContext;
import net.shibboleth.idp.profile.config.ProfileConfiguration;
import net.shibboleth.idp.profile.config.SecurityConfiguration;
import net.shibboleth.idp.relyingparty.RelyingPartyConfiguration;
import net.shibboleth.idp.relyingparty.RelyingPartyContext;
import net.shibboleth.utilities.java.support.component.AbstractIdentifiableInitializableComponent;
import net.shibboleth.utilities.java.support.component.IdentifiableComponent;
import net.shibboleth.utilities.java.support.logic.Constraint;
import net.shibboleth.utilities.java.support.security.RandomIdentifierGenerationStrategy;

import org.glite.sts.profile.config.WsTrustInteroperabilityProfileConfiguration;
import org.opensaml.messaging.context.MessageContext;
import org.opensaml.soap.soap11.Envelope;
import org.springframework.webflow.execution.Action;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;

/**
 * This action initializes the {@link ProfileRequestContext} using the {@link WsTrustInteroperabilityProfileConfiguration}.
 * Both inbound and outbound message formats are thus {@link Envelope}. 
 */
public class InitializeProfile extends AbstractIdentifiableInitializableComponent implements Action, IdentifiableComponent {
	
	/** Relying party context identifier. */
	private String rpContextId;

	/** Relying party configuration identifier. */
	private String rpConfigurationId;
	
	/** Relying party responder identifier. */
	private String rpResponderId;
	
	/** Clock skew in milliseconds allowed for the profile messages. */
	private int clockSkew;
	
	/**
	 * Constructor.
	 * @param contextId Relying party context identifier
	 * @param configurationId Relying party configuration identifier
	 * @param responderId Relying party responder identifier
	 * @param skew Clock skew in milliseconds
	 */
	public InitializeProfile(String contextId, String configurationId, String responderId, int skew) {
		super();
		setId(getClass().getName());
		this.rpContextId = Constraint.isNotNull(contextId, "Relying party context id cannot be null!");
		this.rpConfigurationId = Constraint.isNotNull(configurationId, "Relying party configuration id cannot be null!");
		this.rpResponderId = Constraint.isNotNull(responderId, "Relying party responder id cannot be null!");
		this.clockSkew = (int) Constraint.isGreaterThan(0, skew, "Clock skew cannot be zero!");
	}
	
	/** {@inheritDoc} */
	@Override
	public Event execute(RequestContext springRequestContext) throws Exception {
		ProfileRequestContext<Envelope, Envelope> profileContext = new ProfileRequestContext<Envelope, Envelope>();
		profileContext.setOutboundMessageContext(new MessageContext<Envelope>());
		final RelyingPartyContext rpCtx = new RelyingPartyContext(this.rpContextId);
		profileContext.addSubcontext(rpCtx);
		rpCtx.setProfileConfiguration(buildProfileConfiguration(this.clockSkew));
		rpCtx.setRelyingPartyConfiguration(new RelyingPartyConfiguration(this.rpConfigurationId, this.rpResponderId, buildProfileConfigurations(this.clockSkew)));
        springRequestContext.getConversationScope().put(ProfileRequestContext.BINDING_KEY, profileContext);
        return ActionSupport.buildProceedEvent(this);
    }
	
    /**
     * Builds a {@link ProfileConfiguration} collection containing a {@link WsTrustInteroperabilityProfileConfiguration}.
     * 
     * @return the constructed {@link ProfileConfiguration}
     */
    public static Collection<ProfileConfiguration> buildProfileConfigurations(int clockSkew) {
        ArrayList<ProfileConfiguration> profileConfigs = new ArrayList<ProfileConfiguration>();
        profileConfigs.add(buildProfileConfiguration(clockSkew));
        return profileConfigs;
    }
    
    /**
     * Build a {@link WsTrustInteroperabilityProfileConfiguration}.
     */
    public static WsTrustInteroperabilityProfileConfiguration buildProfileConfiguration(int clockSkew) {
        SecurityConfiguration securityConfig = new SecurityConfiguration(clockSkew, new RandomIdentifierGenerationStrategy());
        WsTrustInteroperabilityProfileConfiguration wsTrustConfig = new WsTrustInteroperabilityProfileConfiguration();
        wsTrustConfig.setSecurityConfiguration(securityConfig);
        return wsTrustConfig;
    }

}
