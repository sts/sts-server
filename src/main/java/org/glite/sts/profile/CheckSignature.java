/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.profile;

import java.security.interfaces.RSAPublicKey;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.shibboleth.idp.profile.AbstractProfileAction;
import net.shibboleth.idp.profile.ActionSupport;
import net.shibboleth.idp.profile.EventIds;
import net.shibboleth.idp.profile.ProfileException;
import net.shibboleth.idp.profile.ProfileRequestContext;

import org.glite.sts.profile.soap.SoapActionSupport;
import org.opensaml.core.xml.XMLObject;
import org.opensaml.messaging.context.MessageContext;
import org.opensaml.messaging.context.navigate.ChildContextLookup;
import org.opensaml.security.credential.BasicCredential;
import org.opensaml.security.credential.Credential;
import org.opensaml.soap.soap11.Envelope;
import org.opensaml.soap.soap11.Header;
import org.opensaml.soap.wssecurity.Security;
import org.opensaml.xmlsec.signature.Signature;
import org.opensaml.xmlsec.signature.support.SignatureException;
import org.opensaml.xmlsec.signature.support.SignatureValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.webflow.execution.RequestContext;

import com.google.common.base.Function;

/**
 * Checks whether the signature in the {@link Envelope} is valid.
 */
public class CheckSignature extends AbstractProfileAction<Envelope, Object> {
	
    /** Class logger. */
    private final Logger log = LoggerFactory.getLogger(CheckSignature.class);
    
    /**
     * Strategy used to look up the {@link TokenRequestMessageContext} associated with the inbound message context.
     */
    private Function<MessageContext<Envelope>, TokenRequestMessageContext> tokenRequestMessageContextLookupStrategy;
    
    /** Is signature required. */
    private boolean requireSignature;
    
    /**
     * Constructor.
     * @param signatureRequired is signature required from the messages
     */
    public CheckSignature(boolean signatureRequired) {
    	super();
        tokenRequestMessageContextLookupStrategy =
                new ChildContextLookup<MessageContext<Envelope>, TokenRequestMessageContext>(TokenRequestMessageContext.class,
                        false);
    	this.requireSignature = signatureRequired;
    }
    
    /** {@inheritDoc} */
    protected org.springframework.webflow.execution.Event doExecute(final HttpServletRequest httpRequest, final HttpServletResponse httpResponse,
            final RequestContext springRequestContext,
            final ProfileRequestContext<Envelope, Object> profileRequestContext) throws ProfileException {
    	final MessageContext<Envelope> inboundMessageContext = profileRequestContext.getInboundMessageContext();
    	final Envelope envelope = inboundMessageContext.getMessage();
    	final TokenRequestMessageContext tokenRequestContext = tokenRequestMessageContextLookupStrategy.apply(inboundMessageContext);
    	final Header header = envelope.getHeader();
        if (header == null) {
        	log.error("Action {}: Header was not found from the envelope!", getId());
        	return ActionSupport.buildEvent(this, EventIds.INVALID_PROFILE_CTX);
        }
        List<XMLObject> objects = header.getUnknownXMLObjects(Security.ELEMENT_NAME);
        if (objects.size() != 1) {
        	log.debug("Action {}: No or more than one security elements in the header!", getId());
        	throw new InvalidSignatureException("More than one security elements in the header!");
        }
        final Security security = (Security) objects.get(0);
        objects = security.getUnknownXMLObjects(Signature.DEFAULT_ELEMENT_NAME);
        if (objects.size() == 0) {
        	if (this.requireSignature) {
        		log.debug("Action {}: No signature element found from the security header, even though it's required!", getId());
        		throw new InvalidSignatureException("No signature element found from the security header, even though it's required!");
        	} else {
        		log.debug("Action {}: No signature element found from the security header, nothing left to do.", getId());
        		return ActionSupport.buildProceedEvent(this);
        	}
        } else if (objects.size() > 1) {
        	log.debug("Action {}: More than one signature elements in the security header!", getId());
        	throw new InvalidSignatureException("More than one signature elements in the security header!");
        }
        RSAPublicKey publicKey = SoapActionSupport.getPublicKeyFromEnvelope(this, envelope);
        if (signatureValidated((Signature)objects.get(0), new BasicCredential(publicKey), tokenRequestContext.getId())) {
        	log.debug("Action {}: Signature was valid!", getId());
        } else {
        	log.debug("Action {}: Could not validate the signature", getId());
        	throw new InvalidSignatureException("Could not validate the signature in " + tokenRequestContext.getId());
        }
        return ActionSupport.buildProceedEvent(this);
    }
    
	/**
	 * Verify a XML digital signature.
	 * @param signature the signature from the envelope
	 * @param validatingCredential the credentials used for the validation
	 * @param messageId the id of the mssage to be validated
	 * @return true if valid, false otherwise
	 */
	protected boolean signatureValidated(Signature signature, Credential validatingCredential, String messageId) {
		SignatureValidator validator = new SignatureValidator(
				validatingCredential);
		try {
			validator.validate(signature);
		} catch (SignatureException e) {
			log.warn("Action {}: The signature could not be validated for {}", getId(), messageId);
			log.debug("Action {}: The stack for the failure", getId(), e);
			return false;
		}
		return true;
	}
    
    /** An exception thrown when the signature is missing or invalid. */
    public static class InvalidSignatureException extends ProfileException {

        /** Serial version UID. */
        private static final long serialVersionUID = -367932432432124555L;

        /**
         * Constructor.
         * 
         * @param message exception message
         */
        public InvalidSignatureException(String message) {
        	super(message);
        }
    }
}
