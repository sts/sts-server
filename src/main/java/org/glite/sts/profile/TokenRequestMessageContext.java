/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.profile;

import org.opensaml.messaging.context.BaseContext;
import org.opensaml.messaging.context.MessageContext;

/** 
 * A {@link Context} intended to be used as a subcontext of a {@link MessageContext}  that carries 
 * some basic data about the RequestSecurityToken message.
 */
public class TokenRequestMessageContext extends BaseContext {
	
	/** The id attribute of the message. */
	private String id;
	
	/** The context attribute of the message. */
	private String context;
	
	/** The action of the request message, same with the RequestType. */
	private String action;
	
	/** The recipient of the request message. */
	private String recipient;
	
	/** The token type desired to be obtained. */
	private String tokenType;
	
	/** The claims dialect attribute of the message. */
	private String claimsDialect;
	
	/** The token reference identifier in the claims */
	private String tokenReference;

	/** Gets the recipient of the request message.
	 * 
	 * @return the recipient of the request message
	 */
	public String getRecipient() {
		return recipient;
	}

	/** Sets the recipient of the request message.
	 * 
	 * @param recipient of the request message.
	 */
	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}

	/** Gets the token type desired to be obtained by the request message.
	 * 
	 * @return the desired token type
	 */
	public String getTokenType() {
		return tokenType;
	}

	/** Sets the token type desired to be obtained by the request message.
	 * 
	 * @param tokenType the desired token type
	 */
	public void setTokenType(String tokenType) {
		this.tokenType = tokenType;
	}

	/** Gets the action of the request message.
	 * 
	 * @return the action of the request message.
	 */
	public String getAction() {
		return action;
	}

	/** Sets the action of the request message.
	 * 
	 * @param action of the request message
	 */
	public void setAction(String action) {
		this.action = action;
	}
	
	/** Gets the context of the request message.
	 * 
	 * @return the context of the request message.
	 */
	public String getContext() {
		return context;
	}

	/** Sets the context of the request message.
	 * 
	 * @param context of the request message
	 */
	public void setContext(String context) {
		this.context = context;
	}

	/** Gets the id of the request message.
	 * 
	 * @return the id of the request message.
	 */
	public String getId() {
		return id;
	}

	/** Sets the id of the request message.
	 * 
	 * @param id of the request message
	 */
	public void setId(String id) {
		this.id = id;
	}

	/** Gets the claims dialect of the request message.
	 * 
	 * @return the claims dialect of the request message.
	 */
	public String getClaimsDialect() {
		return claimsDialect;
	}

	/** Sets the claims dialect of the request message.
	 * 
	 * @param claims dialect of the request message
	 */
	public void setClaimsDialect(String claimsDialect) {
		this.claimsDialect = claimsDialect;
	}

	/** Gets the token reference of the request message.
	 * 
	 * @return the token reference of the request message.
	 */
	public String getTokenReference() {
		return tokenReference;
	}

	/** Sets the token reference of the request message.
	 * 
	 * @param token reference of the request message
	 */
	public void setTokenReference(String tokenReference) {
		this.tokenReference = tokenReference;
	}

}
