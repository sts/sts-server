/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.profile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.glite.sts.profile.soap.SoapActionSupport;
import org.opensaml.messaging.context.BasicMessageMetadataContext;
import org.opensaml.messaging.context.MessageContext;
import org.opensaml.messaging.context.navigate.ChildContextLookup;
import org.opensaml.soap.soap11.Envelope;
import org.opensaml.soap.soap11.Header;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.webflow.execution.RequestContext;

import com.google.common.base.Function;

import net.shibboleth.ext.spring.webflow.Event;
import net.shibboleth.ext.spring.webflow.Events;
import net.shibboleth.idp.profile.AbstractProfileAction;
import net.shibboleth.idp.profile.ActionSupport;
import net.shibboleth.idp.profile.EventIds;
import net.shibboleth.idp.profile.ProfileException;
import net.shibboleth.idp.profile.ProfileRequestContext;
import net.shibboleth.utilities.java.support.primitive.StringSupport;

/**
 * Builds a {@link RelatesTo} and adds it to the {@link Envelope} header of the {@link ProfileRequestContext#getOutboundMessageContext()}. 
 */
@Events({
        @Event(id = EventIds.PROCEED_EVENT_ID),
        @Event(id = EventIds.INVALID_MSG_CTX)})
public class AddRelatesToToEnvelope extends AbstractProfileAction<Object, Envelope> {

    /** Class logger. */
    private Logger log = LoggerFactory.getLogger(AddRelatesToToEnvelope.class);

    /**
     * Strategy used to look up the {@link BasicMessageMetadataContext} associated with the inbound message context.
     */
    private Function<MessageContext<?>, BasicMessageMetadataContext> messageMetadataContextLookupStrategy;

    /** Constructor. */
    public AddRelatesToToEnvelope() {
    	super();
        messageMetadataContextLookupStrategy =
                new ChildContextLookup<MessageContext<?>, BasicMessageMetadataContext>(BasicMessageMetadataContext.class,
                        false);
    }

    /** {@inheritDoc} */
    protected org.springframework.webflow.execution.Event doExecute(final HttpServletRequest httpRequest, final HttpServletResponse httpResponse,
            final RequestContext springRequestContext,
            final ProfileRequestContext<Object, Envelope> profileRequestContext) throws ProfileException {
    	
    	final MessageContext<Envelope> outboundMessageCtx = profileRequestContext.getOutboundMessageContext();
    	final Envelope envelope = outboundMessageCtx.getMessage();
        if (envelope == null) {
        	log.debug("Action {}: Outbound message in the context is null", getId());
        	return ActionSupport.buildEvent(this, EventIds.INVALID_MSG_CTX);
        }
        final Header header = envelope.getHeader();
        final BasicMessageMetadataContext messageSubcontext =
                messageMetadataContextLookupStrategy.apply(profileRequestContext.getInboundMessageContext());

        String requestId = StringSupport.trimOrNull(messageSubcontext.getMessageId());
        if (requestId == null) {
        	log.debug("Action {}: No message id found from the message metadata", getId());
        	SoapActionSupport.addRelatesToToHeader(this, header, "<no_id_found>");
        } else {
        	log.debug("Action {}: Outgoing message is related to {}", getId(), requestId);
        	SoapActionSupport.addRelatesToToHeader(this, header, requestId);
        }
        return ActionSupport.buildProceedEvent(this);
    }
}
