/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.profile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.glite.sts.ta.TokenGenerationContext;
import org.glite.sts.ta.x509.X509TokenGenerationContext;
import org.glite.sts.x509.dn.DNBuilder;
import org.glite.sts.x509.dn.DNBuildingException;
import org.opensaml.messaging.context.MessageContext;
import org.opensaml.messaging.context.navigate.ChildContextLookup;
import org.opensaml.soap.soap11.Envelope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.webflow.execution.RequestContext;

import com.google.common.base.Function;

import net.shibboleth.ext.spring.webflow.Event;
import net.shibboleth.ext.spring.webflow.Events;
import net.shibboleth.idp.attribute.AttributeContext;
import net.shibboleth.idp.profile.AbstractProfileAction;
import net.shibboleth.idp.profile.ActionSupport;
import net.shibboleth.idp.profile.EventIds;
import net.shibboleth.idp.profile.ProfileException;
import net.shibboleth.idp.profile.ProfileRequestContext;
import net.shibboleth.utilities.java.support.logic.Constraint;

/** Generates the certificate subject DN and attaches it to the {@link X509TokenGenerationContext}. */
@Events({
    @Event(id = EventIds.PROCEED_EVENT_ID),
    @Event(id = EventIds.INVALID_PROFILE_CTX),
    @Event(id = EventIds.INVALID_ATTRIBUTE_CTX)})
public class GenerateX509SubjectDn  extends AbstractProfileAction<Envelope, Envelope> {

	/** Class logger. */
	private final Logger log = LoggerFactory.getLogger(GenerateX509SubjectDn.class);
	
	/** Subject DN builder. */
	private DNBuilder dnBuilder;
	
    /**
     * Strategy used to locate the {@link TokenGenerationContext} associated with a given {@link MessageContext}.
     */
    private Function<MessageContext<?>, TokenGenerationContext> tokenGenerationContextLookupStrategy;

	/**
     * Strategy used to locate the {@link X509TokenGenerationContext} associated with a given {@link TokenGenerationContext}.
     */
    private Function<TokenGenerationContext, X509TokenGenerationContext> x509TokenGenerationContextLookupStrategy;
    
    /**
     * Strategy used to locate the {@link AttributeContext} associated with a given {@link ProfileRequestContext}.
     */
    private Function<ProfileRequestContext<?,?>, AttributeContext> attributeContextLookupStrategy;

    /** {@inheritDoc} */
    public GenerateX509SubjectDn() {
    	super();
        tokenGenerationContextLookupStrategy =
                new ChildContextLookup<MessageContext<?>, TokenGenerationContext>(TokenGenerationContext.class,
                        false);
        x509TokenGenerationContextLookupStrategy =
        		new ChildContextLookup<TokenGenerationContext, X509TokenGenerationContext>(X509TokenGenerationContext.class,
        				false);
        attributeContextLookupStrategy = new ChildContextLookup<ProfileRequestContext<?, ?>, AttributeContext>(AttributeContext.class, false);
    }
    
    /**
     * Constructor.
     * @param dnBuilder the builder for the subject DNs
     */
    public GenerateX509SubjectDn(DNBuilder dnBuilder) {
    	this();
    	this.dnBuilder = Constraint.isNotNull(dnBuilder, "The subject DN builder cannot be null!");
    }
    
    /** {@inheritDoc} */
    protected org.springframework.webflow.execution.Event doExecute(final HttpServletRequest httpRequest, final HttpServletResponse httpResponse,
            final RequestContext springRequestContext,
            final ProfileRequestContext<Envelope, Envelope> profileRequestContext) throws ProfileException {

    	final TokenGenerationContext tokenGenerationContext = tokenGenerationContextLookupStrategy.apply(profileRequestContext.getOutboundMessageContext());
        if (tokenGenerationContext == null) {
            log.debug("Action {}: Token generation context is null, unable to proceed", getId());
            return ActionSupport.buildEvent(this, EventIds.INVALID_PROFILE_CTX);
        }
    	final X509TokenGenerationContext x509TokenGenerationContext = x509TokenGenerationContextLookupStrategy.apply(tokenGenerationContext);
    	if (x509TokenGenerationContext == null) {
    		log.debug("Action {}: X.509 token generation context is null, unable to proceed", getId());
    		return ActionSupport.buildEvent(this, EventIds.INVALID_PROFILE_CTX);
    	}
    	AttributeContext attributeContext = attributeContextLookupStrategy.apply(profileRequestContext);
    	if (attributeContext == null) {
    		log.debug("Action {}: Attribute context is null, unable to proceed", getId());
    		return ActionSupport.buildEvent(this, EventIds.INVALID_PROFILE_CTX);
    	}
    	String subjectDn;
    	try {
			subjectDn = this.dnBuilder.createDN(attributeContext);
		} catch (DNBuildingException e) {
			log.debug("Could not build the subject DN for the upcoming certificate", e);
			throw new SubjectDnCreationException("Could not generate the subject DN from the set of available attributes!");
		}
    	x509TokenGenerationContext.setSubjectDN(subjectDn);
    	log.debug("Action {}: Using {} as the subject DN", getId(), subjectDn);
    	return ActionSupport.buildProceedEvent(this);
    }
    
    /** Exception thrown when the subject DN could not be constructed. */
    public static class SubjectDnCreationException extends ProfileException {

        /** Serial version UID. */
        private static final long serialVersionUID = -872917123123634555L;

        /**
         * Constructor.
         * 
         * @param message exception message
         */
        public SubjectDnCreationException(String message) {
            super(message);
        }
    }

}
