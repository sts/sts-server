/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.profile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.glite.sts.profile.config.WsTrustInteroperabilityProfileConfiguration;
import org.opensaml.messaging.context.MessageContext;
import org.opensaml.messaging.context.navigate.ChildContextLookup;
import org.opensaml.soap.soap11.Envelope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.webflow.execution.RequestContext;

import com.google.common.base.Function;

import net.shibboleth.ext.spring.webflow.Event;
import net.shibboleth.ext.spring.webflow.Events;
import net.shibboleth.idp.profile.AbstractProfileAction;
import net.shibboleth.idp.profile.ActionSupport;
import net.shibboleth.idp.profile.EventIds;
import net.shibboleth.idp.profile.ProfileException;
import net.shibboleth.idp.profile.ProfileRequestContext;

/** Checks whether the inbound SOAP envelope has a supported WS-Trust RequestType (ISSUE-operation). */
@Events({
    @Event(id = EventIds.PROCEED_EVENT_ID)})
public class CheckRequestType extends AbstractProfileAction<Envelope, Object> {
	
    /** Class logger. */
    private final Logger log = LoggerFactory.getLogger(CheckRequestType.class);
    
    /**
     * Strategy used to look up the {@link TokenRequestMessageContext} associated with the inbound message context.
     */
    private Function<MessageContext<Envelope>, TokenRequestMessageContext> tokenRequestMessageContextLookupStrategy;

    /** Constructor. */
    public CheckRequestType() {
    	super();
        tokenRequestMessageContextLookupStrategy =
                new ChildContextLookup<MessageContext<Envelope>, TokenRequestMessageContext>(TokenRequestMessageContext.class,
                        false);
    }
    
    /** {@inheritDoc} */
    protected org.springframework.webflow.execution.Event doExecute(final HttpServletRequest httpRequest, final HttpServletResponse httpResponse,
            final RequestContext springRequestContext,
            final ProfileRequestContext<Envelope, Object> profileRequestContext) throws ProfileException {
    	final MessageContext<Envelope> messageContext = profileRequestContext.getInboundMessageContext();
    	final TokenRequestMessageContext tokenRequestContext = tokenRequestMessageContextLookupStrategy.apply(messageContext);
    	String requestType = tokenRequestContext.getAction();
        if (!WsTrustInteroperabilityProfileConfiguration.ISSUE_OPERATION_ID.equals(requestType)) {
        	log.debug("Action {}: Ignoring an {} operation request.", getId(), requestType);
        	throw new UnSupportedWsTrustRequestTypeException(requestType);
        }
        return ActionSupport.buildProceedEvent(this);
    }
	
    /** Exception thrown when the incoming WS-Trust message had not supported request type. */
    public static class UnSupportedWsTrustRequestTypeException extends ProfileException {

        /** Serial version UID. */
		private static final long serialVersionUID = -7078373199423732050L;
		
        /**
         * Constructor.
         * 
         * @param message exception message
         */
        public UnSupportedWsTrustRequestTypeException(String wrongType) {
        	super("Request type '" + wrongType + "' is not supported!");
        }
    	
    }
    
}
