/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.profile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.shibboleth.ext.spring.webflow.Event;
import net.shibboleth.ext.spring.webflow.Events;
import net.shibboleth.idp.profile.AbstractProfileAction;
import net.shibboleth.idp.profile.ActionSupport;
import net.shibboleth.idp.profile.EventIds;
import net.shibboleth.idp.profile.ProfileException;
import net.shibboleth.idp.profile.ProfileRequestContext;

import org.glite.sts.authn.CheckAssertionAuthnStatement.FutureAuthnInstantInAssertionException;
import org.glite.sts.authn.CheckAssertionAuthnStatement.InvalidRemoteAddressInAssertionException;
import org.glite.sts.authn.CheckAssertionAuthnStatement.NoSingleAuthnStatementInAssertionException;
import org.glite.sts.authn.CheckAssertionAuthnStatement.PastAuthnInstantInAssertionException;
import org.glite.sts.authn.CheckAssertionConditions.ExpiredAssertionException;
import org.glite.sts.authn.CheckAssertionConditions.NoConditionsInAssertionException;
import org.glite.sts.authn.CheckAssertionConditions.NotInAssertionAudienceException;
import org.glite.sts.authn.CheckAssertionConditions.NotYetValidAssertionException;
import org.glite.sts.authn.CheckAssertionLifetime.FutureAssertionException;
import org.glite.sts.authn.CheckAssertionLifetime.NoIssueInstantException;
import org.glite.sts.authn.CheckAssertionLifetime.PastAssertionException;
import org.glite.sts.authn.CheckAssertionSignature.InvalidAssertionSignatureException;
import org.glite.sts.authn.CheckAssertionSubjectConfirmation.InvalidSubjectConfirmationException;
import org.glite.sts.authn.ExtractSamlAssertionFromWssToken.NonParsableSamlAssertionException;
import org.glite.sts.authn.ExtractUsernamePasswordFromWssToken.NonParsableUsernamePasswordException;
import org.glite.sts.authn.ValidateUsernamePasswordAgainstLdap.InvalidUsernamePasswordException;
import org.glite.sts.profile.CheckAction.UnSupportedActionException;
import org.glite.sts.profile.CheckClaims.UnSupportedIncomingTokenTypeException;
import org.glite.sts.profile.CheckFrameworkVersion.InvalidFrameworkVersionException;
import org.glite.sts.profile.CheckMessageLifetime.FutureMessageException;
import org.glite.sts.profile.CheckMessageLifetime.PastMessageException;
import org.glite.sts.profile.CheckRecipient.InvalidRecipientException;
import org.glite.sts.profile.CheckRequestType.UnSupportedWsTrustRequestTypeException;
import org.glite.sts.profile.CheckSignature.InvalidSignatureException;
import org.glite.sts.profile.CheckXmlSchema.SchemaInvalidXmlMessageException;
import org.glite.sts.profile.DecodeAttributesFromAssertion.UnSupportedAttributeValueTypeException;
import org.glite.sts.profile.GenerateX509SubjectDn.SubjectDnCreationException;
import org.glite.sts.profile.CheckMessageId.NoMessageIdFoundException;
import org.glite.sts.profile.InitializeTokenGenerationContext.UnSupportedOutgoingTokenTypeException;
import org.glite.sts.profile.IssueCertificate.CertificateIssuanceException;
import org.glite.sts.profile.IssueVomsProxyCertificate.ProxyCertificateInitializationException;
import org.glite.sts.profile.soap.xmlobject.Status;
import org.opensaml.soap.soap11.Envelope;
import org.opensaml.soap.soap11.FaultCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.webflow.execution.RequestContext;

/**
 * An action for initializing a {@link SoapFaultContext}, depending on the type of {@link ProfileException}
 * provided to the action. Internal server error is used if the exception is not recognized.
 */
@Events({
    @Event(id = EventIds.PROCEED_EVENT_ID)})
public class InitializeSoapFaultContext extends AbstractProfileAction<Envelope, Object> {

    /** Class logger. */
    private final Logger log = LoggerFactory.getLogger(InitializeSoapFaultContext.class);
    
    /**
     * Constructor.
     */
    public InitializeSoapFaultContext() {
    	super();
    }
	
    /** {@inheritDoc} */
    protected org.springframework.webflow.execution.Event doExecute(final HttpServletRequest httpRequest, final HttpServletResponse httpResponse,
            final RequestContext springRequestContext,
            final ProfileRequestContext<Envelope, Object> profileRequestContext) throws ProfileException {
    	return this.generateContext(springRequestContext, null);
    }
    
    /**
     * Generates a {@link SoapFaultContext} according to the caught exception and adds it to the
     * {@link ProfileRequestContext}.
     * 
     * @param springRequestContext the Spring request context
     * @param exception the exception stack causing the SOAP fault to be generated
     * @return proveed event
     */
    public org.springframework.webflow.execution.Event generateContext(final RequestContext springRequestContext, Exception exception) {
    	if (exception == null) {
    		log.debug("Action {}: No exception supplied, nothing to do", getId());
    		ActionSupport.buildProceedEvent(this);
    	}
    	log.debug("Action {}: Finding the root cause from the exception {}", getId(), exception.toString());
    	Throwable cause = getRootCause(exception);
    	log.debug("Action {}: The root cause is {}", getId(), cause.toString());
    	final ProfileRequestContext<?, ?> profileRequestContext = getProfileContextLookupStrategy().apply(springRequestContext);
    	SoapFaultContext faultContext = generateSoapFaultContext(cause);
  		profileRequestContext.addSubcontext(faultContext, true);
  		log.debug("Action {}: SOAP Fault context has been added, nothing else to do.", getId());
    	return ActionSupport.buildProceedEvent(this);
    }
    
    /**
     * Generates a {@link SoapFaultContext} according to the causing throwable.
     * 
     * @param cause the throwable causing the SOAP fault to be generated
     * @return the soap fault context
     */
    protected SoapFaultContext generateSoapFaultContext(Throwable cause) {
   		if (cause instanceof FutureMessageException) {
   			return new SoapFaultContext(FaultCode.CLIENT, "Message has been issued in the future, it cannot be accepted", Status.CODE_STALE_MSG);
   		} else if (cause instanceof PastMessageException) {
   			return new SoapFaultContext(FaultCode.CLIENT, "Message has expired, it cannot be accepted", Status.CODE_STALE_MSG);
   		} else if (cause instanceof org.glite.sts.profile.CheckMessageLifetime.NoIssueInstantException) {
   			return new SoapFaultContext(FaultCode.CLIENT, cause.getMessage(), Status.CODE_STALE_MSG);
  		} else if (cause instanceof InvalidFrameworkVersionException) {
   			return new SoapFaultContext(FaultCode.CLIENT, cause.getMessage());
  		} else if (cause instanceof UnSupportedActionException) {
  			return new SoapFaultContext(FaultCode.CLIENT, cause.getMessage());
  		} else if (cause instanceof UnSupportedWsTrustRequestTypeException) {
  			return new SoapFaultContext(FaultCode.CLIENT, cause.getMessage());
  		} else if (cause instanceof UnSupportedOutgoingTokenTypeException) {
  			return new SoapFaultContext(FaultCode.CLIENT, cause.getMessage());
   		} else if (cause instanceof UnSupportedIncomingTokenTypeException) {
   			return new SoapFaultContext(FaultCode.CLIENT, cause.getMessage());
   		} else if (cause instanceof FutureAssertionException) {
   	   		return new SoapFaultContext(FaultCode.CLIENT, cause.getMessage());
   	   	} else if (cause instanceof PastAssertionException) {
   	   		return new SoapFaultContext(FaultCode.CLIENT, cause.getMessage());
   	   	} else if (cause instanceof CertificateIssuanceException) {
   	   		return new SoapFaultContext(FaultCode.SERVER, "Could not obtain X.509 certificate from the online CA", "CAClient", cause.getMessage());
   	   	} else if (cause instanceof ProxyCertificateInitializationException) {
   	   		return new SoapFaultContext(FaultCode.SERVER, "Could not initialize the proxy", "VomsProxyInit", cause.getMessage());
   	   	} else if (cause instanceof SchemaInvalidXmlMessageException) {
   	   		return new SoapFaultContext(FaultCode.CLIENT, "Incoming message had invalid schema!", "Schema", cause.getMessage());
   	   	} else if (cause instanceof InvalidAssertionSignatureException) {
   	   		return new SoapFaultContext(FaultCode.CLIENT, cause.getMessage());
   	   	} else if (cause instanceof InvalidUsernamePasswordException) {
   	   		return new SoapFaultContext(FaultCode.CLIENT, cause.getMessage());
   	   	} else if (cause instanceof InvalidSubjectConfirmationException) {
   	   		return new SoapFaultContext(FaultCode.CLIENT, cause.getMessage());
   	   	} else if (cause instanceof SubjectDnCreationException) {
   	   		return new SoapFaultContext(FaultCode.SERVER, cause.getMessage());
   	   	} else if (cause instanceof NotInAssertionAudienceException) {
   	   		return new SoapFaultContext(FaultCode.CLIENT, cause.getMessage());
   	   	} else if (cause instanceof NotYetValidAssertionException) {
   	   		return new SoapFaultContext(FaultCode.CLIENT, cause.getMessage());
   	   	} else if (cause instanceof ExpiredAssertionException) {
   	   		return new SoapFaultContext(FaultCode.CLIENT, cause.getMessage());
   	   	} else if (cause instanceof NonParsableUsernamePasswordException) {
   	   		return new SoapFaultContext(FaultCode.CLIENT, cause.getMessage());
   	   	} else if (cause instanceof NonParsableSamlAssertionException) {
   	   		return new SoapFaultContext(FaultCode.CLIENT, cause.getMessage());
   	   	} else if (cause instanceof InvalidRecipientException) {
   	   		return new SoapFaultContext(FaultCode.CLIENT, cause.getMessage());
   	   	} else if (cause instanceof NoMessageIdFoundException) {
   	   		return new SoapFaultContext(FaultCode.CLIENT, cause.getMessage());
   	   	} else if (cause instanceof NoConditionsInAssertionException) {
   	   		return new SoapFaultContext(FaultCode.CLIENT, cause.getMessage());
   	   	} else if (cause instanceof NoIssueInstantException) {
   	   		return new SoapFaultContext(FaultCode.CLIENT, cause.getMessage());
   	   	} else if (cause instanceof InvalidRemoteAddressInAssertionException) {
   	   		return new SoapFaultContext(FaultCode.CLIENT, cause.getMessage());
   	   	} else if (cause instanceof FutureAuthnInstantInAssertionException) {
   	   		return new SoapFaultContext(FaultCode.CLIENT, cause.getMessage());
   	   	} else if (cause instanceof PastAuthnInstantInAssertionException) {
   	   		return new SoapFaultContext(FaultCode.CLIENT, cause.getMessage());
   	   	} else if (cause instanceof NoSingleAuthnStatementInAssertionException) {
   	   		return new SoapFaultContext(FaultCode.CLIENT, cause.getMessage());
   	   	} else if (cause instanceof InvalidSignatureException) {
   	   		return new SoapFaultContext(FaultCode.CLIENT, cause.getMessage());
   	   	} else if (cause instanceof UnSupportedAttributeValueTypeException) {
   	   		return new SoapFaultContext(FaultCode.CLIENT, cause.getMessage());
   	   	} else {
   			log.debug("Action {}: Exception is not identified", getId());
   			return new SoapFaultContext(FaultCode.SERVER, "Internal server error");
   		}
    }
    
    /**
     * Gets the root cause of the given exception.
     * 
     * @param exception the exception stack
     * @return the root throwable causing the exception
     */
    protected Throwable getRootCause(Exception exception) {
    	Throwable cause = exception;
    	do {
    		cause = cause.getCause();
    	} while (cause.getCause() != null);
    	return cause;
    }
}
