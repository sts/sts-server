/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.profile.xmlsec;

import javax.annotation.Nonnull;

import net.shibboleth.idp.profile.AbstractProfileAction;

import org.opensaml.core.xml.config.XMLObjectProviderRegistrySupport;
import org.opensaml.security.credential.Credential;
import org.opensaml.xmlsec.signature.KeyInfo;
import org.opensaml.xmlsec.signature.Signature;
import org.opensaml.xmlsec.signature.impl.KeyInfoBuilder;
import org.opensaml.xmlsec.signature.impl.SignatureBuilder;
import org.opensaml.xmlsec.signature.support.SignatureConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class contains helper methods for building XML-Security objects.
 */
public class XmlSecActionSupport {
	
    /**
     * Constructs a {@link Signature}.
     *
     * @param action the action
     * @param credential credential used for generating the signature
     * @return the signature that was constructed.
     */
    @Nonnull public static Signature buildSignatureObject(@Nonnull final AbstractProfileAction<?, ?> action, @Nonnull final Credential credential) {
    	SignatureBuilder builder = (SignatureBuilder) XMLObjectProviderRegistrySupport.getBuilderFactory().getBuilder(Signature.DEFAULT_ELEMENT_NAME);
    	Signature signature = builder.buildObject();
		signature.setSigningCredential(credential);
		//TODO: get the configuration from RelyingPartyContext
		signature.setSignatureAlgorithm(SignatureConstants.ALGO_ID_SIGNATURE_RSA_SHA1);
		signature.setCanonicalizationAlgorithm(SignatureConstants.ALGO_ID_C14N_EXCL_OMIT_COMMENTS);
		signature.setKeyInfo(buildKeyInfoObject(action));
		getLogger().debug("Action {}: Signature object built.", action.getId());
		return signature;
    }
    
    /**
     * Constructs a {@link KeyInfo}.
     * 
     * @param action the action
     * @return the key info that was constructed.
     */
    @Nonnull public static KeyInfo buildKeyInfoObject(@Nonnull final AbstractProfileAction<?, ?> action) {
    	KeyInfoBuilder builder = (KeyInfoBuilder) XMLObjectProviderRegistrySupport.getBuilderFactory().getBuilder(KeyInfo.DEFAULT_ELEMENT_NAME);
    	KeyInfo keyInfo = builder.buildObject();
		getLogger().debug("Action {}: KeyInfo object built.", action.getId());
    	return keyInfo;
    }
    
    /**
     * Gets the logger for this class.
     * 
     * @return logger for this class, never null
     */
    private static Logger getLogger() {
        return LoggerFactory.getLogger(XmlSecActionSupport.class);
    }
}
