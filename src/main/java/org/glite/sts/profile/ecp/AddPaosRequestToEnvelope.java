/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.profile.ecp;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.shibboleth.ext.spring.webflow.Event;
import net.shibboleth.ext.spring.webflow.Events;
import net.shibboleth.idp.profile.AbstractProfileAction;
import net.shibboleth.idp.profile.ActionSupport;
import net.shibboleth.idp.profile.EventIds;
import net.shibboleth.idp.profile.ProfileException;
import net.shibboleth.idp.profile.ProfileRequestContext;
import net.shibboleth.idp.relyingparty.RelyingPartyContext;

import org.glite.sts.profile.soap.SoapActionSupport;
import org.glite.sts.profile.soap.xmlobject.Request;
import org.opensaml.messaging.context.MessageContext;
import org.opensaml.messaging.context.navigate.ChildContextLookup;
import org.opensaml.saml.common.xml.SAMLConstants;
import org.opensaml.soap.soap11.Envelope;
import org.opensaml.soap.soap11.Header;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.webflow.execution.RequestContext;

import com.google.common.base.Function;

/**
 * Builds a {@link Request} and adds it to the {@link Envelope} header of the {@link ProfileRequestContext#getOutboundMessageContext()}. 
 */
@Events({
        @Event(id = EventIds.PROCEED_EVENT_ID),
        @Event(id = EventIds.INVALID_MSG_CTX)})
public class AddPaosRequestToEnvelope  extends AbstractProfileAction<Object, Envelope> {

    /** Class logger. */
    private Logger log = LoggerFactory.getLogger(AddPaosRequestToEnvelope.class);
    
    /**
     * Strategy used to locate the {@link RelyingPartyContext} associated with a given {@link ProfileRequestContext}.
     */
    private Function<ProfileRequestContext<?, ?>, RelyingPartyContext> relyingPartyContextLookupStrategy;
    
    /** Constructor. */
    public AddPaosRequestToEnvelope() {
    	super();
        relyingPartyContextLookupStrategy =
                new ChildContextLookup<ProfileRequestContext<?, ?>, RelyingPartyContext>(RelyingPartyContext.class,
                        false);
    }
    
    /** {@inheritDoc} */
    protected org.springframework.webflow.execution.Event doExecute(final HttpServletRequest httpRequest, final HttpServletResponse httpResponse,
            final RequestContext springRequestContext,
            final ProfileRequestContext<Object, Envelope> profileRequestContext) throws ProfileException {
    	
    	final MessageContext<Envelope> outboundMessageCtx = profileRequestContext.getOutboundMessageContext();
    	final Envelope envelope = outboundMessageCtx.getMessage();
        if (envelope == null) {
        	log.debug("Action {}: Outbound message in the context is null", getId());
        	return ActionSupport.buildEvent(this, EventIds.INVALID_MSG_CTX);
        }
        final RelyingPartyContext relyingPartyCtx = relyingPartyContextLookupStrategy.apply(profileRequestContext);
        final Header header = envelope.getHeader();
        final Request request = SoapActionSupport.addPaosRequestToHeader(this, header);
        request.setService(SAMLConstants.SAML20ECP_NS);
        request.setResponseConsumerURL("/soap");
        request.setSOAP11Actor(Request.SOAP11_ACTOR_NEXT);
        request.setMessageID(relyingPartyCtx.getProfileConfig().getSecurityConfiguration().getIdGenerator().generateIdentifier());
        request.setSOAP11MustUnderstand(Boolean.TRUE);
        log.debug("Action {}: All done, proceeding to the next state.", getId());
        return ActionSupport.buildProceedEvent(this);
    }
}
