/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.profile.ecp;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.shibboleth.ext.spring.webflow.Event;
import net.shibboleth.ext.spring.webflow.Events;
import net.shibboleth.idp.profile.AbstractProfileAction;
import net.shibboleth.idp.profile.ActionSupport;
import net.shibboleth.idp.profile.EventIds;
import net.shibboleth.idp.profile.ProfileException;
import net.shibboleth.idp.profile.ProfileRequestContext;

import org.glite.sts.profile.xmlsec.XmlSecActionSupport;
import org.opensaml.core.xml.config.XMLObjectProviderRegistrySupport;
import org.opensaml.core.xml.io.Marshaller;
import org.opensaml.core.xml.io.MarshallerFactory;
import org.opensaml.core.xml.io.MarshallingException;
import org.opensaml.messaging.context.MessageContext;
import org.opensaml.saml.saml2.core.AuthnRequest;
import org.opensaml.security.credential.Credential;
import org.opensaml.security.x509.BasicX509Credential;
import org.opensaml.soap.soap11.Body;
import org.opensaml.soap.soap11.Envelope;
import org.opensaml.xmlsec.signature.Signature;
import org.opensaml.xmlsec.signature.support.Signer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.webflow.execution.RequestContext;
import org.w3c.dom.Element;

import eu.emi.security.authn.x509.impl.CertificateUtils;
import eu.emi.security.authn.x509.impl.CertificateUtils.Encoding;

/**
 * Signs the {@link AuthnRequest} in the {@link Envelope} body of the {@link ProfileRequestContext#getOutboundMessageContext()}. 
 */
@Events({
    @Event(id = EventIds.PROCEED_EVENT_ID),
    @Event(id = EventIds.INVALID_MSG_CTX)})
public class SignAuthnRequestInEnvelope extends AbstractProfileAction<Object, Envelope> {
	
    /** Class logger. */
    private final Logger log = LoggerFactory.getLogger(SignAuthnRequestInEnvelope.class);
    
    /** Credential used for signing the {@link AuthnRequest}. */
    private Credential credential;
    
    /**
     * Constructor.
     * @param certificateFile the filename for the X.509 certificate
     * @param privateKeyFile the filename for the private key
     * @throws ProfileException if the certificate and/or the private key cannot be initialized
     */
    public SignAuthnRequestInEnvelope(String certificateFile, String privateKeyFile) throws ProfileException {
    	this(certificateFile, privateKeyFile, null);
    }

    /**
     * Constructor.
     * @param certificateFile the filename for the X.509 certificate
     * @param privateKeyFile the filename for the private key
     * @param password the password protecting the private key
     * @throws ProfileException if the certificate and/or the private key cannot be initialized
     */
    public SignAuthnRequestInEnvelope(String certificateFile, String privateKeyFile, String password) throws ProfileException {
    	super();
        try {
        	X509Certificate cert = CertificateUtils.loadCertificate(new FileInputStream(certificateFile), Encoding.PEM);
        	PrivateKey privKey;
        	if (password == null) {
        		privKey = CertificateUtils.loadPrivateKey(new FileInputStream(privateKeyFile), Encoding.PEM, null);
        	} else {
        		privKey = CertificateUtils.loadPrivateKey(new FileInputStream(privateKeyFile), Encoding.PEM, password.toCharArray());
        	}
        	this.credential = new BasicX509Credential(cert, privKey);
        } catch (FileNotFoundException fnfe) {
        	log.error("Action {}: Certificate and/or private key file not found, exiting.", getId());
        } catch (IOException ioe) {
        	log.error("Action {}: Could not load certificate and/or private key, exiting.", getId());
        }

    }
    
    /** {@inheritDoc} */
    protected org.springframework.webflow.execution.Event doExecute(final HttpServletRequest httpRequest, final HttpServletResponse httpResponse,
            final RequestContext springRequestContext,
            final ProfileRequestContext<Object, Envelope> profileRequestContext) throws ProfileException {
    	
    	final MessageContext<Envelope> outboundMessageCtx = profileRequestContext.getOutboundMessageContext();
    	final Envelope envelope = outboundMessageCtx.getMessage();
        if (envelope == null) {
        	log.debug("Action {}: Outbound message in the context is null", getId());
        	return ActionSupport.buildEvent(this, EventIds.INVALID_MSG_CTX);
        }
        final AuthnRequest authnRequest = getAuthnRequestFromBody(envelope.getBody());
        try {
        	Signature signature = XmlSecActionSupport.buildSignatureObject(this, this.credential);
        	authnRequest.setSignature(signature);
        	// authnRequest needs to be marshalled before signing.
        	marshall(authnRequest);
			Signer.signObject(signature);
        } catch (Exception e) {
        	log.error("Could not load cert/key or sign", e);
        	throw new ProfileException("Could not load cert/key or sign", e);
        }
        return ActionSupport.buildProceedEvent(this);
    }
    
    protected AuthnRequest getAuthnRequestFromBody(Body body) {
    	return (AuthnRequest) body.getUnknownXMLObjects(AuthnRequest.DEFAULT_ELEMENT_NAME).get(0);
    }
    
	protected Element marshall(AuthnRequest object) {
		MarshallerFactory MarshallerFactory = XMLObjectProviderRegistrySupport
				.getMarshallerFactory();
		Marshaller marshaller = MarshallerFactory
				.getMarshaller(object.getElementQName());
		Element element = null;
		try {
			element = marshaller.marshall(object);
		} catch (MarshallingException e) {
			log.debug("Could not marshall AuthnRequest element", e);
			return null;
		}
		return element;
	}	

}
