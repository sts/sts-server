/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.profile.ecp;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.shibboleth.ext.spring.webflow.Event;
import net.shibboleth.ext.spring.webflow.Events;
import net.shibboleth.idp.profile.AbstractProfileAction;
import net.shibboleth.idp.profile.ActionSupport;
import net.shibboleth.idp.profile.EventIds;
import net.shibboleth.idp.profile.ProfileException;
import net.shibboleth.idp.profile.ProfileRequestContext;

import org.apache.http.HttpHeaders;
import org.glite.sts.profile.config.EcpProfileConfiguration;
import org.opensaml.saml.common.xml.SAMLConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.webflow.execution.RequestContext;

/**
 * This action checks that the current request is from a SAML ECP client.
 */
@Events({
    @Event(id = EventIds.PROCEED_EVENT_ID),
    @Event(id = "error")})
public class CheckPaosHeaders extends AbstractProfileAction<Object, Object> {
	
    /** Class logger. */
    private final Logger log = LoggerFactory.getLogger(CheckPaosHeaders.class);
	
	/** Constructor. */
    public CheckPaosHeaders() {
		super();
	}

    /** {@inheritDoc} */
	protected org.springframework.webflow.execution.Event doExecute(HttpServletRequest httpRequest,
			HttpServletResponse httpResponse,
			RequestContext springRequestContext,
			ProfileRequestContext<Object, Object> profileRequestContext)
			throws ProfileException {
		String supportsService = "\"" + SAMLConstants.SAML20ECP_NS +"\"";
		String supportsPaosV_1_1 = "ver=\"" + SAMLConstants.PAOS_NS + "\"";

		String acceptHeader = httpRequest.getHeader(HttpHeaders.ACCEPT);
		log.debug("Action {}: accept header: {}", getId(), acceptHeader);
		if (acceptHeader == null || !acceptHeader.contains(EcpProfileConfiguration.PAOS_CONTENT_TYPE)) {
			log.debug("Action {}: headers didn't advertise acceptance of PAOS", getId());
			return ActionSupport.buildEvent(this, "error");
		}
		String paosHeader = httpRequest.getHeader(EcpProfileConfiguration.PAOS_HEADER);
		log.debug("Action {}: got PAOS-header {}", getId(), paosHeader);
		if (paosHeader == null || !paosHeader.contains(supportsPaosV_1_1) || !paosHeader.contains(supportsService)) {
			log.debug("Action {}: No supported profile found from the PAOS headers", getId());
			return ActionSupport.buildEvent(this, "error");
		}
		
		log.debug("Action {}: Request was from an ECP client, all done.", getId());
		return ActionSupport.buildProceedEvent(this);
	}
	
}
