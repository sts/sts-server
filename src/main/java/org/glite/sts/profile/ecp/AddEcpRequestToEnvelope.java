/*
f * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.profile.ecp;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.glite.sts.profile.saml.SamlActionSupport;
import org.glite.sts.saml.TrustedIdpMetadataService;
import org.opensaml.messaging.context.MessageContext;
import org.opensaml.messaging.context.navigate.ChildContextLookup;
import org.opensaml.saml.saml2.core.IDPList;
import org.opensaml.saml.saml2.ecp.Request;
import org.opensaml.soap.soap11.ActorBearing;
import org.opensaml.soap.soap11.Envelope;
import org.opensaml.soap.soap11.Header;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.webflow.execution.RequestContext;

import com.google.common.base.Function;

import net.shibboleth.ext.spring.webflow.Event;
import net.shibboleth.ext.spring.webflow.Events;
import net.shibboleth.idp.profile.AbstractProfileAction;
import net.shibboleth.idp.profile.ActionSupport;
import net.shibboleth.idp.profile.EventIds;
import net.shibboleth.idp.profile.ProfileException;
import net.shibboleth.idp.profile.ProfileRequestContext;
import net.shibboleth.idp.relyingparty.RelyingPartyContext;

/**
 * Builds a {@link Request} and adds it to the {@link Envelope} header of the {@link ProfileRequestContext#getOutboundMessageContext()}. 
 */
@Events({
    @Event(id = EventIds.PROCEED_EVENT_ID),
    @Event(id = EventIds.INVALID_MSG_CTX)})
public class AddEcpRequestToEnvelope extends AbstractProfileAction<Object, Envelope> {
	
    /** Class logger. */
    private final Logger log = LoggerFactory.getLogger(AddEcpRequestToEnvelope.class);
    
    /** The list of trusted Identity Providers. */
    private TrustedIdpMetadataService trustedIdpMetadataService;

    /**
     * Strategy used to locate the {@link RelyingPartyContext} associated with a given {@link ProfileRequestContext}.
     */
    private Function<ProfileRequestContext<?, ?>, RelyingPartyContext> relyingPartyContextLookupStrategy;
    
    /** Constructor. */
    public AddEcpRequestToEnvelope(TrustedIdpMetadataService trustedIdpMetadata) {
    	super();
    	this.trustedIdpMetadataService = trustedIdpMetadata;
        relyingPartyContextLookupStrategy =
                new ChildContextLookup<ProfileRequestContext<?, ?>, RelyingPartyContext>(RelyingPartyContext.class,
                        false);
    }
    
    /** {@inheritDoc} */
    protected org.springframework.webflow.execution.Event doExecute(final HttpServletRequest httpRequest, final HttpServletResponse httpResponse,
            final RequestContext springRequestContext,
            final ProfileRequestContext<Object, Envelope> profileRequestContext) throws ProfileException {
    	
    	final MessageContext<Envelope> outboundMessageCtx = profileRequestContext.getOutboundMessageContext();
    	final Envelope envelope = outboundMessageCtx.getMessage();
        if (envelope == null) {
        	log.debug("Action {}: Outbound message in the context is null", getId());
        	return ActionSupport.buildEvent(this, EventIds.INVALID_MSG_CTX);
        }
        final Header header = envelope.getHeader();

    	final RelyingPartyContext relyingPartyCtx = relyingPartyContextLookupStrategy.apply(profileRequestContext);
    	final Request request = SamlActionSupport.addEcpRequestToHeader(this, header);
    	IDPList idpList = this.trustedIdpMetadataService.getTrustedIdpList();
    	idpList.detach();
    	request.setIDPList(idpList);
    	request.setSOAP11Actor(ActorBearing.SOAP11_ACTOR_NEXT);
    	request.setSOAP11MustUnderstand(Boolean.TRUE);
    	request.setIssuer(SamlActionSupport.buildIssuer(this, relyingPartyCtx.getConfiguration().getResponderEntityId()));
    	return ActionSupport.buildProceedEvent(this);

    }

}
