/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.profile.ecp;

import java.util.ArrayList;
import java.util.Collection;

import net.shibboleth.ext.spring.webflow.Events;
import net.shibboleth.ext.spring.webflow.Event;
import net.shibboleth.idp.profile.ActionSupport;
import net.shibboleth.idp.profile.EventIds;
import net.shibboleth.idp.profile.ProfileRequestContext;
import net.shibboleth.idp.profile.config.ProfileConfiguration;
import net.shibboleth.idp.profile.config.SecurityConfiguration;
import net.shibboleth.idp.relyingparty.RelyingPartyConfiguration;
import net.shibboleth.idp.relyingparty.RelyingPartyContext;
import net.shibboleth.utilities.java.support.component.AbstractIdentifiableInitializableComponent;
import net.shibboleth.utilities.java.support.component.IdentifiableComponent;
import net.shibboleth.utilities.java.support.logic.Constraint;

import org.glite.sts.profile.config.EcpProfileConfiguration;
import org.opensaml.messaging.context.MessageContext;
import org.opensaml.soap.soap11.Envelope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.webflow.execution.Action;
import org.springframework.webflow.execution.RequestContext;

/**
 * This action simply constructs a new {@link ProfileRequestContext}, including
 * a {@link MessageContext<Envelope> as the outbound message context), and
 * attaches it to the Spring {@link RequestContext}.
 */
@Events({
	@Event(id = EventIds.PROCEED_EVENT_ID)})
public class InitializeProfileContext extends
		AbstractIdentifiableInitializableComponent implements Action,
		IdentifiableComponent {

	/** Class logger. */
	private final Logger log = LoggerFactory
			.getLogger(InitializeProfileContext.class);
	
	/** Relying party id. */
	private String relyingPartyId;
	
	/** Relying party configuration id. */
	private String relyingPartyConfigurationId;
	
	/** SAML SP entity id. */
	private String samlEntityId;

	/**
	 * Constructor.
	 * @param rpId the relying party id
	 * @param rpConfigId the relying party configuration id
	 * @param entityId the SAML SP entity id
	 */
	public InitializeProfileContext(String rpId, String rpConfigId, String entityId) {
		super();
		setId(getClass().getName());
		this.relyingPartyId = Constraint.isNotNull(rpId, "Relying party context id cannot be null!");
		this.relyingPartyConfigurationId = Constraint.isNotNull(rpConfigId, "Relying party configuration id cannot be null!");
		this.samlEntityId = Constraint.isNotNull(entityId, "Relying party entity id cannot be null!");
	}

	/** {@inheritDoc} */
	public org.springframework.webflow.execution.Event execute(
			final RequestContext context) throws Exception {
		final ProfileRequestContext<?, Envelope> profileContext = new ProfileRequestContext<Object, Envelope>();
		profileContext
				.setOutboundMessageContext(new MessageContext<Envelope>());
		final RelyingPartyContext rpCtx = new RelyingPartyContext(relyingPartyId);
		profileContext.addSubcontext(rpCtx);
		rpCtx.setProfileConfiguration(buildProfileConfiguration());
		rpCtx.setRelyingPartyConfiguration(new RelyingPartyConfiguration(this.relyingPartyConfigurationId, this.samlEntityId, buildProfileConfigurations()));

		context.getConversationScope().put(ProfileRequestContext.BINDING_KEY,
				profileContext);
		log.debug("Action {}: Profile context initialized and attached to Spring request context", getId());
		return ActionSupport.buildProceedEvent(this);
	}
	
    /**
     * Builds a {@link ProfileConfiguration} collection containing a {@link EcpProfileConfiguration}.
     * 
     * @return the constructed {@link ProfileConfiguration}
     */
    public static Collection<ProfileConfiguration> buildProfileConfigurations() {
        ArrayList<ProfileConfiguration> profileConfigs = new ArrayList<ProfileConfiguration>();
        profileConfigs.add(buildProfileConfiguration());
        return profileConfigs;
    }
    
    /**
     * Build a {@link EcpProfileConfiguration}.
     */
    public static EcpProfileConfiguration buildProfileConfiguration() {
        SecurityConfiguration securityConfig = new SecurityConfiguration();
        EcpProfileConfiguration ecpConfig = new EcpProfileConfiguration();
        ecpConfig.setSecurityConfiguration(securityConfig);
        return ecpConfig;
    }

}
