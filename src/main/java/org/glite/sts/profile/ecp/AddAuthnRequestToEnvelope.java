/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.profile.ecp;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.shibboleth.ext.spring.webflow.Event;
import net.shibboleth.ext.spring.webflow.Events;
import net.shibboleth.idp.profile.AbstractProfileAction;
import net.shibboleth.idp.profile.ActionSupport;
import net.shibboleth.idp.profile.EventIds;
import net.shibboleth.idp.profile.ProfileException;
import net.shibboleth.idp.profile.ProfileRequestContext;
import net.shibboleth.idp.relyingparty.RelyingPartyContext;
import net.shibboleth.utilities.java.support.logic.Constraint;

import org.glite.sts.profile.saml.SamlActionSupport;
import org.glite.sts.saml.idstore.AuthnRequestIdManager;
import org.joda.time.DateTime;
import org.opensaml.messaging.context.MessageContext;
import org.opensaml.messaging.context.navigate.ChildContextLookup;
import org.opensaml.saml.common.SAMLVersion;
import org.opensaml.saml.common.xml.SAMLConstants;
import org.opensaml.saml.saml2.core.AuthnRequest;
import org.opensaml.soap.soap11.Body;
import org.opensaml.soap.soap11.Envelope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.webflow.execution.RequestContext;

import com.google.common.base.Function;

/**
 * Builds a {@link AuthnRequest} and adds it to the {@link Envelope} body of the {@link ProfileRequestContext#getOutboundMessageContext()}. 
 */
@Events({
    @Event(id = EventIds.PROCEED_EVENT_ID),
    @Event(id = EventIds.INVALID_MSG_CTX)})
public class AddAuthnRequestToEnvelope extends AbstractProfileAction<Object, Envelope> {
	
    /** Class logger. */
    private final Logger log = LoggerFactory.getLogger(AddAuthnRequestToEnvelope.class);
    
    /**
     * Strategy used to locate the {@link RelyingPartyContext} associated with a given {@link ProfileRequestContext}.
     */
    private Function<ProfileRequestContext<?, ?>, RelyingPartyContext> relyingPartyContextLookupStrategy;
    
    /** The url to be added to the assertion consumer service. */
    private String consumerServiceUrl;
    
    /** The manager object for the authentication request id storage. */
    AuthnRequestIdManager authnRequestIdManager;
    
    /** Constructor. 
     * 
     * @param consumerServiceUrl the URL to the assertion consumer service
     * @param authnIdManager the manager class for the authentication request id storage
     */
    public AddAuthnRequestToEnvelope(String consumerServiceUrl, AuthnRequestIdManager authnIdManager) {
    	super();
        relyingPartyContextLookupStrategy =
                new ChildContextLookup<ProfileRequestContext<?, ?>, RelyingPartyContext>(RelyingPartyContext.class,
                        false);
        this.consumerServiceUrl = Constraint.isNotNull(consumerServiceUrl, "Assertion consumer service URL cannot be empty!");
        this.authnRequestIdManager = Constraint.isNotNull(authnIdManager, "Authentication request id manager cannot be null if response-to check is required!");
    }

    /** {@inheritDoc} */
    protected org.springframework.webflow.execution.Event doExecute(final HttpServletRequest httpRequest, final HttpServletResponse httpResponse,
            final RequestContext springRequestContext,
            final ProfileRequestContext<Object, Envelope> profileRequestContext) throws ProfileException {
    	
    	final MessageContext<Envelope> outboundMessageCtx = profileRequestContext.getOutboundMessageContext();
    	final Envelope envelope = outboundMessageCtx.getMessage();
        if (envelope == null) {
        	log.debug("Action {}: Outbound message in the context is null", getId());
        	return ActionSupport.buildEvent(this, EventIds.INVALID_MSG_CTX);
        }
        final RelyingPartyContext relyingPartyCtx = relyingPartyContextLookupStrategy.apply(profileRequestContext);
        final Body body = envelope.getBody();
        final AuthnRequest authnRequest = SamlActionSupport.addAuthnRequestToBody(this, body);
        
        String id = "_" + relyingPartyCtx.getProfileConfig().getSecurityConfiguration().getIdGenerator().generateIdentifier();
		authnRequest.setID(id);
		authnRequest.setVersion(SAMLVersion.VERSION_20);
		authnRequest.setIssuer(SamlActionSupport.buildIssuer(this, relyingPartyCtx.getConfiguration().getResponderEntityId()));
		authnRequest.setIssueInstant(new DateTime());
		authnRequest.setProtocolBinding(SAMLConstants.SAML2_PAOS_BINDING_URI);
		authnRequest.setAssertionConsumerServiceURL(this.consumerServiceUrl);
		this.authnRequestIdManager.storeAuthnRequestId(id);

        return ActionSupport.buildProceedEvent(this);
    }
}
