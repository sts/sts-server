/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.profile.config;

import org.opensaml.saml.common.xml.SAMLConstants;

import net.shibboleth.idp.profile.config.AbstractProfileConfiguration;

/**
 * The profile configuration for the ECP profile, including some variables needed in multiple actions.
 */
public class EcpProfileConfiguration extends AbstractProfileConfiguration {
	
    /** ID for this profile configuration. */
    public static final String PROFILE_ID = SAMLConstants.SAML20ECP_NS;
    
	/** The PAOS header identifier. */
    public static final String PAOS_HEADER = "PAOS";
	
	/** The content type for the PAOS content. */
    public static final String PAOS_CONTENT_TYPE = "application/vnd.paos+xml";

    /** Constructor. */
    public EcpProfileConfiguration() {
        this(PROFILE_ID);
    }
    
    /** Constructor. 
     * @param profileId unique ID for this profile
     * */
    public EcpProfileConfiguration(String profileId) {
		super(profileId);
	}
    
    /**
     * Gets the PAOS header identifier.
     * @return the PAOS header identifier.
     */
    public static String getPaosHeader() {
    	return PAOS_HEADER;
    }
    
    /**
     * Gets the content type for the PAOS content.
     * @return the content type for the PAOS content.
     */
    public static String getPaosContentType() {
    	return PAOS_CONTENT_TYPE;
    }

}
