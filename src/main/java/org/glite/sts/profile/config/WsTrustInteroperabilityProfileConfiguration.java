/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.profile.config;

import net.shibboleth.idp.profile.config.AbstractProfileConfiguration;

/**
 * A {@link ProfileConfiguration} for the WS-Trust 1.3 Interoperability Profile.
 */
public class WsTrustInteroperabilityProfileConfiguration extends AbstractProfileConfiguration {
	
    /** ID for this profile configuration. */
    public static final String PROFILE_ID = "urn:mace:switch.ch:doc:wst:profile:interop-200801";
    
    /** Framework version number. */
    public static final String FRAMEWORK_VERSION = "2.0";
    
    /** ISSUE operation identifier */
    public static final String ISSUE_OPERATION_ID = "http://docs.oasis-open.org/ws-sx/ws-trust/200512/RST/Issue";
    
    /** Username/Password token type identifier. */
    public static final String TOKEN_TYPE_USERNAME_ID = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0";

    /** X.509 token type identifier. */
    public static final String TOKEN_TYPE_X509_ID = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-x509-tokenprofile-1.0#X509v3";
    
    /** SAML token type identifier. */
    public static final String TOKEN_TYPE_SAML_ID = "http://docs.oasis-open.org/wss/oasis-wss-saml-token-profile-1.1#SAMLV2.0";
    
    /** X.509 proxy token type identifier. */
    public static final String TOKEN_TYPE_X509_PROXY_ID = "urn:glite.org:sts:GridProxy";

    /** Constructor. */
    public WsTrustInteroperabilityProfileConfiguration() {
        this(PROFILE_ID);
    }
    
    /** Constructor. 
     * @param profileId unique ID for this profile
     * */
    public WsTrustInteroperabilityProfileConfiguration(String profileId) {
		super(profileId);
	}
    
    /** Returns the framework version number used in the SOAP header.
     * @return the version number
     */
    public String getFrameworkVersion() {
        return FRAMEWORK_VERSION;
    }
    
    /** Returns the ISSUE operation identifier.
     * @return the issue operation identifier
     */
    public String getIssueOperationIdentifier() {
        return ISSUE_OPERATION_ID;
    }
    
}
