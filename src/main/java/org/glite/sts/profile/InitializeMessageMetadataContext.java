/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.profile;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.shibboleth.idp.profile.AbstractProfileAction;
import net.shibboleth.idp.profile.ActionSupport;
import net.shibboleth.idp.profile.ProfileException;
import net.shibboleth.idp.profile.ProfileRequestContext;

import org.opensaml.core.xml.XMLObject;
import org.opensaml.messaging.context.BasicMessageMetadataContext;
import org.opensaml.messaging.context.MessageContext;
import org.opensaml.soap.soap11.Envelope;
import org.opensaml.soap.soap11.Header;
import org.opensaml.soap.wsaddressing.MessageID;
import org.opensaml.soap.wssecurity.Created;
import org.opensaml.soap.wssecurity.Security;
import org.opensaml.soap.wssecurity.Timestamp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.webflow.execution.RequestContext;

/**
 * Initializes the {@link BasicMessageMetadataContext} and attaches it to the inbound {@link MessageContext}.
 */
public class InitializeMessageMetadataContext extends AbstractProfileAction<Envelope, Object> {
	
    /** Class logger. */
    private final Logger log = LoggerFactory.getLogger(InitializeMessageMetadataContext.class);
	
    /**
     * Constructor.
     */
    public InitializeMessageMetadataContext() {
    	super();
    }
    
    /** {@inheritDoc} */
    protected org.springframework.webflow.execution.Event doExecute(final HttpServletRequest httpRequest, final HttpServletResponse httpResponse,
            final RequestContext springRequestContext,
            final ProfileRequestContext<Envelope, Object> profileRequestContext) throws ProfileException {
    	final MessageContext<Envelope> messageContext = profileRequestContext.getInboundMessageContext();
    	final BasicMessageMetadataContext messageMetadata = new BasicMessageMetadataContext();
    	final Header header = messageContext.getMessage().getHeader();
        if (header == null) {
        	log.debug("Action {}: Could not find the Envelope header!", getId());
            throw new ProfileException("Header was not found from the envelope!");
        }
    	log.debug("Action {}: Looking for creation timestamp from the envelope header", getId());
    	long created = getCreatedFromHeader(header);
    	log.debug("Action {}: Looking for message ID from the envelope header", getId());
    	String messageID = getMessageIDFromHeader(header);
    	messageMetadata.setMessageIssueInstant(created);
    	messageMetadata.setMessageId(messageID);
    	messageContext.addSubcontext(messageMetadata, true);
    	return ActionSupport.buildProceedEvent(this);
    }
    
    protected long getCreatedFromHeader(final Header header) throws ProfileException {
       	List<XMLObject> objects = header.getUnknownXMLObjects(Security.ELEMENT_NAME);
       	if (objects.size() == 1) {
       		Security security = (Security) objects.get(0);
       		objects = security.getUnknownXMLObjects(Timestamp.ELEMENT_NAME);
       		if (objects.size() == 1) {
       			Timestamp timestamp = (Timestamp) objects.get(0);
       			Created created = timestamp.getCreated();
       			if (created != null) {
       				log.debug("Action {}: Found creation timestamp: {}", getId(), created.getDateTime().toString());
       				return created.getDateTime().getMillis();
       			}
       		}
       	}
        log.debug("Action {}: Could not find a timestamp from the envelope header", getId());
    	return 0;
    }
    
    protected String getMessageIDFromHeader(final Header header) throws ProfileException {
    	List<XMLObject> objects = header.getUnknownXMLObjects(MessageID.ELEMENT_NAME);
    	if (objects.size() == 1) {
    		MessageID messageId = (MessageID) objects.get(0);
    		log.debug("Action {}: Found message ID {}", getId(), messageId.getValue());
    		return messageId.getValue();
    	}
    	log.debug("Action {}: Could not find a message ID from the header", getId());
    	return null;
    }
    
}
