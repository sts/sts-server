/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.profile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.glite.sts.profile.soap.SoapActionSupport;
import org.opensaml.core.xml.config.XMLObjectProviderRegistrySupport;
import org.opensaml.messaging.context.MessageContext;
import org.opensaml.soap.common.SOAPObjectBuilder;
import org.opensaml.soap.soap11.Envelope;
import org.opensaml.soap.soap11.Header;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.webflow.execution.RequestContext;

import net.shibboleth.ext.spring.webflow.Event;
import net.shibboleth.ext.spring.webflow.Events;
import net.shibboleth.idp.profile.AbstractProfileAction;
import net.shibboleth.idp.profile.ActionSupport;
import net.shibboleth.idp.profile.EventIds;
import net.shibboleth.idp.profile.ProfileException;
import net.shibboleth.idp.profile.ProfileRequestContext;

/**
 * Builds an {@link Envelope} and adds it to the message of the {@link ProfileRequestContext#getOutboundMessageContext()}. 
 * The {@link Envelope} contains a {@link Header} and a {@link Body}.
 */
@Events({
        @Event(id = EventIds.PROCEED_EVENT_ID),
        @Event(id = EventIds.INVALID_PROFILE_CTX)})
public class AddEnvelopeShell extends AbstractProfileAction<Object, Envelope> {

    /** Class logger. */
    private Logger log = LoggerFactory.getLogger(AddEnvelopeShell.class);
    
    /** Constructor. */
    public AddEnvelopeShell() {
    	super();
    }
    
    /** {@inheritDoc} */
    protected org.springframework.webflow.execution.Event doExecute(final HttpServletRequest httpRequest, final HttpServletResponse httpResponse,
            final RequestContext springRequestContext,
            final ProfileRequestContext<Object, Envelope> profileRequestContext) throws ProfileException {
    	
        final MessageContext<Envelope> outboundMessageCtx = profileRequestContext.getOutboundMessageContext();
        if (outboundMessageCtx.getMessage() != null) {
        	log.error("Action {}: Outbound message context already contains an Envelope", getId());
        	return ActionSupport.buildEvent(this, EventIds.INVALID_PROFILE_CTX);
        }
        
        @SuppressWarnings("unchecked")
		final SOAPObjectBuilder<Envelope> envelopeBuilder =
            (SOAPObjectBuilder<Envelope>) XMLObjectProviderRegistrySupport.getBuilderFactory().getBuilder(Envelope.DEFAULT_ELEMENT_NAME);
        final Envelope envelope = envelopeBuilder.buildObject();

        SoapActionSupport.addHeadertoEnvelope(this, envelope);
        SoapActionSupport.addBodytoEnvelope(this, envelope);
        
        outboundMessageCtx.setMessage(envelope);
    
        log.debug("Action {}: All done, proceeding to the next state", getId());
        return ActionSupport.buildProceedEvent(this);
    }

}
