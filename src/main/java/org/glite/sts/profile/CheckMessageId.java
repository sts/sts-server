/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.profile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.opensaml.messaging.context.BasicMessageMetadataContext;
import org.opensaml.messaging.context.MessageContext;
import org.opensaml.messaging.context.navigate.ChildContextLookup;
import org.opensaml.soap.soap11.Envelope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.webflow.execution.RequestContext;

import com.google.common.base.Function;

import net.shibboleth.idp.profile.AbstractProfileAction;
import net.shibboleth.idp.profile.ActionSupport;
import net.shibboleth.idp.profile.ProfileException;
import net.shibboleth.idp.profile.ProfileRequestContext;

/**
 * Checks that the message identifier exists in the incoming message, if required.
 */
public class CheckMessageId extends AbstractProfileAction<Envelope, Object> {
	
    /** Class logger. */
    private final Logger log = LoggerFactory.getLogger(CheckMessageId.class);
	
    /** If set, the message id is required from the incoming messages. */
    private boolean requireId;
    
    /**
     * Strategy used to look up the {@link BasicMessageMetadataContext} associated with the inbound message context.
     */
    private Function<MessageContext<Envelope>, BasicMessageMetadataContext> messageMetadataContextLookupStrategy;
    
    /**
     * Constructor.
     * @param idRequired is message id required from the incoming messages
     */
    public CheckMessageId(boolean idRequired) {
    	super();
    	this.requireId = idRequired;
        messageMetadataContextLookupStrategy =
                new ChildContextLookup<MessageContext<Envelope>, BasicMessageMetadataContext>(BasicMessageMetadataContext.class,
                        false);

    }
    
    /** {@inheritDoc} */
    protected org.springframework.webflow.execution.Event doExecute(final HttpServletRequest httpRequest, final HttpServletResponse httpResponse,
            final RequestContext springRequestContext,
            final ProfileRequestContext<Envelope, Object> profileRequestContext) throws ProfileException {
    	log.debug("Action {}: Checking for the message ID in the incoming message", getId());
        final BasicMessageMetadataContext messageSubcontext =
                messageMetadataContextLookupStrategy.apply(profileRequestContext.getInboundMessageContext());
    	if (messageSubcontext.getMessageId() == null && this.requireId) {
    		log.debug("Action {}: Message ID is not found, even though it's required", getId());
    		throw new NoMessageIdFoundException("Message ID is not found, even though it's required");
    	}
    	log.debug("Action {}: Message ID was found, nothing left to do", getId());
    	return ActionSupport.buildProceedEvent(this);

    }
	
    /** 
     * Exception thrown when the incoming message did not have message ID
     * even though it's required.
     */
    public static class NoMessageIdFoundException extends ProfileException {

        /** Serial version UID. */
        private static final long serialVersionUID = -872917446121234555L;

        /**
         * Constructor.
         * 
         * @param message exception message
         */
        public NoMessageIdFoundException(String message) {
            super(message);
        }
    }

}
