/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.profile;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.shibboleth.ext.spring.webflow.Event;
import net.shibboleth.ext.spring.webflow.Events;
import net.shibboleth.idp.profile.AbstractProfileAction;
import net.shibboleth.idp.profile.ActionSupport;
import net.shibboleth.idp.profile.EventIds;
import net.shibboleth.idp.profile.ProfileException;
import net.shibboleth.idp.profile.ProfileRequestContext;

import org.opensaml.core.xml.XMLObject;
import org.opensaml.messaging.context.MessageContext;
import org.opensaml.soap.soap11.Body;
import org.opensaml.soap.soap11.Envelope;
import org.opensaml.soap.wssecurity.IdBearing;
import org.opensaml.soap.wssecurity.Reference;
import org.opensaml.soap.wssecurity.SecurityTokenReference;
import org.opensaml.soap.wstrust.Claims;
import org.opensaml.soap.wstrust.RequestSecurityToken;
import org.opensaml.soap.wstrust.RequestType;
import org.opensaml.soap.wstrust.TokenType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.webflow.execution.RequestContext;

/** Initializes the security token request message context and attaches it to the inbound message context. */
@Events({
    @Event(id = EventIds.PROCEED_EVENT_ID),
    @Event(id = EventIds.INVALID_MSG_CTX)})
public class InitializeTokenRequestMessageContext extends AbstractProfileAction<Envelope, Object> {
	
    /** Class logger. */
    private final Logger log = LoggerFactory.getLogger(InitializeTokenRequestMessageContext.class);
    
    /** {@inheritDoc} */
    protected org.springframework.webflow.execution.Event doExecute(final HttpServletRequest httpRequest, final HttpServletResponse httpResponse,
            final RequestContext springRequestContext,
            final ProfileRequestContext<Envelope, Object> profileRequestContext) throws ProfileException {
    	final MessageContext<Envelope> messageContext = profileRequestContext.getInboundMessageContext();
    	Envelope envelope = messageContext.getMessage();
    	Body body = envelope.getBody();
        if (body == null) {
        	log.error("Action {}: Body was not found from the envelope!", getId());
        	return ActionSupport.buildEvent(this, EventIds.INVALID_MSG_CTX);
        }
        
        RequestSecurityToken rst = getRequestSecurityTokenFromBody(body);
        TokenRequestMessageContext tokenRequestCtx = new TokenRequestMessageContext();
        
        tokenRequestCtx.setId(getIdFromRequestSecurityToken(rst));
        tokenRequestCtx.setContext(rst.getContext());
        tokenRequestCtx.setAction(getRequestTypeFromRequestSecurityToken(rst));
        tokenRequestCtx.setTokenType(getTokenTypeFromRequestSecurityToken(rst));
        Claims claims = getClaimsFromRequestSecurityToken(rst);
        tokenRequestCtx.setClaimsDialect(claims.getDialect());
        tokenRequestCtx.setTokenReference(getTokenReferenceUriFromClaims(claims));
        
    	messageContext.addSubcontext(tokenRequestCtx);
    	return ActionSupport.buildProceedEvent(this);
    }
    
	/**
	 * Gets the {@link RequestSecurityToken} from the given {@link Body}.
	 * @param body the envelope body
	 * @return the request security token element
	 * @throws ProfileException if single element was not found
	 */
    protected RequestSecurityToken getRequestSecurityTokenFromBody(Body body) throws ProfileException {
    	List<XMLObject> objects = body.getUnknownXMLObjects(RequestSecurityToken.ELEMENT_NAME);
    	if (objects.size() == 1) {
    		return (RequestSecurityToken) objects.get(0);
    	} else {
    		throw new ProfileException("No single RequestSecurityToken -element found from the envelope body!");
    	}
    }
    
    /**
     * Gets {@link IdBearing} from the given {@link RequestSecurityToken}.
     * @param rst the request security token element
     * @return id
     */
    protected String getIdFromRequestSecurityToken(RequestSecurityToken rst) {
    	return rst.getUnknownAttributes().get(IdBearing.WSU_ID_ATTR_NAME);
    }
    
    /**
     * Gets the {@link RequestType} from the given {@link RequestSecurityToken}.
     * @param rst the request security token element
     * @return request type
     * @throws ProfileException if no single value was found
     */
    protected String getRequestTypeFromRequestSecurityToken(RequestSecurityToken rst) throws ProfileException {
		List<XMLObject> objects = rst.getUnknownXMLObjects(RequestType.ELEMENT_NAME);
		if (objects.size() == 1) {
			RequestType requestType = (RequestType) objects.get(0);
			return requestType.getValue();
		} else {
			throw new ProfileException("No single RequestType element found from the RST!");
		}
    }
    
	/**
	 * Gets the {@link TokenType} from the given {@link RequestSecurityToken}.
	 * @param rst the request security token element
	 * @return token type
	 * @throws ProfileException if no single value was found
	 */
    protected String getTokenTypeFromRequestSecurityToken(RequestSecurityToken rst) throws ProfileException {
		List<XMLObject> objects = rst.getUnknownXMLObjects(TokenType.ELEMENT_NAME);
		if (objects.size() == 1) {
			TokenType tokenType = (TokenType) objects.get(0);
			return tokenType.getValue();
		} else {
			throw new ProfileException("No single TokenType element found from the RST!");
		}
    }
	
	/**
	 * Gets {@link Claims} from the given {@link RequestSecurityToken}.
	 * @param rst the request security token element
	 * @return claims
	 * @throws ProfileException if no single element was found
	 */
    protected Claims getClaimsFromRequestSecurityToken(RequestSecurityToken rst) throws ProfileException {
		List<XMLObject> objects = rst.getUnknownXMLObjects(Claims.ELEMENT_NAME);
		if (objects.size() == 1) {
			return (Claims) objects.get(0);
		} else {
			throw new ProfileException("No single Claims element found from the RST!");
		}		
	}
	
	/**
	 * Gets token reference URI value from the given {@link Claims}.
	 * @param claims the claims element
	 * @return the token reference URI
	 * @throws ProfileException if no single value was found
	 */
    protected String getTokenReferenceUriFromClaims(Claims claims) throws ProfileException {
		List<XMLObject> objects = claims.getUnknownXMLObjects(SecurityTokenReference.ELEMENT_NAME);
		if (objects.size() == 1) {
			SecurityTokenReference tokenReference = (SecurityTokenReference) objects.get(0);
			objects = tokenReference.getUnknownXMLObjects(Reference.ELEMENT_NAME);
			if (objects.size() == 1) {
				Reference reference = (Reference) objects.get(0);
				return reference.getURI();
			} else {
				throw new ProfileException("No single Reference element found from the SecurityTokenReference!");
			}
		} else {
			throw new ProfileException("No single SecurityTokenReference element found from the Claims!");
		}
	}
}
