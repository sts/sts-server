/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.config;

import org.ldaptive.pool.BlockingConnectionPool;
import org.ldaptive.pool.PoolException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import net.shibboleth.utilities.java.support.component.AbstractInitializableComponent;
import net.shibboleth.utilities.java.support.component.ComponentInitializationException;

/**
 * Verifies that the LDAP pool is properly configured.
 */
public class LdapConfigurationValidator extends AbstractInitializableComponent implements ApplicationContextAware {
	
	/** Class logger. */
    private final Logger log = LoggerFactory.getLogger(LdapConfigurationValidator.class);
	
	/** Is LDAP enabled, i.e. should the connection be verified. */
    private boolean ldapEnabled;
    
    /** The Spring application context. */
    private ApplicationContext springApplicationContext;
	
    /** 
     * Constructor. 
     * 
     * @param enableLdap is LDAP configuration enabled
     */
	public LdapConfigurationValidator(boolean enableLdap) {
		this.ldapEnabled = enableLdap;
	}
	
	/** {@inheritDoc} */
	public void doInitialize() throws ComponentInitializationException {
		if (this.ldapEnabled) {
			log.debug("LDAP is enabled, verifying the connection.");
			BlockingConnectionPool pool = this.springApplicationContext.getBean("stsLdapPool", BlockingConnectionPool.class);
			try {
				pool.getConnection();
			} catch (PoolException e) {
				log.error("Could not get a connection from the LDAP connection pool", e);
				throw new ComponentInitializationException("Could not get a connection from the LDAP connection pool", e);
			}
		} else {
			log.debug("LDAP is not enabled, ignoring the configuration verification.");
		}
	}

	/** {@inheritDoc} */
	public void setApplicationContext(ApplicationContext appContext)
			throws BeansException {
		this.springApplicationContext = appContext;
	}
}
