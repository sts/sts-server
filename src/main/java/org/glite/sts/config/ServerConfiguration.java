/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.config;

import java.io.File;
import java.io.IOException;

import net.shibboleth.utilities.java.support.primitive.StringSupport;

import org.glite.sts.STSException;
import org.ini4j.Ini;
import org.ini4j.InvalidFileFormatException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The configuration class for the STS service, using the INI file.
 */
public class ServerConfiguration {
	
    /** Class logger. */
    private final Logger log = LoggerFactory.getLogger(ServerConfiguration.class);
    
    /** The name of the configuration (INI) section for messages */
    public static final String CFG_SECTION_MESSAGES = "Messages";
    
    /** The name of the configuration (INI) section for CA client */
    public static final String CFG_SECTION_CACLIENT = "CAClient";

    /** The name of the configuration (INI) section for SAML. */
    public static final String CFG_SECTION_SAML = "SAML";
    
    /** The name of the configuration (INI) section for LDAP. */
    public static final String CFG_SECTION_LDAP = "LDAP";
    
    /** The name of the configuration (INI) section for VOMS. */
    public static final String CFG_SECTION_VOMS = "VOMS";    

    /** The INI configuration. */
    private Ini iniConfiguration;
    
    /**
     * Constructor.
     * @param iniFile the configuration file in INI format
     * @throws STSException if the configuration file cannot be read
     */
    public ServerConfiguration(String iniFile) throws STSException {
    	log.debug("Trying to construct a new configuration from the file {}", iniFile);
    	try {
			this.iniConfiguration = new Ini(new File(iniFile));
		} catch (InvalidFileFormatException e) {
			log.error("The configuration file {} has an invalid format!", iniFile);
			throw new STSException("The configuration file has an invalid format!", e);
		} catch (IOException e) {
			log.error("Could not read the configuration file {}!", iniFile);
			throw new STSException("Could not read the configuration file!", e);
		}
    	log.info("Successfully constructed a new configuration from the file {}", iniFile);
    }

    /**
     * Gets the configuration value for CAClient.
     * @param key the configuration key
     * @return the value corresponding to the key
     * @throws STSException if the configuration cannot be found
     */
    public String getCAClientConfigurationValue(String key) throws STSException {
    	return getStringConfigurationValue(CFG_SECTION_CACLIENT, key);
    }

    /**
     * Gets the configuration value for messages configuration.
     * @param key the configuration key
     * @return the value corresponding to the key
     * @throws STSException if the configuration cannot be found
     */
    public String getMessagesConfigurationValue(String key) throws STSException {
    	return getStringConfigurationValue(CFG_SECTION_MESSAGES, key);
    }
    
    /**
     * Gets the configuration value for SAML.
     * @param key the configuration key
     * @return the value corresponding to the key
     * @throws STSException if the configuration cannot be found
     */
    public String getSamlConfigurationValue(String key) throws STSException {
    	return getStringConfigurationValue(CFG_SECTION_SAML, key);
    }
    
    /**
     * Gets the configuration value for LDAP.
     * @param key the configuration key
     * @return the value corresponding to the key
     * @throws STSException if the configuration cannot be found
     */
    public String getLdapConfigurationValue(String key) throws STSException {
    	return getStringConfigurationValue(CFG_SECTION_LDAP, key);
    }

    /**
     * Gets the configuration value for VOMS.
     * @param key the configuration key
     * @return the value corresponding to the key
     * @throws STSException if the configuration cannot be found
     */
    public String getVomsConfigurationValue(String key) throws STSException {
    	return getStringConfigurationValue(CFG_SECTION_VOMS, key);
    }
    
    /**
     * Gets the configuration value as String.
     * @param sectionId the INI section to look after
     * @param key the configuration key
     * @return the value corresponding to the key
     * @throws STSException if the configuration cannot be found
     */
    public String getStringConfigurationValue(String sectionId, String key) throws STSException {
    	String stringValue = getConfigurationSection(sectionId).get(key);
    	return StringSupport.trimOrNull(stringValue);
    }

    /**
     * Gets the configuration value as integer.
     * @param sectionId the INI section to look after
     * @param key the configuration key
     * @return the value corresponding to the key
     * @throws STSException if the configuration cannot be found
     */
    public int getIntConfigurationValue(String sectionId, String key) throws STSException {
    	String stringValue = getStringConfigurationValue(sectionId, key);
    	if (stringValue == null) {
    		return -1;
    	}
    	Integer integer = new Integer(stringValue);
    	return integer.intValue();
    }
    
    /**
     * Gets the configuration value as long.
     * @param sectionId the INI section to look after
     * @param key the configuration key
     * @return the value corresponding to the key
     * @throws STSException if the configuration cannot be found
     */
    public long getLongConfigurationValue(String sectionId, String key) throws STSException {
    	String stringValue = getStringConfigurationValue(sectionId, key);
    	if (stringValue == null) {
    		return -1;
    	}
    	Long longInt = new Long(stringValue);
    	return longInt.longValue();
    }
    
    /**
     * Gets the configuration value as boolean.
     * @param sectionId the INI section to look after
     * @param key the configuration key
     * @return the value corresponding to the key
     * @throws STSException if the configuration cannot be found
     */
    public boolean getBoolConfigurationValue(String sectionId, String key) throws STSException {
    	String stringValue = getConfigurationSection(sectionId).get(key);
    	if (stringValue != null && stringValue.equalsIgnoreCase("true")) {
    		return true;
    	}
    	return false;
    }

    /**
     * Gets the configuration value as boolean and changes it to opposite.
     * @param sectionId the INI section to look after
     * @param key the configuration key
     * @return the opposite value corresponding to the key
     * @throws STSException if the configuration cannot be found
     */
    public boolean getOppositeBoolConfigurationValue(String sectionId, String key) throws STSException {
    	if (getBoolConfigurationValue(sectionId, key)) {
    		return false;
    	} else {
    		return true;
    	}
    }

    /**
     * Gets the configuration section corresponding to the given id.
     * @param sectionId the configuration section id
     * @return the configuration section
     * @throws STSException if the configuration section cannot be found
     */
    public Ini.Section getConfigurationSection(String sectionId) throws STSException {
    	Ini.Section iniSection = this.iniConfiguration.get(sectionId);
    	if (iniSection == null) {
    		throw new STSException("Could not find a section with id " + sectionId);
    	}
    	return iniSection;
    }
}
