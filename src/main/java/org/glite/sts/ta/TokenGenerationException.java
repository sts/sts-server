/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.ta;

/** Exception thrown if there is a problem with generating a security token. */
public class TokenGenerationException extends Exception {
	
    /** Serial version UID. */
    private static final long serialVersionUID = 8775123213213709410L;

    /** Constructor. */
    public TokenGenerationException() {
        super();
    }

    /**
     * Constructor.
     * 
     * @param message exception message
     */
    public TokenGenerationException(String message) {
        super(message);
    }

    /**
     * Constructor.
     * 
     * @param wrappedException exception to be wrapped by this one
     */
    public TokenGenerationException(Exception wrappedException) {
        super(wrappedException);
    }

    /**
     * Constructor.
     * 
     * @param message exception message
     * @param wrappedException exception to be wrapped by this one
     */
    public TokenGenerationException(String message, Exception wrappedException) {
        super(message, wrappedException);
    }


}
