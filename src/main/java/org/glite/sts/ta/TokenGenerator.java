/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.ta;

/**
 * This simple interface is implemented by the generators issuing a security token.
 */
public interface TokenGenerator {

	/**
	 * Issues a security token.
	 * @param ctx the context containing the data needed for issuing the security token
	 * @throws TokenGenerationException in case of problems during the generation
	 */
	public void issueToken(TokenGenerationContext ctx) throws TokenGenerationException;
	
}
