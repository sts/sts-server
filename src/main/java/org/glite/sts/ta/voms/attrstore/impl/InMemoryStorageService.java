package org.glite.sts.ta.voms.attrstore.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.rmi.RemoteException;
import java.util.Hashtable;
import java.util.Timer;
import java.util.TimerTask;

import javax.xml.rpc.ServiceException;

import net.shibboleth.utilities.java.support.primitive.StringSupport;

import org.apache.axis.AxisFault;
import org.apache.axis.configuration.SimpleProvider;
import org.glite.security.voms.User;
import org.glite.security.voms.VOMSException;
import org.glite.security.voms.service.admin.VOMSAdmin;
import org.glite.security.voms.service.admin.VOMSAdminServiceLocator;
import org.glite.security.voms.service.attributes.AttributeValue;
import org.glite.security.voms.service.attributes.VOMSAttributes;
import org.glite.security.voms.service.attributes.VOMSAttributesServiceLocator;
import org.glite.sts.STSException;
import org.glite.sts.config.ServerConfiguration;
import org.glite.sts.ta.voms.attrstore.StorageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.emi.security.authn.x509.CrlCheckingMode;
import eu.emi.security.authn.x509.NamespaceCheckingMode;
import eu.emi.security.authn.x509.OCSPCheckingMode;
import eu.emi.security.authn.x509.X509CertChainValidator;
import eu.emi.security.authn.x509.X509Credential;

public class InMemoryStorageService implements StorageService {

	/** Logging. */
	private final Logger log = LoggerFactory.getLogger(InMemoryStorageService.class);
	
	/** The storage table. */
	private Hashtable<String, User> linkedUsers;
	
    private VOMSAttributes vomsAttrService;
    
    private VOMSAdmin vomsAdminService;
    
    private String voName;
    
    private String matchAttributeName;
    
    private String cacheFilename;

    /**
     * Constructor.
     * @param credential
     * @param serverConfiguration
	 * @param timer the scheduler
	 * @param frequency the frequency for updating the links (in milliseconds).
     * @throws AxisFault
     * @throws STSException
     * @throws ServiceException
     */
	public InMemoryStorageService(X509Credential credential, ServerConfiguration serverConfiguration, X509CertChainValidator chainValidator, Timer timer, long frequency,
			NamespaceCheckingMode namespaceCheckingMode, CrlCheckingMode crlCheckingMode, OCSPCheckingMode ocspCheckingMode, String cacheFile) throws AxisFault, STSException, ServiceException, IOException {
		log.info("Building a new Storage Service with updating frequency of {}", frequency);
		
    	/*
		VomsEventListener vomsEventListener = new VomsEventListener();
    	String vomsDir = serverConfiguration.getVomsConfigurationValue("vomsDir");
    	long updateInterval = serverConfiguration.getLongConfigurationValue("VOMS", "updateInterval");
   		X509CertChainValidatorExt chainValidator = CertificateValidatorBuilder.buildCertificateValidator(vomsDir, 
   				vomsEventListener, vomsEventListener, updateInterval, namespaceCheckingMode, crlCheckingMode, ocspCheckingMode);
        */
        SimpleProvider provider = SSLConfigSender
                .getTransportProvider(credential, chainValidator);
        String baseUrl = serverConfiguration.getVomsConfigurationValue("baseUrl");
        log.debug("Using baseUrl={}", baseUrl);
        voName = serverConfiguration.getVomsConfigurationValue("voName");
        log.debug("Using voName={}", voName);
        matchAttributeName = serverConfiguration.getVomsConfigurationValue("matchAttributeName");
        log.debug("Using matchAttributeName={}", matchAttributeName);
        VOMSAttributesServiceLocator attrLocator = new VOMSAttributesServiceLocator(
                provider);
        attrLocator.setVOMSAttributesEndpointAddress(baseUrl + "/services/VOMSAttributes");
        this.vomsAttrService = attrLocator.getVOMSAttributes();
        VOMSAdminServiceLocator adminLocator = new VOMSAdminServiceLocator(provider);
        adminLocator.setVOMSAdminEndpointAddress(baseUrl + "/services/VOMSAdmin");
        this.vomsAdminService = adminLocator.getVOMSAdmin();
		cacheFilename = cacheFile;
		this.linkedUsers = initializeLinkedUsers();
		if (frequency > 0) {
			timer.schedule(new MemoryUpdateTask(), 0, frequency);
		} else {
			log.info("Frequency is set to 0, storage update is disabled");
		}
	}
	
	@Override
	/** {@inheritDoc} */
	public User getUser(String attribute) {
		return linkedUsers.get(attribute);
	}
	
	@SuppressWarnings("unchecked")
	protected Hashtable<String, User> initializeLinkedUsers() throws IOException {
		final File cache = new File(cacheFilename);
		if (cache.exists() && cache.canRead() && cache.canWrite()) {
			ObjectInputStream objectinputstream = new ObjectInputStream(new FileInputStream(cache));
			final Hashtable<String, User> cachedUsers;
			try {
				cachedUsers =(Hashtable<String, User>) objectinputstream.readObject();
				log.info("{} users successfully restored from cache {}", cachedUsers.size(), cacheFilename);
				return cachedUsers;
			} catch (ClassNotFoundException e) {
				log.error("Could not parse the cache from the file {}", cacheFilename, e);
			} finally {
				objectinputstream.close();
			}
		}
		log.info("Could not restore users from cache {}, initializing a new set", cacheFilename);
		return new Hashtable<String, User>();
	}
	
	protected void saveCache(final Hashtable<String, User> newCachedUsers) {
		final File cache = new File(cacheFilename);
		FileOutputStream fileOutStream;
		try {
			fileOutStream = new FileOutputStream(cache, false);
		} catch (FileNotFoundException e) {
			log.error("Could not open file {} for writing", cacheFilename, e);
			return;
		}
		try {
			ObjectOutputStream outStream = new ObjectOutputStream(fileOutStream);
			outStream.writeObject(newCachedUsers);
			outStream.close();
			fileOutStream.close();
			log.info("Cache successfully updated");
		} catch (IOException e) {
			log.error("Could not write the users to cache {}", cacheFilename);
		}
	}
	
	/**
	 * The task for cleaning the expired identifiers from the storage.
	 */
	class MemoryUpdateTask extends TimerTask {

		@Override
		public void run() {
			log.debug("Running the update tasks for the attribute links");
			final Hashtable<String, User> newLinkedUsers = new Hashtable<String, User>();
			log.debug("Getting the list of all users from VO {}", voName);
			User[] users;
			try {
				users = vomsAdminService.listMembers(voName);
			} catch (VOMSException e1) {
				log.error("Could not get the VOMS users, the list won't be updated", e1);
				return;
			} catch (RemoteException e1) {
				log.error("Could not get the VOMS users, the list won't be updated", e1);
				return;
			}
			log.debug("Building the new storage with {} users", users.length);
			for (int i = 0; i < users.length; i++) {
				try {
					addUserLink(users[i], newLinkedUsers);
				} catch (VOMSException e) {
					log.error("Could not get VOMS attributes for user " + users[i].getDN(), e);
				} catch (RemoteException e) {
					log.error("Could not get VOMS attributes for user " + users[i].getDN(), e);
				}
			}
			log.info("Updating the list of linked users (total users: {})", newLinkedUsers.size());
			saveCache(newLinkedUsers);
			linkedUsers = newLinkedUsers;
		}
		
		public void addUserLink(User user, final Hashtable<String, User> newLinkedUsers) throws VOMSException, RemoteException {
			log.trace("Attempting to add a link for user {}", user.getDN());
			AttributeValue[] attributeValues = vomsAttrService.listUserAttributes(user);
			if (attributeValues == null) {
				log.warn("No attribute values found for user {}", user.getDN());
				return;
			}
			for (int i = 0; i < attributeValues.length; i++) {
				String attributeName = attributeValues[i].getAttributeClass().getName();
				if (matchAttributeName.equals(attributeName)) {
					log.trace("Found matching attribute {}", matchAttributeName);
					String attributeValue = StringSupport.trimOrNull(attributeValues[i].getValue());
					if (attributeValue != null) {
						if (newLinkedUsers.containsKey(attributeValue)) {
							log.warn("User entry {} already exists for {}! The attribute is not added for {}!", 
									new Object[] { attributeValue, newLinkedUsers.get(attributeValue).getDN(), user.getDN() });
						} else {
							newLinkedUsers.put(attributeValue, user);
							log.trace("Added entry {} for {}", attributeValue, user.getDN());
						}
					} else {
						log.warn("Attribute value was null for user {}", user.getDN());
					}
				} else {
					log.trace("Ignoring attribute {}", attributeName);
				}
			}
		}
	}
}
