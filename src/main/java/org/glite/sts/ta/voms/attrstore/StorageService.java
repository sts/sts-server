package org.glite.sts.ta.voms.attrstore;

import org.glite.security.voms.User;

/**
 * Interface used for obtaining existing VOMS user with an attribute value.
 */
public interface StorageService {
	
	/**
	 * Get user via attribute value.
	 * @param attribute The desired attribute value.
	 * @return The corresponding VOMS user.
	 */
	public User getUser(String attribute);

}
