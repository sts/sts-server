package org.glite.sts.ta.voms;

import java.io.File;
import java.lang.Thread.UncaughtExceptionHandler;
import java.security.cert.X509Certificate;
import java.util.List;

import org.italiangrid.voms.ac.ACLookupListener;
import org.italiangrid.voms.ac.VOMSValidationResult;
import org.italiangrid.voms.ac.ValidationResultListener;
import org.italiangrid.voms.credential.LoadCredentialsEventListener;
import org.italiangrid.voms.request.VOMSACRequest;
import org.italiangrid.voms.request.VOMSErrorMessage;
import org.italiangrid.voms.request.VOMSProtocolListener;
import org.italiangrid.voms.request.VOMSRequestListener;
import org.italiangrid.voms.request.VOMSResponse;
import org.italiangrid.voms.request.VOMSServerInfo;
import org.italiangrid.voms.request.VOMSServerInfoStoreListener;
import org.italiangrid.voms.request.VOMSWarningMessage;
import org.italiangrid.voms.store.LSCInfo;
import org.italiangrid.voms.store.VOMSTrustStore;
import org.italiangrid.voms.store.VOMSTrustStoreStatusListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.emi.security.authn.x509.StoreUpdateListener;
import eu.emi.security.authn.x509.ValidationError;
import eu.emi.security.authn.x509.ValidationErrorListener;

/**
 * This class logs the events for various stages of the interaction with the VOMS service. 
 */
public class VomsEventListener implements ACLookupListener, ValidationResultListener, VOMSServerInfoStoreListener, LoadCredentialsEventListener, VOMSTrustStoreStatusListener,
	UncaughtExceptionHandler, VOMSRequestListener, VOMSProtocolListener, ValidationErrorListener, StoreUpdateListener {

	/** Logging */
    private static Logger log = LoggerFactory.getLogger(VomsEventListener.class);
    
    /** {@inheritDoc} */
    public void notifyVOMSRequestStart(VOMSACRequest request, VOMSServerInfo si) {
    	log.debug("VOMS request start: {}", si);
    }

    /** {@inheritDoc} */
    public void notifyVOMSRequestSuccess(VOMSACRequest request, VOMSServerInfo endpoint) {
    	log.debug("VOMS request was successful: {}", endpoint);
    }

    /** {@inheritDoc} */
    public void notifyVOMSRequestFailure(VOMSACRequest request, VOMSServerInfo endpoint, Throwable error) {
    	log.error("VOMS request failed: {}", endpoint, error);
    }

    /** {@inheritDoc} */
    public void notifyErrorsInVOMSReponse(VOMSACRequest request, VOMSServerInfo si, VOMSErrorMessage[] errors) {
    	log.error("The following {} errors were found: {}", errors.length, si);
    	for (int i = 0; i < errors.length; i++) {
    		log.error("Error {}: {}", i, errors[i].toString());
    	}
    }

    /** {@inheritDoc} */
    public void notifyWarningsInVOMSResponse(VOMSACRequest request, VOMSServerInfo si, VOMSWarningMessage[] warnings) {
    	log.warn("The following {} warnings were found: {}", warnings.length, si);
    	for (int i = 0; i < warnings.length; i++) {
    		log.warn("Error {}: {}", i, warnings[i].toString());
    	}
    }

    /** {@inheritDoc} */
    public void uncaughtException(Thread t, Throwable e) {
    	log.error("Uncaught exception found: {}", e);
    }

    /** {@inheritDoc} */
    public void notifyTrustStoreUpdate(VOMSTrustStore store) {
    	log.info("The VOMS trust store has been updated.");
    }

    /** {@inheritDoc} */
    public void notifyCertficateLookupEvent(String dir) {
    	log.debug("Looking up a certificate in directory {}", dir);
    }

    /** {@inheritDoc} */
    public void notifyLSCLookupEvent(String dir) {
    	log.debug("LSC lookup event occured in directory {}", dir);
    }

    /** {@inheritDoc} */
    public void notifyCertificateLoadEvent(X509Certificate cert, File f) {
    	log.info("Loaded a certificate {}", cert.toString());
    }

    /** {@inheritDoc} */
    public void notifyLSCLoadEvent(LSCInfo lsc, File f) {
    	log.info("LSC loaded for host {}", lsc.getHostname());
    }

    /** {@inheritDoc} */
    public void notifyCredentialLookup(String... locations) {
    	log.debug("Looking up for credentials {}", locations);
    }

    /** {@inheritDoc} */
    public void notifyLoadCredentialSuccess(String... locations) {
    	log.info("Successfully loaded credentials from {}", locations);
    }

    /** {@inheritDoc} */
    public void notifyLoadCredentialFailure(Throwable error, String... locations) {
    	log.error("Error while loading credentials from {}", locations, error);
    }

    /** {@inheritDoc} */
    public void notifyNoValidVOMSESError(List<String> searchedPaths) {
    	log.error("Error while loading vomses files from the following paths");
    	for (int i = 0; i < searchedPaths.size(); i++) {
    		log.error("Path {}: {}", i, searchedPaths.get(i));
    	}
    }

    /** {@inheritDoc} */
    public void notifyVOMSESlookup(String vomsesPath) {
    	log.debug("Looking up a vomses file from {}", vomsesPath);
    }

    /** {@inheritDoc} */
    public void notifyVOMSESInformationLoaded(String vomsesPath, VOMSServerInfo info) {
    	log.info("The vomses file {} loaded for {}", vomsesPath, info);
    }

    /** {@inheritDoc} */
    public void notifyValidationResult(VOMSValidationResult result) {
    	log.debug("VOMS validation result {}", result);
    }

    /** {@inheritDoc} */
    public void notifyACLookupEvent(X509Certificate[] chain, int chainLevel) {
    	log.debug("Looking up an AC for {}", chain[chainLevel].getSubjectDN());
    }

    /** {@inheritDoc} */
    public void notifyACParseEvent(X509Certificate[] chain, int chainLevel) {
    	log.debug("Parsing an AC for {}", chain[chainLevel].getSubjectDN());
    }

    /** {@inheritDoc} */
    public void notifyHTTPRequest(String url) {
    	log.debug("Sending an HTTP request to {}", url);
    }

    /** {@inheritDoc} */
    public void notifyLegacyRequest(String xmlLegacyRequest) {
    	log.debug("Sending a legacy request to {}", xmlLegacyRequest);
    }

    /** {@inheritDoc} */
    public void notifyReceivedResponse(VOMSResponse r) {
    	log.debug("Obtained a VOMS response");
    }

    /** {@inheritDoc} */
    public void loadingNotification(String location, String type,
			Severity level, Exception cause) {
		log.debug("Loading a file from {}", location);
	}

    /** {@inheritDoc} */
	public boolean onValidationError(ValidationError error) {
		log.error("Validation error {}, code {}, message {}", new Object[] { error.getErrorCategory(), error.getErrorCode(), error.getMessage() });
		return false;
	}
}