package org.glite.sts.ta.voms.attrstore;

import net.shibboleth.utilities.java.support.logic.Constraint;

import org.glite.security.voms.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AttributeToUserLinkManager {

	/** Logging. */
    private static final Logger log = LoggerFactory.getLogger(AttributeToUserLinkManager.class);
    
    /** The implementation for the {@link StorageService}. */
    private StorageService linkStorageService;
    
    /** The attribute id in the inbound message context whose value is used for linking the user. */
    private String incomingAttributeId;
    
    /**
     * Constructor.
     * @param storageService the {@link StorageService} implementation
     */
    public AttributeToUserLinkManager(StorageService storageService, String attributeId) {
    	log.info("Initializing VOMS attribute to user link manager");
    	this.linkStorageService = Constraint.isNotNull(storageService, "The storage service cannot be null!");
    	this.incomingAttributeId = Constraint.isNotNull(attributeId, "The attributeId cannot be empty!");
    }
    
    /**
     * Returns the user corresponding to the given attribute.
     * @param attribute The attribute identifying the user in {@link StorageService}.
     * @return The user object.
     */
    public User getUser(String attribute) {
    	return this.linkStorageService.getUser(attribute);
    }
    
    /**
     * Returns the attribute id in the inbound message context whose value is used for linking the user.
     * @return The attribute id
     */
    public String getIncomingAttributeId() {
    	return this.incomingAttributeId;
    }
}
