package org.glite.sts.ta.voms.attrstore.impl;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.security.GeneralSecurityException;

import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

import org.apache.axis.AxisFault;
import org.apache.axis.Handler;
import org.apache.axis.MessageContext;
import org.apache.axis.SimpleChain;
import org.apache.axis.SimpleTargetedChain;
import org.apache.axis.components.net.BooleanHolder;
import org.apache.axis.configuration.SimpleProvider;
import org.apache.axis.transport.http.HTTPSender;
import org.apache.axis.transport.http.HTTPTransport;
import org.apache.axis.transport.http.SocketHolder;

import eu.emi.security.authn.x509.X509CertChainValidator;
import eu.emi.security.authn.x509.X509Credential;
import eu.emi.security.authn.x509.impl.SocketFactoryCreator;

public class SSLConfigSender extends HTTPSender {
	
    /** Serial version uid. */
	private static final long serialVersionUID = 5618874867252751715L;

	/** The credential used for the client authentication. */
    private X509Credential credential;
    
    /** The validator used for the service authentication. */
    private X509CertChainValidator chainValidator;

	public SSLConfigSender(X509Credential credential, X509CertChainValidator validator) {
		this.credential = credential;
		this.chainValidator = validator;
	}
	
    public static SimpleProvider getTransportProvider(X509Credential credential, X509CertChainValidator validator) throws AxisFault {
            SimpleProvider transportProvider = new SimpleProvider();
            Handler sslHandler = new SSLConfigSender(credential, validator);
            Handler transport = new SimpleTargetedChain(new SimpleChain(), sslHandler, new SimpleChain());
            transportProvider.deployTransport(HTTPTransport.DEFAULT_TRANSPORT_NAME, transport);

            return transportProvider;
        }

    /**
     * The method Axis calls to get a socket.
     *
     * @param sockHolder The holder that holds the Socket
     * @param msgContext the message context for the messages.
     * @param protocol the protocol to use, for example 'http' or 'https'.
     * @param host The host to connect to.
     * @param port The port to connect to.
     * @param timeout The timeout in milliseconds for the connection.
     * @param otherHeaders possible other headers.
     * @param useFullURL whether to use full headers or not.
     *
     * @throws IOException in case the credential reading fails.
     * @throws GeneralSecurityException in case there is other problems witht he credentials.
     * @throws Exception thrown in case the protocol is http and there is problems creating the socket.
     */
    protected void getSocket(SocketHolder sockHolder, MessageContext msgContext, String protocol,
        String host, int port, int timeout, StringBuffer otherHeaders, BooleanHolder useFullURL)
        throws IOException, GeneralSecurityException, Exception {
//    	System.out.println("host = " + host + ", port = " + port + ", timeout = " + timeout);
        if (protocol.equalsIgnoreCase("https")) {
            final SSLSocketFactory fac = SocketFactoryCreator.getSocketFactory(credential, chainValidator);
            SSLSocket socket = (SSLSocket) fac.createSocket();
            String[] protocols = socket.getEnabledProtocols();
            for (int i = 0; i < protocols.length; i++)
            socket.setEnabledProtocols(socket.getEnabledProtocols());
            socket.setUseClientMode(true);
            // if timeout is given use it overriding the defaults and system settings and contextfactory properties
            if (timeout >= 0) {
                socket.connect(new InetSocketAddress(host, port), timeout);
                socket.setSoTimeout(timeout);
            } else {
                socket.connect(new InetSocketAddress(host, port));
            }
            sockHolder.setSocket(socket);
        } else {
            super.getSocket(sockHolder, msgContext, protocol, host, port, timeout, otherHeaders,
                useFullURL);
        }
    }
}