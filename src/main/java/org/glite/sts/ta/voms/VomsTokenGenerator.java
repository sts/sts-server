/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.ta.voms;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.security.cert.CertificateParsingException;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.bouncycastle.asn1.x509.AttributeCertificate;
import org.glite.sts.ta.TokenGenerationContext;
import org.glite.sts.ta.TokenGenerationException;
import org.glite.sts.ta.TokenGenerator;
import org.italiangrid.voms.request.VOMSACRequest;
import org.italiangrid.voms.request.VOMSACService;
import org.italiangrid.voms.request.impl.DefaultVOMSACRequest;
import org.italiangrid.voms.request.impl.DefaultVOMSACService;
import org.italiangrid.voms.util.CertificateValidatorBuilder;
import org.opensaml.messaging.context.navigate.ChildContextLookup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Function;

import eu.emi.security.authn.x509.CrlCheckingMode;
import eu.emi.security.authn.x509.NamespaceCheckingMode;
import eu.emi.security.authn.x509.OCSPCheckingMode;
import eu.emi.security.authn.x509.X509CertChainValidatorExt;
import eu.emi.security.authn.x509.proxy.ProxyCertificateOptions;
import eu.emi.security.authn.x509.proxy.ProxyGenerator;

/** 
 * The VOMS token generator implementation, using VOMS Java API.
 */
public class VomsTokenGenerator implements TokenGenerator {
	
	/** Logging */
    private static Logger log = LoggerFactory.getLogger(VomsTokenGenerator.class);
    
    /**
     * Strategy used to locate the {@link VomsTokenGenerationContext} associated with a given {@link TokenGenerationContext}.
     */
    private Function<TokenGenerationContext, VomsTokenGenerationContext> vomsTokenGenerationContextLookupStrategy;

    /** The VOMS Attribute Certificate service. */
    private VOMSACService vomsService;
    
    /** The proxy path length limit. */
    private int proxyPathLen;
    
    /** Constructor.
     * 
     * @param vomsesPath the directory containing vomses files
     * @param vomsDir the trust store directory
     * @param updateInterval the interval for refreshing the trust store
     * @param namespaceCheckingMode the checking mode for namespaces
     * @param crlCheckingMode the checking mode for CRLs
     * @param ocspCheckingMode the checking mode for OCSP
     */
    public VomsTokenGenerator(String vomsesPath, String vomsDir, long updateInterval, NamespaceCheckingMode namespaceCheckingMode, CrlCheckingMode crlCheckingMode, OCSPCheckingMode ocspCheckingMode) {
    	this(vomsesPath, vomsDir, updateInterval, namespaceCheckingMode, crlCheckingMode, ocspCheckingMode, -1);
    }

    /** Constructor.
     * 
     * @param vomsesPath the directory containing vomses files
     * @param vomsDir the trust store directory
     * @param updateInterval the interval for refreshing the trust store
     * @param namespaceCheckingMode the checking mode for namespaces
     * @param crlCheckingMode the checking mode for CRLs
     * @param ocspCheckingMode the checking mode for OCSP
     * @param pathLen the proxy path length limit
     */
    public VomsTokenGenerator(String vomsesPath, String vomsDir, long updateInterval, NamespaceCheckingMode namespaceCheckingMode, CrlCheckingMode crlCheckingMode, OCSPCheckingMode ocspCheckingMode, int pathLen) {
    	log.info("Constructing a new VomsTokenGenerator with vomsesPath={} and vomsDir={}", vomsesPath, vomsDir);
    	vomsTokenGenerationContextLookupStrategy =
        		new ChildContextLookup<TokenGenerationContext, VomsTokenGenerationContext>(VomsTokenGenerationContext.class,
        				false);
    	VomsEventListener vomsEventListener = new VomsEventListener();   	
   		X509CertChainValidatorExt validator = CertificateValidatorBuilder.buildCertificateValidator(vomsDir, vomsEventListener, vomsEventListener, updateInterval, namespaceCheckingMode, crlCheckingMode, ocspCheckingMode);
   		DefaultVOMSACService.Builder builder = new DefaultVOMSACService.Builder(validator);
   		List<String> vomses = new Vector<String>();
   		vomses.add(vomsesPath);
   		builder.vomsesLocations(vomses);
   		builder.requestListener(vomsEventListener);
   		vomsService = builder.build();
   		log.info("The maximum path length for the proxies is {}", pathLen);
   		proxyPathLen = pathLen;
    }
    
    /** {@inheritDoc} */
    public void issueToken(final TokenGenerationContext ctx) throws TokenGenerationException {
    	final VomsTokenGenerationContext vomsContext = vomsTokenGenerationContextLookupStrategy.apply(ctx);
    	Map<String, List<String>> fqans = vomsContext.getFqans();
    	if (fqans.size() > 1) {
	    	log.debug("Invalid size of the VO names in the request: {}", fqans.size());
	    	throw new TokenGenerationException("STS only supports at most one VO in the request!");    		
    	}
    	AttributeCertificate attributeCertificate = null;
    	if (fqans.size() == 1) {
    		String voName = fqans.entrySet().iterator().next().getKey();
    		VOMSACRequest request = new DefaultVOMSACRequest.Builder(voName).lifetime(vomsContext.getLifetime()).fqans(fqans.get(voName)).build();
    		attributeCertificate = vomsService.getVOMSAttributeCertificate(vomsContext.getIssuerCredentials(), request);
    		log.debug("Obtained attribute certificate {}", attributeCertificate);
    		if (attributeCertificate == null) {
    			throw new TokenGenerationException("Could not obtain an attribute certificate from the VOMS service");
    		}
    	}
    	ProxyCertificateOptions proxyOptions = new ProxyCertificateOptions(vomsContext.getIssuerCredentials().getCertificateChain());
    	proxyOptions.setPublicKey(vomsContext.getProxyKeys().getPublicKey());
    	proxyOptions.setLifetime(vomsContext.getLifetime());
    	proxyOptions.setProxyPathLimit(proxyPathLen);
    	if (attributeCertificate != null) {
    		proxyOptions.setAttributeCertificates(new AttributeCertificate[] { attributeCertificate });
    	}
    	try {
			vomsContext.setProxyCredentials(ProxyGenerator.generate(proxyOptions, vomsContext.getIssuerCredentials().getKey()));
		} catch (InvalidKeyException e) {
	    	log.debug("Error during the VOMS proxy initialization", e);
	    	throw new TokenGenerationException(e);
		} catch (CertificateParsingException e) {
	    	log.debug("Error during the VOMS proxy initialization", e);
	    	throw new TokenGenerationException(e);
		} catch (SignatureException e) {
	    	log.debug("Error during the VOMS proxy initialization", e);
	    	throw new TokenGenerationException(e);
		} catch (NoSuchAlgorithmException e) {
	    	log.debug("Error during the VOMS proxy initialization", e);
	    	throw new TokenGenerationException(e);
		}
    }

}
