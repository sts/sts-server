/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.ta.voms;

import java.util.List;
import java.util.Map;

import org.glite.sts.x509.CertificateKeys;
import org.glite.sts.ta.TokenGenerationContext;
import org.opensaml.messaging.context.BaseContext;

import eu.emi.security.authn.x509.X509Credential;
import eu.emi.security.authn.x509.proxy.ProxyCertificate;

/** A context intended to be used as a subcontext of a  {@link TokenGenerationContext} that carries 
 * data about the VOMS proxy security token. */
public class VomsTokenGenerationContext extends BaseContext {
	
	/** Mandatory credentials used for issuing the proxy. */
	private X509Credential issuerCredentials;
	
	/** Mandatory key-pair to be used for the proxy, public key cannot be null. */
	private CertificateKeys proxyKeys;
	
	/** The credentials for the initialized proxy. */
	private ProxyCertificate proxyCredentials;
	
	/** The list of FQANs. */
	private Map<String, List<String>> fqans;
	
    /** Optional lifetime of the proxy, default is 12 * 60 * 60. */
    private int lifetime;
    
    /** Optional password for the private key, default: NONE. */
    private String keyPassword;
    
    /** Optional proxy type identifier, default is 1. */
    private int proxyType;
    
    /** Optional delegation type identifier, default is 1. */
    private int delegationType;
    
    /** Optional policy type of the proxy. Only significant with proxyType >= GT3_PROXY. */
    private String policyType;
    
    /** Optional user's VOMS attribute reference. */
    private String attributeReference;
    
	/**
	 * Constructor.
	 */
    public VomsTokenGenerationContext() {
		super();
		this.setProxyType(1);
		this.setDelegationType(1);
		this.setLifetime(12 * 60 * 60);
		this.setKeyPassword(null);
	}

	/**
	 * Gets the credentials used for issuing the proxy.
	 * @return the issuerCredentials
	 */
	public X509Credential getIssuerCredentials() {
		return issuerCredentials;
	}

	/**
	 * Sets the credentials used for issuing the proxy.
	 * @param issuerCredentials the issuerCredentials to set
	 */
	public void setIssuerCredentials(X509Credential issuerCredentials) {
		this.issuerCredentials = issuerCredentials;
	}

	/**
	 * Gets the key-pair used for the proxy.
	 * @return the proxyKeys
	 */
	public CertificateKeys getProxyKeys() {
		return proxyKeys;
	}

	/**
	 * Sets the key-pair used for the proxy.
	 * @param proxyKeys the proxyKeys to set
	 */
	public void setProxyKeys(CertificateKeys proxyKeys) {
		this.proxyKeys = proxyKeys;
	}

	/**
	 * Gets the credentials of the proxy.
	 * @return the proxyCredentials
	 */
	public ProxyCertificate getProxyCredentials() {
		return proxyCredentials;
	}

	/**
	 * Sets the credentials of the proxy.
	 * @param proxyCredentials the proxyCredentials to set
	 */
	public void setProxyCredentials(ProxyCertificate proxyCredentials) {
		this.proxyCredentials = proxyCredentials;
	}

	/**
	 * Gets the list of FQANs used for initializing the proxy.
	 * @return the fqans
	 */
	public Map<String, List<String>> getFqans() {
		return fqans;
	}

	/**
	 * Sets the list of FQANs used for initializing the proxy.
	 * @param fqans the fqans to set
	 */
	public void setFqans(Map<String, List<String>> fqans) {
		this.fqans = fqans;
	}

	/**
	 * Gets the lifetime of the proxy.
	 * @return the lifetime
	 */
	public int getLifetime() {
		return lifetime;
	}

	/**
	 * Sets the lifetime of the proxy.
	 * @param lifetime the lifetime to set
	 */
	public void setLifetime(int lifetime) {
		this.lifetime = lifetime;
	}

	/**
	 * Gets the password protecting the private key of the proxy.
	 * @return the keyPassword
	 */
	public String getKeyPassword() {
		return keyPassword;
	}

	/**
	 * Sets the password protecting the private key of the proxy.
	 * @param keyPassword the keyPassword to set
	 */
	public void setKeyPassword(String keyPassword) {
		this.keyPassword = keyPassword;
	}

	/**
	 * Gets the proxy type of the proxy.
	 * @return the proxyType
	 */
	public int getProxyType() {
		return proxyType;
	}

	/**
	 * Sets the proxy type of the proxy.
	 * @param proxyType the proxyType to set
	 */
	public void setProxyType(int proxyType) {
		this.proxyType = proxyType;
	}

	/**
	 * Gets the delegation type of the proxy.
	 * @return the delegationType
	 */
	public int getDelegationType() {
		return delegationType;
	}

	/**
	 * Sets the delegation type of the proxy.
	 * @param delegationType the delegationType to set
	 */
	public void setDelegationType(int delegationType) {
		this.delegationType = delegationType;
	}

	/**
	 * Gets the policy type of the proxy.
	 * @return the policyType
	 */
	public String getPolicyType() {
		return policyType;
	}

	/**
	 * Sets the policy type for the proxy.
	 * @param policyType the policyType to set
	 */
	public void setPolicyType(String policyType) {
		this.policyType = policyType;
	}
	
	/**
	 * Gets the user's attribute reference.
	 * @return the attributeReference
	 */
	public String getAttributeReference() {
		return attributeReference;
	}
	
	/**
	 * Sets the user's attribute reference.
	 * @param reference the reference attribute value
	 */
	public void setAttributeReference(String reference) {
		this.attributeReference = reference;
	}

}
