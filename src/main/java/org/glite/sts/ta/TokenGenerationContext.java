/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.ta;

import org.opensaml.messaging.context.BaseContext;
import org.opensaml.messaging.context.MessageContext;

/** A {@link Context} intended to be used as a subcontext of a  {@link MessageContext}  that carries 
 * some basic data about the security token to be issued. */
public class TokenGenerationContext extends BaseContext  {
	
	/** The token type desired to be generated. */
	private String tokenType;

	/** {@inheritDoc} */
	public TokenGenerationContext() {
		super();
	}
	
	/** Gets the token type desired to be generated.
	 * 
	 * @return the desired token type
	 */
	public String getTokenType() {
		return tokenType;
	}

	/** Sets the token type desired to be generated.
	 * 
	 * @param tokenType the desired token type
	 */
	public void setTokenType(String tokenType) {
		this.tokenType = tokenType;
	}

}
