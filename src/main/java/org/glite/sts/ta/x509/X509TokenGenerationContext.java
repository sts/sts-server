/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.ta.x509;

import java.util.Set;

import net.shibboleth.utilities.java.support.collection.LazySet;

import org.glite.sts.x509.CertificateExtension;
import org.glite.sts.x509.CertificateKeys;
import org.glite.sts.x509.caclient.CARequest;
import org.glite.sts.x509.caclient.CAResponse;

import org.opensaml.messaging.context.BaseContext;

/** A {@link Context} intended to be used as a subcontext of a  {@link TokenGenerationContext} that carries 
 * data about the X.509 security token. */
public class X509TokenGenerationContext extends BaseContext {
	
	/** Set of certificate extension related to this certificate. */
	private Set<CertificateExtension> certExtensions;
	
	/** The key-pair used for the certificate. */
	private CertificateKeys certKeys;
	
	/** The subject DN of the certificate. */
	private String subjectDN;
	
	/** The request sent to the online CA. */
	private CARequest caRequest;
	
	/** The response obtained from the online CA. */
	private CAResponse caResponse;
	
	/**
	 * Constructor.
	 */
	public X509TokenGenerationContext() {
		super();
		this.certExtensions = new LazySet<CertificateExtension>();
		this.certKeys = null;
		this.subjectDN = null;
	}
	
	/**
	 * Gets the response from the online CA.
	 * @return the response from the online CA.
	 */
	public CAResponse getCaResponse() {
		return caResponse;
	}

	/**
	 * Sets the response from the online CA.
	 * @param caResponse the response from the online CA.
	 */
	public void setCaResponse(CAResponse caResponse) {
		this.caResponse = caResponse;
	}

	/**
	 * Gets the request to the online CA.
	 * @return the request to the online CA.
	 */
	public CARequest getCaRequest() {
		return caRequest;
	}

	/**
	 * Sets the request to the online CA.
	 * @param caRequest the request to the online CA.
	 */
	public void setCaRequest(CARequest caRequest) {
		this.caRequest = caRequest;
	}

	/**
	 * Gets the subject DN of the certificate.
	 * @return the subject DN of the certificate.
	 */
	public String getSubjectDN() {
		return subjectDN;
	}

	/**
	 * Sets the subject DN of the certificate.
	 * @param subjectDN the subject DN of the certificate.
	 */
	public void setSubjectDN(String subjectDN) {
		this.subjectDN = subjectDN;
	}

	/**
	 * Gets the key-pair related to the certificate.
	 * @return the key-pair related to the certificate.
	 */
	public CertificateKeys getCertKeys() {
		return certKeys;
	}

	/**
	 * Sets the key-pair related to the certificate.
	 * @param certKeys the key-pair related to the certificate.
	 */
	public void setCertKeys(CertificateKeys certKeys) {
		this.certKeys = certKeys;
	}

	/**
	 * Gets the certificate extensions related to the certificate.
	 * @return the certificate extensions related to the certificate.
	 */
	public Set<CertificateExtension> getExtensions() {
		return certExtensions;
	}

	/**
	 * Sets the certificate extensions related to the certificate.
	 * @param extensions the certificate extensions related to the certificate.
	 */
	public void setExtensions(final Set<CertificateExtension> extensions) {
        LazySet<CertificateExtension> newExtensions = new LazySet<CertificateExtension>();
        if (extensions != null && !extensions.isEmpty()) {
            newExtensions.addAll(extensions);
        }
		this.certExtensions = newExtensions;
	}
}
