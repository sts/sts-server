/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.ta.x509;

import org.glite.sts.ta.TokenGenerationContext;
import org.glite.sts.ta.TokenGenerationException;
import org.glite.sts.ta.TokenGenerator;
import org.glite.sts.x509.caclient.CAClient;
import org.glite.sts.x509.caclient.CAClientException;
import org.glite.sts.x509.caclient.CAConnection;
import org.opensaml.messaging.context.navigate.ChildContextLookup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Function;

/** The X.509 token generator implementation, currently supporting CMP protocol. Uses {@link X509TokenGenerationContext}
 * expected to be found as a subcontext to the {@link TokenGenerationContext}. 
 */
public class X509TokenGenerator implements TokenGenerator {

	/** Logging */
    static private Logger log = LoggerFactory.getLogger(X509TokenGenerator.class);
    
    /**
     * Strategy used to locate the {@link X509TokenGenerationContext} associated with a given {@link TokenGenerationContext}.
     */
    private Function<TokenGenerationContext, X509TokenGenerationContext> x509TokenGenerationContextLookupStrategy;
    
	/** The online CA client related to this token generator. */
    private CAClient caClient;
	
	/** Constructor.
	 * 
	 * @param caClient the online CA client related to this token generator
	 */
    public X509TokenGenerator(CAClient caClient) {
		this.caClient = caClient;
        x509TokenGenerationContextLookupStrategy =
        		new ChildContextLookup<TokenGenerationContext, X509TokenGenerationContext>(X509TokenGenerationContext.class,
        				false);
	}
	
    /** {@inheritDoc} */
    public void issueToken(TokenGenerationContext ctx) throws TokenGenerationException {
		final X509TokenGenerationContext x509ctx = x509TokenGenerationContextLookupStrategy.apply(ctx);
		CAConnection caConnection = null;
		try {
			caConnection = caClient.getConnection();
			caConnection.createRequest(x509ctx);
			caConnection.submitRequest(x509ctx);
		} catch (CAClientException e) {
			log.error("Error while connecting the remote CA!", e);
			throw new TokenGenerationException(e);
		}
	}

}
