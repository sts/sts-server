/**
 * Copyright (c) Members of the EMI Collaboration. 2011.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0
 */
package org.glite.sts.util;

import java.io.IOException;
import java.io.StringWriter;
import java.security.GeneralSecurityException;
import java.util.StringTokenizer;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.util.EntityUtils;
import org.glite.sts.STSException;
import org.glite.sts.config.ServerConfiguration;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.ISODateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.emi.security.authn.x509.CrlCheckingMode;
import eu.emi.security.authn.x509.NamespaceCheckingMode;
import eu.emi.security.authn.x509.OCSPCheckingMode;
import eu.emi.security.authn.x509.OCSPParametes;
import eu.emi.security.authn.x509.ProxySupport;
import eu.emi.security.authn.x509.RevocationParameters;
import eu.emi.security.authn.x509.X509CertChainValidator;
import eu.emi.security.authn.x509.X509Credential;
import eu.emi.security.authn.x509.impl.OpensslCertChainValidator;
import eu.emi.security.authn.x509.impl.PEMCredential;
import eu.emi.security.authn.x509.impl.ValidatorParams;

import net.shibboleth.utilities.java.support.component.AbstractInitializableComponent;
import net.shibboleth.utilities.java.support.logic.Constraint;

/**
 * Publishes the service information to EMI registry as configured in the service configuration.
 */
public class EMIRPublisher extends AbstractInitializableComponent {
	
    /** Logging */
    private static final Logger log = LoggerFactory.getLogger(EMIRPublisher.class);

    /** The name of the configuration (INI) section for EMIR. */
    public static final String CFG_SECTION_EMIR = "EMIR";
    
    /** The delimiter character in the values for array. */
    public static final String ARRAY_DELIMITER = ",";
    
    /** Http client used for publishing the service information. */
    private HttpClient httpClient;
    
    /** The URL for the EMIR service. */
    private String emirUrl;

    /** Service_ID field in the JSON. */
    private String serviceId;

    /** Service_Name field in the JSON. */
    private String serviceName;

    /** Service_Type field in the JSON. */
    private String serviceType;

    /** Service_Endpoint_ID field in the JSON. */
    private String serviceEndpointId;

    /** Service_Endpoint_URL field in the JSON. */
    private String serviceEndpointUrl;

    /** Service_Endpoint_Capability field in the JSON. */
    private String[] serviceEndpointCapability;

    /** Service_Endpoint_Technology field in the JSON. */
    private String[] serviceEndpointTechnology;

    /** Service_Endpoint_InterfaceName field in the JSON. */
    private String serviceEndpointInterfaceName;

    /** Service_Endpoint_InterfaceVersion field in the JSON. */
    private String serviceEndpointInterfaceVersion;

    /** The lifetime in minutes to build Service_ExpireOn field in the JSON. */
    private long serviceRecordTtl;
    
	/**
	 * Constructor.
	 * @param timer the task timer
	 * @param stsConfiguration the server configuration
	 */
	public EMIRPublisher(Timer timer, ServerConfiguration stsConfiguration) throws STSException {
		log.debug("Initializing EMIR Publisher..");
		X509Credential credential = null;
		X509CertChainValidator chainValidator = null;
		boolean configured = true;
		long publishInterval = 60;
		try {
			this.emirUrl = Constraint.isNotNull(getStringConfigurationValue(stsConfiguration, "emirUrl"), "emirUrl cannot be null!");
			this.serviceId = Constraint.isNotNull(getStringConfigurationValue(stsConfiguration, "serviceId"), "serviceId cannot be null!");
			this.serviceName = Constraint.isNotNull(getStringConfigurationValue(stsConfiguration, "serviceName"), "serviceName cannot be null!");
			this.serviceType = Constraint.isNotNull(getStringConfigurationValue(stsConfiguration, "serviceType"), "serviceType cannot be null!");
			this.serviceEndpointId = Constraint.isNotNull(getStringConfigurationValue(stsConfiguration, "serviceEndpointId"), "serviceEndpointId cannot be null!");
			this.serviceEndpointUrl = Constraint.isNotNull(getStringConfigurationValue(stsConfiguration, "serviceEndpointUrl"), "serviceEndpointUrl cannot be null!");
			this.serviceEndpointCapability = extractStringIntoArray(Constraint.isNotNull(getStringConfigurationValue(stsConfiguration, "serviceEndpointCapability"), "serviceEndpointCapability cannot be null!"));
			this.serviceEndpointTechnology = extractStringIntoArray(Constraint.isNotNull(getStringConfigurationValue(stsConfiguration, "serviceEndpointTechnology"), "serviceEndpointTechnology cannot be null!"));
			this.serviceEndpointInterfaceName = Constraint.isNotNull(getStringConfigurationValue(stsConfiguration, "serviceEndpointInterfaceName"), "serviceEndpointInterfaceName cannot be null!");
			this.serviceEndpointInterfaceVersion = Constraint.isNotNull(getStringConfigurationValue(stsConfiguration, "serviceEndpointInterfaceVersion"), "serviceEndpointInterfaceVersion cannot be null!");
			this.serviceRecordTtl = Constraint.isGreaterThan(0, getLongConfigurationValue(stsConfiguration, "serviceRecordTtl"), "serviceRecordTtl must be more than zero");
			publishInterval = Constraint.isGreaterThan(0, getLongConfigurationValue(stsConfiguration, "serviceRecordPublishInterval"), "serviceRecordPublishInterval must be more than zero");
			String keyPass = getStringConfigurationValue(stsConfiguration, "keyPass");
			if (keyPass == null) {
				credential = new PEMCredential(getStringConfigurationValue(stsConfiguration, "keyFile"), getStringConfigurationValue(stsConfiguration, "certFile"), null);
			} else {
				credential = new PEMCredential(getStringConfigurationValue(stsConfiguration, "keyFile"), getStringConfigurationValue(stsConfiguration, "certFile"), keyPass.toCharArray());
			}
			OCSPParametes ocspParams = new OCSPParametes(OCSPCheckingMode.valueOf(getStringConfigurationValue(stsConfiguration, "ocspCheckingMode")));
			RevocationParameters revocParams = new RevocationParameters(CrlCheckingMode.valueOf(getStringConfigurationValue(stsConfiguration, "crlCheckingMode")), ocspParams);
			ValidatorParams validatorParams = new ValidatorParams(revocParams, ProxySupport.valueOf(getStringConfigurationValue(stsConfiguration, "proxySupport")));
			chainValidator = new OpensslCertChainValidator(getStringConfigurationValue(stsConfiguration, "trustStoreDir"),
					NamespaceCheckingMode.valueOf(getStringConfigurationValue(stsConfiguration, "namespaceCheckingMode")),
					getLongConfigurationValue(stsConfiguration, "trustStoreUpdateInterval"), validatorParams);
		} catch (Exception e) {
			log.debug("Could not initialize EMIR publisher", e);
			configured = false;
		}
		if (configured) {
			HttpClientBuilder httpBuilder = new HttpClientBuilder(this.emirUrl, credential, chainValidator);
			try {
				this.httpClient = httpBuilder.buildClient();
			} catch (IOException e) {
				log.error("Could not build the HTTP client", e);
				throw new STSException(e);
			} catch (GeneralSecurityException e) {
				log.error("Security error while building the HTTP client", e);
				throw new STSException(e);
			}
			timer.schedule(new RegistryPublishingTask(), 0, publishInterval * 60 * 1000);
			log.info("EMIR publisher configured to publish the service information every {} minutes", publishInterval);
		} else {
			log.info("EMIR publisher was not properly configured, information won't be published!");
		}
	}
	
	/**
	 * Gets a string value corresponding to the key from the configuration.
	 * @param serverConfiguration the server configuration
	 * @param key the configuration key
	 * @return the configuration value
	 * @throws STSException
	 */
	protected static String getStringConfigurationValue(ServerConfiguration serverConfiguration, String key) throws STSException {
		return serverConfiguration.getStringConfigurationValue(CFG_SECTION_EMIR, key);
	}
	
	/**
	 * Gets a long value corresponding to the key from the configuration.
	 * @param serverConfiguration the server configuration
	 * @param key the configuration key
	 * @return the configuration value
	 * @throws STSException
	 */
	protected static long getLongConfigurationValue(ServerConfiguration serverConfiguration, String key) throws STSException {
		return serverConfiguration.getLongConfigurationValue(CFG_SECTION_EMIR, key);
	}
	
	/**
	 * Extracts the comma-delimited string into an array
	 * @param string the comma-delimited string
	 * @return the string array
	 */
	protected static String[] extractStringIntoArray(String string) {
		StringTokenizer tokenizer = new StringTokenizer(string, ARRAY_DELIMITER);
		String[] array = new String[tokenizer.countTokens()];
		for (int i = 0; i < array.length; i++) {
			array[i] = tokenizer.nextToken();
		}
		return array;
	}
	
	/**
	 * Gets the HTTP request from the given HTTP entity.
	 * @param initial POST method if true, PUT otherwise
	 * @param httpEntity the HTTP request entity
	 * @return the HTTP request
	 */
	protected HttpRequestBase getHttpUriRequest(boolean initial, HttpEntity httpEntity) {
		HttpEntityEnclosingRequest httpRequest;
		if (initial) {
			httpRequest = new HttpPost(this.emirUrl);
		} else {
			httpRequest = new HttpPut(this.emirUrl);
		}
		httpRequest.setEntity(httpEntity);
		return (HttpRequestBase) httpRequest;
	}
	
	/**
	 * Publishes the service information to the EMIR service.
	 * @param initial initial request
	 */
	protected void publish(boolean initial) {
		HttpEntity httpEntity = generateHttpEntity();
		HttpRequestBase httpRequest = getHttpUriRequest(initial, httpEntity);
		HttpResponse httpResponse = null;
        try {
            log.info("Sending the request to the EMIR service with {} method", httpRequest.getMethod());
            httpResponse = httpClient.execute(httpRequest);
            int statusCode = httpResponse.getStatusLine().getStatusCode();
            log.debug("The response code from the EMIR service: {}", statusCode);
            if (statusCode != 200) {
            	log.warn("Invalid status code {} from the EMIR service!", statusCode);
            }
            if (log.isDebugEnabled()) {
            	StringWriter writer = new StringWriter();
            	IOUtils.copy(httpResponse.getEntity().getContent(), writer, "UTF-8");
            	log.debug("Response from the EMIR service: {}", writer.toString());
            }
        } catch (IOException e) {
            log.error("IO error while sending the request to the EMIR service.", e);
        } finally {
        	httpRequest.releaseConnection();
        	EntityUtils.consumeQuietly(httpEntity);
        }
	}
	
	/**
	 * Generates the HTTP request entity.
	 * @return the byte array HTTP request entity
	 */
	protected ByteArrayEntity generateHttpEntity() {
		String json = "{ ";
		json = json + "\"Service_ID\" : \"" + this.serviceId + "\",";
		json = json + "\"Service_Name\" : \"" + this.serviceName + "\",";
		json = json + "\"Service_Type\" : \"" + this.serviceType + "\",";
		json = json + "\"Service_Endpoint_ID\" : \"" + this.serviceEndpointId + "\",";
		json = json + "\"Service_Endpoint_URL\" : \"" + this.serviceEndpointUrl + "\",";
		json = json + "\"Service_Endpoint_Capability\" : [ " + getJsonArray(this.serviceEndpointCapability) + " ],";
		json = json + "\"Service_Endpoint_Technology\" : [ " + getJsonArray(this.serviceEndpointTechnology) + " ],";
		json = json + "\"Service_Endpoint_InterfaceName\" : \"" + this.serviceEndpointInterfaceName + "\",";
		json = json + "\"Service_Endpoint_InterfaceVersion\" : \"" + this.serviceEndpointInterfaceVersion + "\",";
		json = json + "\"Service_ExpireOn\" : " + getJsonDate(DateTime.now(DateTimeZone.UTC).plusMinutes((int)this.serviceRecordTtl));
		json = json + " }";
		log.debug("Final JSON message: {}", json);
		return new ByteArrayEntity(json.getBytes());
	}
	
	/**
	 * Gets the JSON representation from the given string array.
	 * @param array the string array
	 * @return the JSON representation of the given string array
	 */
	protected static String getJsonArray(String[] array) {
		String json = "";
		for (int i = 0; i < array.length - 1; i++) {
			json = json + "\"" + array[i] + "\",";
		}
		json = json + "\"" + array[array.length - 1] + "\"";
		return json;
	}
	
	protected static String getJsonDate(DateTime date) {
		return "{ \"$date\" : \"" + ISODateTimeFormat.dateTime().print(date) + "\" }";
	}
	
	/**
	 * The task for cleaning the expired identifiers from the storage.
	 */
	class RegistryPublishingTask extends TimerTask {

		private boolean initial = true;
			
		@Override
		public void run() {
			log.debug("Publishing the information to the EMIR service...");
			publish(initial);
			initial = false;
		}
	}
}
