/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts.util;

import javax.xml.validation.Schema;

import net.shibboleth.utilities.java.support.logic.Constraint;

import org.opensaml.core.xml.schema.SchemaBuilder;
import org.opensaml.core.xml.schema.SchemaBuilder.SchemaLanguage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

/**
 * Helper class for building {@link Schema} used for validating the incoming
 * messages.
 */
public class XMLSchemaBuilder {
	
    /** Logging */
    private static final Logger log = LoggerFactory.getLogger(XMLSchemaBuilder.class);
	
	/** The directory location for the schema files. */
	private String location;
	
	/**
	 * Constructor.
	 * @param location the directory location for the schema files
	 */
	public XMLSchemaBuilder(String location) {
		log.debug("Constructing a new builder using directory {}", location);
		this.location = Constraint.isNotNull(location, "The schema directory location cannot be null!");
	}
	
	/**
	 * Builds schema using the configured location.
	 * @return schema the schema used for validating the incoming messages
	 * @throws SAXException if the schema cannot be built
	 */
	public Schema buildSchema() throws SAXException {
		return SchemaBuilder.buildSchema(SchemaLanguage.XML, this.location);
	}

}
