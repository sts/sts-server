/**
 * Copyright (c) Members of the EMI Collaboration. 2011.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0
 */
package org.glite.sts.util;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.GeneralSecurityException;

import org.apache.http.client.HttpClient;
import org.apache.http.client.protocol.RequestAcceptEncoding;
import org.apache.http.client.protocol.ResponseContentEncoding;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.BasicClientConnectionManager;
import org.apache.http.impl.conn.PoolingClientConnectionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.emi.security.authn.x509.X509CertChainValidator;
import eu.emi.security.authn.x509.X509Credential;
import eu.emi.security.authn.x509.impl.SocketFactoryCreator;

/**
 * A builder for {@link HttpClient}s.
 * 
 * This builder will produce clients that employ the {@link MultiThreadedHttpConnectionManager} and as such users of the
 * clients MUST be sure to invoke {@link org.apache.commons.httpclient.HttpMethod#releaseConnection()} after they have
 * finished with the method.
 * 
 * This class is partly based on <code>org.opensaml.ws.soap.client.http.HttpClientBuilder</code>.
 */
public class HttpClientBuilder {

    /** Logging */
    private static final Logger log = LoggerFactory.getLogger(HttpClientBuilder.class);
    
    /** The URL for the server's end-point used for the HttpClient to be built. */
    private URL serverUrl;
    
    /** Determines whether to pool connections to a host. Default value: true */
    private boolean connectionPooling;

    /** Total number of connections that may be open. Default value: 20 */
    private int connectionsMaxTotal;

    /** Maximum number of connections that may be opened to a single host. Default value: 2 */
    private int connectionsMaxPerRoute;
    
    /** The credential used for the client authentication. */
    private X509Credential credential;
    
    /** The validator used for the service authentication. */
    private X509CertChainValidator chainValidator;
    
    /**
     * Constructor.
     * @param url the service endpoint
     * @param credential the credential used for the client authentication
     * @param chainValidator the validator used for the service authentication
     */
    public HttpClientBuilder(String url, X509Credential credential, X509CertChainValidator chainValidator) {
    	this.setUrl(url);
    	this.credential = credential;
    	this.chainValidator = chainValidator;
    }
        
    /**
     * Sets the server's end-point url.
     * @param url the end-point url as <code>String</code>.
     */
    protected void setUrl(String url) {
        log.debug("Setting service URL for {}", url);
        try {
            this.serverUrl = new URL(url);
        } catch (MalformedURLException e) {
            log.error("The given URL {} is not valid", url);
        }
    }

    /**
     * Builds a new <code>HttpClient</code> with the current parameters.
     * @return the <code>HttpClient</code>
     */
    public HttpClient buildClient() throws IOException, GeneralSecurityException {
        final DefaultHttpClient client = new DefaultHttpClient(buildConnectionManager());
        client.addRequestInterceptor(new RequestAcceptEncoding());
        client.addResponseInterceptor(new ResponseContentEncoding());
        return client;
    }

    /**
     * Builds the connection manager used by the HTTP client. If {@link #connectionPooling} is false then the
     * {@link SingleClientConnManager} is used. Otherwise the {@link ThreadSafeClientConnManager} is used with
     * {@link ThreadSafeClientConnManager#setDefaultMaxPerRoute(int)} set to {@link #connectionsMaxPerRoute} and
     * {@link ThreadSafeClientConnManager#setMaxTotalConnections(int)} set to {@link #connectionsMaxTotal}.
     * @return the connection manager used by the HttpClient
     */
    private ClientConnectionManager buildConnectionManager() throws IOException, GeneralSecurityException {
        final SchemeRegistry registry = buildSchemeRegistry();

        if (!connectionPooling) {
        	return new BasicClientConnectionManager(registry);
        } else {
        	final PoolingClientConnectionManager manager = new PoolingClientConnectionManager(registry);
            manager.setDefaultMaxPerRoute(connectionsMaxPerRoute);
            manager.setMaxTotal(connectionsMaxTotal);
            return manager;
        }
    }

    private SchemeRegistry buildSchemeRegistry() throws IOException, GeneralSecurityException {
        final SchemeRegistry registry = new SchemeRegistry();
        String protocol = serverUrl.getProtocol();
        log.debug("Trying to register protocol {}", protocol);
        if (protocol.equals("http")) {
            registry.register(new Scheme("http", serverUrl.getPort(), PlainSocketFactory.getSocketFactory()));
        } else if (protocol.equals("https")){
            final SSLSocketFactory sslSF = new SSLSocketFactory(SocketFactoryCreator.getSocketFactory(this.credential, this.chainValidator), SSLSocketFactory.BROWSER_COMPATIBLE_HOSTNAME_VERIFIER);
            registry.register(new Scheme("https", serverUrl.getPort(), sslSF));
            log.info("Registered the scheme 'https' for port {}", serverUrl.getPort());
        } else {
            log.error("Invalid protocol ({}), HTTP client could not be initialized!", protocol);
        }
        return registry;
    }

}
