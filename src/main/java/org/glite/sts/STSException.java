/*
 * Copyright (c) Members of the EMI Collaboration. 2010-2012.
 * See http://eu-emi.eu/partners/ for details on the copyright holders.
 * For license conditions see http://www.apache.org/licenses/LICENSE-2.0 
 */
package org.glite.sts;

import net.shibboleth.idp.ShibbolethException;

/** Base class for STS exceptions. */
public class STSException extends ShibbolethException {

    /** Serial version UID. */
    private static final long serialVersionUID = -297160493254913962L;

    /** Constructor. */
    public STSException() {
        super();
    }

    /**
     * Constructor.
     * 
     * @param message exception message
     */
    public STSException(final String message) {
        super(message);
    }

    /**
     * Constructor.
     * 
     * @param wrappedException exception to be wrapped by this one
     */
    public STSException(final Exception wrappedException) {
        super(wrappedException);
    }

    /**
     * Constructor.
     * 
     * @param message exception message
     * @param wrappedException exception to be wrapped by this one
     */
    public STSException(final String message, final Exception wrappedException) {
        super(message, wrappedException);
    }


}
