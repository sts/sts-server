# Copyright (c) Members of the EMI Collaboration. 2011.
# See http://eu-emi.eu/partners/ for details on the copyright holders.
# For license conditions see http://www.apache.org/licenses/LICENSE-2.0
#

## Sets the base of the STS service installation
STS_SERVER_HOME=${STS_SERVER_HOME}

## The STS service pid file
STS_RUN_FILE=/var/run/sts-server.pid

# This variable can be used to pass arguments to the JVM used to execute the service.
#STS_JAVA_OPTS=