# Copyright (c) Members of the EMI Collaboration. 2011.
# See http://eu-emi.eu/partners/ for details on the copyright holders.
# For license conditions see http://www.apache.org/licenses/LICENSE-2.0
#
# This script sets up the environment for the STS service.
# 
# DO NOT CHANGE THE CONTENT OF THIS FILE UNLESS YOU REALLY KNOW WHAT YOU ARE DOING
#

set -e

# Location of the STS jars
STS_LIBS=$STS_SERVER_HOME/lib

# Location of the STS endorsed jars
STS_ENDORSED_LIBS=$STS_LIBS/endorsed

# STS configuration file location
STS_CONF_FILE="$STS_SERVER_HOME/conf/sts-server.ini"

# Sets the heap size for the JVM  
if [ -z $STS_JAVA_OPTS ]; then
        STS_JAVA_OPTS="-Xmx256m "
fi

# The name of the class that implements the standalone server
STS_CLASS="org.glite.sts.standalone.StandaloneService"

# The name of the class the implements the STSnymity shutdown helper
STS_SHUTDOWN_CLASS="org.glite.sts.standalone.ShutdownClient"

# ':' separated list of  STS dependencies, used to build the classpath
STS_DEPS=`ls -x $STS_LIBS/*.jar | tr '\n' ':'`

# Location of the jar file
STS_JAR="$STS_SERVER_HOME/lib/sts-server.jar"

# Classpath for the service
STS_CP="$STS_DEPS:$STS_SERVER_HOME/conf/logging/standalone::/usr/share/java/bcprov-1.46.jar:/usr/share/java/log4j.jar:/usr/share/java/canl.jar:/usr/share/java/voms-api-java3.jar:/usr/share/java/bcmail-1.46.jar"

# Environment for the STS service
STS_ENV="-DSTS_SERVER_HOME=$STS_SERVER_HOME -Djava.endorsed.dirs=$STS_ENDORSED_LIBS"

# Command used to start the STS  service
STS_CMD="java $STS_JAVA_OPTS $STS_ENV -cp $STS_CP $STS_CLASS --conf-dir $STS_SERVER_HOME/conf"

# The hostname property as appears in the STS configuration file
STS_HOST=`grep 'hostname =' $STS_CONF_FILE | awk '{print $3}'`

# The port property as appears in the STS configuration file
STS_PORT=`grep 'port =' $STS_CONF_FILE | awk '{print $3}'`

# The shutdown port property as appears in the STS configuration file
STS_SHUTDOWN_PORT=`grep 'shutdown_port =' $STS_CONF_FILE | awk '{print $3}'`

# The certificate property as appears in the STS configuration file
STS_CERT=`grep 'certificate =' $STS_CONF_FILE  | awk '{print $3}'`

# The private key property as appears in the STS configuration file
STS_KEY=`grep 'private_key =' $STS_CONF_FILE | awk '{print $3}'`

# The command used to shutdown the STS service
STS_SHUTDOWN_CMD="java -DSTS_SERVER_HOME=$STS_SERVER_HOME -cp $STS_CP $STS_SHUTDOWN_CLASS"
