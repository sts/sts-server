# Copyright (c) Members of the EMI Collaboration. 2011.
# See http://eu-emi.eu/partners/ for details on the copyright holders.
# For license conditions see http://www.apache.org/licenses/LICENSE-2.0
#

#   chkconfig: 345 97 97
#   description:  STS server startup script
#   processname: sts-service

set -e

prog="$(basename $0)"

if [ -r "/etc/sysconfig/sts-service" ]; then
        source /etc/sysconfig/sts-service
fi

if [ -z $STS_SERVER_HOME ]; then
        STS_SERVER_HOME="$(cd "${0%/*}/.." && pwd)"
fi

if [ ! -r $STS_SERVER_HOME/conf/sts-server.ini ]; then
        echo "File $STS_SERVER_HOME/conf/sts-server.ini doesn't exist or is not readable!"
        exit 1
fi 
        
if [ -z $STS_RUN_FILE ]; then
        STS_RUN_FILE=$STS_SERVER_HOME/.sts-service.pid
fi

. $STS_SERVER_HOME/sbin/sts-env.sh

pre_checks(){
        check_openssl
        check_certificates
}

success () {
        if [ ! -z "$1" ]; then
                echo "$1"
        fi
        exit 0
}

failure () {
        if [ ! -z "$1" ]; then
                echo "$1"
        fi
        exit 1
}

sts_pid(){

        if test ! -f $STS_RUN_FILE; then
                echo "No pid file found for $prog!"
                failure
        fi
        
        STS_PID=`head -n 1 $STS_RUN_FILE`
        
}

kill_sts_proc(){

        status
        if [ $? -eq 0 ]; then
                ## sts process is running
                pid=`head -1 $STS_RUN_FILE`
                
                ## Use shutdown service hook
                $STS_SHUTDOWN_CMD
                
                if [ $? -ne 0 ]; then
                        echo "Error shutting down the STS service! Will kill the process..."
                        
                        kill -9 $pid
                        
                        if [ $? -ne 0 ]; then
                                failure "Error killing the STS service... maybe you don't have the permissions to kill it!"
                        else
                                ## remove pid file
                                rm $STS_RUN_FILE
                        fi
                        
                else
                        ## remove pid file
                        rm $STS_RUN_FILE
                fi
        else
                failure "STS server is not running!"
        fi 
}
        
status(){

        if [ -f $STS_RUN_FILE ]; then
                pid=`head -1 $STS_RUN_FILE`
                ps -p $pid >/dev/null 2>&1
                
                if [ $? -ne 0 ]; then
                        echo "STS service not running...removing stale pid file"
                        rm $STS_RUN_FILE
                        return 1
                else
                        return 0
                fi
        else
                ## No PID file found, check that there is no STS running 
                ## without PID...
                pid=`ps -efww | grep 'java.*StandaloneService' | grep -v grep | awk '{print $2}'`
                if [ -z $pid ]; then
                        return 1
                else
                        echo "An STS service is running, but no pid file found! Will recreate the pid file"
                        echo $pid > $STS_RUN_FILE
                        if [ $? -ne 0 ]; then
                                failure "Error creating pid file for running STS service!"
                        fi
                        return 0
                fi
        fi
}


start(){

        # echo -n "Starting $prog: "
        
        status && failure "STS service already running..."
        
        $STS_CMD &
        
        if [ $? -eq 0 ]; then
                echo "$!" > $STS_RUN_FILE;
                status || failure "STS service not running after being just started!"
                success 
        else
                failure "failed!"
        fi
                
}

restart(){
        
        # echo -n "Restarting $prog: "
        kill_sts_proc && (rm -f $STS_RUN_FILE; sleep 5; start) || failure "Error restarting STS service process!"

}
stop(){
        # echo -n "Stopping $prog: "
        kill_sts_proc && (rm -f $STS_RUN_FILE; success "Ok.") || failure "Error killing STS service process!"
}

case "$1" in
        start)
                start
                ;;
        
        stop)
                stop
                ;;
        
        status)
                status && success "STS service running!" || failure "STS service not running!"
                ;;
        
        restart)
                restart
                ;;
        *)
                echo "Usage: $0 {start|stop|status}"
                RETVAL=1
                ;;
esac
exit $RETVAL