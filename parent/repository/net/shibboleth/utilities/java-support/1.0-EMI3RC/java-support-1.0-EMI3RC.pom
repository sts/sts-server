<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">

    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>net.shibboleth</groupId>
        <artifactId>parent</artifactId>
        <version>TRUNK-EMI3RC</version>
    </parent>

    <name>java-support</name>
    <groupId>net.shibboleth.utilities</groupId>
    <artifactId>java-support</artifactId>
    <version>1.0-EMI3RC</version>
    <packaging>jar</packaging>

    <properties>
        <svn.relative.location>/utilities</svn.relative.location>
    </properties>

    <repositories>
        <repository>
            <id>switch</id>
            <name>SWITCH Releases</name>
            <url>http://www.switch.ch/grid/authz/docs/maven2/release</url>
        </repository>
        <repository>
            <id>shib-release</id>
            <url>https://build.shibboleth.net/nexus/content/groups/public</url>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>
        </repository>
        <repository>
            <id>shib-snapshot</id>
            <url>https://build.shibboleth.net/nexus/content/repositories/snapshots</url>
            <releases>
                <enabled>false</enabled>
            </releases>
        </repository>
    </repositories>

    <dependencies>
        <!-- Compile Dependencies -->
        <dependency>
            <groupId>commons-codec</groupId>
            <artifactId>commons-codec</artifactId>
        </dependency>
        <dependency>
            <groupId>com.google.guava</groupId>
            <artifactId>guava</artifactId>
        </dependency>
        <dependency>
            <!-- Required if you're using anything in the httpclient package -->
            <groupId>org.apache.httpcomponents</groupId>
            <artifactId>httpclient</artifactId>
            <optional>true</optional>
        </dependency>
        <dependency>
            <!-- Required if you're using anything in the httpclient package -->
            <groupId>org.apache.httpcomponents</groupId>
            <artifactId>httpclient-cache</artifactId>
            <optional>true</optional>
        </dependency>
        <dependency>
            <!-- Required if you're using classes from the velocity package -->
            <groupId>org.apache.velocity</groupId>
            <artifactId>velocity</artifactId>
            <optional>true</optional>
        </dependency>

        <!-- Provided Dependencies -->
        <dependency>
            <groupId>ch.qos.logback</groupId>
            <artifactId>logback-classic</artifactId>
            <scope>provided</scope>
        </dependency>

        <!-- Runtime Dependencies -->
        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>servlet-api</artifactId>
        </dependency>

        <!-- Test Dependencies -->
        <dependency>
            <groupId>${spring.groupId}</groupId>
            <artifactId>spring-test</artifactId>
        </dependency>
        <dependency>
            <groupId>${spring.groupId}</groupId>
            <artifactId>spring-webmvc</artifactId>
            <scope>test</scope>
        </dependency>
    </dependencies>

</project>
