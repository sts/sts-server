# Security Token Service (STS)

## Overview

This repository contains EMI Security Token Service (STS) implementation together with some extensions related
to CERN IOTA CA and VOMS interaction.

Original documentation can be found from here: https://twiki.cern.ch/twiki/bin/view/EMI/EMISTSDocumentation

## Configuration

Original configuration options can be seen from here: https://twiki.cern.ch/twiki/bin/view/EMI/STSConfiguration

The following configuration options can be used to configure the additional features existing in this
repository:

### Messages

- _enableX509_: Flag to support X.509 certificate (EEC) issuance (true/false)
- _enableProxy_: Flag to support proxy certificate issuance (true/false)

### VOMS

- _linkUpdateInterval_: How often the attribute (see _matchAttributeName_) to VOMS object links are updated (in milliseconds)
- _baseUrl_: Base URL for the VOMS server used for attribute linking and certificate registration
- _voName_: The name of the VO where the users are searched for the links
- _matchAttributeName_: The matching attribute on the VOMS-side (VOMS attribute name), see _incomingLinkAttributeId_
- _certFile_: The client certificate used for authenticating to VOMSAttributes and VOMSCertificates interfaces
- _keyFile_: The private key corresponding to _certFile_
- _keyPass_: The password protecting the private key (optional)
- _trustStoreDir_: The CA certificate directory for validating the server certificate
- _incomingLinkAttributeId_: The matching attribute on the incoming SAML assertion, see _matchAttributeName_
- _attributeLinkCache_: The file used for backing up the in-memory attribute->VOMS object link information
- _proxyPathLength_: The proxy path length limit ('Path Length Constraint' value in 'Proxy Certificate Information' extension).